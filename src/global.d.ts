import { Actorpf2ettw } from "@actor/base";
import { AutomaticBonusProgression } from "@actor/character/automatic-bonus-progression";
import {
    AbilityModifier,
    CheckModifier,
    Modifierpf2ettw,
    MODIFIER_TYPE,
    ProficiencyModifier,
    StatisticModifier,
} from "@actor/modifiers";
import { Itempf2ettw } from "@item/base";
import { Coinspf2ettw } from "@item/physical/helpers";
import { ActiveEffectpf2ettw } from "@module/active-effect";
import { CompendiumBrowser } from "@module/apps/compendium-browser";
import { EffectsPanel } from "@module/apps/effects-panel";
import { LicenseViewer } from "@module/apps/license-viewer";
import { ChatLogpf2ettw, CompendiumDirectorypf2ettw, EncounterTrackerpf2ettw } from "@module/apps/ui";
import { Hotbarpf2ettw } from "@module/apps/ui/hotbar";
import { WorldClock } from "@module/apps/world-clock";
import { Canvaspf2ettw } from "@module/canvas";
import { ChatMessagepf2ettw } from "@module/chat-message";
import { Actorspf2ettw } from "@module/collection/actors";
import { FogExplorationpf2ettw } from "@module/fog-exploration";
import { Folderpf2ettw } from "@module/folder";
import { Macropf2ettw } from "@module/macro";
import { RuleElementpf2ettw, RuleElements } from "@module/rules";
import {
    AmbientLightDocumentpf2ettw,
    MeasuredTemplateDocumentpf2ettw,
    Scenepf2ettw,
    TileDocumentpf2ettw,
    TokenDocumentpf2ettw,
} from "@module/scene";
import { Userpf2ettw } from "@module/user";
import { StatusEffects } from "@scripts/actor/status-effects";
import { pf2ettwCONFIG, StatusEffectIconTheme } from "@scripts/config";
import { Dicepf2ettw } from "@scripts/dice";
import { rollActionMacro, rollItemMacro } from "@scripts/macros/hotbar";
import { launchTravelSheet } from "@scripts/macros/travel/travel-speed-sheet";
import { calculateXP } from "@scripts/macros/xp";
import { ModuleArt } from "@scripts/register-module-art";
import { remigrate } from "@scripts/system/remigrate";
import { UserVisibility } from "@scripts/ui/user-visibility";
import { EffectTracker } from "@system/effect-tracker";
import { ActorImporter } from "@system/importer/actor-importer";
import { Checkpf2ettw } from "@system/rolls";
import type { HomebrewSettingsKey, HomebrewTag } from "@system/settings/homebrew";
import { TextEditorpf2ettw } from "@system/text-editor";
import { sluggify } from "@util";
import { Combatantpf2ettw, Encounterpf2ettw } from "./module/encounter";
import { ConditionManager } from "./module/system/conditions";
import { EffectManager } from "@system/effect";

declare global {
    interface Game {
        pf2ettw: {
            actions: Record<string, Function>;
            compendiumBrowser: CompendiumBrowser;
            licenseViewer: LicenseViewer;
            worldClock: WorldClock;
            effectPanel: EffectsPanel;
            effectTracker: EffectTracker;
            rollActionMacro: typeof rollActionMacro;
            rollItemMacro: typeof rollItemMacro;
            gm: {
                calculateXP: typeof calculateXP;
                launchTravelSheet: typeof launchTravelSheet;
            };
            system: {
                moduleArt: Map<ActorUUID, ModuleArt>;
                remigrate: typeof remigrate;
                sluggify: typeof sluggify;
            };
            importer: {
                actor: typeof ActorImporter;
            };
            variantRules: {
                AutomaticBonusProgression: typeof AutomaticBonusProgression;
            };
            Coins: typeof Coinspf2ettw;
            Dice: typeof Dicepf2ettw;
            StatusEffects: typeof StatusEffects;
            ConditionManager: typeof ConditionManager;
            EffectManager: typeof EffectManager;
            ModifierType: typeof MODIFIER_TYPE;
            Modifier: typeof Modifierpf2ettw;
            AbilityModifier: typeof AbilityModifier;
            ProficiencyModifier: typeof ProficiencyModifier;
            StatisticModifier: typeof StatisticModifier;
            CheckModifier: typeof CheckModifier;
            Check: typeof Checkpf2ettw;
            RuleElements: typeof RuleElements;
            RuleElement: typeof RuleElementpf2ettw;
            TextEditor: typeof TextEditorpf2ettw;
        };
    }

    interface Configpf2ettw extends ConfiguredConfig {
        debug: ConfiguredConfig["debug"] & {
            ruleElement: boolean;
        };
        pf2ettw: typeof pf2ettwCONFIG;
        time: {
            roundTime: number;
        };
    }

    const CONFIG: Configpf2ettw;
    const canvas: Canvaspf2ettw;

    namespace globalThis {
        // eslint-disable-next-line no-var
        var game: Game<
            Actorpf2ettw,
            Actorspf2ettw,
            ChatMessagepf2ettw,
            Encounterpf2ettw,
            Folderpf2ettw,
            Itempf2ettw,
            Macropf2ettw,
            Scenepf2ettw,
            Userpf2ettw
        >;

        // eslint-disable-next-line no-var
        var ui: FoundryUI<Actorpf2ettw, Itempf2ettw, ChatLogpf2ettw, CompendiumDirectorypf2ettw>;
    }

    interface Window {
        AutomaticBonusProgression: typeof AutomaticBonusProgression;
    }

    interface ClientSettings {
        get(module: "pf2ettw", setting: "automation.actorsDeadAtZero"): "neither" | "npcsOnly" | "pcsOnly" | "both";
        get(module: "pf2ettw", setting: "automation.effectExpiration"): boolean;
        get(module: "pf2ettw", setting: "automation.flankingDetection"): boolean;
        get(module: "pf2ettw", setting: "automation.lootableNPCs"): boolean;
        get(module: "pf2ettw", setting: "automation.removeExpiredEffects"): boolean;
        get(module: "pf2ettw", setting: "automation.rulesBasedVision"): boolean;

        get(module: "pf2ettw", setting: "ancestryParagonVariant"): boolean;
        get(
            module: "pf2ettw",
            setting: "automaticBonusVariant"
        ): "noABP" | "ABPFundamentalPotency" | "ABPRulesAsWritten";
        get(module: "pf2ettw", setting: "dualClassVariant"): boolean;
        get(module: "pf2ettw", setting: "freeArchetypeVariant"): boolean;
        get(module: "pf2ettw", setting: "proficiencyVariant"): "ProficiencyWithLevel" | "ProficiencyWithoutLevel";
        get(module: "pf2ettw", setting: "staminaVariant"): 0 | 1;

        get(module: "pf2ettw", setting: "proficiencyUntrainedModifier"): number;
        get(module: "pf2ettw", setting: "proficiencyTrainedModifier"): number;
        get(module: "pf2ettw", setting: "proficiencyExpertModifier"): number;
        get(module: "pf2ettw", setting: "proficiencyMasterModifier"): number;
        get(module: "pf2ettw", setting: "proficiencyLegendaryModifier"): number;

        get(module: "pf2ettw", setting: "metagame.partyVision"): boolean;
        get(module: "pf2ettw", setting: "metagame.secretCondition"): boolean;
        get(module: "pf2ettw", setting: "metagame.secretDamage"): boolean;
        get(module: "pf2ettw", setting: "metagame.showDC"): UserVisibility;
        get(module: "pf2ettw", setting: "metagame.showResults"): UserVisibility;
        get(module: "pf2ettw", setting: "metagame.tokenSetsNameVisibility"): boolean;

        get(module: "pf2ettw", setting: "tokens.autoscale"): boolean;

        get(module: "pf2ettw", setting: "worldClock.dateTheme"): "AR" | "IC" | "AD" | "CE";
        get(module: "pf2ettw", setting: "worldClock.playersCanView"): boolean;
        get(module: "pf2ettw", setting: "worldClock.showClockButton"): boolean;
        get(module: "pf2ettw", setting: "worldClock.syncDarkness"): boolean;
        get(module: "pf2ettw", setting: "worldClock.timeConvention"): 24 | 12;
        get(module: "pf2ettw", setting: "worldClock.worldCreatedOn"): string;

        get(module: "pf2ettw", setting: "campaignFeats"): boolean;

        get(module: "pf2ettw", setting: "homebrew.weaponCategories"): HomebrewTag<"weaponCategories">[];
        get(module: "pf2ettw", setting: HomebrewSettingsKey): HomebrewTag[];

        get(module: "pf2ettw", setting: "compendiumBrowserPacks"): string;
        get(module: "pf2ettw", setting: "critFumbleButtons"): boolean;
        get(module: "pf2ettw", setting: "deathIcon"): ImagePath;
        get(module: "pf2ettw", setting: "drawCritFumble"): boolean;
        get(module: "pf2ettw", setting: "enabledRulesUI"): boolean;
        get(module: "pf2ettw", setting: "identifyMagicNotMatchingTraditionModifier"): 0 | 2 | 5 | 10;
        get(module: "pf2ettw", setting: "nathMode"): boolean;
        get(module: "pf2ettw", setting: "npcAttacksFromWeapons"): boolean;
        get(module: "pf2ettw", setting: "statusEffectType"): StatusEffectIconTheme;
        get(module: "pf2ettw", setting: "worldSchemaVersion"): number;
        get(module: "pf2ettw", setting: "worldSystemVersion"): string;
    }

    interface ClientSettingsMap {
        get(key: "pf2ettw.worldClock.worldCreatedOn"): SettingConfig & { default: string };
        get(key: "core.chatBubblesPan"): SettingConfig & { default: boolean };
    }

    interface RollMathProxy {
        eq: (a: number, b: number) => boolean;
        gt: (a: number, b: number) => boolean;
        gte: (a: number, b: number) => boolean;
        lt: (a: number, b: number) => boolean;
        lte: (a: number, b: number) => boolean;
        ne: (a: number, b: number) => boolean;
        ternary: (condition: boolean | number, ifTrue: number, ifFalse: number) => number;
    }

    const BUILD_MODE: "development" | "production";
}

type ConfiguredConfig = Config<
    AmbientLightDocumentpf2ettw,
    ActiveEffectpf2ettw,
    Actorpf2ettw,
    ChatLogpf2ettw,
    ChatMessagepf2ettw,
    Encounterpf2ettw,
    Combatantpf2ettw,
    EncounterTrackerpf2ettw,
    CompendiumDirectorypf2ettw,
    FogExplorationpf2ettw,
    Folderpf2ettw,
    Hotbarpf2ettw,
    Itempf2ettw,
    Macropf2ettw,
    MeasuredTemplateDocumentpf2ettw,
    TileDocumentpf2ettw,
    TokenDocumentpf2ettw,
    Scenepf2ettw,
    Userpf2ettw
>;
