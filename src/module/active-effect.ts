export class ActiveEffectpf2ettw extends ActiveEffect {
    /** Disable ActiveEffects */
    constructor(
        data: DeepPartial<foundry.data.ActiveEffectSource>,
        context?: DocumentConstructionContext<ActiveEffectpf2ettw>
    ) {
        data.disabled = true;
        data.transfer = false;
        super(data, context);
    }
}
