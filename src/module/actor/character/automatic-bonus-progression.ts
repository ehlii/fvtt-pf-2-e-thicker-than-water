import { Weaponpf2ettw } from "@item";
import { Modifierpf2ettw, MODIFIER_TYPE, DamageDicepf2ettw } from "@actor/modifiers";
import { RuleElementSynthetics, Strikingpf2ettw, WeaponPotencypf2ettw } from "@module/rules/rule-element";
import { FlatModifierRuleElement } from "@module/rules/rule-element/flat-modifier";

export class AutomaticBonusProgression {
    static get isEnabled(): boolean {
        return game.settings.get("pf2ettw", "automaticBonusVariant") !== "noABP";
    }

    /**
     * @param level The name of this collection of statistic modifiers.
     * @param synthetics All relevant modifiers for this statistic.
     */
    static concatModifiers(level: number, synthetics: RuleElementSynthetics): void {
        if (!this.isEnabled) return;

        const values = this.abpValues(level);
        const ac = values.ac;
        const perception = values.perception;
        const save = values.save;
        const shieldHardness = values.shieldHardness;
        const shieldHP = values.shieldHP;
        const bucklerHardness = values.bucklerHardness;
        const bucklerHP = values.bucklerHP;
        const setting = game.settings.get("pf2ettw", "automaticBonusVariant");

        if (save > 0) {
            const modifiers = (synthetics.statisticsModifiers["saving-throw"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "save-potency",
                        label: "pf2ettw.AutomaticBonusProgression.savePotency",
                        modifier: save,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (ac > 0) {
            const modifiers = (synthetics.statisticsModifiers["ac"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "defense-potency",
                        label: "pf2ettw.AutomaticBonusProgression.defensePotency",
                        modifier: ac,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (perception > 0) {
            const modifiers = (synthetics.statisticsModifiers["perception"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "perception-potency",
                        label: "pf2ettw.AutomaticBonusProgression.perceptionPotency",
                        modifier: perception,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (shieldHardness > 0) {
            const modifiers = (synthetics.statisticsModifiers["shield"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "shield-hardness-potency",
                        label: "Shield Hardness",
                        modifier: shieldHardness,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (shieldHP > 0) {
            const modifiers = (synthetics.statisticsModifiers["shield"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "shield-hp-potency",
                        label: "Shield HP",
                        modifier: shieldHP,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (bucklerHardness > 0) {
            const modifiers = (synthetics.statisticsModifiers["shield"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "buckler-hardness-potency",
                        label: "Buckler Hardness",
                        modifier: bucklerHardness,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (bucklerHP > 0) {
            const modifiers = (synthetics.statisticsModifiers["shield"] ??= []);
            modifiers.push(
                () =>
                    new Modifierpf2ettw({
                        slug: "buckler-hp-potency",
                        label: "Buckler HP",
                        modifier: bucklerHP,
                        type: MODIFIER_TYPE.ITEM,
                    })
            );
        }

        if (setting === "ABPRulesAsWritten") {
            const values = this.abpValues(level);
            const attack = values.attack;
            const damage = values.damage;
            if (attack > 0) {
                const modifiers = (synthetics.statisticsModifiers["mundane-attack"] ??= []);
                modifiers.push(
                    () =>
                        new Modifierpf2ettw({
                            slug: "attack-potency",
                            label: "pf2ettw.AutomaticBonusProgression.attackPotency",
                            modifier: attack,
                            type: MODIFIER_TYPE.ITEM,
                        })
                );
            }

            if (damage > 0) {
                synthetics.damageDice["damage"] = (synthetics.damageDice["damage"] || []).concat(
                    new DamageDicepf2ettw({
                        slug: "devasting-attacks",
                        label: game.i18n.localize("pf2ettw.AutomaticBonusProgression.devastatingAttacks"),
                        selector: "damage",
                        diceNumber: damage,
                    })
                );
            }
        }

        if (setting === "ABPFundamentalPotency") {
            const values = this.abpValues(level);
            const attack = values.attack;
            const damage = values.damage;

            if (damage > 0) {
                const s: Strikingpf2ettw = {
                    label: game.i18n.localize("pf2ettw.AutomaticBonusProgression.devastatingAttacks"),
                    bonus: damage,
                };
                (synthetics.striking["strike-damage"] ??= []).push(s);
            }
            if (attack > 0) {
                const potency: WeaponPotencypf2ettw = {
                    label: game.i18n.localize("pf2ettw.AutomaticBonusProgression.attackPotency"),
                    type: MODIFIER_TYPE.ITEM,
                    bonus: attack,
                };
                synthetics.weaponPotency["mundane-attack"] = (synthetics.weaponPotency["mundane-attack"] || []).concat(
                    potency
                );
            }
        }
    }

    /** Remove stored runes from specific magic weapons or otherwise set prior to enabling ABP */
    static cleanupRunes(weapon: Weaponpf2ettw): void {
        const setting = game.settings.get("pf2ettw", "automaticBonusVariant");
        const systemData = weapon.data.data;

        switch (setting) {
            case "noABP":
                return;
            case "ABPRulesAsWritten": {
                systemData.potencyRune.value = null;
                systemData.strikingRune.value = null;
                const propertyRunes = ([1, 2, 3, 4] as const).map((n) => systemData[`propertyRune${n}` as const]);
                for (const rune of propertyRunes) {
                    rune.value = null;
                }
                return;
            }
            case "ABPFundamentalPotency": {
                systemData.potencyRune.value = null;
                systemData.strikingRune.value = null;
                return;
            }
        }
    }

    static applyPropertyRunes(potency: WeaponPotencypf2ettw[], weapon: Embedded<Weaponpf2ettw>): void {
        if (game.settings.get("pf2ettw", "automaticBonusVariant") !== "ABPFundamentalPotency") return;
        const potencyBonuses = potency.filter((p) => p.type === "potency");
        for (const bonus of potencyBonuses) {
            bonus.property = deepClone(weapon.data.data.runes.property);
        }
    }

    /**
     * Determine whether a rule element can be applied to an actor.
     * @param rule The rule element to assess
     * @returns Whether the rule element is to be ignored
     */
    static suppressRuleElement(rule: FlatModifierRuleElement, value: number): boolean {
        if (!(rule.actor.type === "character" && this.isEnabled)) {
            return false;
        }

        return rule.type === "item" && value >= 0 && rule.fromEquipment;
    }

    private static abpValues(level: number) {
        let attack: number;
        let damage: number;
        let ac: number;
        let perception: number;
        let save: number;
        let shieldHardness: number;
        let shieldHP: number;
        let bucklerHardness: number;
        let bucklerHP: number;
        if (level >= 2 && level < 10) {
            attack = 1;
        } else if (level >= 10 && level < 16) {
            attack = 2;
        } else if (level >= 16) {
            attack = 3;
        } else {
            attack = 0;
        }
        if (level >= 4 && level < 12) {
            damage = 1;
        } else if (level >= 12 && level < 19) {
            damage = 2;
        } else if (level >= 19) {
            damage = 3;
        } else {
            damage = 0;
        }
        if (level >= 5 && level < 11) {
            ac = 1;
        } else if (level >= 11 && level < 18) {
            ac = 2;
        } else if (level >= 18) {
            ac = 3;
        } else {
            ac = 0;
        }
        if (level >= 7 && level < 13) {
            perception = 1;
        } else if (level >= 13 && level < 19) {
            perception = 2;
        } else if (level >= 19) {
            perception = 3;
        } else {
            perception = 0;
        }
        if (level >= 8 && level < 14) {
            save = 1;
        } else if (level >= 14 && level < 20) {
            save = 2;
        } else if (level >= 20) {
            save = 3;
        } else {
            save = 0;
        }
        if (level >= 4 && level < 7) {
            shieldHardness = 8;
            shieldHP = 64;
            bucklerHardness = 5;
            bucklerHP = 40;
        } else if (level >= 7 && level < 10) {
            shieldHardness = 10;
            shieldHP = 80;
            bucklerHardness = 8;
            bucklerHP = 64;
        } else if (level >= 10 && level < 13) {
            shieldHardness = 13;
            shieldHP = 104;
            bucklerHardness = 10;
            bucklerHP = 80;
        } else if (level >= 13 && level < 16) {
            shieldHardness = 15;
            shieldHP = 120;
            bucklerHardness = 13;
            bucklerHP = 104;
        } else if (level >= 16 && level < 19) {
            shieldHardness = 17;
            shieldHP = 136;
            bucklerHardness = 15;
            bucklerHP = 120;
        } else if (level >= 19) {
            shieldHardness = 20;
            shieldHP = 160;
            bucklerHardness = 17;
            bucklerHP = 136;
        } else {
            shieldHardness = 0;
            shieldHP = 0;
            bucklerHardness = 0;
            bucklerHP = 0;
        }
        return {
            attack: attack,
            damage: damage,
            ac: ac,
            perception: perception,
            save: save,
            shieldHardness: shieldHardness,
            shieldHP: shieldHP,
            bucklerHardness: bucklerHardness,
            bucklerHP: bucklerHP,
        };
    }
}
