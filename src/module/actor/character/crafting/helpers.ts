/**
 * Implementation of Crafting rules on https://2e.aonprd.com/Actions.aspx?ID=43
 */
import { Coinspf2ettw } from "@item/physical/helpers";
import { DegreeOfSuccess } from "@system/degree-of-success";
import { Actorpf2ettw, Characterpf2ettw } from "@actor";
import { getIncomeForLevel, TrainedProficiency } from "@scripts/macros/earn-income";
import { Consumablepf2ettw, PhysicalItempf2ettw, Spellpf2ettw } from "@item";
import { ZeroToFour } from "@module/data";
import { createConsumableFromSpell } from "@item/consumable/spell-consumables";
import { CheckRoll } from "@system/check/roll";
import { ChatMessagepf2ettw } from "@module/chat-message";

interface Costs {
    reductionPerDay: Coinspf2ettw;
    materials: Coinspf2ettw;
    itemPrice: Coinspf2ettw;
    lostMaterials: Coinspf2ettw;
}

function calculateDaysToNoCost(costs: Costs): number {
    return Math.ceil((costs.itemPrice.copperValue - costs.materials.copperValue) / costs.reductionPerDay.copperValue);
}

function prepStrings(costs: Costs, item: PhysicalItempf2ettw) {
    const rollData = item.getRollData();

    return {
        reductionPerDay: costs.reductionPerDay.toString(),
        materialCost: game.i18n.format("pf2ettw.Actions.Craft.Details.PayMaterials", {
            cost: costs.materials.toString(),
        }),
        itemCost: game.i18n.format("pf2ettw.Actions.Craft.Details.PayFull", {
            cost: costs.itemPrice.toString(),
        }),
        lostMaterials: game.i18n.format("pf2ettw.Actions.Craft.Details.LostMaterials", {
            cost: costs.lostMaterials.toString(),
        }),
        itemLink: game.pf2ettw.TextEditor.enrichHTML(item.link, { rollData }),
    };
}

function calculateCosts(
    item: PhysicalItempf2ettw,
    quantity: number,
    actor: Characterpf2ettw,
    degreeOfSuccess: number
): Costs | null {
    const itemPrice = Coinspf2ettw.fromPrice(item.price, quantity);
    const materialCosts = itemPrice.scale(0.5);
    const lostMaterials = new Coinspf2ettw();
    const reductionPerDay = new Coinspf2ettw();

    const proficiency = skillRankToProficiency(actor.data.data.skills.cra.rank);
    if (!proficiency) return null;

    if (degreeOfSuccess === DegreeOfSuccess.CRITICAL_SUCCESS) {
        Object.assign(reductionPerDay, getIncomeForLevel(actor.level + 1).rewards[proficiency]);
    } else if (degreeOfSuccess === DegreeOfSuccess.SUCCESS) {
        Object.assign(reductionPerDay, getIncomeForLevel(actor.level).rewards[proficiency]);
    } else if (degreeOfSuccess === DegreeOfSuccess.CRITICAL_FAILURE) {
        Object.assign(lostMaterials, materialCosts.scale(0.1));
    }

    return {
        itemPrice: itemPrice,
        materials: materialCosts,
        lostMaterials: lostMaterials,
        reductionPerDay: reductionPerDay,
    };
}

function skillRankToProficiency(rank: ZeroToFour): TrainedProficiency | null {
    switch (rank) {
        case 1:
            return "trained";
        case 2:
            return "expert";
        case 3:
            return "master";
        case 4:
            return "legendary";
        default:
            return null;
    }
}

export async function craftItem(
    item: PhysicalItempf2ettw,
    itemQuantity: number,
    actor: Actorpf2ettw,
    infused?: boolean
): Promise<void> {
    const itemSource = item.toObject();
    itemSource.data.quantity = itemQuantity;
    const itemTraits = item.traits;
    if (infused && itemTraits.has("alchemical") && itemTraits.has("consumable")) {
        const sourceTraits: string[] = itemSource.data.traits.value;
        sourceTraits.push("infused");
        itemSource.data.temporary = true;
    }
    const result = await actor.addToInventory(itemSource);
    if (!result) {
        ui.notifications.warn(game.i18n.localize("pf2ettw.Actions.Craft.Warning.CantAddItem"));
        return;
    }

    await ChatMessagepf2ettw.create({
        user: game.user.id,
        content: game.i18n.format("pf2ettw.Actions.Craft.Information.ReceiveItem", {
            actorName: actor.name,
            quantity: itemQuantity,
            itemName: item.name,
        }),
        speaker: { alias: actor.name },
    });
}

export async function craftSpellConsumable(
    item: Consumablepf2ettw,
    itemQuantity: number,
    actor: Actorpf2ettw
): Promise<void> {
    const consumableType = item.consumableType;
    if (!(consumableType === "scroll" || consumableType === "wand")) return;
    const spellLevel = consumableType === "wand" ? Math.ceil(item.level / 2) - 1 : Math.ceil(item.level / 2);
    const validSpells = actor.itemTypes.spell
        .filter((spell) => spell.baseLevel <= spellLevel && !spell.isCantrip && !spell.isFocusSpell && !spell.isRitual)
        .reduce((result, spell) => {
            result[spell.baseLevel] = [...(result[spell.baseLevel] || []), spell];
            return result;
        }, <Record<number, Embedded<Spellpf2ettw>[]>>{});
    const content = await renderTemplate("systems/pf2ettw/templates/actors/crafting-select-spell-dialog.html", {
        spells: validSpells,
    });

    new Dialog({
        title: game.i18n.localize("pf2ettw.Actions.Craft.SelectSpellDialog.Title"),
        content,
        buttons: {
            cancel: {
                icon: '<i class="fa fa-times"></i>',
                label: game.i18n.localize("Cancel"),
            },
            craft: {
                icon: '<i class="fa fa-hammer"></i>',
                label: game.i18n.localize("pf2ettw.Actions.Craft.SelectSpellDialog.CraftButtonLabel"),
                callback: async ($dialog) => {
                    const spellId = String($dialog.find("select[name=spell]").val());
                    const spell = actor.items.get(spellId);
                    if (!spell?.isOfType("spell")) return;
                    const item = await createConsumableFromSpell(consumableType, spell, spellLevel);

                    return craftItem(new Consumablepf2ettw(item), itemQuantity, actor);
                },
            },
        },
        default: "craft",
    }).render(true);
}

export async function renderCraftingInline(
    item: PhysicalItempf2ettw,
    roll: Rolled<CheckRoll>,
    quantity: number,
    actor: Actorpf2ettw
): Promise<string | null> {
    if (!(actor instanceof Characterpf2ettw)) return null;

    const degreeOfSuccess = roll.data.degreeOfSuccess ?? 0;
    const costs = calculateCosts(item, quantity, actor, degreeOfSuccess);
    if (!costs) return null;

    const daysForZeroCost = degreeOfSuccess > 1 ? calculateDaysToNoCost(costs) : 0;

    return await renderTemplate("systems/pf2ettw/templates/chat/crafting-result.html", {
        daysForZeroCost: daysForZeroCost,
        strings: prepStrings(costs, item),
        item,
        quantity,
        success: degreeOfSuccess > 1,
        criticalFailure: degreeOfSuccess === 0,
    });
}
