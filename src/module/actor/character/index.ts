import { Creaturepf2ettw, Familiarpf2ettw } from "@actor";
import { Abilities, CreatureSpeeds, LabeledSpeed, MovementType, SkillAbbreviation } from "@actor/creature/data";
import { AttackItem, AttackRollContext, StrikeRollContext, StrikeRollContextParams } from "@actor/creature/types";
import { ItemEffectData } from "@actor/character/data";
import { CharacterSource } from "@actor/data";
import { ActorSizepf2ettw } from "@actor/data/size";
import { calculateMAPs } from "@actor/helpers";
import {
    AbilityModifier,
    CheckModifier,
    ensureProficiencyOption,
    Modifierpf2ettw,
    MODIFIER_TYPE,
    ProficiencyModifier,
    StatisticModifier,
} from "@actor/modifiers";
import { AbilityString, SaveType } from "@actor/types";
import {
    ABILITY_ABBREVIATIONS,
    SAVE_TYPES,
    SKILL_ABBREVIATIONS,
    SKILL_DICTIONARY,
    SKILL_DICTIONARY_REVERSE,
    SKILL_EXPANDED,
} from "@actor/values";
import {
    Ancestrypf2ettw,
    Backgroundpf2ettw,
    Classpf2ettw,
    Consumablepf2ettw,
    Deitypf2ettw,
    Featpf2ettw,
    Heritagepf2ettw,
    Itempf2ettw,
    PhysicalItempf2ettw,
    Weaponpf2ettw,
} from "@item";
import { AncestryBackgroundClassManager } from "@item/abc/manager";
import { ActionTrait } from "@item/action/data";
import { ARMOR_CATEGORIES } from "@item/armor/data";
import { FeatData, ItemSourcepf2ettw, ItemType, PhysicalItemSource } from "@item/data";
import { ItemGrantData } from "@item/data/base";
import { ItemCarryType } from "@item/physical/data";
import { getPropertyRunes, getPropertySlots, getResiliencyBonus } from "@item/runes";
import { MAGIC_TRADITIONS } from "@item/spell/values";
import { WeaponDamage, WeaponSource, WeaponSystemSource } from "@item/weapon/data";
import { WeaponCategory, WeaponPropertyRuneType } from "@item/weapon/types";
import { WEAPON_CATEGORIES, WEAPON_PROPERTY_RUNE_TYPES } from "@item/weapon/values";
import { ActiveEffectpf2ettw } from "@module/active-effect";
import { ChatMessagepf2ettw } from "@module/chat-message";
import { PROFICIENCY_RANKS, ZeroToFour, ZeroToThree } from "@module/data";
import { RollNotepf2ettw } from "@module/notes";
import { extractModifiers, extractNotes, extractRollSubstitutions, extractRollTwice } from "@module/rules/util";
import { Userpf2ettw } from "@module/user";
import { CheckRoll } from "@system/check/roll";
import { DamageRollContext } from "@system/damage/damage";
import { WeaponDamagepf2ettw } from "@system/damage/weapon";
import { Predicatepf2ettw } from "@system/predication";
import { Checkpf2ettw, CheckRollContext, DamageRollpf2ettw, RollParameters, StrikeRollParams } from "@system/rolls";
import { Statistic } from "@system/statistic";
import {
    Errorpf2ettw,
    getActionGlyph,
    objectHasKey,
    setHasElement,
    sluggify,
    sortedStringify,
    traitSlugToObject,
} from "@util";
import { fromUUIDs } from "@util/from-uuids";
import { CraftingEntry, CraftingEntryData, CraftingFormula } from "./crafting";
import {
    AuxiliaryAction,
    BaseWeaponProficiencyKey,
    CharacterArmorClass,
    CharacterAttributes,
    CharacterData,
    CharacterProficiency,
    CharacterSaves,
    CharacterSkillData,
    CharacterStrike,
    CharacterSystemData,
    FeatSlot,
    GrantedFeat,
    LinkedProficiency,
    MagicTraditionProficiencies,
    MartialProficiencies,
    MartialProficiency,
    SlottedFeat,
    TraitAction,
    WeaponGroupProficiencyKey,
} from "./data";
import { CharacterSheetTabVisibility } from "./data/sheet";
import { CHARACTER_SHEET_TABS } from "./data/values";
import { StrikeWeaponTraits } from "./strike-weapon-traits";
import { CharacterHitPointsSummary, CharacterSkills, CreateAuxiliaryParams } from "./types";
import { ActionMacros } from "@system/action-macros";
import { ITEM_EFFECT_COMPENDIUMS } from "@system/effect/values";

class Characterpf2ettw extends Creaturepf2ettw {
    /** Core singular embeds for PCs */
    ancestry!: Embedded<Ancestrypf2ettw> | null;
    heritage!: Embedded<Heritagepf2ettw> | null;
    background!: Embedded<Backgroundpf2ettw> | null;
    class!: Embedded<Classpf2ettw> | null;
    deity!: Embedded<Deitypf2ettw> | null;

    /** A cached reference to this PC's familiar */
    familiar: Familiarpf2ettw | null = null;

    featGroups!: Record<string, FeatSlot | undefined>;
    pfsBoons!: FeatData[];
    deityBoonsCurses!: FeatData[];

    override get allowedItemTypes(): (ItemType | "physical")[] {
        const buildItems = ["ancestry", "heritage", "background", "class", "deity", "feat"] as const;
        return [...super.allowedItemTypes, ...buildItems, "physical", "spellcastingEntry", "spell", "action", "lore"];
    }

    get itemEffects(): ItemEffectData[] {
        return this.data.data.itemEffects;
    }

    get keyAbility(): AbilityString {
        return this.data.data.details.keyability.value || "str";
    }

    /** This PC's ability scores */
    get abilities(): Abilities {
        return deepClone(this.data.data.abilities);
    }

    override get hitPoints(): CharacterHitPointsSummary {
        return {
            ...super.hitPoints,
            recoveryMultiplier: this.data.data.attributes.hp.recoveryMultiplier,
            recoveryAddend: this.data.data.attributes.hp.recoveryAddend,
        };
    }

    override get skills(): CharacterSkills {
        const skills = super.skills;
        for (const [key, skill] of Object.entries(skills)) {
            if (!skill) continue;
            const originalKey = SKILL_DICTIONARY_REVERSE[skill.slug] ?? skill.slug;
            if (!objectHasKey(this.data.data.skills, originalKey)) continue;

            const data = this.data.data.skills[originalKey];
            skills[key] = mergeObject(skill, {
                rank: data.rank,
                ability: data.ability,
                abilityModifier: data.modifiers.find((m) => m.enabled && m.type === "ability") ?? null,
            });
        }

        return skills as CharacterSkills;
    }

    get heroPoints(): { value: number; max: number } {
        return deepClone(this.data.data.resources.heroPoints);
    }

    async getCraftingFormulas(): Promise<CraftingFormula[]> {
        const { formulas } = this.data.data.crafting;
        const formulaMap = new Map(formulas.map((data) => [data.uuid, data]));
        const items: unknown[] = await fromUUIDs(formulas.map((data) => data.uuid));
        if (!items.every((i): i is Itempf2ettw => i instanceof Itempf2ettw)) return [];

        return items
            .filter((item): item is PhysicalItempf2ettw => item instanceof PhysicalItempf2ettw)
            .map((item) => {
                const { dc, batchSize, deletable } = formulaMap.get(item.uuid) ?? { deletable: false };
                return new CraftingFormula(item, { dc, batchSize, deletable });
            });
    }

    async getCraftingEntries(): Promise<CraftingEntry[]> {
        const craftingFormulas = await this.getCraftingFormulas();
        return Object.values(this.data.data.crafting.entries)
            .filter((entry): entry is CraftingEntryData => CraftingEntry.isValid(entry))
            .map((entry) => new CraftingEntry(this, craftingFormulas, entry));
    }

    async getCraftingEntry(selector: string): Promise<CraftingEntry | null> {
        const craftingFormulas = await this.getCraftingFormulas();
        const craftingEntryData = this.data.data.crafting.entries[selector];
        if (CraftingEntry.isValid(craftingEntryData)) {
            return new CraftingEntry(this, craftingFormulas, craftingEntryData);
        }

        return null;
    }

    async performDailyCrafting(): Promise<void> {
        const entries = (await this.getCraftingEntries()).filter((e) => e.isDailyPrep);
        const alchemicalEntries = entries.filter((e) => e.isAlchemical);
        const reagentCost = alchemicalEntries.reduce((sum, entry) => sum + entry.reagentCost, 0);
        const reagentValue = (this.data.data.resources.crafting.infusedReagents.value || 0) - reagentCost;
        if (reagentValue < 0) {
            ui.notifications.warn(game.i18n.localize("pf2ettw.CraftingTab.Alerts.MissingReagents"));
            return;
        } else {
            await this.update({ "data.resources.crafting.infusedReagents.value": reagentValue });
        }

        // Remove infused/temp items
        for (const item of this.inventory) {
            if (item.data.data.temporary) await item.delete();
        }

        for (const entry of entries) {
            for (const prepData of entry.preparedFormulas) {
                const item: PhysicalItemSource = prepData.item.toObject();
                item.data.quantity = prepData.quantity || 1;
                item.data.temporary = true;
                item.data.size = this.ancestry?.size === "tiny" ? "tiny" : "med";

                if (
                    entry.isAlchemical &&
                    (item.type === "consumable" || item.type === "weapon" || item.type === "equipment")
                ) {
                    item.data.traits.value.push("infused");
                }
                await this.addToInventory(item);
            }
        }
    }

    async insertFeat(feat: Featpf2ettw, featType: string, slotId?: string): Promise<Itempf2ettw[]> {
        const group = this.featGroups[featType];
        const location = group?.slotted ? slotId ?? "" : featType;

        const resolvedFeatType = (() => {
            if (feat.featType === "archetype") {
                if (feat.data.data.traits.value.includes("skill")) {
                    return "skill";
                } else {
                    return "class";
                }
            }

            return feat.featType;
        })();

        const isFeatValidInSlot = group && (group.supported === "all" || group.supported.includes(resolvedFeatType));
        const alreadyHasFeat = this.items.has(feat.id);
        const existing = this.itemTypes.feat.filter((x) => x.data.data.location === location);

        // Handle case where its actually dragging away from a location
        if (alreadyHasFeat && feat.data.data.location && !isFeatValidInSlot) {
            return this.updateEmbeddedDocuments("Item", [{ _id: feat.id, "data.location": "" }]);
        }

        const changed: Itempf2ettw[] = [];

        // If this is a new feat, create a new feat item on the actor first
        if (!alreadyHasFeat && isFeatValidInSlot) {
            const source = feat.toObject();
            source.data.location = location;
            changed.push(...(await this.createEmbeddedDocuments("Item", [source])));
        }

        // Determine what feats we have to move around
        const locationUpdates = group?.slotted ? existing.map((x) => ({ _id: x.id, "data.location": "" })) : [];
        if (alreadyHasFeat && isFeatValidInSlot) {
            locationUpdates.push({ _id: feat.id, "data.location": location });
        }

        if (locationUpdates.length > 0) {
            changed.push(...(await this.updateEmbeddedDocuments("Item", locationUpdates)));
        }

        return changed;
    }

    /** If one exists, prepare this character's familiar */
    override prepareData(): void {
        super.prepareData();

        if (game.ready && this.familiar && game.actors.has(this.familiar.id)) {
            this.familiar.prepareData({ fromMaster: true });
        }
    }

    /** Setup base ephemeral data to be modified by active effects and derived-data preparation */
    override prepareBaseData(): void {
        super.prepareBaseData();
        const systemData: DeepPartial<CharacterSystemData> & { abilities: Abilities } = this.data.data;

        // Flags
        const { flags } = this.data;
        flags.pf2ettw.favoredWeaponRank = 0;
        flags.pf2ettw.freeCrafting ??= false;
        flags.pf2ettw.quickAlchemy ??= false;
        flags.pf2ettw.sheetTabs = mergeObject(
            CHARACTER_SHEET_TABS.reduce(
                (tabs, tab) => ({
                    ...tabs,
                    [tab]: true,
                }),
                {} as CharacterSheetTabVisibility
            ),
            flags.pf2ettw.sheetTabs ?? {}
        );

        // Build selections: boosts and skill trainings
        systemData.build = {
            abilities: {
                manual: Object.keys(systemData.abilities).length > 0,
                keyOptions: [],
                boosts: {
                    ancestry: [],
                    background: [],
                    class: null,
                    1: systemData.build?.abilities?.boosts?.[1] ?? [],
                    5: systemData.build?.abilities?.boosts?.[5] ?? [],
                    10: systemData.build?.abilities?.boosts?.[10] ?? [],
                    15: systemData.build?.abilities?.boosts?.[15] ?? [],
                    20: systemData.build?.abilities?.boosts?.[20] ?? [],
                },
                flaws: {
                    ancestry: [],
                },
            },
        };

        // Base ability scores
        for (const abbrev of ABILITY_ABBREVIATIONS) {
            systemData.abilities[abbrev] = mergeObject({ value: 10 }, systemData.abilities[abbrev] ?? {});
        }

        // Actor document and data properties from items
        const { details } = this.data.data;
        for (const property of ["ancestry", "heritage", "background", "class", "deity"] as const) {
            this[property] = null;

            if (property === "deity") {
                details.deities = {
                    primary: null,
                    secondary: null,
                    domains: {},
                };
            } else if (property !== "background") {
                details[property] = null;
            }
        }

        // Attributes
        const attributes: DeepPartial<CharacterAttributes> = this.data.data.attributes;
        attributes.ac = {};
        attributes.classDC = { rank: 0 };
        attributes.vampireDC = { ability: attributes.vampireDC?.ability ?? "str", rank: 0 };
        attributes.dexCap = [{ value: Infinity, source: "" }];
        attributes.polymorphed = false;
        attributes.battleForm = false;

        const perception = (attributes.perception ??= { ability: "wis", rank: 0 });
        perception.ability = "wis";
        perception.rank ??= 0;

        attributes.thirst = { value: 0, max: 4 };

        // Hit points
        const hitPoints = this.data.data.attributes.hp;
        hitPoints.recoveryMultiplier = 1;
        hitPoints.recoveryAddend = 0;
        attributes.ancestryhp = 0;
        attributes.classhp = 0;

        // Familiar abilities
        attributes.familiarAbilities = { value: 0 };

        // Saves and skills
        const saves: DeepPartial<CharacterSaves> = this.data.data.saves;
        for (const save of SAVE_TYPES) {
            saves[save] = {
                ability: saves[save]?.ability ?? CONFIG.pf2ettw.savingThrowDefaultAbilities[save],
                abilityOptions: CONFIG.pf2ettw.savingThrowOptionAbilities[save],
                rank: saves[save]?.rank ?? 0,
            };
        }

        const skills = this.data.data.skills;
        for (const key of SKILL_ABBREVIATIONS) {
            const skill = skills[key];
            skill.ability = SKILL_EXPANDED[SKILL_DICTIONARY[key]].ability;
            skill.armor = ["dex", "str"].includes(skill.ability);
        }

        // Spellcasting-tradition proficiencies
        systemData.proficiencies = {
            traditions: Array.from(MAGIC_TRADITIONS).reduce(
                (accumulated: DeepPartial<MagicTraditionProficiencies>, t) => ({
                    ...accumulated,
                    [t]: { rank: 0 },
                }),
                {}
            ),
        };

        // Resources
        const { resources } = this.data.data;
        resources.heroPoints.max = 3;
        resources.investiture = { value: 0, max: 10 };

        resources.focus = mergeObject({ value: 0, max: 0 }, resources.focus ?? {});
        resources.focus.max = 0;

        resources.crafting = mergeObject({ infusedReagents: { value: 0, max: 0 } }, resources.crafting ?? {});
        resources.crafting.infusedReagents.max = 0;

        // Size
        this.data.data.traits.size = new ActorSizepf2ettw({ value: "med" });

        // Alliance
        this.data.data.details.alliance = this.hasPlayerOwner ? "party" : "opposition";

        // Weapon and Armor category proficiencies
        const martial: DeepPartial<MartialProficiencies> = this.data.data.martial;
        for (const category of [...ARMOR_CATEGORIES, ...WEAPON_CATEGORIES]) {
            const proficiency: Partial<CharacterProficiency> = martial[category] ?? {};
            proficiency.rank = martial[category]?.rank ?? 0;
            martial[category] = proficiency;
        }

        const homebrewCategories = game.settings.get("pf2ettw", "homebrew.weaponCategories").map((tag) => tag.id);
        for (const category of homebrewCategories) {
            martial[category] ??= {
                rank: 0,
                value: 0,
                breakdown: "",
            };
        }

        // Indicate that crafting formulas stored directly on the actor are deletable
        for (const formula of this.data.data.crafting.formulas) {
            formula.deletable = true;
        }

        // PC level is never a derived number, so it can be set early
        this.rollOptions.all[`self:level:${this.level}`] = true;

        // Prepare a blank list of item effects
        this.data.data.itemEffects = [];
    }

    /** After AE-likes have been applied, set numeric roll options */
    override prepareEmbeddedDocuments(): void {
        super.prepareEmbeddedDocuments();

        this.setAbilityModifiers();
        this.setNumericRollOptions();
        this.deity?.setFavoredWeaponRank();
    }

    /**
     * Immediately after boosts from this PC's ancestry, background, and class have been acquired, set ability scores
     * according to them.
     */
    override prepareDataFromItems(): void {
        super.prepareDataFromItems();
        this.setAbilityScores();
    }

    override prepareDerivedData(): void {
        super.prepareDerivedData();

        const systemData = this.data.data;
        const { synthetics } = this;

        if (!this.data.flags.pf2ettw.disableABP) {
            game.pf2ettw.variantRules.AutomaticBonusProgression.concatModifiers(this.level, synthetics);
        }

        // Extract as separate variables for easier use in this method.
        const { statisticsModifiers, rollNotes } = synthetics;

        // Update experience percentage from raw experience amounts.
        systemData.details.xp.pct = Math.min(
            Math.round((systemData.details.xp.value * 100) / systemData.details.xp.max),
            99.5
        );

        // Get the itemTypes object only once for the entire run of the method
        const itemTypes = this.itemTypes;

        // Set thirst statuses according to embedded conditions
        for (const conditionName of ["thirst"] as const) {
            const condition = itemTypes.condition.find((condition) => condition.slug === conditionName);
            const status = systemData.attributes[conditionName];
            status.value = Math.min(condition?.value ?? 0, status.max);
        }

        // PFS Level Bump - check and DC modifiers
        if (systemData.pfs.levelBump) {
            const modifiersAll = (statisticsModifiers.all ??= []);
            modifiersAll.push(() => new Modifierpf2ettw("pf2ettw.PFS.LevelBump", 1, MODIFIER_TYPE.UNTYPED));
        }

        // Calculate HP and SP
        {
            const ancestryHP = systemData.attributes.ancestryhp;
            const classHP = systemData.attributes.classhp;
            const hitPoints = systemData.attributes.hp;
            const modifiers = [new Modifierpf2ettw("pf2ettw.AncestryHP", ancestryHP, MODIFIER_TYPE.UNTYPED)];

            if (game.settings.get("pf2ettw", "staminaVariant")) {
                const halfClassHp = Math.floor(classHP / 2);
                systemData.attributes.sp.max = (halfClassHp + systemData.abilities.con.mod) * this.level;
                systemData.attributes.resolve.max = systemData.abilities[systemData.details.keyability.value].mod;

                modifiers.push(new Modifierpf2ettw("pf2ettw.ClassHP", halfClassHp * this.level, MODIFIER_TYPE.UNTYPED));
            } else {
                modifiers.push(new Modifierpf2ettw("pf2ettw.ClassHP", classHP * this.level, MODIFIER_TYPE.UNTYPED));

                const conLevelBonus = systemData.abilities.con.mod * this.level;
                modifiers.push(
                    new Modifierpf2ettw({
                        slug: "hp-con",
                        label: "pf2ettw.AbilityCon",
                        ability: "con",
                        type: MODIFIER_TYPE.ABILITY,
                        modifier: conLevelBonus,
                        adjustments: this.getModifierAdjustments(["con-based"], "hp-con"),
                    })
                );
            }

            const hpRollOptions = this.getRollOptions(["hp"]);
            modifiers.push(...extractModifiers(statisticsModifiers, ["hp"], { test: hpRollOptions }));

            const perLevelRollOptions = this.getRollOptions(["hp-per-level"]);
            modifiers.push(
                ...extractModifiers(statisticsModifiers, ["hp-per-level"], { test: perLevelRollOptions }).map(
                    (clone) => {
                        clone.modifier *= this.level;
                        return clone;
                    }
                )
            );

            const stat = mergeObject(new StatisticModifier("hp", modifiers), hitPoints, { overwrite: false });

            // PFS Level Bump - hit points
            if (systemData.pfs.levelBump) {
                const hitPointsBump = Math.max(10, stat.totalModifier * 0.1);
                stat.push(new Modifierpf2ettw("pf2ettw.PFS.LevelBump", hitPointsBump, MODIFIER_TYPE.UNTYPED));
            }

            stat.max = stat.totalModifier;
            stat.value = Math.min(stat.value, stat.max); // Make sure the current HP isn't higher than the max HP
            stat.breakdown = stat.modifiers
                .filter((m) => m.enabled)
                .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                .join(", ");

            systemData.attributes.hp = stat;
        }

        this.prepareFeats();
        this.prepareSaves();
        this.prepareMartialProficiencies();

        // Perception
        {
            const proficiencyRank = systemData.attributes.perception.rank || 0;
            const modifiers = [
                AbilityModifier.fromScore("wis", systemData.abilities.wis.value),
                ProficiencyModifier.fromLevelAndRank(this.level, proficiencyRank),
            ];

            const domains = ["perception", "wis-based", "all"];
            modifiers.push(...extractModifiers(statisticsModifiers, domains));

            const stat = mergeObject(
                new StatisticModifier("perception", modifiers, this.getRollOptions(domains)),
                systemData.attributes.perception,
                { overwrite: false }
            );
            stat.breakdown = stat.modifiers
                .filter((m) => m.enabled)
                .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                .join(", ");
            stat.notes = extractNotes(rollNotes, domains);
            stat.value = stat.totalModifier;
            stat.roll = async (args: RollParameters): Promise<Rolled<CheckRoll> | null> => {
                const label = game.i18n.localize("pf2ettw.PerceptionCheck");
                const rollOptions = args.options ?? [];
                ensureProficiencyOption(rollOptions, proficiencyRank);
                if (args.dc && stat.adjustments) {
                    args.dc.adjustments = stat.adjustments;
                }

                // Get just-in-time roll options from rule elements
                for (const rule of this.rules.filter((r) => !r.ignored)) {
                    rule.beforeRoll?.(domains, rollOptions);
                }

                const rollTwice = extractRollTwice(synthetics.rollTwice, domains, rollOptions);
                const context: CheckRollContext = {
                    actor: this,
                    type: "perception-check",
                    options: rollOptions,
                    dc: args.dc,
                    rollTwice,
                    notes: stat.notes,
                };

                const roll = await Checkpf2ettw.roll(
                    new CheckModifier(label, stat),
                    context,
                    args.event,
                    args.callback
                );

                for (const rule of this.rules.filter((r) => !r.ignored)) {
                    await rule.afterRoll?.({ roll, selectors: domains, domains, rollOptions });
                }

                return roll;
            };

            systemData.attributes.perception = stat;
        }

        // Senses
        this.data.data.traits.senses = this.prepareSenses(this.data.data.traits.senses, synthetics);

        // Class DC
        {
            const domains = ["class", `${systemData.details.keyability.value}-based`, "all"];
            const modifiers = [
                AbilityModifier.fromScore(
                    systemData.details.keyability.value,
                    systemData.abilities[systemData.details.keyability.value].value
                ),
                ProficiencyModifier.fromLevelAndRank(this.level, systemData.attributes.classDC.rank ?? 0),
                ...extractModifiers(statisticsModifiers, domains),
            ];

            const stat = mergeObject(
                new StatisticModifier("class", modifiers, this.getRollOptions(domains)),
                systemData.attributes.classDC,
                { overwrite: false }
            );
            stat.notes = extractNotes(rollNotes, domains);
            stat.value = 10 + stat.totalModifier;
            stat.ability = systemData.details.keyability.value;
            stat.breakdown = [game.i18n.localize("pf2ettw.ClassDCBase")]
                .concat(
                    stat.modifiers
                        .filter((m) => m.enabled)
                        .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                )
                .join(", ");

            systemData.attributes.classDC = stat;
        }

        // Vampire DC
        {
            const vampireDCAbility = systemData.attributes.vampireDC.ability;
            const domains = ["vampire", `${vampireDCAbility}-based`, "all"];
            const modifiers = [
                AbilityModifier.fromScore(vampireDCAbility, systemData.abilities[vampireDCAbility].value),
                ProficiencyModifier.fromLevelAndRank(this.level, systemData.attributes.vampireDC.rank ?? 0),
                ...extractModifiers(statisticsModifiers, domains),
            ];

            const stat = mergeObject(
                new StatisticModifier("vampire", modifiers, this.getRollOptions(domains)),
                systemData.attributes.vampireDC,
                { overwrite: false }
            );
            stat.notes = extractNotes(rollNotes, domains);
            stat.value = 10 + stat.totalModifier;
            stat.ability = vampireDCAbility;
            stat.breakdown = [game.i18n.localize("pf2ettw.VampireDCBase")]
                .concat(
                    stat.modifiers
                        .filter((m) => m.enabled)
                        .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                )
                .join(", ");

            systemData.attributes.vampireDC = stat;
        }

        // Armor Class
        const { wornArmor, heldShield } = this;
        {
            const modifiers = [this.getShieldBonus() ?? []].flat();
            const dexCapSources = systemData.attributes.dexCap;
            let armorCheckPenalty = 0;
            const proficiency = wornArmor?.category ?? "unarmored";

            if (wornArmor && wornArmor.acBonus > 0) {
                dexCapSources.push({ value: Number(wornArmor.dexCap ?? 0), source: wornArmor.name });
                if (wornArmor.checkPenalty) {
                    // armor check penalty
                    if (typeof wornArmor.strength === "number" && systemData.abilities.str.value < wornArmor.strength) {
                        armorCheckPenalty = Number(wornArmor.checkPenalty ?? 0);
                    }
                }

                const slug = wornArmor.baseType ?? wornArmor.slug ?? sluggify(wornArmor.name);
                modifiers.unshift(
                    new Modifierpf2ettw({
                        label: wornArmor.name,
                        type: MODIFIER_TYPE.ITEM,
                        slug,
                        modifier: wornArmor.acBonus,
                        adjustments: this.getModifierAdjustments(["all", "ac"], slug),
                    })
                );
            }

            // Proficiency bonus
            modifiers.unshift(
                ProficiencyModifier.fromLevelAndRank(this.level, systemData.martial[proficiency]?.rank ?? 0)
            );

            // DEX modifier is limited by the lowest cap, usually from armor
            const dexterity = AbilityModifier.fromScore("dex", systemData.abilities.dex.value);
            const dexCap = dexCapSources.reduce((lowest, candidate) =>
                lowest.value > candidate.value ? candidate : lowest
            );
            dexterity.modifier = Math.min(dexterity.modifier, dexCap.value);
            modifiers.unshift(dexterity);

            // In case an ability other than DEX is added, find the best ability modifier and use that as the ability on
            // which AC is based
            const abilityModifier = modifiers
                .filter((m) => m.type === "ability" && !!m.ability)
                .reduce((best, modifier) => (modifier.modifier > best.modifier ? modifier : best), dexterity);
            const acAbility = abilityModifier.ability!;
            const domains = ["ac", `${acAbility}-based`];
            modifiers.push(...extractModifiers(statisticsModifiers, ["all", ...domains]));

            const rollOptions = this.getRollOptions(domains);
            const stat: CharacterArmorClass = mergeObject(new StatisticModifier("ac", modifiers, rollOptions), {
                value: 10,
                breakdown: "",
                check: armorCheckPenalty,
                dexCap,
            });
            stat.value += stat.totalModifier;
            stat.breakdown = [game.i18n.localize("pf2ettw.ArmorClassBase")]
                .concat(
                    stat.modifiers
                        .filter((m) => m.enabled)
                        .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                )
                .join(", ");

            systemData.attributes.ac = stat;
        }

        // Apply ABP to shield attributes if applicable
        if (heldShield) {
            const modifiers = extractModifiers(statisticsModifiers, ["shield"]);

            if (modifiers.length > 0) {
                const heldShieldType = heldShield.name.includes("Buckler") ? "buckler" : "shield"; // Awful heuristic
                const hpABP = modifiers
                    .filter((m) => m.slug === `${heldShieldType}-hp-potency`)
                    .reduce((prev, curr) => (prev.modifier > curr.modifier ? prev : curr)).modifier;
                const hardnessABP = modifiers
                    .filter((m) => m.slug === `${heldShieldType}-hardness-potency`)
                    .reduce((prev, curr) => (prev.modifier > curr.modifier ? prev : curr)).modifier;

                if (hardnessABP > heldShield.hardness) {
                    systemData.attributes.shield.hardness = hardnessABP;
                    heldShield.data.data.hardness = hardnessABP;
                }

                if (hpABP > heldShield.hitPoints.max) {
                    systemData.attributes.shield.hp.max = hpABP;
                    systemData.attributes.shield.brokenThreshold = hpABP / 2;

                    heldShield.data.data.hp.max = hpABP;
                    heldShield.data.data.hp.value = Math.min(hpABP, heldShield.data.data.hp.value);
                    heldShield.data.data.hp.brokenThreshold = hpABP / 2;
                }
            }

            // Ensure that shield HP is correct when going to a non-ABP lower level shield
            systemData.attributes.shield.hp.value = Math.min(
                systemData.attributes.shield.hp.value,
                systemData.attributes.shield.hp.max
            );
        }

        // Apply the speed penalty from this character's held shield
        if (heldShield?.speedPenalty) {
            const speedPenalty = new Modifierpf2ettw(heldShield.name, heldShield.speedPenalty, MODIFIER_TYPE.UNTYPED);
            speedPenalty.predicate.not = ["self:shield:ignore-speed-penalty"];
            statisticsModifiers.speed ??= [];
            statisticsModifiers.speed.push(() => speedPenalty);
        }

        // Skills
        systemData.skills = this.prepareSkills();

        // Speeds
        systemData.attributes.speed = this.prepareSpeed("land");
        const { otherSpeeds } = systemData.attributes.speed;
        for (let idx = 0; idx < otherSpeeds.length; idx++) {
            otherSpeeds[idx] = this.prepareSpeed(otherSpeeds[idx].type);
        }

        systemData.actions = this.prepareStrikes();

        systemData.actions.sort((l, r) => {
            if (l.ready !== r.ready) return (l.ready ? 0 : 1) - (r.ready ? 0 : 1);
            return (l.weapon?.data.sort ?? 0) - (r.weapon?.data.sort ?? 0);
        });

        // Spellcasting Entries
        for (const entry of itemTypes.spellcastingEntry) {
            const { ability, tradition } = entry;
            const rank = (entry.data.data.proficiency.value = entry.rank);

            const baseSelectors = ["all", `${ability}-based`, "spell-attack-dc"];
            const attackSelectors = [
                `${tradition}-spell-attack`,
                "spell-attack",
                "spell-attack-roll",
                "attack",
                "attack-roll",
            ];
            const saveSelectors = [`${tradition}-spell-dc`, "spell-dc"];

            // assign statistic data to the spellcasting entry
            entry.statistic = new Statistic(this, {
                slug: sluggify(entry.name),
                label: CONFIG.pf2ettw.magicTraditions[tradition],
                ability: entry.ability,
                rank,
                modifiers: extractModifiers(statisticsModifiers, baseSelectors),
                notes: extractNotes(rollNotes, [...baseSelectors, ...attackSelectors]),
                domains: baseSelectors,
                rollOptions: entry.getRollOptions("spellcasting"),
                check: {
                    type: "spell-attack-roll",
                    modifiers: extractModifiers(statisticsModifiers, attackSelectors),
                    domains: attackSelectors,
                },
                dc: {
                    modifiers: extractModifiers(statisticsModifiers, saveSelectors),
                    domains: saveSelectors,
                },
            });

            entry.data.data.statisticData = entry.statistic.getChatData();
        }

        // Expose best spellcasting DC to character attributes
        if (itemTypes.spellcastingEntry.length > 0) {
            const best = itemTypes.spellcastingEntry.reduce((previous, current) => {
                return current.statistic.dc.value > previous.statistic.dc.value ? current : previous;
            });
            this.data.data.attributes.spellDC = { rank: best.statistic.rank ?? 0, value: best.statistic.dc.value };
        } else {
            this.data.data.attributes.spellDC = null;
        }

        // Expose the higher between highest spellcasting DC and (if present) class DC
        this.data.data.attributes.classOrSpellDC = ((): { rank: number; value: number } => {
            const classDC = this.data.data.attributes.classDC.rank > 0 ? this.data.data.attributes.classDC : null;
            const spellDC = this.data.data.attributes.spellDC;
            return spellDC && classDC
                ? spellDC.value > classDC.value
                    ? { ...spellDC }
                    : { rank: classDC.rank, value: classDC.value }
                : classDC && !spellDC
                ? { rank: classDC.rank, value: classDC.value }
                : spellDC && !classDC
                ? { ...spellDC }
                : { rank: 0, value: 0 };
        })();

        // Initiative
        this.prepareInitiative();

        // Resources
        const { focus, crafting } = this.data.data.resources;
        focus.max = Math.clamped(focus.max, 0, 3);
        crafting.infusedReagents.value = Math.clamped(crafting.infusedReagents.value, 0, crafting.infusedReagents.max);
        // Ensure the character has a focus pool of at least one point if they have a focus spellcasting entry
        if (focus.max === 0 && this.spellcasting.regular.some((entry) => entry.isFocusPool)) {
            focus.max = 1;
        }

        // Set a roll option for whether this character has a familiar
        if (systemData.attributes.familiarAbilities.value > 0) {
            this.rollOptions.all["self:has-familiar"] = true;
        }

        // Call post-data-preparation RuleElement hooks
        for (const rule of this.rules) {
            try {
                rule.afterPrepareData?.();
            } catch (error) {
                // ensure that a failing rule element does not block actor initialization
                console.error(`pf2ettw | Failed to execute onAfterPrepareData on rule element ${rule}.`, error);
            }
        }

        // Identify item effects
        const compendiumPattern = /@Compendium\[pf2ettw\.(?<packName>[^.]+)\.(?<docName>[^\]]+)\]/g;

        for (const item of this.items.values()) {
            const matches = Array.from(item.description.matchAll(compendiumPattern));

            if (matches.length > 0) {
                matches.map((match) => {
                    if (match.groups && ITEM_EFFECT_COMPENDIUMS.includes(match.groups.packName as any)) {
                        systemData.itemEffects.push({ compendium: match.groups.packName, id: match.groups.docName });
                    }
                });
            }
        }
    }

    private setAbilityScores(): void {
        const { build, details } = this.data.data;

        if (!build.abilities.manual) {
            for (const section of ["ancestry", "background", "class", 1, 5, 10, 15, 20] as const) {
                // Skip applying boosts from levels higher than the character's
                if (typeof section === "number" && this.level < section) {
                    continue;
                }

                const boosts = build.abilities.boosts[section];
                if (typeof boosts === "string") {
                    // Class's key ability score
                    const ability = this.data.data.abilities[boosts];
                    ability.value += ability.value >= 18 ? 1 : 2;
                } else if (Array.isArray(boosts)) {
                    for (const abbrev of boosts) {
                        const ability = this.data.data.abilities[abbrev];
                        ability.value += ability.value >= 18 ? 1 : 2;
                    }
                }

                // Optional and non-optional flaws only come from the ancestry section
                const flaws = section === "ancestry" ? build.abilities.flaws[section] : [];
                for (const abbrev of flaws) {
                    const ability = this.data.data.abilities[abbrev];
                    ability.value -= 2;
                }
            }

            details.keyability.value = build.abilities.boosts.class ?? "str";
        }

        // Enforce a minimum of 8 and a maximum of 30 for homebrew "mythic" mechanics
        for (const ability of Object.values(this.data.data.abilities)) {
            ability.value = Math.clamped(ability.value, 8, 30);
            // Record base values: same as stored value if in manual mode, and prior to RE modifications otherwise
            ability.base = ability.value;
        }
    }

    private setAbilityModifiers(): void {
        // Set modifiers
        for (const ability of Object.values(this.data.data.abilities)) {
            ability.mod = Math.floor((ability.value - 10) / 2);
        }
    }

    /** Set roll operations for ability scores, proficiency ranks, and number of hands free */
    protected setNumericRollOptions(): void {
        const rollOptionsAll = this.rollOptions.all;

        const perceptionRank = this.data.data.attributes.perception.rank;
        rollOptionsAll[`perception:rank:${perceptionRank}`] = true;

        for (const key of ABILITY_ABBREVIATIONS) {
            const score = this.abilities[key].value;
            rollOptionsAll[`ability:${key}:score:${score}`] = true;
        }

        for (const key of SKILL_ABBREVIATIONS) {
            const rank = this.data.data.skills[key].rank;
            rollOptionsAll[`skill:${key}:rank:${rank}`] = true;
        }

        for (const key of ARMOR_CATEGORIES) {
            const rank = this.data.data.martial[key].rank;
            rollOptionsAll[`defense:${key}:rank:${rank}`] = true;
        }

        for (const key of WEAPON_CATEGORIES) {
            const rank = this.data.data.martial[key].rank;
            rollOptionsAll[`attack:${key}:rank:${rank}`] = true;
        }

        for (const key of SAVE_TYPES) {
            const rank = this.data.data.saves[key].rank;
            rollOptionsAll[`save:${key}:rank:${rank}`] = true;
        }

        // Set number of hands free
        const heldItems = this.inventory.filter((i) => i.isHeld);
        const handsFree = heldItems.reduce((count, item) => {
            const handsOccupied = item.traits.has("free-hand") ? 0 : item.handsHeld;
            return Math.max(count - handsOccupied, 0);
        }, 2);

        this.attributes.handsFree = handsFree;
        rollOptionsAll[`hands-free:${handsFree}`] = true;

        // Some rules specify ignoring the Free Hand trait
        const handsReallyFree = heldItems.reduce((count, i) => Math.max(count - i.handsHeld, 0), 2);
        rollOptionsAll[`hands-free:but-really:${handsReallyFree}`] = true;
        // `
    }

    private prepareSaves(): void {
        const systemData = this.data.data;
        const { wornArmor } = this;
        const { rollNotes, statisticsModifiers } = this.synthetics;

        // Saves
        const saves: Partial<Record<SaveType, Statistic>> = {};
        for (const saveType of SAVE_TYPES) {
            const save = systemData.saves[saveType];
            const saveName = game.i18n.localize(CONFIG.pf2ettw.saves[saveType]);
            const modifiers: Modifierpf2ettw[] = [];

            // Add resilient bonuses for wearing armor with a resilient rune.
            if (wornArmor?.data.data.resiliencyRune.value) {
                const resilientBonus = getResiliencyBonus(wornArmor.data.data);
                if (resilientBonus > 0 && wornArmor.isInvested) {
                    modifiers.push(new Modifierpf2ettw(wornArmor.name, resilientBonus, MODIFIER_TYPE.ITEM));
                }
            }

            const affectedByBulwark = saveType === "reflex" && wornArmor?.traits.has("bulwark");
            if (affectedByBulwark) {
                const bulwarkModifier = new Modifierpf2ettw({
                    slug: "bulwark",
                    type: MODIFIER_TYPE.UNTYPED,
                    label: CONFIG.pf2ettw.armorTraits.bulwark,
                    modifier: 3,
                    predicate: { all: ["damaging-effect"] },
                });
                modifiers.push(bulwarkModifier);

                // Add a modifier adjustment to be picked up by the construction of this saving throw's Statistic
                const reflexAdjustments = (this.synthetics.modifierAdjustments[saveType] ??= []);
                reflexAdjustments.push({
                    slug: "dex",
                    predicate: new Predicatepf2ettw({ all: ["damaging-effect"] }),
                    suppress: true,
                });
            }

            // Add custom modifiers and roll notes relevant to this save.
            const selectors = [saveType, `${save.ability}-based`, "saving-throw", "all"];
            modifiers.push(...extractModifiers(statisticsModifiers, selectors));

            const stat = new Statistic(this, {
                slug: saveType,
                label: saveName,
                ability: save.ability,
                rank: save.rank,
                notes: extractNotes(rollNotes, selectors),
                modifiers,
                domains: selectors,
                check: {
                    type: "saving-throw",
                },
                dc: {},
            });

            saves[saveType] = stat;
            this.data.data.saves[saveType] = mergeObject(this.data.data.saves[saveType], stat.getCompatData());
        }

        this.saves = saves as Record<SaveType, Statistic>;
    }

    private prepareSkills(): Record<SkillAbbreviation, CharacterSkillData> {
        const systemData = this.data.data;

        // rebuild the skills object to clear out any deleted or renamed skills from previous iterations
        const { synthetics, wornArmor } = this;

        const skills = Array.from(SKILL_ABBREVIATIONS).reduce((builtSkills, shortForm) => {
            const skill = systemData.skills[shortForm];
            const longForm = SKILL_DICTIONARY[shortForm];

            const domains = [longForm, `${skill.ability}-based`, "skill-check", `${skill.ability}-skill-check`, "all"];
            const modifiers = [
                AbilityModifier.fromScore(skill.ability, systemData.abilities[skill.ability].value),
                ProficiencyModifier.fromLevelAndRank(this.level, skill.rank),
            ];
            for (const modifier of modifiers) {
                modifier.adjustments = this.getModifierAdjustments(domains, modifier.slug);
            }

            // Indicate that the strength requirement of this actor's armor is met
            if (typeof wornArmor?.strength === "number" && this.data.data.abilities.str.value >= wornArmor.strength) {
                for (const selector of ["skill-check", "initiative"]) {
                    const rollOptions = (this.rollOptions[selector] ??= {});
                    // Nullish assign to not overwrite setting by rule element
                    rollOptions["self:armor:strength-requirement-met"] ??= true;
                }
            }

            if (skill.armor && typeof wornArmor?.checkPenalty === "number") {
                const slug = "armor-check-penalty";
                const armorCheckPenalty = new Modifierpf2ettw({
                    slug,
                    label: "pf2ettw.ArmorCheckPenalty",
                    modifier: wornArmor.checkPenalty,
                    type: MODIFIER_TYPE.UNTYPED,
                    adjustments: this.getModifierAdjustments(domains, slug),
                });

                // Set requirements for ignoring the check penalty according to skill
                armorCheckPenalty.predicate.not = ["attack", "armor:ignore-check-penalty"];
                if (["acr", "ath"].includes(shortForm)) {
                    armorCheckPenalty.predicate.not.push(
                        "self:armor:strength-requirement-met",
                        "self:armor:trait:flexible"
                    );
                } else if (shortForm === "ste" && wornArmor.traits.has("noisy")) {
                    armorCheckPenalty.predicate.not.push({
                        and: ["self:armor:strength-requirement-met", "armor:ignore-noisy-penalty"],
                    });
                } else {
                    armorCheckPenalty.predicate.not.push("self:armor:strength-requirement-met");
                }

                modifiers.push(armorCheckPenalty);
            }

            modifiers.push(...extractModifiers(synthetics.statisticsModifiers, domains));

            const stat = mergeObject(new StatisticModifier(longForm, modifiers, this.getRollOptions(domains)), skill, {
                overwrite: false,
            });
            stat.breakdown = stat.modifiers
                .filter((modifier) => modifier.enabled)
                .map((modifier) => {
                    const prefix = modifier.modifier < 0 ? "" : "+";
                    return `${modifier.label} ${prefix}${modifier.modifier}`;
                })
                .join(", ");
            stat.value = stat.totalModifier;
            stat.notes = extractNotes(synthetics.rollNotes, domains);
            stat.rank = skill.rank;
            stat.roll = async (args: RollParameters): Promise<Rolled<CheckRoll> | null> => {
                console.warn(
                    `Rolling skill checks via actor.data.data.skills.${shortForm}.roll() is deprecated, use actor.skills.${longForm}.check.roll() instead`
                );
                const label = game.i18n.format("pf2ettw.SkillCheckWithName", {
                    skillName: game.i18n.localize(CONFIG.pf2ettw.skills[shortForm]),
                });
                const rollOptions = args.options ?? [];
                ensureProficiencyOption(rollOptions, skill.rank);
                if (args.dc && stat.adjustments) {
                    args.dc.adjustments = stat.adjustments;
                }

                // Get just-in-time roll options from rule elements
                for (const rule of this.rules.filter((r) => !r.ignored)) {
                    rule.beforeRoll?.(domains, rollOptions);
                }

                const rollTwice = extractRollTwice(synthetics.rollTwice, domains, rollOptions);
                const substitutions = extractRollSubstitutions(synthetics.rollSubstitutions, domains, rollOptions);
                const context: CheckRollContext = {
                    actor: this,
                    type: "skill-check",
                    options: rollOptions,
                    dc: args.dc,
                    rollTwice,
                    substitutions,
                    notes: stat.notes,
                };

                const roll = await Checkpf2ettw.roll(
                    new CheckModifier(label, stat),
                    context,
                    args.event,
                    args.callback
                );

                for (const rule of this.rules.filter((r) => !r.ignored)) {
                    await rule.afterRoll?.({ roll, selectors: domains, domains, rollOptions });
                }

                return roll;
            };

            builtSkills[shortForm] = stat;
            return builtSkills;
        }, {} as Record<SkillAbbreviation, CharacterSkillData>);

        // Lore skills
        for (const item of this.itemTypes.lore) {
            const skill = item.data;
            // normalize skill name to lower-case and dash-separated words
            const shortForm = sluggify(skill.name) as SkillAbbreviation;
            const rank = skill.data.proficient.value;

            const domains = [shortForm, "int-based", "skill-check", "lore-skill-check", "int-skill-check", "all"];
            const modifiers = [
                AbilityModifier.fromScore("int", systemData.abilities.int.value),
                ProficiencyModifier.fromLevelAndRank(this.level, rank),
                ...extractModifiers(synthetics.statisticsModifiers, domains),
            ];

            const loreSkill = systemData.skills[shortForm];
            const stat = mergeObject(
                new StatisticModifier(shortForm, modifiers, this.getRollOptions(domains)),
                loreSkill,
                { overwrite: false }
            );
            stat.label = skill.name;
            stat.ability = "int";
            stat.itemID = skill._id;
            stat.notes = extractNotes(synthetics.rollNotes, domains);
            stat.rank = rank ?? 0;
            stat.shortform = shortForm;
            stat.expanded = skill;
            stat.value = stat.totalModifier;
            stat.lore = true;
            stat.breakdown = stat.modifiers
                .filter((m) => m.enabled)
                .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                .join(", ");
            stat.roll = async (args: RollParameters): Promise<Rolled<CheckRoll> | null> => {
                console.warn(
                    `Rolling skill checks via actor.data.data.skills.${shortForm}.roll() is deprecated, use actor.skills.${shortForm}.check.roll() instead`
                );
                const label = game.i18n.format("pf2ettw.SkillCheckWithName", { skillName: skill.name });
                const rollOptions = args.options ?? [];
                ensureProficiencyOption(rollOptions, rank);

                // Get just-in-time roll options from rule elements
                for (const rule of this.rules.filter((r) => !r.ignored)) {
                    rule.beforeRoll?.(domains, rollOptions);
                }

                const rollTwice = extractRollTwice(synthetics.rollTwice, domains, rollOptions);
                const substitutions = extractRollSubstitutions(synthetics.rollSubstitutions, domains, rollOptions);
                const context: CheckRollContext = {
                    actor: this,
                    type: "skill-check",
                    options: rollOptions,
                    dc: args.dc,
                    rollTwice,
                    substitutions,
                    notes: stat.notes,
                };

                const roll = await Checkpf2ettw.roll(
                    new CheckModifier(label, stat),
                    context,
                    args.event,
                    args.callback
                );

                for (const rule of this.rules.filter((r) => !r.ignored)) {
                    await rule.afterRoll?.({ roll, selectors: domains, domains, rollOptions });
                }

                return roll;
            };

            skills[shortForm] = stat;
        }

        return skills;
    }

    override prepareSpeed(movementType: "land"): CreatureSpeeds;
    override prepareSpeed(movementType: Exclude<MovementType, "land">): LabeledSpeed & StatisticModifier;
    override prepareSpeed(movementType: MovementType): CreatureSpeeds | (LabeledSpeed & StatisticModifier);
    override prepareSpeed(movementType: MovementType): CreatureSpeeds | (LabeledSpeed & StatisticModifier) {
        const { wornArmor } = this;
        const basePenalty = wornArmor?.speedPenalty ?? 0;
        const strength = this.data.data.abilities.str.value;
        const requirement = wornArmor?.strength ?? strength;
        const value = strength >= requirement ? Math.min(basePenalty + 5, 0) : basePenalty;

        const modifierName = wornArmor?.name ?? "pf2ettw.ArmorSpeedLabel";
        const slug = "armor-speed-penalty";
        const armorPenalty = value
            ? new Modifierpf2ettw({
                  slug,
                  label: modifierName,
                  modifier: value,
                  type: MODIFIER_TYPE.UNTYPED,
                  adjustments: this.getModifierAdjustments(["speed", `${movementType}-speed`], slug),
              })
            : null;
        if (armorPenalty) {
            const speedModifiers = (this.synthetics.statisticsModifiers["speed"] ??= []);
            armorPenalty.predicate.not = ["armor:ignore-speed-penalty"];
            armorPenalty.test(this.getRollOptions(["speed", `${movementType}-speed`]));
            speedModifiers.push(() => armorPenalty);
        }
        return super.prepareSpeed(movementType);
    }

    prepareFeats(): void {
        this.featGroups = {
            ancestryfeature: {
                label: "pf2ettw.FeaturesAncestryHeader",
                feats: [],
                supported: ["ancestryfeature"],
            },
            classfeature: {
                label: "pf2ettw.FeaturesClassHeader",
                feats: [],
                supported: ["classfeature"],
            },
            ancestry: {
                label: "pf2ettw.FeatAncestryHeader",
                feats: [],
                slotted: true,
                featFilter: "ancestry-" + this.ancestry?.slug,
                supported: ["ancestry"],
            },
            class: {
                label: "pf2ettw.FeatClassHeader",
                feats: [],
                slotted: true,
                featFilter: "classes-" + this.class?.slug,
                supported: ["class"],
            },
            dualclass: {
                label: "pf2ettw.FeatDualClassHeader",
                feats: [],
                slotted: true,
                supported: ["class"],
            },
            archetype: {
                label: "pf2ettw.FeatArchetypeHeader",
                feats: [],
                slotted: true,
                supported: ["class"],
            },
            skill: {
                label: "pf2ettw.FeatSkillHeader",
                feats: [],
                slotted: true,
                supported: ["skill"],
            },
            general: {
                label: "pf2ettw.FeatGeneralHeader",
                feats: [],
                slotted: true,
                supported: ["general", "skill"],
            },
            vampire: {
                label: "pf2ettw.FeatVampireHeader",
                feats: [],
                slotted: true,
                supported: ["vampire"],
            },
            campaign: {
                label: "pf2ettw.FeatCampaignHeader",
                feats: [],
                supported: "all",
            },
            bonus: {
                label: "pf2ettw.FeatBonusHeader",
                feats: [],
                supported: "all",
            },
        };

        this.pfsBoons = [];
        this.deityBoonsCurses = [];

        if (game.settings.get("pf2ettw", "dualClassVariant")) {
            this.featGroups.dualclass?.feats.push({ id: "dualclass-1", level: 1, grants: [] });
            for (let level = 2; level <= this.level; level += 2) {
                this.featGroups.dualclass?.feats.push({ id: `dualclass-${level}`, level, grants: [] });
            }
        } else {
            // Use delete so it is in the right place on the sheet
            delete this.featGroups.dualclass;
        }
        if (game.settings.get("pf2ettw", "freeArchetypeVariant")) {
            for (let level = 2; level <= this.level; level += 2) {
                this.featGroups.archetype?.feats.push({ id: `archetype-${level}`, level, grants: [] });
            }
        } else {
            // Use delete so it is in the right place on the sheet
            delete this.featGroups.archetype;
        }
        if (!game.settings.get("pf2ettw", "campaignFeats")) {
            // Use delete so it is in the right place on the sheet
            delete this.featGroups.campaign;
        }

        // Add feat slots from class
        if (this.class) {
            const classItem = this.class.data;
            const mapFeatLevels = (featLevels: number[], prefix: string): SlottedFeat[] => {
                if (!featLevels) {
                    return [];
                }
                return featLevels
                    .filter((featSlotLevel: number) => this.level >= featSlotLevel)
                    .map((level) => ({ id: `${prefix}-${level}`, level: `${Math.round(level)}`, grants: [] }));
            };

            mergeObject(this.featGroups, {
                ancestry: { feats: mapFeatLevels(classItem.data.ancestryFeatLevels?.value, "ancestry") },
                class: {
                    feats: mapFeatLevels(
                        classItem.data.classFeatLevels?.value
                            .concat([1.9, 5.9, 9.9, 13.9, 17.9])
                            .sort((n1, n2) => n1 - n2),
                        "class"
                    ),
                },
                skill: { feats: mapFeatLevels(classItem.data.skillFeatLevels?.value, "skill") },
                general: { feats: mapFeatLevels(classItem.data.generalFeatLevels?.value, "general") },
                vampire: { feats: mapFeatLevels(classItem.data.vampireFeatLevels?.value, "vampire") },
            });
        }

        if (game.settings.get("pf2ettw", "ancestryParagonVariant")) {
            this.featGroups.ancestry?.feats.unshift({
                id: "ancestry-bonus",
                level: 1,
                grants: [],
            });
            for (let level = 3; level <= this.level; level += 4) {
                const index = (level + 1) / 2;
                this.featGroups.ancestry?.feats.splice(index, 0, { id: `ancestry-${level}`, level, grants: [] });
            }
        }

        const background = this.background;
        if (background && Object.keys(background.data.data.items).length > 0) {
            this.featGroups.skill?.feats.unshift({
                id: background.id,
                level: game.i18n.localize("pf2ettw.FeatBackgroundShort"),
                grants: [],
            });
        }

        // put the feats in their feat slots
        const allFeatSlots = Object.values(this.featGroups).flatMap((slot) => slot?.feats ?? []);
        const feats = this.itemTypes.feat.sort((f1, f2) => f1.data.sort - f2.data.sort);
        for (const feat of feats) {
            const featData = feat.data;
            if (featData.flags.pf2ettw.grantedBy && !featData.data.location) {
                const granter = this.items.get(featData.flags.pf2ettw.grantedBy.id);
                if (granter?.isOfType("feat")) continue;
            }

            const location = featData.data.location;
            const featType = featData.data.featType.value;
            let slotIndex = allFeatSlots.findIndex((slotted) => "id" in slotted && slotted.id === location);
            const existing = allFeatSlots[slotIndex]?.feat;
            if (slotIndex !== -1 && existing) {
                console.debug(`Foundry VTT | Multiple feats with same index: ${featData.name}, ${existing.name}`);
                slotIndex = -1;
            }

            const getGrantedItems = (grants: ItemGrantData[]): GrantedFeat[] => {
                return grants.flatMap((grant) => {
                    const item = this.items.get(grant.id);
                    return item?.isOfType("feat") && !item.data.data.location
                        ? { feat: item, grants: getGrantedItems(item.data.flags.pf2ettw.itemGrants) }
                        : [];
                });
            };

            // If we know the slot, place directly into the slot
            if (slotIndex !== -1) {
                const slot = allFeatSlots[slotIndex];
                slot.feat = featData;
                slot.grants = getGrantedItems(featData.flags.pf2ettw.itemGrants);
                continue;
            }

            // Handle PFS and Deity boons and curses
            if (featType === "pfsboon") {
                this.pfsBoons.push(featData);
                continue;
            } else if (["deityboon", "curse"].includes(featType)) {
                this.deityBoonsCurses.push(featData);
                continue;
            }

            // Perhaps this belongs to a un-slotted group matched on the location or
            // on the feat type. Failing that, it gets dumped into bonuses.
            const groups: Record<string, FeatSlot | undefined> = this.featGroups;
            const lookedUpGroup = groups[location ?? ""] ?? groups[featType];
            const group = lookedUpGroup && !lookedUpGroup.slotted ? lookedUpGroup : this.featGroups.bonus;
            if (group && !group.slotted) {
                const grants = getGrantedItems(featData.flags.pf2ettw.itemGrants);
                group.feats.push({ feat: featData, grants });
            }
        }

        this.featGroups.classfeature?.feats.sort(
            (a, b) => (a.feat?.data.level.value || 0) - (b.feat?.data.level.value || 0)
        );
    }

    /** Create an "auxiliary" action, an Interact or Release action using a weapon */
    createAuxAction({ weapon, action, purpose, hands }: CreateAuxiliaryParams): AuxiliaryAction {
        // A variant title reflects the options to draw, pick up, or retrieve a weapon with one or two hands */
        const [actions, carryType, fullPurpose] = ((): [ZeroToThree, ItemCarryType, string] => {
            switch (purpose) {
                case "Draw":
                    return [1, "held", `${purpose}${hands}H`];
                case "PickUp":
                    return [1, "held", `${purpose}${hands}H`];
                case "Retrieve":
                    return [weapon.container?.isHeld ? 2 : 3, "held", `${purpose}${hands}H`];
                case "Grip":
                    return [action === "Interact" ? 1 : 0, "held", purpose];
                case "Sheathe":
                    return [1, "worn", purpose];
                case "Drop":
                    return [0, "dropped", purpose];
            }
        })();
        const actionGlyph = getActionGlyph(actions);

        return {
            label: game.i18n.localize(`pf2ettw.Actions.${action}.${fullPurpose}.Title`),
            img: actionGlyph,
            execute: async (): Promise<void> => {
                await this.adjustCarryType(weapon, carryType, hands);

                if (!game.combat) return; // Only send out messages if in encounter mode

                const templates = {
                    flavor: "./systems/pf2ettw/templates/chat/action/flavor.html",
                    content: "./systems/pf2ettw/templates/chat/action/content.html",
                };

                const flavorAction = {
                    title: `pf2ettw.Actions.${action}.Title`,
                    subtitle: `pf2ettw.Actions.${action}.${fullPurpose}.Title`,
                    typeNumber: actionGlyph,
                };

                const flavor = await renderTemplate(templates.flavor, {
                    action: flavorAction,
                    traits: [
                        {
                            name: CONFIG.pf2ettw.featTraits.manipulate,
                            description: CONFIG.pf2ettw.traitsDescriptions.manipulate,
                        },
                    ],
                });

                const content = await renderTemplate(templates.content, {
                    imgPath: weapon.img,
                    message: game.i18n.format(`pf2ettw.Actions.${action}.${fullPurpose}.Description`, {
                        actor: this.name,
                        weapon: weapon.name,
                    }),
                });

                await ChatMessagepf2ettw.create({
                    content,
                    speaker: ChatMessagepf2ettw.getSpeaker({ actor: this }),
                    flavor,
                    type: CONST.CHAT_MESSAGE_TYPES.EMOTE,
                });
            },
        };
    }

    /** Prepare this character's strike actions */
    prepareStrikes({ includeBasicUnarmed = true } = {}): CharacterStrike[] {
        const { itemTypes, synthetics } = this;

        // Acquire the character's handwraps of mighty blows and apply its runes to all unarmed attacks
        const handwrapsSlug = "handwraps-of-mighty-blows";
        const handwraps = itemTypes.weapon.find(
            (w) => w.slug === handwrapsSlug && w.category === "unarmed" && w.isEquipped
        );
        const unarmedRunes = ((): DeepPartial<WeaponSystemSource> | null => {
            const { potencyRune, strikingRune, propertyRune1, propertyRune2, propertyRune3, propertyRune4 } =
                handwraps?.data._source.data ?? {};
            return handwraps?.isInvested
                ? deepClone({
                      potencyRune,
                      strikingRune,
                      propertyRune1,
                      propertyRune2,
                      propertyRune3,
                      propertyRune4,
                  })
                : null;
        })();

        // Add a basic unarmed strike
        const basicUnarmed = includeBasicUnarmed
            ? ((): Embedded<Weaponpf2ettw> => {
                  const source: PreCreate<WeaponSource> & { data: { damage?: Partial<WeaponDamage> } } = {
                      _id: "xxpf2exUNARMEDxx",
                      name: game.i18n.localize("pf2ettw.WeaponTypeUnarmed"),
                      type: "weapon",
                      img: "systems/pf2ettw/icons/features/classes/powerful-fist.webp",
                      data: {
                          slug: "basic-unarmed",
                          category: "unarmed",
                          baseItem: null,
                          bonus: { value: 0 },
                          damage: { dice: 1, die: "d4", damageType: "bludgeoning" },
                          equipped: {
                              carryType: "worn",
                              inSlot: true,
                              handsHeld: 0,
                          },
                          group: "brawling",
                          traits: { value: ["agile", "finesse", "nonlethal", "unarmed"] },
                          usage: { value: "worngloves" },
                          ...(unarmedRunes ?? {}),
                      },
                  };

                  // No handwraps, so generate straight from source
                  return new Weaponpf2ettw(source, {
                      parent: this,
                      pf2ettw: { ready: true },
                  }) as Embedded<Weaponpf2ettw>;
              })()
            : null;

        // Regenerate unarmed strikes from handwraps so that all runes are included
        if (unarmedRunes) {
            for (const [slug, weapon] of synthetics.strikes.entries()) {
                if (weapon.category === "unarmed") {
                    synthetics.strikes.set(slug, weapon.clone({ data: unarmedRunes }, { keepId: true }));
                }

                // Prevent synthetic strikes from being renamed by runes
                const clone = synthetics.strikes.get(slug)!;
                clone.data.name = clone.data._source.name;
            }
        }

        const ammos = itemTypes.consumable.filter((i) => i.consumableType === "ammo" && !i.isStowed);
        const homebrewCategoryTags = game.settings.get("pf2ettw", "homebrew.weaponCategories");
        const offensiveCategories = [...WEAPON_CATEGORIES, ...homebrewCategoryTags.map((tag) => tag.id)];

        // Exclude handwraps as a strike
        const weapons = [
            itemTypes.weapon.filter((w) => w.slug !== handwrapsSlug),
            Array.from(synthetics.strikes.values()),
            basicUnarmed ?? [],
        ].flat();

        return weapons.map((w) => this.prepareStrike(w, { categories: offensiveCategories, ammos }));
    }

    /** Prepare a strike action from a weapon */
    private prepareStrike(
        weapon: Embedded<Weaponpf2ettw>,
        options: {
            categories: WeaponCategory[];
            ammos?: Embedded<Consumablepf2ettw>[];
            defaultAbility?: AbilityString;
        }
    ): CharacterStrike {
        const itemData = weapon.data;
        const { synthetics } = this;
        const { rollNotes, statisticsModifiers, strikeAdjustments } = synthetics;
        const modifiers: Modifierpf2ettw[] = [];
        const systemData = this.data.data;
        const { categories } = options;
        const ammos = options.ammos ?? [];

        // Apply strike adjustments affecting the weapon
        for (const adjustment of strikeAdjustments) {
            adjustment.adjustWeapon?.(weapon);
        }
        const weaponRollOptions = weapon.getRollOptions();
        const weaponTraits = weapon.traits;

        // Determine the default ability and score for this attack.
        const defaultAbility = options.defaultAbility ?? (weapon.isMelee ? "str" : "dex");
        const score = systemData.abilities[defaultAbility].value;
        modifiers.push(AbilityModifier.fromScore(defaultAbility, score));
        if (weapon.isMelee && weaponTraits.has("finesse")) {
            const dexScore = systemData.abilities.dex.value;
            modifiers.push(AbilityModifier.fromScore("dex", dexScore));
        }

        // If the character has an ancestral weapon familiarity or similar feature, it will make weapons that meet
        // certain criteria also count as weapon of different category
        const categoryRank = systemData.martial[weapon.category]?.rank ?? 0;
        const groupRank = systemData.martial[`weapon-group-${weapon.group}`]?.rank ?? 0;

        // Weapons that are interchangeable for all rules purposes (e.g., longbow and composite longbow)
        const equivalentWeapons: Record<string, string | undefined> = CONFIG.pf2ettw.equivalentWeapons;
        const baseWeapon = equivalentWeapons[weapon.baseType ?? ""] ?? weapon.baseType;
        const baseWeaponRank = systemData.martial[`weapon-base-${baseWeapon}`]?.rank ?? 0;

        const syntheticRanks = Object.values(systemData.martial)
            .filter((p): p is MartialProficiency => "definition" in p && p.definition.test(weaponRollOptions))
            .map((p) => p.rank);

        // If a weapon matches against a linked proficiency, temporarily add the `sameAs` category to the weapon's
        // item roll options
        const equivalentCategories = Object.values(systemData.martial).flatMap((p) =>
            "sameAs" in p && p.definition.test(weaponRollOptions) ? `weapon:category:${p.sameAs}` : []
        );
        weaponRollOptions.push(...equivalentCategories);

        const proficiencyRank = Math.max(categoryRank, groupRank, baseWeaponRank, ...syntheticRanks);
        modifiers.push(ProficiencyModifier.fromLevelAndRank(this.level, proficiencyRank));
        weaponRollOptions.push(`weapon:proficiency:rank:${proficiencyRank}`);

        const unarmedOrWeapon = weapon.category === "unarmed" ? "unarmed" : "weapon";
        const meleeOrRanged = weapon.isMelee ? "melee" : "ranged";
        const slug = weapon.slug ?? sluggify(weapon.name);

        const weaponSpecificSelectors = [
            weapon.baseType ? `${weapon.baseType}-base-attack-roll` : [],
            weapon.group ? `${weapon.group}-group-attack-roll` : [],
            weapon.data.data.traits.otherTags.map((t) => `${t}-tag-attack-roll`),
        ].flat();

        const baseSelectors = [
            ...weaponSpecificSelectors,
            "attack",
            "mundane-attack",
            `${weapon.id}-attack`,
            `${slug}-attack`,
            `${slug}-attack-roll`,
            "strike-attack-roll",
            `${unarmedOrWeapon}-attack-roll`,
            `${meleeOrRanged}-attack-roll`,
            "attack-roll",
            "all",
        ];
        const baseOptions = [
            ...this.getRollOptions(baseSelectors),
            ...weaponTraits, // always add weapon traits as options
            ...weaponRollOptions,
            meleeOrRanged,
        ];
        ensureProficiencyOption(baseOptions, proficiencyRank);

        // Determine the ability-based synthetic selectors according to the prevailing ability modifier
        const selectors = (() => {
            const options = { resolvables: { weapon } };
            const abilityModifier = [...modifiers, ...extractModifiers(statisticsModifiers, baseSelectors, options)]
                .filter((m): m is Modifierpf2ettw & { ability: AbilityString } => m.type === "ability")
                .flatMap((modifier) => (modifier.predicate.test(baseOptions) ? modifier : []))
                .reduce((best, candidate) => (candidate.modifier > best.modifier ? candidate : best));

            if (!abilityModifier) {
                console.warn(
                    `pf2ettw System | No ability modifier was determined for attack roll with ${weapon.name} (${weapon.uuid})`
                );
                return baseSelectors;
            }

            const ability = abilityModifier.ability;

            return [
                baseSelectors,
                baseWeapon && !baseWeapon.includes(`${baseWeapon}-attack`) ? `${baseWeapon}-attack` : [],
                weapon.group ? `${weapon.group}-weapon-group-attack` : [],
                `${ability}-attack`,
                `${ability}-based`,
            ].flat();
        })();

        // Extract weapon roll notes
        const attackRollNotes = extractNotes(rollNotes, selectors);
        const ABP = game.pf2ettw.variantRules.AutomaticBonusProgression;

        if (weapon.group === "bomb" && !ABP.isEnabled) {
            const attackBonus = Number(itemData.data.bonus?.value) || 0;
            if (attackBonus !== 0) {
                modifiers.push(new Modifierpf2ettw("pf2ettw.ItemBonusLabel", attackBonus, MODIFIER_TYPE.ITEM));
            }
        }

        // Get best weapon potency
        const weaponPotency = (() => {
            const potency = selectors
                .flatMap((key) => deepClone(synthetics.weaponPotency[key] ?? []))
                .filter((wp) => Predicatepf2ettw.test(wp.predicate, baseOptions));
            ABP.applyPropertyRunes(potency, weapon);
            const potencyRune = Number(itemData.data.potencyRune?.value) || 0;

            if (potencyRune) {
                const property = getPropertyRunes(itemData, getPropertySlots(itemData)).filter(
                    (r): r is WeaponPropertyRuneType => setHasElement(WEAPON_PROPERTY_RUNE_TYPES, r)
                );
                potency.push({ label: "pf2ettw.PotencyRuneLabel", bonus: potencyRune, type: "item", property });
            }
            return potency.length > 0
                ? potency.reduce((highest, current) => (highest.bonus > current.bonus ? highest : current))
                : null;
        })();

        if (weaponPotency) {
            modifiers.push(new Modifierpf2ettw(weaponPotency.label, weaponPotency.bonus, weaponPotency.type));
            weaponTraits.add("magical");
        }

        // Everything from relevant synthetics
        modifiers.push(
            ...extractModifiers(statisticsModifiers, selectors, { injectables: { weapon }, resolvables: { weapon } })
        );

        // Multiple attack penalty
        const multipleAttackPenalty = calculateMAPs(weapon, { domains: selectors, options: baseOptions });

        const auxiliaryActions: AuxiliaryAction[] = [];
        const isRealItem = this.items.has(weapon.id);

        if (isRealItem && weapon.category !== "unarmed") {
            const traitsArray = weapon.data.data.traits.value;
            const hasFatalAimTrait = traitsArray.some((t) => t.startsWith("fatal-aim"));
            const hasTwoHandTrait = traitsArray.some((t) => t.startsWith("two-hand"));
            const { usage } = weapon.data.data;
            const canWield2H = (usage.type === "held" && usage.hands === 2) || hasFatalAimTrait || hasTwoHandTrait;

            switch (weapon.carryType) {
                case "held": {
                    if (weapon.handsHeld === 2) {
                        auxiliaryActions.push(
                            this.createAuxAction({ weapon, action: "Release", purpose: "Grip", hands: 1 })
                        );
                    } else if (weapon.handsHeld === 1 && canWield2H) {
                        auxiliaryActions.push(
                            this.createAuxAction({ weapon, action: "Interact", purpose: "Grip", hands: 2 })
                        );
                    }
                    auxiliaryActions.push(
                        this.createAuxAction({ weapon, action: "Interact", purpose: "Sheathe", hands: 0 })
                    );
                    auxiliaryActions.push(
                        this.createAuxAction({ weapon, action: "Release", purpose: "Drop", hands: 0 })
                    );
                    break;
                }
                case "worn": {
                    if (canWield2H) {
                        auxiliaryActions.push(
                            this.createAuxAction({ weapon, action: "Interact", purpose: "Draw", hands: 2 })
                        );
                    }
                    auxiliaryActions.push(
                        this.createAuxAction({ weapon, action: "Interact", purpose: "Draw", hands: 1 })
                    );
                    break;
                }
                case "stowed": {
                    auxiliaryActions.push(
                        this.createAuxAction({ weapon, action: "Interact", purpose: "Retrieve", hands: 1 })
                    );
                    break;
                }
                case "dropped": {
                    if (canWield2H) {
                        auxiliaryActions.push(
                            this.createAuxAction({ weapon, action: "Interact", purpose: "PickUp", hands: 2 })
                        );
                    }
                    auxiliaryActions.push(
                        this.createAuxAction({ weapon, action: "Interact", purpose: "PickUp", hands: 1 })
                    );
                    break;
                }
            }
        }

        // Figure out any trip etc actions from weapon traits
        // TODO: Make it show the modifier to the roll properly
        // TODO: Make it include assurance appropriately
        const traitActions: TraitAction[] = [];

        if (isRealItem) {
            const traitsArray = weapon.data.data.traits.value;

            const weaponTraits: Record<string, string | undefined> = CONFIG.pf2ettw.weaponTraits;
            const actionMacroList: Record<string, Function> = { ...ActionMacros };

            for (const action of ["disarm", "grapple", "scale", "shove", "ranged-trip", "trip"] as const) {
                if (traitsArray.some((t) => t === action)) {
                    const realAction = action === "ranged-trip" ? "trip" : action;
                    const traitActionDetail: TraitAction = { action: realAction, variants: [] };

                    const labels: [string, string, string] = [
                        game.i18n.localize(`${weaponTraits[action]}`),
                        game.i18n.format("pf2ettw.MAPAbbreviationLabel", { penalty: multipleAttackPenalty.map1 }),
                        game.i18n.format("pf2ettw.MAPAbbreviationLabel", { penalty: multipleAttackPenalty.map2 }),
                    ];

                    const modifiers: [Modifierpf2ettw[], Modifierpf2ettw[], Modifierpf2ettw[]] = [
                        [],
                        [
                            new Modifierpf2ettw(
                                multipleAttackPenalty.label,
                                multipleAttackPenalty.map1,
                                MODIFIER_TYPE.UNTYPED
                            ),
                        ],
                        [
                            new Modifierpf2ettw(
                                multipleAttackPenalty.label,
                                multipleAttackPenalty.map2,
                                MODIFIER_TYPE.UNTYPED
                            ),
                        ],
                    ];

                    if (traitsArray.some((t) => t === "finesse")) {
                        const finesseModifier = new Modifierpf2ettw(
                            game.i18n.format("pf2ettw.AbilityDex"),
                            systemData.abilities.dex.mod,
                            MODIFIER_TYPE.ABILITY
                        );
                        modifiers.forEach((a) => a.push(finesseModifier));
                    }

                    traitActionDetail.variants = [0, 1, 2].map((index) => ({
                        label: labels[index],
                        roll: actionMacroList[realAction],
                        modifiers: modifiers[index],
                        weapon: itemData._id,
                    }));

                    traitActions.push(traitActionDetail);
                }
            }
        }

        const flavor = this.getStrikeDescription(weapon);
        const rollOptions = [...this.getRollOptions(selectors), ...weaponRollOptions, ...weaponTraits, meleeOrRanged];
        const strikeStat = new StatisticModifier(weapon.name, modifiers, rollOptions);
        const altUsages = weapon.getAltUsages().map((w) => this.prepareStrike(w, { categories }));

        const action: CharacterStrike = mergeObject(strikeStat, {
            imageUrl: weapon.img,
            quantity: weapon.quantity,
            slug: weapon.slug,
            ready: weapon.isEquipped,
            glyph: "A",
            item: weapon,
            type: "strike" as const,
            ...flavor,
            options: itemData.data.options?.value ?? [],
            traits: [],
            variants: [],
            selectedAmmoId: itemData.data.selectedAmmoId,
            altUsages,
            auxiliaryActions,
            traitActions,
        });

        // Define these as getters so that Foundry's TokenDocument#getBarAttribute method doesn't recurse infinitely
        Object.defineProperty(action, "origin", {
            get: () => this.items.get(weapon.id),
        });

        // Show the ammo list if the weapon requires ammo
        if (weapon.requiresAmmo) {
            const compatible = ammos.filter((ammo) => ammo.isAmmoFor(weapon)).map((ammo) => ammo.toObject(false));
            const incompatible = ammos.filter((ammo) => !ammo.isAmmoFor(weapon)).map((ammo) => ammo.toObject(false));

            const ammo = weapon.ammo;
            const selected = ammo && {
                id: ammo.id,
                compatible: ammo.isAmmoFor(weapon),
            };
            action.ammunition = { compatible, incompatible, selected: selected ?? undefined };
        }

        const actionTraits: ActionTrait[] = [
            "attack" as const,
            // CRB p. 544: "Due to the complexity involved in preparing bombs, Strikes to throw alchemical bombs gain
            // the manipulate trait."
            weapon.baseType === "alchemical-bomb" ? ("manipulate" as const) : [],
        ].flat();
        for (const adjustment of this.synthetics.strikeAdjustments) {
            adjustment.adjustTraits?.(weapon, actionTraits);
        }
        action.traits = actionTraits.map((t) => traitSlugToObject(t, CONFIG.pf2ettw.actionTraits));

        action.breakdown = action.modifiers
            .filter((m) => m.enabled)
            .map((m) => `${m.label} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
            .join(", ");

        const checkName = game.i18n.format(
            weapon.isMelee ? "pf2ettw.Action.Strike.MeleeLabel" : "pf2ettw.Action.Strike.RangedLabel",
            { weapon: weapon.name }
        );

        const noMAPLabel = ((): string => {
            const strike = game.i18n.localize("pf2ettw.WeaponStrikeLabel");
            const value = action.totalModifier;
            const sign = value < 0 ? "" : "+";
            return `${strike} ${sign}${value}`;
        })();

        const labels: [string, string, string] = [
            noMAPLabel,
            game.i18n.format("pf2ettw.MAPAbbreviationLabel", { penalty: multipleAttackPenalty.map1 }),
            game.i18n.format("pf2ettw.MAPAbbreviationLabel", { penalty: multipleAttackPenalty.map2 }),
        ];
        const checkModifiers = [
            (otherModifiers: Modifierpf2ettw[]) => new CheckModifier(checkName, action, otherModifiers),
            (otherModifiers: Modifierpf2ettw[]) =>
                new CheckModifier(checkName, action, [
                    ...otherModifiers,
                    new Modifierpf2ettw(multipleAttackPenalty.label, multipleAttackPenalty.map1, MODIFIER_TYPE.UNTYPED),
                ]),
            (otherModifiers: Modifierpf2ettw[]) =>
                new CheckModifier(checkName, action, [
                    ...otherModifiers,
                    new Modifierpf2ettw(multipleAttackPenalty.label, multipleAttackPenalty.map2, MODIFIER_TYPE.UNTYPED),
                ]),
        ];

        const getRangeIncrement = (distance: number | null): number | null =>
            weapon.rangeIncrement && typeof distance === "number"
                ? Math.max(Math.ceil(distance / weapon.rangeIncrement), 1)
                : null;

        action.variants = [0, 1, 2]
            .map((index): [string, (otherModifiers: Modifierpf2ettw[]) => CheckModifier] => [
                labels[index],
                checkModifiers[index],
            ])
            .map(([label, constructModifier]) => ({
                label,
                roll: async (args: StrikeRollParams): Promise<Rolled<CheckRoll> | null> => {
                    if (weapon.requiresAmmo && !weapon.ammo) {
                        ui.notifications.warn(
                            game.i18n.format("pf2ettw.Strike.Ranged.NoAmmo", { weapon: weapon.name, actor: this.name })
                        );
                        return null;
                    }

                    const context = this.getAttackRollContext({
                        domains: [],
                        item: weapon,
                        viewOnly: args.getFormula ?? false,
                    });

                    // Set range-increment roll option and penalty
                    const rangeIncrement = getRangeIncrement(context.target?.distance ?? null);
                    const incrementOption = rangeIncrement ? `target:range-increment:${rangeIncrement}` : [];
                    const otherModifiers = [
                        this.getRangePenalty(rangeIncrement, selectors, baseOptions) ?? [],
                        context.self.modifiers,
                    ].flat();

                    // Collect roll options from all sources
                    args.options ??= [];
                    const options = [
                        args.options,
                        context.options,
                        action.options,
                        baseOptions,
                        incrementOption,
                    ].flat();

                    // Get just-in-time roll options from rule elements
                    for (const rule of this.rules.filter((r) => !r.ignored)) {
                        rule.beforeRoll?.(selectors, options);
                    }
                    const finalRollOptions = Array.from(new Set(options));

                    const dc = args.dc ?? context.dc;
                    if (dc && action.adjustments) {
                        dc.adjustments = action.adjustments;
                    }

                    const item = context.self.item;
                    const rollTwice = extractRollTwice(synthetics.rollTwice, selectors, finalRollOptions);

                    const checkContext: CheckRollContext = {
                        actor: context.self.actor,
                        target: context.target,
                        item,
                        type: "attack-roll",
                        altUsage: args.altUsage ?? null,
                        options: finalRollOptions,
                        notes: attackRollNotes,
                        dc,
                        traits: context.traits,
                        rollTwice,
                    };

                    if (!this.consumeAmmo(item, args)) return null;

                    const roll = await Checkpf2ettw.roll(
                        constructModifier(otherModifiers),
                        checkContext,
                        args.event,
                        args.callback
                    );

                    for (const rule of this.rules.filter((r) => !r.ignored)) {
                        await rule.afterRoll?.({ roll, selectors, domains: selectors, rollOptions: finalRollOptions });
                    }

                    return roll;
                },
            }));
        action.attack = action.roll = action.variants[0].roll;

        for (const method of ["damage", "critical"] as const) {
            action[method] = async (args: StrikeRollParams): Promise<string | void> => {
                const context = this.getDamageRollContext({
                    item: weapon,
                    viewOnly: args.getFormula ?? false,
                });

                // Set range-increment roll option
                const rangeIncrement = getRangeIncrement(context.target?.distance ?? null);
                const incrementOption =
                    typeof rangeIncrement === "number" ? `target:range-increment:${rangeIncrement}` : [];
                args.options ??= [];
                const options = new Set(
                    [args.options, context.options, action.options, baseOptions, incrementOption].flat().sort()
                );

                const damage = WeaponDamagepf2ettw.calculate(
                    context.self.item.data,
                    context.self.actor,
                    context.traits,
                    statisticsModifiers,
                    this.cloneSyntheticsRecord(synthetics.damageDice),
                    proficiencyRank,
                    Array.from(options),
                    this.cloneSyntheticsRecord(rollNotes),
                    weaponPotency,
                    synthetics.striking,
                    synthetics.strikeAdjustments
                );
                const outcome = method === "damage" ? "success" : "criticalSuccess";

                // Find a critical specialization note
                const critSpecs = context.self.actor.synthetics.criticalSpecalizations;
                if (outcome === "criticalSuccess" && !args.getFormula && critSpecs.standard.length > 0) {
                    // If an alternate critical specialization effect is available, apply it only if there is also a
                    // qualifying non-alternate
                    const standard = critSpecs.standard
                        .flatMap((cs): RollNotepf2ettw | never[] => cs(context.self.item, options) ?? [])
                        .pop();
                    const alternate = critSpecs.alternate
                        .flatMap((cs): RollNotepf2ettw | never[] => cs(context.self.item, options) ?? [])
                        .pop();
                    const note = standard ? alternate ?? standard : null;

                    if (note) damage.notes.push(note);
                }

                if (args.getFormula) {
                    return damage.formula[outcome].formula;
                } else {
                    const { self, target, options } = context;

                    const damageContext: DamageRollContext = { type: "damage-roll", self, target, outcome, options };

                    await DamageRollpf2ettw.roll(damage, damageContext, args.callback);
                }
            };
        }

        return action;
    }

    getStrikeDescription(weapon: Weaponpf2ettw): { description: string; criticalSuccess: string; success: string } {
        const flavor = {
            description: "pf2ettw.Strike.Default.Description",
            criticalSuccess: "pf2ettw.Strike.Default.CriticalSuccess",
            success: "pf2ettw.Strike.Default.Success",
        };
        const traits = weapon.traits;
        if (traits.has("unarmed")) {
            flavor.description = "pf2ettw.Strike.Unarmed.Description";
            flavor.success = "pf2ettw.Strike.Unarmed.Success";
        } else if ([...traits].some((trait) => trait.startsWith("thrown-") || trait === "combination")) {
            flavor.description = "pf2ettw.Strike.Combined.Description";
            flavor.success = "pf2ettw.Strike.Combined.Success";
        } else if (weapon.isMelee) {
            flavor.description = "pf2ettw.Strike.Melee.Description";
            flavor.success = "pf2ettw.Strike.Melee.Success";
        } else {
            flavor.description = "pf2ettw.Strike.Ranged.Description";
            flavor.success = "pf2ettw.Strike.Ranged.Success";
        }
        return flavor;
    }

    /** Possibly modify this weapon depending on its */
    protected override getStrikeRollContext<I extends AttackItem>(
        params: StrikeRollContextParams<I>
    ): StrikeRollContext<this, I> {
        const context = super.getStrikeRollContext(params);
        if (context.self.item.isOfType("weapon")) {
            StrikeWeaponTraits.adjustWeapon(context.self.item);
        }

        return context;
    }

    /** Create attack-roll modifiers from weapon traits */
    override getAttackRollContext<I extends AttackItem>(
        params: StrikeRollContextParams<I>
    ): AttackRollContext<this, I> {
        const context = super.getAttackRollContext(params);
        if (context.self.item.isOfType("weapon")) {
            context.self.modifiers.push(...StrikeWeaponTraits.createAttackModifiers(context.self.item));
        }

        return context;
    }

    consumeAmmo(weapon: Weaponpf2ettw, args: RollParameters): boolean {
        const ammo = weapon.ammo;
        if (!ammo) {
            return true;
        } else if (ammo.quantity < 1) {
            ui.notifications.warn(game.i18n.localize("pf2ettw.ErrorMessage.NotEnoughAmmo"));
            return false;
        } else {
            const existingCallback = args.callback;
            args.callback = async (roll: Rolled<Roll>) => {
                existingCallback?.(roll);
                await ammo.consume();
            };
            return true;
        }
    }

    /** Prepare stored and synthetic martial proficiencies */
    prepareMartialProficiencies(): void {
        const systemData = this.data.data;

        // Set ranks of linked proficiencies to their respective categories
        const linkedProficiencies = Object.values(systemData.martial).filter(
            (p): p is LinkedProficiency => "sameAs" in p && String(p.sameAs) in systemData.martial
        );
        for (const proficiency of linkedProficiencies) {
            const category = systemData.martial[proficiency.sameAs ?? ""];
            proficiency.rank = ((): ZeroToFour => {
                const maxRankIndex = PROFICIENCY_RANKS.indexOf(proficiency.maxRank ?? "legendary");
                return Math.min(category.rank, maxRankIndex) as ZeroToFour;
            })();
        }

        // Deduplicate proficiencies, set proficiency bonuses to all
        const allProficiencies = Object.entries(systemData.martial);
        for (const [_key, proficiency] of allProficiencies) {
            const stringDefinition = "definition" in proficiency ? sortedStringify(proficiency.definition) : null;
            const duplicates = allProficiencies.flatMap(([k, p]) =>
                proficiency !== p &&
                proficiency.rank >= p.rank &&
                "definition" in proficiency &&
                "definition" in p &&
                proficiency.sameAs === p.sameAs &&
                sortedStringify(p.definition) === stringDefinition
                    ? k
                    : []
            );
            for (const duplicate of duplicates) {
                delete systemData.martial[duplicate];
            }

            const proficiencyBonus = ProficiencyModifier.fromLevelAndRank(this.level, proficiency.rank || 0);
            proficiency.value = proficiencyBonus.modifier;
            const sign = proficiencyBonus.modifier < 0 ? "" : "+";
            proficiency.breakdown = `${proficiencyBonus.label} ${sign}${proficiencyBonus.modifier}`;
        }
    }

    /** Toggle the invested state of an owned magical item */
    async toggleInvested(itemId: string): Promise<boolean> {
        const item = this.inventory.get(itemId);
        if (!item?.traits.has("invested")) {
            throw Errorpf2ettw("Unexpected error toggling item investment");
        }

        return !!(await item.update({ "data.equipped.invested": !item.isInvested }));
    }

    /** Add a proficiency in a weapon group or base weapon */
    async addCombatProficiency(key: BaseWeaponProficiencyKey | WeaponGroupProficiencyKey): Promise<void> {
        const currentProficiencies = this.data.data.martial;
        if (key in currentProficiencies) return;
        const newProficiency: CharacterProficiency = { rank: 0, value: 0, breakdown: "", custom: true };
        await this.update({ [`data.martial.${key}`]: newProficiency });
    }

    async removeCombatProficiency(key: BaseWeaponProficiencyKey | WeaponGroupProficiencyKey): Promise<void> {
        await this.update({ [`data.martial.-=${key}`]: null });
    }

    /** Remove any features linked to a to-be-deleted ABC item */
    override async deleteEmbeddedDocuments(
        embeddedName: "ActiveEffect" | "Item",
        ids: string[],
        context: DocumentModificationContext = {}
    ): Promise<ActiveEffectpf2ettw[] | Itempf2ettw[]> {
        if (embeddedName === "Item") {
            const abcItems = [this.ancestry, this.background, this.class].filter(
                (item): item is Embedded<Ancestrypf2ettw | Backgroundpf2ettw | Classpf2ettw> =>
                    !!item && ids.includes(item.id)
            );
            const featureIds = abcItems.flatMap((item) => item.getLinkedFeatures().map((feature) => feature.id));
            ids.push(...featureIds);
        }
        return super.deleteEmbeddedDocuments(embeddedName, [...new Set(ids)], context) as Promise<
            ActiveEffectpf2ettw[] | Itempf2ettw[]
        >;
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    protected override async _preUpdate(
        changed: DeepPartial<CharacterSource>,
        options: DocumentModificationContext<this>,
        user: Userpf2ettw
    ): Promise<void> {
        const systemData = this.data.data;

        // Clamp level, allowing for level-0 variant rule and enough room for homebrew "mythical" campaigns
        const level = changed.data?.details?.level;
        if (level?.value !== undefined) {
            level.value = Math.clamped(Number(level.value) || 0, 0, 30);
        }

        // Clamp Stamina and Resolve
        if (game.settings.get("pf2ettw", "staminaVariant")) {
            // Do not allow stamina to go over max
            if (changed.data?.attributes?.sp) {
                changed.data.attributes.sp.value = Math.clamped(
                    changed.data?.attributes?.sp?.value || 0,
                    0,
                    systemData.attributes.sp.max
                );
            }

            // Do not allow resolve to go over max
            if (changed.data?.attributes?.resolve) {
                changed.data.attributes.resolve.value = Math.clamped(
                    changed.data?.attributes?.resolve?.value || 0,
                    0,
                    systemData.attributes.resolve.max
                );
            }
        }

        // Add or remove class features as necessary
        const newLevel = changed.data?.details?.level?.value ?? this.level;
        if (newLevel !== this.level) {
            await AncestryBackgroundClassManager.ensureClassFeaturesForLevel(this, newLevel);
        }

        // Constrain PFS player and character numbers
        for (const property of ["playerNumber", "characterNumber"] as const) {
            if (typeof changed.data?.pfs?.[property] === "number") {
                const [min, max] = property === "playerNumber" ? [1, 9_999_999] : [2001, 9999];
                changed.data.pfs[property] = Math.clamped(changed.data.pfs[property] || 0, min, max);
            } else if (changed.data?.pfs && changed.data.pfs[property] !== null) {
                changed.data.pfs[property] = this.data.data.pfs[property] ?? null;
            }
        }

        await super._preUpdate(changed, options, user);
    }

    /** Perform heritage and deity deletions prior to the creation of new ones */
    async preCreateDelete(toCreate: PreCreate<ItemSourcepf2ettw>[]): Promise<void> {
        const { itemTypes } = this;
        const singularTypes = ["heritage", "deity"] as const;
        const deletionTypes = singularTypes.filter((t) => toCreate.some((i) => i.type === t));
        const preCreateDeletions = deletionTypes.flatMap((t): Itempf2ettw[] => itemTypes[t]).map((i) => i.id);
        if (preCreateDeletions.length > 0) {
            await this.deleteEmbeddedDocuments("Item", preCreateDeletions, { render: false });
        }
    }

    /** Toggle between boost-driven and manual management of ability scores */
    async toggleAbilityManagement(): Promise<void> {
        if (Object.keys(this.data._source.data.abilities).length === 0) {
            // Add stored ability scores for manual management
            const baseAbilities = Array.from(ABILITY_ABBREVIATIONS).reduce(
                (accumulated: Record<string, { value: 10 }>, abbrev) => ({
                    ...accumulated,
                    [abbrev]: { value: 10 as const },
                }),
                {}
            );
            await this.update({ "data.abilities": baseAbilities });
        } else {
            // Delete stored ability scores for boost-driven management
            const deletions = Array.from(ABILITY_ABBREVIATIONS).reduce(
                (accumulated: Record<string, null>, abbrev) => ({
                    ...accumulated,
                    [`-=${abbrev}`]: null,
                }),
                {}
            );
            await this.update({ "data.abilities": deletions });
        }
    }

    /** Toggle between boost-driven and manual management of ability scores */
    async toggleVoluntaryFlaw(): Promise<void> {
        if (!this.ancestry) return;
        if (Object.keys(this.ancestry?.data._source.data.voluntaryFlaws || []).length === 0) {
            await this.ancestry.update({
                "data.voluntaryBoosts.vb": { value: Array.from(ABILITY_ABBREVIATIONS), selected: null },
                "data.voluntaryFlaws.vf1": { value: Array.from(ABILITY_ABBREVIATIONS), selected: null },
                "data.voluntaryFlaws.vf2": { value: Array.from(ABILITY_ABBREVIATIONS), selected: null },
            });
        } else {
            await this.ancestry.update({
                "data.voluntaryBoosts.-=vb": null,
                "data.voluntaryFlaws.-=vf1": null,
                "data.voluntaryFlaws.-=vf2": null,
            });
        }
    }
}

interface Characterpf2ettw {
    readonly data: CharacterData;

    deleteEmbeddedDocuments(
        embeddedName: "ActiveEffect",
        dataId: string[],
        context?: DocumentModificationContext
    ): Promise<ActiveEffectpf2ettw[]>;
    deleteEmbeddedDocuments(
        embeddedName: "Item",
        dataId: string[],
        context?: DocumentModificationContext
    ): Promise<Itempf2ettw[]>;
    deleteEmbeddedDocuments(
        embeddedName: "ActiveEffect" | "Item",
        dataId: string[],
        context?: DocumentModificationContext
    ): Promise<ActiveEffectpf2ettw[] | Itempf2ettw[]>;
}

export { Characterpf2ettw };
