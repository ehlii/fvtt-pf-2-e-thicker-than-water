import { SkillAbbreviation } from "@actor/creature/data";
import { MODIFIER_TYPE, ProficiencyModifier } from "@actor/modifiers";
import { ActorSheetDatapf2ettw } from "@actor/sheet/data-types";
import { Featpf2ettw, Itempf2ettw, Lorepf2ettw, PhysicalItempf2ettw, SpellcastingEntrypf2ettw } from "@item";
import { AncestryBackgroundClassManager } from "@item/abc/manager";
import { isSpellConsumable } from "@item/consumable/spell-consumables";
import { ItemDatapf2ettw, ItemSourcepf2ettw, LoreData } from "@item/data";
import { isPhysicalData } from "@item/data/helpers";
import { BaseWeaponType, WeaponGroup } from "@item/weapon/types";
import { WEAPON_CATEGORIES } from "@item/weapon/values";
import { restForTheNight } from "@scripts/macros/rest-for-the-night";
import { craft } from "@system/action-macros/crafting/craft";
import { CheckDC } from "@system/degree-of-success";
import { Localizepf2ettw } from "@system/localize";
import { Errorpf2ettw, groupBy, objectHasKey, setHasElement, tupleHasValue } from "@util";
import { Characterpf2ettw } from ".";
import { CreatureSheetpf2ettw } from "../creature/sheet";
import { ManageCombatProficiencies } from "../sheet/popups/manage-combat-proficiencies";
import { CraftingFormula, craftItem, craftSpellConsumable } from "./crafting";
import { CharacterProficiency, CharacterSkillData, CharacterStrike, MartialProficiencies } from "./data";
import { CharacterSheetData, CraftingEntriesSheetData } from "./data/sheet";
import { PCSheetTabManager } from "./tab-manager";
import { AbilityBuilderPopup } from "../sheet/popups/ability-builder";
import { ABILITY_ABBREVIATIONS } from "@actor/values";

class CharacterSheetpf2ettw extends CreatureSheetpf2ettw<Characterpf2ettw> {
    // A cache of this PC's known formulas, for use by sheet callbacks
    private knownFormulas: Record<string, CraftingFormula> = {};

    // Non-persisted tweaks to formula data
    private formulaQuantities: Record<string, number> = {};

    static override get defaultOptions(): ActorSheetOptions {
        return mergeObject(super.defaultOptions, {
            classes: ["default", "sheet", "actor", "character"],
            width: 750,
            height: 850,
            tabs: [
                { navSelector: ".sheet-navigation", contentSelector: ".sheet-content", initial: "character" },
                { navSelector: ".actions-nav", contentSelector: ".actions-panels", initial: "encounter" },
            ],
        });
    }

    override get template(): string {
        const template = this.actor.limited && !game.user.isGM ? "limited" : "sheet";
        return `systems/pf2ettw/templates/actors/character/${template}.html`;
    }

    override async getData(options?: ActorSheetOptions): Promise<CharacterSheetData> {
        const sheetData = (await super.getData(options)) as CharacterSheetData;

        // Martial Proficiencies
        const proficiencies = Object.entries(sheetData.data.martial);
        for (const [key, proficiency] of proficiencies) {
            const groupMatch = /^weapon-group-([-\w]+)$/.exec(key);
            const baseWeaponMatch = /^weapon-base-([-\w]+)$/.exec(key);
            const label = ((): string => {
                if (objectHasKey(CONFIG.pf2ettw.martialSkills, key)) {
                    return CONFIG.pf2ettw.martialSkills[key];
                }
                if (objectHasKey(CONFIG.pf2ettw.weaponCategories, key)) {
                    return CONFIG.pf2ettw.weaponCategories[key];
                }
                if (Array.isArray(groupMatch)) {
                    const weaponGroup = groupMatch[1] as WeaponGroup;
                    return CONFIG.pf2ettw.weaponGroups[weaponGroup];
                }
                if (Array.isArray(baseWeaponMatch)) {
                    const baseWeapon = baseWeaponMatch[1] as BaseWeaponType;
                    return Localizepf2ettw.translations.pf2ettw.Weapon.Base[baseWeapon];
                }
                return proficiency.label ?? key;
            })();

            proficiency.label = game.i18n.localize(label);
            proficiency.value = ProficiencyModifier.fromLevelAndRank(
                sheetData.data.details.level.value,
                proficiency.rank || 0
            ).modifier;
        }

        // A(H)BCD
        sheetData.ancestry = this.actor.ancestry;
        sheetData.heritage = this.actor.heritage;
        sheetData.background = this.actor.background;
        sheetData.class = this.actor.class;
        sheetData.deity = this.actor.deity;

        // Update hero points label
        sheetData.data.resources.heroPoints.icon = this.getHeroPointsIcon(sheetData.data.resources.heroPoints.value);
        sheetData.data.resources.heroPoints.hover = game.i18n.format(
            this.actor.heroPoints.value === 1 ? "pf2ettw.HeroPointRatio.One" : "pf2ettw.HeroPointRatio.Many",
            this.actor.heroPoints
        );

        // Update class DC label
        sheetData.data.attributes.classDC.icon = this.getProficiencyIcon(sheetData.data.attributes.classDC.rank);
        sheetData.data.attributes.classDC.hover =
            CONFIG.pf2ettw.proficiencyLevels[sheetData.data.attributes.classDC.rank];

        // Spell Details
        sheetData.magicTraditions = CONFIG.pf2ettw.magicTraditions;
        sheetData.preparationType = CONFIG.pf2ettw.preparationType;

        // Update thirst
        sheetData.data.attributes.thirst.icon = this.getThirstIcon(sheetData.data.attributes.thirst.value);

        // preparing the name of the rank, as this is displayed on the sheet
        sheetData.data.attributes.perception.rankName = game.i18n.format(
            `pf2ettw.ProficiencyLevel${sheetData.data.attributes.perception.rank}`
        );

        // ensure saves are displayed in the following order:
        sheetData.data.saves = {
            fortitude: sheetData.data.saves.fortitude,
            reflex: sheetData.data.saves.reflex,
            will: sheetData.data.saves.will,
        };
        for (const save of Object.values(sheetData.data.saves as Record<any, any>)) {
            save.rankName = game.i18n.format(`pf2ettw.ProficiencyLevel${save.rank}`);
        }
        sheetData.data.attributes.classDC.rankName = game.i18n.format(
            `pf2ettw.ProficiencyLevel${sheetData.data.attributes.classDC.rank}`
        );
        sheetData.data.attributes.vampireDC.rankName = game.i18n.format(
            `pf2ettw.ProficiencyLevel${sheetData.data.attributes.vampireDC.rank}`
        );

        // limiting the amount of characters for the save labels
        for (const save of Object.values(sheetData.data.saves as Record<any, any>)) {
            save.short = game.i18n.format(`pf2ettw.Saves${save.label}Short`);
        }

        // Is the character's key ability score overridden by an Active Effect?
        sheetData.data.details.keyability.singleOption = this.actor.class?.data.data.keyAbility.value.length === 1;

        // Is the stamina variant rule enabled?
        sheetData.hasStamina = game.settings.get("pf2ettw", "staminaVariant") > 0;

        this.prepareSpellcasting(sheetData);

        const formulasByLevel = await this.prepareCraftingFormulas();
        const flags = this.actor.data.flags.pf2ettw;
        const hasQuickAlchemy = !!this.actor.rollOptions.all["feature:quick-alchemy"];
        const useQuickAlchemy = hasQuickAlchemy && flags.quickAlchemy;

        sheetData.crafting = {
            noCost: flags.freeCrafting || useQuickAlchemy,
            hasQuickAlchemy,
            knownFormulas: formulasByLevel,
            entries: await this.prepareCraftingEntries(),
        };

        this.knownFormulas = Object.values(formulasByLevel)
            .flat()
            .reduce((result: Record<string, CraftingFormula>, entry) => {
                entry.batchSize = this.formulaQuantities[entry.uuid] ?? entry.batchSize;
                result[entry.uuid] = entry;
                return result;
            }, {});

        sheetData.abpEnabled = game.settings.get("pf2ettw", "automaticBonusVariant") !== "noABP";

        // Sort attack/defense proficiencies
        const combatProficiencies: MartialProficiencies = sheetData.data.martial;

        const isWeaponProficiency = (key: string): boolean =>
            setHasElement(WEAPON_CATEGORIES, key) || /\bweapon\b/.test(key);
        sheetData.data.martial = Object.entries(combatProficiencies)
            .sort(([keyA, valueA], [keyB, valueB]) =>
                isWeaponProficiency(keyA) && !isWeaponProficiency(keyB)
                    ? -1
                    : !isWeaponProficiency(keyA) && isWeaponProficiency(keyB)
                    ? 1
                    : (valueA.label ?? "").localeCompare(valueB.label ?? "")
            )
            .reduce(
                (proficiencies: Record<string, CharacterProficiency>, [key, proficiency]) => ({
                    ...proficiencies,
                    [key]: proficiency,
                }),
                {}
            ) as MartialProficiencies;

        // Sort skills by localized label
        sheetData.data.skills = Object.fromEntries(
            Object.entries(sheetData.data.skills).sort(([_keyA, skillA], [_keyB, skillB]) =>
                game.i18n
                    .localize(skillA.label ?? "")
                    .localeCompare(game.i18n.localize(skillB.label ?? ""), game.i18n.lang)
            )
        ) as Record<SkillAbbreviation, CharacterSkillData>;

        // show hints for some things being modified
        const baseData = this.actor.toObject();
        sheetData.adjustedBonusEncumbranceBulk =
            this.actor.attributes.bonusEncumbranceBulk !== baseData.data.attributes.bonusEncumbranceBulk;
        sheetData.adjustedBonusLimitBulk =
            this.actor.attributes.bonusLimitBulk !== baseData.data.attributes.bonusLimitBulk;

        sheetData.tabVisibility = deepClone(this.actor.data.flags.pf2ettw.sheetTabs);

        // Return data for rendering
        return sheetData;
    }

    /** Organize and classify Items for Character sheets */
    protected prepareItems(sheetData: ActorSheetDatapf2ettw<Characterpf2ettw>): void {
        const actorData = sheetData.actor;

        // Actions
        const actions: Record<string, { label: string; actions: any[] }> = {
            action: { label: game.i18n.localize("pf2ettw.ActionsActionsHeader"), actions: [] },
            reaction: { label: game.i18n.localize("pf2ettw.ActionsReactionsHeader"), actions: [] },
            free: { label: game.i18n.localize("pf2ettw.ActionsFreeActionsHeader"), actions: [] },
        };

        const readonlyEquipment: unknown[] = [];

        // Skills
        const lores: LoreData[] = [];

        for (const itemData of sheetData.items) {
            const physicalData: ItemDatapf2ettw = itemData;
            const item = this.actor.items.get(itemData._id, { strict: true });
            if (item instanceof PhysicalItempf2ettw && isPhysicalData(physicalData)) {
                const { isEquipped, isIdentified, isInvested, isTemporary } = item;
                itemData.isEquipped = isEquipped;
                itemData.isIdentified = isIdentified;
                itemData.isInvested = isInvested;
                itemData.isTemporary = isTemporary;
                itemData.showEdit = sheetData.user.isGM || isIdentified;
                itemData.img ||= CONST.DEFAULT_TOKEN;
                itemData.assetValue = item.assetValue;
                itemData.isInvestable = isEquipped && isIdentified && isInvested !== null;

                // Read-Only Equipment
                if (
                    physicalData.type === "armor" ||
                    physicalData.type === "equipment" ||
                    physicalData.type === "consumable" ||
                    physicalData.type === "backpack"
                ) {
                    readonlyEquipment.push(itemData);
                    actorData.hasEquipment = true;
                }
            }

            // Feats
            else if (itemData.type === "feat") {
                const actionType = itemData.data.actionType.value || "passive";
                if (Object.keys(actions).includes(actionType)) {
                    itemData.feat = true;
                    itemData.img = Characterpf2ettw.getActionGraphics(
                        actionType,
                        parseInt((itemData.data.actions || {}).value, 10) || 1
                    ).imageUrl;
                    actions[actionType].actions.push(itemData);
                }
            }

            // Lore Skills
            else if (itemData.type === "lore") {
                itemData.data.icon = this.getProficiencyIcon((itemData.data.proficient || {}).value);
                itemData.data.hover = CONFIG.pf2ettw.proficiencyLevels[(itemData.data.proficient || {}).value];

                const rank = itemData.data.proficient?.value || 0;
                const proficiency = ProficiencyModifier.fromLevelAndRank(
                    actorData.data.details.level.value,
                    rank
                ).modifier;
                const modifier = actorData.data.abilities.int.mod;
                const itemBonus = Number((itemData.data.item || {}).value || 0);
                itemData.data.itemBonus = itemBonus;
                itemData.data.value = modifier + proficiency + itemBonus;
                itemData.data.breakdown = `int modifier(${modifier}) + proficiency(${proficiency}) + item bonus(${itemBonus})`;

                lores.push(itemData);
            }

            // Actions
            else if (itemData.type === "action") {
                const actionType = ["free", "reaction", "passive"].includes(itemData.data.actionType.value)
                    ? itemData.data.actionType.value
                    : "action";
                itemData.img = Characterpf2ettw.getActionGraphics(
                    actionType,
                    parseInt((itemData.data.actions || {}).value, 10) || 1
                ).imageUrl;
                if (actionType === "passive") actions.free.actions.push(itemData);
                else actions[actionType].actions.push(itemData);
            }
        }

        // assign mode to actions
        Object.values(actions)
            .flatMap((section) => section.actions)
            .forEach((action: any) => {
                action.downtime = action.data.traits.value.includes("downtime");
                action.exploration = action.data.traits.value.includes("exploration");
                action.encounter = !(action.downtime || action.exploration);
            });

        // Assign and return
        actorData.featSlots = this.actor.featGroups;
        actorData.pfsBoons = this.actor.pfsBoons;
        actorData.deityBoonsCurses = this.actor.deityBoonsCurses;
        actorData.actions = actions;
        actorData.readonlyEquipment = readonlyEquipment;
        actorData.lores = lores;
    }

    private prepareSpellcasting(sheetData: CharacterSheetData): void {
        sheetData.spellcastingEntries = [];
        for (const itemData of sheetData.items) {
            if (itemData.type === "spellcastingEntry") {
                const entry = this.actor.spellcasting.get(itemData._id);
                if (!(entry instanceof SpellcastingEntrypf2ettw)) continue;
                sheetData.spellcastingEntries.push({
                    ...itemData,
                    ...entry.getSpellData(),
                });
            }
        }
    }

    protected async prepareCraftingFormulas(): Promise<Record<number, CraftingFormula[]>> {
        const craftingFormulas = await this.actor.getCraftingFormulas();
        return Object.fromEntries(groupBy(craftingFormulas, (formula) => formula.level));
    }

    protected async prepareCraftingEntries(): Promise<CraftingEntriesSheetData> {
        const actorCraftingEntries = await this.actor.getCraftingEntries();
        const craftingEntries: CraftingEntriesSheetData = {
            dailyCrafting: false,
            other: [],
            alchemical: {
                entries: [],
                totalReagentCost: 0,
                infusedReagents: this.actor.data.data.resources.crafting.infusedReagents,
            },
        };

        for (const entry of actorCraftingEntries) {
            if (entry.isAlchemical) {
                craftingEntries.alchemical.entries.push(entry);
                craftingEntries.alchemical.totalReagentCost += entry.reagentCost || 0;
                craftingEntries.dailyCrafting = true;
            } else {
                craftingEntries.other.push(entry);
                if (entry.isDailyPrep) craftingEntries.dailyCrafting = true;
            }
        }

        return craftingEntries;
    }

    /** Disable the initiative button located on the sidebar */
    disableInitiativeButton(): void {
        this.element
            .find(".sidebar a.roll-init")
            .addClass("disabled")
            .attr({ title: game.i18n.localize("pf2ettw.Encounter.NoActiveEncounter") });
    }

    /** Enable the initiative button located on the sidebar */
    enableInitiativeButton(): void {
        this.element.find(".sidebar a.roll-init").removeClass("disabled").removeAttr("title");
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        // Initiative button
        if (game.combat) {
            this.enableInitiativeButton();
        } else {
            this.disableInitiativeButton();
        }

        // Recheck for the presence of an encounter in case the button state has somehow fallen out of sync
        $html.find(".roll-init").on("mouseenter", (event) => {
            const $target = $(event.currentTarget);
            if ($target.hasClass("disabled") && game.combat) {
                this.enableInitiativeButton();
            } else if (!$target.hasClass("disabled") && !game.combat) {
                this.disableInitiativeButton();
            }
        });

        // Left/right-click adjustments (increment or decrement) of actor and item stats
        $html.find(".adjust-stat").on("click contextmenu", (event) => this.onClickAdjustStat(event));
        $html
            .find(".adjust-stat-save-ability")
            .on("click contextmenu", (event) => this.onClickAdjustSaveAbility(event));
        $html
            .find(".adjust-vampiredc-save-ability")
            .on("click contextmenu", (event) => this.onClickAdjustVampireDCAbility(event));
        $html.find(".adjust-stat-select").on("change", (event) => this.onChangeAdjustStat(event));
        $html.find(".adjust-item-stat").on("click contextmenu", (event) => this.onClickAdjustItemStat(event));
        $html.find(".adjust-item-stat-select").on("change", (event) => this.onChangeAdjustItemStat(event));

        {
            // ensure correct tab name is displayed after actor update
            const title = $(".sheet-navigation .active").attr("title");
            if (title) {
                $html.find(".navigation-title").text(title);
            }
        }

        $html.find(".sheet-navigation").on("mouseover", ".item,.manage-tabs", (event) => {
            const title = event.currentTarget.title;
            if (title) {
                $(event.currentTarget).parents(".sheet-navigation").find(".navigation-title").text(title);
            }
        });

        $html.find(".sheet-navigation").on("mouseout", ".item,.manage-tabs", (event) => {
            const parent = $(event.currentTarget).parents(".sheet-navigation");
            const title = parent.find(".item.active").attr("title");
            if (title) {
                parent.find(".navigation-title").text(title);
            }
        });

        // open ancestry, background, or class compendium
        $html.find(".open-compendium").on("click", (event) => {
            if (event.currentTarget.dataset.compendium) {
                const compendium = game.packs.get(event.currentTarget.dataset.compendium);
                if (compendium) {
                    compendium.render(true);
                }
            }
        });

        $html.find(".crb-trait-selector").on("click", (event) => this.onTraitSelector(event));

        // ACTIONS
        const $actions = $html.find(".tab.actions");

        // Filter strikes
        $actions.find(".toggle-unready-strikes").on("click", () => {
            this.actor.setFlag("pf2ettw", "showUnreadyStrikes", !this.actor.data.flags.pf2ettw.showUnreadyStrikes);
        });

        const $strikesList = $actions.find(".strikes-list");

        // Set damage-formula tooltips on damage buttons
        const damageButtonSelectors = [
            'button[data-action="strike-damage"]',
            'button[data-action="strike-critical"]',
        ].join(", ");
        const $damageButtons = $strikesList.find<HTMLButtonElement>(damageButtonSelectors);
        for (const damageButton of $damageButtons) {
            const $button = $(damageButton);
            const method = $button.attr("data-action") === "strike-damage" ? "damage" : "critical";
            const altUsage = tupleHasValue(["thrown", "melee"] as const, damageButton.dataset.altUsage)
                ? damageButton.dataset.altUsage
                : null;

            const strike = this.getStrikeFromDOM($button[0]);
            strike?.[method]?.({ getFormula: true, altUsage }).then((formula) => {
                if (!formula) return;
                $button.attr({ title: formula });
                $button.tooltipster({
                    position: "bottom",
                    theme: "crb-hover",
                });
            });
        }

        $strikesList.find(".item-summary .item-properties.tags .tag").each((_idx, span) => {
            if (span.dataset.description) {
                $(span).tooltipster({
                    content: game.i18n.localize(span.dataset.description),
                    maxWidth: 400,
                    theme: "crb-hover",
                });
            }
        });

        const auxiliaryActionSelector = 'button[data-action="auxiliary-action"]';
        $strikesList.find(auxiliaryActionSelector).on("click", (event) => {
            const auxiliaryActionIndex = $(event.currentTarget)
                .closest("[data-auxiliary-action-index]")
                .attr("data-auxiliary-action-index");

            const strike = this.getStrikeFromDOM(event.currentTarget);
            strike?.auxiliaryActions?.[Number(auxiliaryActionIndex)]?.execute();
        });

        $strikesList.find(".melee-icon").tooltipster({
            content: game.i18n.localize("pf2ettw.Item.Weapon.MeleeUsage.Label"),
            position: "left",
            theme: "crb-hover",
        });

        $strikesList.find('select[name="ammo-used"]').on("change", (event) => {
            event.stopPropagation();

            const actionIndex = $(event.currentTarget).parents(".item").attr("data-action-index");
            const action = this.actor.data.data.actions[Number(actionIndex)];
            const weapon = this.actor.items.get(action.item?.id ?? "");
            const ammo = this.actor.items.get($(event.currentTarget).val() as string);

            if (weapon) weapon.update({ data: { selectedAmmoId: ammo?.id ?? null } });
        });

        $html.find(".add-modifier .fas.fa-plus-circle").on("click", (event) => this.onIncrementModifierValue(event));
        $html.find(".add-modifier .fas.fa-minus-circle").on("click", (event) => this.onDecrementModifierValue(event));
        $html.find(".add-modifier .add-modifier-submit").on("click", (event) => this.onAddCustomModifier(event));
        $html.find(".modifier-list .remove-modifier").on("click", (event) => this.onRemoveCustomModifier(event));

        // Toggle invested state
        $html.find(".item-toggle-invest").on("click", (event) => {
            const f = $(event.currentTarget);
            const itemId = f.parents(".item").attr("data-item-id") ?? "";
            this.actor.toggleInvested(itemId);
        });

        $html.find("i.fa-info-circle.small[title]").tooltipster({
            maxWidth: 275,
            position: "right",
            theme: "crb-hover",
            contentAsHTML: true,
        });

        {
            // Add and remove combat proficiencies
            const $tab = $html.find(".tab.proficiencies");
            const $header = $tab.find("ol.combat-proficiencies");
            $header.find("a.add").on("click", (event) => {
                ManageCombatProficiencies.add(this.actor, event);
            });
            const $list = $tab.find("ol.combat-list");
            $list.find("li.skill.custom a.delete").on("click", (event) => {
                ManageCombatProficiencies.remove(this.actor, event);
            });
        }

        $html.find(".hover").tooltipster({
            trigger: "click",
            arrow: false,
            contentAsHTML: true,
            debug: BUILD_MODE === "development",
            interactive: true,
            side: ["right", "bottom"],
            theme: "crb-hover",
            minWidth: 120,
        });

        $html
            .find("a[data-action=rest]")
            .tooltipster({ theme: "crb-hover" })
            .on("click", (event) => {
                restForTheNight({ event, actors: this.actor });
            });

        $html.find("a[data-action=perception-check]").tooltipster({ theme: "crb-hover" });

        $html.find("button[data-action=edit-ability-scores]").on("click", async () => {
            await new AbilityBuilderPopup(this.actor).render(true);
        });

        // Toggle Thirst
        $html.find(".char-header > .char-details > .dotcontainer > .dots.thirst").on("click contextmenu", (event) => {
            type ConditionName = "thirst";
            const condition = Array.from(event.delegateTarget.classList).find((className): className is ConditionName =>
                ["thirst"].includes(className)
            );
            if (condition) {
                this.onClickThirst(condition, event);
            }
        });

        const $craftingTab = $html.find(".tab.crafting");

        const $craftingOptions = $craftingTab.find(".crafting-options input:checkbox");
        $craftingOptions.on("click", async (event) => {
            const flags: string[] = [];
            $craftingOptions.each((_index, element) => {
                if (element !== event.target) {
                    flags.push($(element).attr("flag") as string);
                }
            });
            flags.forEach(async (flag) => {
                await this.actor.setFlag("pf2ettw", flag, false);
            });
        });

        $craftingTab.find("a[data-action=quick-add]").on("click", async (event) => {
            const { itemUuid } = event.currentTarget.dataset;
            const craftingFormulas = await this.actor.getCraftingFormulas();
            const formula = craftingFormulas.find((f) => f.uuid === itemUuid);
            if (!formula) return;

            const entries = (await this.actor.getCraftingEntries()).filter(
                (e) => !!e.selector && e.checkEntryRequirements(formula, { warn: false })
            );
            for (const entry of entries) {
                await entry.prepareFormula(formula);
            }

            if (entries.length === 0) {
                ui.notifications.warn(game.i18n.localize("pf2ettw.CraftingTab.NoEligibleEntry"));
            }
        });

        const $formulas = $craftingTab.find(".craftingEntry-list");

        $formulas.find("a[data-action=craft-item]").on("click", async (event) => {
            const { itemUuid } = event.currentTarget.dataset;
            const itemQuantity =
                Number($(event.currentTarget).parent().siblings(".formula-quantity").children("input").val()) || 1;
            const formula = this.knownFormulas[itemUuid ?? ""];
            if (!formula) return;

            if (this.actor.data.flags.pf2ettw.quickAlchemy) {
                const reagentValue = this.actor.data.data.resources.crafting.infusedReagents.value - itemQuantity;
                if (reagentValue < 0) {
                    ui.notifications.warn(game.i18n.localize("pf2ettw.CraftingTab.Alerts.MissingReagents"));
                    return;
                }
                await this.actor.update({ "data.resources.crafting.infusedReagents.value": reagentValue });

                return craftItem(formula.item, itemQuantity, this.actor, true);
            }

            if (this.actor.data.flags.pf2ettw.freeCrafting) {
                const itemId = itemUuid?.split(".").pop() ?? "";
                if (isSpellConsumable(itemId) && formula.item.isOfType("consumable")) {
                    return craftSpellConsumable(formula.item, itemQuantity, this.actor);
                }

                return craftItem(formula.item, itemQuantity, this.actor);
            }

            const difficultyClass: CheckDC = {
                value: formula.dc,
                visibility: "all",
                adjustments: this.actor.data.data.skills.cra.adjustments,
                scope: "check",
            };

            craft({ difficultyClass, item: formula.item, quantity: itemQuantity, event, actors: this.actor });
        });

        $formulas.find("[data-action=enter-quantity]").on("change", async (event) => {
            const $target = $(event.currentTarget);
            const itemUUID = $target.closest("li.formula-item").attr("data-item-id");
            const entrySelector = $target.closest("li.crafting-entry").attr("data-entry-selector");
            if (entrySelector) {
                const craftingEntry = await this.actor.getCraftingEntry(entrySelector);
                if (!craftingEntry) throw Errorpf2ettw("Crafting entry not found");

                const index = $target.closest("li.formula-item").attr("data-item-index");
                await craftingEntry.setFormulaQuantity(Number(index), itemUUID ?? "", Number($target.val()));
                return;
            }

            const formula = this.knownFormulas[itemUUID ?? ""];
            if (!formula) throw Errorpf2ettw("Formula not found");
            this.formulaQuantities[formula.uuid] = Math.max(formula.minimumBatchSize, Number($target.val()));
            this.render(true);
        });

        $formulas
            .find("[data-action=increase-quantity], [data-action=decrease-quantity]")
            .on("click", async (event) => {
                const $target = $(event.currentTarget);

                const itemUUID = $target.closest("li.formula-item").attr("data-item-id");
                const entrySelector = $target.closest("li.crafting-entry").attr("data-entry-selector");
                if (entrySelector) {
                    const craftingEntry = await this.actor.getCraftingEntry(entrySelector);
                    if (!craftingEntry) throw Errorpf2ettw("Crafting entry not found");
                    const index = $target.closest("li.formula-item").attr("data-item-index");
                    $target.attr("data-action") === "increase-quantity"
                        ? await craftingEntry.increaseFormulaQuantity(Number(index), itemUUID ?? "")
                        : await craftingEntry.decreaseFormulaQuantity(Number(index), itemUUID ?? "");
                    return;
                }

                const formula = this.knownFormulas[itemUUID ?? ""];
                if (!formula) throw Errorpf2ettw("Formula not found");

                const minBatchSize = formula.minimumBatchSize;
                const step = $target.attr("data-action") === "increase-quantity" ? minBatchSize : -minBatchSize;
                const newValue = (Number($target.siblings("input").val()) || step) + step;
                this.formulaQuantities[formula.uuid] = Math.max(newValue, minBatchSize);
                this.render();
            });

        $formulas.find(".formula-unprepare").on("click", async (event) => {
            const $target = $(event.currentTarget);
            const itemUUID = $target.closest("li.formula-item").attr("data-item-id");
            const index = $target.closest("li.formula-item").attr("data-item-index");
            const entrySelector = $target.closest("li.crafting-entry").attr("data-entry-selector");

            if (!itemUUID || !index || !entrySelector) return;

            const craftingEntry = await this.actor.getCraftingEntry(entrySelector);
            if (!craftingEntry) throw Errorpf2ettw("Crafting entry not found");
            await craftingEntry.unprepareFormula(Number(index), itemUUID);
        });

        $formulas.find(".toggle-formula-expended").on("click", async (event) => {
            const $target = $(event.currentTarget);
            const itemUUID = $target.closest("li.formula-item").attr("data-item-id");
            const index = $target.closest("li.formula-item").attr("data-item-index");
            const entrySelector = $target.closest("li.crafting-entry").attr("data-entry-selector");

            if (!itemUUID || !index || !entrySelector) return;

            const craftingEntry = await this.actor.getCraftingEntry(entrySelector);
            if (!craftingEntry) throw Errorpf2ettw("Crafting entry not found");
            await craftingEntry.toggleFormulaExpended(Number(index), itemUUID);
        });

        $formulas.find(".toggle-signature-item").on("click", async (event) => {
            const $target = $(event.currentTarget);
            const itemUUID = $target.closest("li.formula-item").attr("data-item-id");
            const index = $target.closest("li.formula-item").attr("data-item-index");
            const entrySelector = $target.closest("li.crafting-entry").attr("data-entry-selector");

            if (!itemUUID || !index || !entrySelector) return;

            const craftingEntry = await this.actor.getCraftingEntry(entrySelector);
            if (!craftingEntry) throw Errorpf2ettw("Crafting entry not found");
            await craftingEntry.toggleSignatureItem(Number(index), itemUUID);
        });

        $formulas.find(".infused-reagents").on("change", (event) => {
            const change = Number($(event.target).val());
            const infusedReagents = this.actor.data.data.resources.crafting.infusedReagents;
            const value = Math.clamped(change, 0, infusedReagents?.max ?? 0);
            this.actor.update({ "data.resources.crafting.infusedReagents.value": value });
        });

        $formulas.find(".daily-crafting").on("click", async () => await this.actor.performDailyCrafting());

        PCSheetTabManager.initialize(this.actor, $html.find<HTMLAnchorElement>('a[data-action="manage-tabs"]')[0]);

        // Feat Browser shortcut links
        $html.find(".feat-browse").on("click", (event) => this.onClickBrowseFeatCompendia(event));
    }

    /** Contextually search the feats tab of the Compendium Browser */
    private async onClickBrowseFeatCompendia(event: JQuery.ClickEvent): Promise<void> {
        const maxLevel = Number($(event.currentTarget).attr("data-level")) || this.actor.level;
        const button: HTMLElement = event.currentTarget;
        const filter = button.dataset.filter?.split(",").filter((f) => !!f) ?? [];
        if (filter.includes("feattype-general")) filter.push("feattype-skill");

        await game.pf2ettw.compendiumBrowser.openTab("feat", filter, maxLevel);
    }

    /** Handle changing of proficiency-rank via dropdown */
    private async onChangeAdjustStat(event: JQuery.TriggeredEvent<HTMLElement>): Promise<void> {
        const $select = $(event.delegateTarget);
        const propertyKey = $select.attr("data-property") ?? "";
        const currentValue = getProperty(this.actor.data, propertyKey);
        const selectedValue = Number($select.val());

        if (typeof currentValue !== "number") throw Errorpf2ettw("Actor property not found");

        const newValue = Math.clamped(selectedValue, 0, 4);

        await this.actor.update({ [propertyKey]: newValue });
        if (newValue !== getProperty(this.actor.data, propertyKey)) {
            ui.notifications.warn(game.i18n.localize("pf2ettw.ErrorMessage.MinimumProfLevelSetByFeatures"));
        }
    }

    /** Handle clicking of proficiency-rank adjustment buttons */
    private async onClickAdjustStat(event: JQuery.TriggeredEvent<HTMLElement>): Promise<void> {
        const $button = $(event.delegateTarget);
        const propertyKey = $button.attr("data-property") ?? "";
        const currentValue = getProperty(this.actor.data, propertyKey);

        if (typeof currentValue !== "number") throw Errorpf2ettw("Actor property not found");

        const change = event.type === "click" ? 1 : -1;
        const max = propertyKey.includes("heroPoints") ? 3 : 4;
        const update = currentValue + change;
        const newValue = Math.clamped(update, 0, max);

        await this.actor.update({ [propertyKey]: newValue });
    }

    /** Handle clicking of save-ability adjustment buttons */
    /** TODO: find a way to make this far less messy */
    private async onClickAdjustSaveAbility(event: JQuery.TriggeredEvent<HTMLElement>): Promise<void> {
        const $button = $(event.delegateTarget);
        const propertyKey = $button.attr("data-property") ?? "";
        const currentValue = $button.attr("value") ?? "";
        const saveToToggle = $button.parent().attr("data-save") ?? "";

        const saves = getProperty(this.actor.data, propertyKey);
        const saveOptions = saves[saveToToggle]["abilityOptions"];
        const newValue = saveOptions.filter((ability: string) => ability !== currentValue).toString();
        saves[saveToToggle]["ability"] = newValue;

        await this.actor.update({ [propertyKey]: saves });
    }

    /** Handle clicking on vampire DC ability adjustment button */
    /** TODO: a lot of the same caveats as the save ability code, all of this is awful */
    private async onClickAdjustVampireDCAbility(event: JQuery.TriggeredEvent<HTMLElement>): Promise<void> {
        const abilities = [...ABILITY_ABBREVIATIONS] as const;

        const $button = $(event.delegateTarget);
        const propertyKey = $button.attr("data-property") ?? "";
        const currentValue: typeof abilities[number] = getProperty(this.actor.data, propertyKey);
        let newIndex =
            event.type === "click" ? abilities.indexOf(currentValue) + 1 : abilities.indexOf(currentValue) - 1;
        newIndex = ((newIndex % abilities.length) + abilities.length) % abilities.length; // Deal with Javascript % being a remainder and not a mod function, causing issues with -ve numbers
        const newValue = abilities[newIndex];

        await this.actor.update({ [propertyKey]: newValue });
    }

    /** Handle changing of lore and spellcasting entry proficiency-rank via dropdown */
    private async onChangeAdjustItemStat(event: JQuery.TriggeredEvent<HTMLElement>): Promise<void> {
        const $select = $(event.delegateTarget);
        const propertyKey = $select.attr("data-item-property") ?? "";
        const selectedValue = Number($select.val());

        const itemId = $select.closest(".item").attr("data-item-id") ?? "";
        const item = this.actor.items.get(itemId);
        if (!item) throw Errorpf2ettw("Item not found");

        // Retrieve and validate the updated value
        const newValue = ((): number | undefined => {
            if (item instanceof SpellcastingEntrypf2ettw) {
                const dispatch: Record<string, () => number> = {
                    "data.proficiency.value": () => Math.clamped(selectedValue, 0, 4),
                };
                return dispatch[propertyKey]?.();
            } else if (item instanceof Lorepf2ettw) {
                return Math.clamped(selectedValue, 0, 4);
            } else {
                throw Errorpf2ettw("Item not recognized");
            }
        })();

        if (typeof newValue === "number") {
            await item.update({ [propertyKey]: newValue });
        }
        if (newValue !== getProperty(item.data, propertyKey)) {
            ui.notifications.warn(game.i18n.localize("pf2ettw.ErrorMessage.MinimumProfLevelSetByFeatures"));
        }
    }

    /** Handle clicking of lore and spellcasting entry adjustment buttons */
    private async onClickAdjustItemStat(event: JQuery.TriggeredEvent<HTMLElement>): Promise<void> {
        const $button = $(event.delegateTarget);
        const itemId = $button.closest(".item").attr("data-item-id") ?? "";
        const item = this.actor.items.get(itemId);
        if (!item) throw Errorpf2ettw("Item not found");

        const propertyKey = $button.attr("data-item-property") ?? "";
        const change = event.type === "click" ? 1 : -1;

        // Retrieve and validate the updated value
        const newValue = ((): number | undefined => {
            if (item instanceof SpellcastingEntrypf2ettw) {
                const proficiencyRank = item.data.data.proficiency.value;
                const dispatch: Record<string, () => number> = {
                    "data.proficiency.value": () => Math.clamped(proficiencyRank + change, 0, 4),
                };
                return dispatch[propertyKey]?.();
            } else if (item instanceof Lorepf2ettw) {
                const currentRank = item.data.data.proficient.value;
                return Math.clamped(currentRank + change, 0, 4);
            } else {
                throw Errorpf2ettw("Item not recognized");
            }
        })();

        if (typeof newValue === "number") {
            await item.update({ [propertyKey]: newValue });
        }
    }

    private onIncrementModifierValue(event: JQuery.ClickEvent): void {
        const parent = $(event.currentTarget).parents(".add-modifier");
        (parent.find(".add-modifier-value input[type=number]")[0] as HTMLInputElement).stepUp();
    }

    private onDecrementModifierValue(event: JQuery.ClickEvent): void {
        const parent = $(event.currentTarget).parents(".add-modifier");
        (parent.find(".add-modifier-value input[type=number]")[0] as HTMLInputElement).stepDown();
    }

    private onAddCustomModifier(event: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement>): void {
        const parent = $(event.currentTarget).parents(".add-modifier");
        const stat = $(event.currentTarget).attr("data-stat") ?? "";
        const modifier = Number(parent.find(".add-modifier-value input[type=number]").val()) || 1;
        const type = parent.find<HTMLSelectElement>(".add-modifier-type")[0]?.value ?? "";
        const name =
            (parent.find<HTMLInputElement>(".add-modifier-name")[0]?.value ?? "").trim() ||
            game.i18n.localize(`pf2ettw.ModifierType.${type}`);
        const errors: string[] = [];
        if (!stat.trim()) {
            // This is a UI error rather than a user error
            throw Errorpf2ettw("No character attribute found");
        }
        const modifierTypes: string[] = Object.values(MODIFIER_TYPE);
        if (!modifierTypes.includes(type)) {
            errors.push("Type is required.");
        }
        if (errors.length > 0) {
            ui.notifications.error(errors.join(" "));
        } else {
            this.actor.addCustomModifier(stat, name, modifier, type);
        }
    }

    private onRemoveCustomModifier(event: JQuery.ClickEvent): void {
        const stat = $(event.currentTarget).attr("data-stat") ?? "";
        const slug = $(event.currentTarget).attr("data-slug") ?? "";
        const errors: string[] = [];
        if (!stat.trim()) {
            errors.push("Statistic is required.");
        }
        if (!slug.trim()) {
            errors.push("Slug is required.");
        }
        if (errors.length > 0) {
            ui.notifications.error(errors.join(" "));
        } else {
            this.actor.removeCustomModifier(stat, slug);
        }
    }

    /** Handle cycling of thirst */
    private onClickThirst(condition: "thirst", event: JQuery.TriggeredEvent) {
        if (event.type === "click") {
            this.actor.increaseCondition(condition, { max: this.actor.data.data.attributes[condition].max });
        } else if (event.type === "contextmenu") {
            this.actor.decreaseCondition(condition);
        }
    }

    private getNearestSlotId(event: ElementDragEvent): JQuery.PlainObject {
        const data = $(event.target).closest("[data-slot-id]").data();
        if (!data) {
            return { slotId: undefined, featType: undefined };
        }
        return data;
    }

    protected override async _onDropItem(
        event: ElementDragEvent,
        data: DropCanvasData<"Item", Itempf2ettw>
    ): Promise<Itempf2ettw[]> {
        const actor = this.actor;
        const isSameActor = data.actorId === actor.id || (actor.isToken && data.tokenId === actor.token?.id);
        if (isSameActor) return super._onDropItem(event, data);

        const item = await Itempf2ettw.fromDropData(data);
        if (!item) throw Errorpf2ettw("Unable to create item from drop data!");

        if (item instanceof Featpf2ettw) {
            const { slotId, featType }: { slotId?: string; featType?: string } = this.getNearestSlotId(event);
            const results = await this.actor.insertFeat(item, featType ?? "", slotId ?? "");
            if (results.length > 0) {
                return results;
            } else {
                return super._onDropItem(event, data);
            }
        }

        const source = item.toObject();

        switch (source.type) {
            case "ancestry":
            case "background":
            case "class":
                return AncestryBackgroundClassManager.addABCItem(source, actor);
            default:
                return super._onDropItem(event, data);
        }
    }

    protected override async _onDrop(event: ElementDragEvent): Promise<boolean | void> {
        const dataString = event.dataTransfer?.getData("text/plain");
        const dropData = JSON.parse(dataString ?? "");
        if ("pf2ettw" in dropData && dropData.pf2ettw.type === "CraftingFormula") {
            // Prepare formula if dropped on a crafting entry.
            const $containerEl = $(event.target).closest(".item-container");
            const dropContainerType = $containerEl.attr("data-container-type");
            if (dropContainerType === "craftingEntry") {
                const entrySelector = $containerEl.attr("data-entry-selector") ?? "";
                const craftingEntry = await this.actor.getCraftingEntry(entrySelector);

                if (!craftingEntry) return;

                const craftingFormulas = await this.actor.getCraftingFormulas();
                const formula = craftingFormulas.find((f) => f.uuid === dropData.pf2ettw.itemUuid);

                if (formula) return craftingEntry.prepareFormula(formula);
            }
        } else {
            return super._onDrop(event);
        }
    }

    /**
     * Handle a drop event for an existing Owned Item to sort that item
     * @param event
     * @param itemData
     */
    protected override async _onSortItem(event: ElementDragEvent, itemData: ItemSourcepf2ettw): Promise<Itempf2ettw[]> {
        const item = this.actor.items.get(itemData._id);
        if (item instanceof Featpf2ettw) {
            const { slotId, featType } = this.getNearestSlotId(event);
            const group = this.actor.featGroups[featType];
            const resorting = group && !group.slotted && item.data.data.location === featType;
            if (slotId && featType && !resorting) {
                return this.actor.insertFeat(item, featType, slotId);
            }
        }

        return super._onSortItem(event, itemData);
    }

    /** Get the font-awesome icon used to display hero points */
    private getHeroPointsIcon(level: number): string {
        const icons = [
            '<i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i>',
            '<i class="fas fa-hospital-symbol"></i><i class="far fa-circle"></i><i class="far fa-circle"></i>',
            '<i class="fas fa-hospital-symbol"></i><i class="fas fa-hospital-symbol"></i><i class="far fa-circle"></i>',
            '<i class="fas fa-hospital-symbol"></i><i class="fas fa-hospital-symbol"></i><i class="fas fa-hospital-symbol"></i>',
        ];
        return icons[level] ?? icons[0];
    }

    /**
     * Get the font-awesome icon used to display a certain level of thirst
     */
    private getThirstIcon(level: number) {
        const maxThirst = this.object.data.data.attributes.thirst.max || 4;
        const icons: Record<number, string> = {};
        const usedPoint = '<i class="fas fa-angry"></i>';
        const unUsedPoint = '<i class="far fa-angry"></i>';

        for (let i = 0; i <= maxThirst; i++) {
            let iconHtml = "";
            for (let iconColumn = 1; iconColumn <= maxThirst; iconColumn++) {
                iconHtml += iconColumn <= i ? usedPoint : unUsedPoint;
            }
            icons[i] = iconHtml;
        }

        return icons[level];
    }
}

interface CharacterSheetpf2ettw extends CreatureSheetpf2ettw<Characterpf2ettw> {
    getStrikeFromDOM(target: HTMLElement): CharacterStrike | null;
}

export { CharacterSheetpf2ettw };
