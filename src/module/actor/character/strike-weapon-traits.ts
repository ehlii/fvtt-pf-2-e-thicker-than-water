import { Weaponpf2ettw } from "@item";
import { Modifierpf2ettw, MODIFIER_TYPE } from "@actor/modifiers";
import { Predicatepf2ettw } from "@system/predication";
import { objectHasKey, setHasElement } from "@util";
import { DAMAGE_DIE_FACES } from "@system/damage";

/** Handle weapon traits that introduce modifiers or add other weapon traits */
class StrikeWeaponTraits {
    static adjustWeapon(weapon: Weaponpf2ettw): void {
        const traits = weapon.data.data.traits.value;
        for (const trait of traits) {
            switch (trait.replace(/-d?\d{1,3}$/, "")) {
                case "fatal-aim": {
                    if (weapon.rangeIncrement && weapon.handsHeld === 2) {
                        const fatal = trait.replace("-aim", "");
                        if (objectHasKey(CONFIG.pf2ettw.weaponTraits, fatal)) {
                            weapon.data.data.traits.value.push(fatal);
                        }
                    }
                    break;
                }
                case "jousting": {
                    if (weapon.handsHeld === 1) {
                        const die = /(d\d{1,2})$/.exec(trait)?.[1];
                        if (setHasElement(DAMAGE_DIE_FACES, die)) {
                            weapon.data.data.damage.die = die;
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    static createAttackModifiers(weapon: Weaponpf2ettw): Modifierpf2ettw[] {
        const traitsAndTags = [weapon.data.data.traits.value, weapon.data.data.traits.otherTags].flat();

        const getLabel = (traitOrTag: string): string => {
            const traits: Record<string, string | undefined> = CONFIG.pf2ettw.weaponTraits;
            const tags: Record<string, string | undefined> = CONFIG.pf2ettw.otherWeaponTags;
            return traits[traitOrTag] ?? tags[traitOrTag] ?? traitOrTag;
        };

        return traitsAndTags.flatMap((trait) => {
            switch (trait.replace(/-d?\d{1,3}$/, "")) {
                case "kickback": {
                    // "Firing a kickback weapon gives a –2 circumstance penalty to the attack roll, but characters with
                    // 14 or more Strength ignore the penalty."
                    return new Modifierpf2ettw({
                        label: CONFIG.pf2ettw.weaponTraits.kickback,
                        modifier: -2,
                        type: MODIFIER_TYPE.CIRCUMSTANCE,
                        predicate: new Predicatepf2ettw({ all: [{ lt: ["ability:str:score", 14] }] }),
                    });
                }
                case "volley": {
                    if (!weapon.rangeIncrement) return [];

                    const penaltyRange = Number(/-(\d+)$/.exec(trait)![1]);
                    return new Modifierpf2ettw({
                        label: getLabel(trait),
                        modifier: -2,
                        type: MODIFIER_TYPE.UNTYPED,
                        ignored: true,
                        predicate: new Predicatepf2ettw({
                            all: [{ lte: ["target:distance", penaltyRange] }],
                            not: ["self:ignore-volley-penalty"],
                        }),
                    });
                }
                case "improvised": {
                    return new Modifierpf2ettw({
                        label: getLabel(trait),
                        modifier: -2,
                        type: MODIFIER_TYPE.ITEM,
                        predicate: new Predicatepf2ettw({ not: ["self:ignore-improvised-penalty"] }),
                    });
                }
                case "sweep": {
                    return new Modifierpf2ettw({
                        label: getLabel(trait),
                        modifier: 1,
                        type: MODIFIER_TYPE.CIRCUMSTANCE,
                        predicate: new Predicatepf2ettw({ all: ["self:sweep-bonus"] }),
                    });
                }
                case "backswing": {
                    return new Modifierpf2ettw({
                        label: getLabel(trait),
                        modifier: 1,
                        type: MODIFIER_TYPE.CIRCUMSTANCE,
                        predicate: new Predicatepf2ettw({ all: ["self:backswing-bonus"] }),
                    });
                }
                default:
                    return [];
            }
        });
    }
}

export { StrikeWeaponTraits };
