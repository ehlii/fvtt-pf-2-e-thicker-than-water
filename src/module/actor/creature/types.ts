import { Actorpf2ettw } from "@actor";
import { ActorSheetDatapf2ettw } from "@actor/sheet/data-types";
import { Meleepf2ettw, Spellpf2ettw, Weaponpf2ettw } from "@item";
import { SpellcastingEntryData } from "@item/data";
import { SpellcastingEntryListData } from "@item/spellcasting-entry/data";
import { Modifierpf2ettw } from "@actor/modifiers";
import { TokenDocumentpf2ettw } from "@scene";
import { CheckDC } from "@system/degree-of-success";
import { Creaturepf2ettw } from ".";
import { SheetOptions } from "@module/sheet/helpers";
import { ALIGNMENTS, ALIGNMENT_TRAITS } from "./values";
import { TraitViewData } from "@actor/data/base";
import { FlattenedCondition } from "@system/conditions";

type Alignment = SetElement<typeof ALIGNMENTS>;
type AlignmentTrait = SetElement<typeof ALIGNMENT_TRAITS>;

type AttackItem = Weaponpf2ettw | Meleepf2ettw | Spellpf2ettw;

type ModeOfBeing = "living" | "undead" | "construct" | "object";

interface StrikeSelf<A extends Actorpf2ettw = Actorpf2ettw, I extends AttackItem = AttackItem> {
    actor: A;
    token: TokenDocumentpf2ettw | null;
    /** The item used for the strike */
    item: I;
    /** Bonuses and penalties added at the time of a strike */
    modifiers: Modifierpf2ettw[];
}

interface AttackTarget {
    actor: Actorpf2ettw;
    token: TokenDocumentpf2ettw;
    distance: number;
}

/** Context for the attack or damage roll of a strike */
interface StrikeRollContext<A extends Actorpf2ettw, I extends AttackItem> {
    /** Roll options */
    options: string[];
    self: StrikeSelf<A, I>;
    target: AttackTarget | null;
    traits: TraitViewData[];
}

interface StrikeRollContextParams<T extends AttackItem> {
    item: T;
    /** Domains from which to draw roll options */
    domains?: string[];
    /** Whether the request is for display in a sheet view. If so, targets are not considered */
    viewOnly?: boolean;
}

interface AttackRollContext<A extends Actorpf2ettw, I extends AttackItem> extends StrikeRollContext<A, I> {
    dc: CheckDC | null;
}

interface GetReachParameters {
    action?: "interact" | "attack";
    weapon?: Weaponpf2ettw | Meleepf2ettw | null;
}

interface IsFlatFootedParams {
    /** The circumstance potentially imposing the flat-footed condition */
    dueTo: "flanking" | "surprise" | "hidden" | "undetected";
}

interface CreatureSheetData<TActor extends Creaturepf2ettw = Creaturepf2ettw> extends ActorSheetDatapf2ettw<TActor> {
    languages: SheetOptions;
    abilities: Configpf2ettw["pf2ettw"]["abilities"];
    skills: Configpf2ettw["pf2ettw"]["skills"];
    actorSizes: Configpf2ettw["pf2ettw"]["actorSizes"];
    alignments: { [K in Alignment]?: string };
    rarity: Configpf2ettw["pf2ettw"]["rarityTraits"];
    attitude: Configpf2ettw["pf2ettw"]["attitude"];
    pfsFactions: Configpf2ettw["pf2ettw"]["pfsFactions"];
    conditions: FlattenedCondition[];
    dying: {
        maxed: boolean;
        remainingDying: number;
        remainingWounded: number;
    };
}

type SpellcastingSheetData = SpellcastingEntryData & SpellcastingEntryListData;

export {
    Alignment,
    AlignmentTrait,
    AttackItem,
    AttackRollContext,
    AttackTarget,
    CreatureSheetData,
    GetReachParameters,
    IsFlatFootedParams,
    ModeOfBeing,
    SpellcastingSheetData,
    StrikeRollContext,
    StrikeRollContextParams,
    StrikeSelf,
};
