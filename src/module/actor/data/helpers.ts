import { ActorDatapf2ettw, CreatureData } from ".";

export function isCreatureData(actorData: ActorDatapf2ettw): actorData is CreatureData {
    return ["character", "npc", "familiar"].includes(actorData.type);
}
