import type { CharacterData, CharacterSource } from "@actor/character/data";
import { CreatureType } from "@actor/creature/data";
import type { FamiliarData, FamiliarSource } from "@actor/familiar/data";
import type { HazardData, HazardSource } from "@actor/hazard/data";
import type { LootData, LootSource } from "@actor/loot/data";
import type { NPCData, NPCSource } from "@actor/npc/data";
import type { VehicleData, VehicleSource } from "@actor/vehicle/data";

type CreatureData = CharacterData | NPCData | FamiliarData;
type ActorType = CreatureType | "hazard" | "loot" | "vehicle";

type ActorDatapf2ettw = CreatureData | HazardData | LootData | VehicleData;
type ActorSourcepf2ettw = ActorDatapf2ettw["_source"];

interface RollInitiativeOptionspf2ettw extends RollInitiativeOptions {
    secret?: boolean;
    skipDialog?: boolean;
}
export {
    ActorDatapf2ettw,
    ActorSourcepf2ettw,
    ActorType,
    CharacterData,
    CharacterSource,
    CreatureData,
    FamiliarData,
    FamiliarSource,
    HazardData,
    HazardSource,
    LootData,
    LootSource,
    NPCData,
    NPCSource,
    RollInitiativeOptionspf2ettw,
    VehicleData,
    VehicleSource,
};
