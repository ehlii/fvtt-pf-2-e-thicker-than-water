import { Characterpf2ettw } from "@actor";
import { CreatureSheetpf2ettw } from "@actor/creature/sheet";
import { Familiarpf2ettw } from "@actor/familiar";
import { ActorSheetDatapf2ettw } from "@actor/sheet/data-types";
import { FamiliarSheetData } from "./types";

/**
 * @category Actor
 */
export class FamiliarSheetpf2ettw extends CreatureSheetpf2ettw<Familiarpf2ettw> {
    static override get defaultOptions() {
        const options = super.defaultOptions;
        mergeObject(options, {
            classes: ["sheet", "actor", "familiar"],
            width: 650,
            height: 680,
            tabs: [{ navSelector: ".sheet-navigation", contentSelector: ".sheet-content", initial: "attributes" }],
        });
        return options;
    }

    override get template() {
        return "systems/pf2ettw/templates/actors/familiar-sheet.html";
    }

    override async getData(options?: ActorSheetOptions): Promise<FamiliarSheetData> {
        const baseData = await super.getData(options);
        const familiar = this.actor;
        // Get all potential masters of the familiar
        const masters = game.actors.filter(
            (a): a is Characterpf2ettw => a.isOfType("character") && a.testUserPermission(game.user, "OWNER")
        );

        // list of abilities that can be selected as spellcasting ability
        const abilities = CONFIG.pf2ettw.abilities;

        const size = CONFIG.pf2ettw.actorSizes[familiar.data.data.traits.size.value] ?? null;
        const familiarAbilities = this.actor.master?.attributes?.familiarAbilities ?? { value: 0 };

        // Update save labels
        if (baseData.data.saves) {
            for (const key of ["fortitude", "reflex", "will"] as const) {
                const save = baseData.data.saves[key];
                save.label = CONFIG.pf2ettw.saves[key];
            }
        }

        return {
            ...baseData,
            master: this.actor.master,
            masters,
            abilities,
            size,
            familiarAbilities,
        };
    }

    protected override prepareItems(_sheetData: ActorSheetDatapf2ettw<Familiarpf2ettw>): void {}

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        $html.find("[data-action=perception-check]").on("click", (event) => {
            const options = this.actor.getRollOptions(["all", "perception"]);
            this.actor.attributes.perception.roll({ event, options });
        });

        $html.find("[data-attack-roll] *").on("click", (event) => {
            const options = this.actor.getRollOptions(["all", "attack"]);
            this.actor.data.data.attack.roll({ event, options });
        });
    }
}
