import { Characterpf2ettw } from "@actor";
import { CreatureSheetData } from "@actor/creature/types";
import { Familiarpf2ettw } from ".";

interface FamiliarSheetData extends CreatureSheetData<Familiarpf2ettw> {
    master: Characterpf2ettw | null;
    masters: Characterpf2ettw[];
    abilities: Configpf2ettw["pf2ettw"]["abilities"];
    size: string;
    familiarAbilities: { value: number };
}

export { FamiliarSheetData };
