import { ActorSheetpf2ettw } from "@actor/sheet/base";
import { SAVE_TYPES } from "@actor/values";
import { Consumablepf2ettw, Spellpf2ettw } from "@item";
import { ItemDatapf2ettw } from "@item/data";
import { Errorpf2ettw } from "@util";
import { Hazardpf2ettw } from ".";
import { HazardSystemData } from "./data";

export class HazardSheetpf2ettw extends ActorSheetpf2ettw<Hazardpf2ettw> {
    static override get defaultOptions() {
        const options = super.defaultOptions;
        mergeObject(options, {
            classes: options.classes.concat("hazard"),
            width: 650,
            height: 680,
        });
        return options;
    }

    /** Get the HTML template path to use depending on whether this sheet is in edit mode */
    override get template(): string {
        const path = "systems/pf2ettw/templates/actors/hazard/";
        if (this.actor.getFlag("pf2ettw", "editHazard.value")) return `${path}sheet.html`;
        return `${path}sheet-no-edit.html`;
    }

    override async getData() {
        const sheetData = await super.getData();

        // Update save labels
        for (const key of SAVE_TYPES) {
            if (!sheetData.data.saves[key]) continue;
            sheetData.data.saves[key].label = CONFIG.pf2ettw.saves[key];
        }
        sheetData.actor.flags.editHazard ??= { value: false };
        const systemData: HazardSystemData = sheetData.data;

        return {
            ...sheetData,
            flags: sheetData.actor.flags,
            hazardTraits: CONFIG.pf2ettw.hazardTraits,
            actorTraits: systemData.traits.traits.value,
            actorRarities: CONFIG.pf2ettw.rarityTraits,
            actorRarity: CONFIG.pf2ettw.rarityTraits[this.actor.rarity],
            stealthDC: (systemData.attributes.stealth?.value ?? 0) + 10,
            hasStealthDescription: systemData.attributes.stealth?.details || false,
            hasResistances: systemData.traits.dr.length > 0,
            hasWeaknesses: systemData.traits.dv.length > 0,
            hasDescription: systemData.details.description || false,
            hasDisable: systemData.details.disable || false,
            hasRoutineDetails: systemData.details.routine || false,
            hasResetDetails: systemData.details.reset || false,
            hasHPDetails: systemData.attributes.hp.details || false,
            hasWillSave: !!systemData.saves.will,
            brokenThreshold: Math.floor(systemData.attributes.hp.max / 2),
        };
    }

    override prepareItems(sheetData: any): void {
        const actorData = sheetData.actor;
        // Actions
        type AttackData = { label: string; items: ItemDatapf2ettw[]; type: "melee" };
        const attacks: Record<"melee" | "ranged", AttackData> = {
            melee: { label: "NPC Melee Attack", items: [], type: "melee" },
            ranged: { label: "NPC Ranged Attack", items: [], type: "melee" },
        };

        // Actions
        type ActionData = { label: string; actions: ItemDatapf2ettw[] };
        const actions: Record<string, ActionData> = {
            action: { label: "Actions", actions: [] },
            reaction: { label: "Reactions", actions: [] },
            free: { label: "Free Actions", actions: [] },
            passive: { label: "Passive Actions", actions: [] },
        };

        // Iterate through items, allocating to containers
        const weaponTraits: Record<string, string> = CONFIG.pf2ettw.weaponTraits;
        const traitsDescriptions: Record<string, string | undefined> = CONFIG.pf2ettw.traitsDescriptions;
        for (const itemData of actorData.items) {
            itemData.img = itemData.img || CONST.DEFAULT_TOKEN;

            // NPC Generic Attacks
            if (itemData.type === "melee") {
                const weaponType: "melee" | "ranged" = itemData.data.weaponType.value || "melee";
                const traits: string[] = itemData.data.traits.value;
                const isAgile = traits.includes("agile");
                itemData.data.bonus.total = Number(itemData.data.bonus.value) || 0;
                itemData.data.isAgile = isAgile;

                // get formated traits for read-only npc sheet
                itemData.traits = traits.map((trait) => ({
                    label: weaponTraits[trait] ?? trait.charAt(0).toUpperCase() + trait.slice(1),
                    description: traitsDescriptions[trait] ?? "",
                }));
                attacks[weaponType].items.push(itemData);
            }

            // Actions
            else if (itemData.type === "action") {
                const actionType = itemData.data.actionType.value || "action";
                itemData.img = Hazardpf2ettw.getActionGraphics(
                    actionType,
                    Number(itemData.data.actions.value) || 1
                ).imageUrl;

                // get formated traits for read-only npc sheet
                const traits: string[] = itemData.data.traits.value;
                const traitObjects = traits.map((trait) => ({
                    label: weaponTraits[trait] || trait.charAt(0).toUpperCase() + trait.slice(1),
                    description: traitsDescriptions[trait] ?? "",
                }));
                if (itemData.data.actionType.value) {
                    const actionType: string = itemData.data.actionType.value;
                    traitObjects.push({
                        label: weaponTraits[actionType] || actionType.charAt(0).toUpperCase() + actionType.slice(1),
                        description: traitsDescriptions[actionType] ?? "",
                    });
                }
                itemData.traits = traitObjects;

                actions[actionType].actions.push(itemData);
            }
        }

        // Assign and return
        actorData.actions = actions;
        actorData.attacks = attacks;
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        // NPC Weapon Rolling
        $html.find("button").on("click", (event) => {
            event.preventDefault();
            event.stopPropagation();

            const itemId = $(event.currentTarget).parents(".item").attr("data-item-id") ?? "";
            const item = this.actor.items.get(itemId);
            if (!item) {
                throw Errorpf2ettw(`Item ${itemId} not found`);
            }
            const spell =
                item instanceof Spellpf2ettw ? item : item instanceof Consumablepf2ettw ? item.embeddedSpell : null;

            // which function gets called depends on the type of button stored in the dataset attribute action
            switch (event.target.dataset.action) {
                case "npcAttack":
                    item.rollNPCAttack(event);
                    break;
                case "npcAttack2":
                    item.rollNPCAttack(event, 2);
                    break;
                case "npcAttack3":
                    item.rollNPCAttack(event, 3);
                    break;
                case "npcDamage":
                    item.rollNPCDamage(event);
                    break;
                case "npcDamageCritical":
                    item.rollNPCDamage(event, true);
                    break;
                case "spellAttack": {
                    spell?.rollAttack(event);
                    break;
                }
                case "spellDamage": {
                    spell?.rollDamage(event);
                    break;
                }
                case "consume":
                    if (item instanceof Consumablepf2ettw) item.consume();
                    break;
                default:
                    throw new Error("Unknown action type");
            }
        });

        if (!this.options.editable) return;

        $html.find<HTMLInputElement>(".isHazardEditable").on("change", (event) => {
            this.actor.setFlag("pf2ettw", "editHazard", { value: event.target.checked });
        });
    }
}
