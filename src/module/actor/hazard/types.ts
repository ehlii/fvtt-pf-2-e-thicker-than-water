import { Hazardpf2ettw } from "@actor";
import { ActorSheetDatapf2ettw } from "@actor/sheet/data-types";
import { SaveType } from "@actor/types";
import { ActionItempf2ettw } from "@item";

interface HazardSheetData extends ActorSheetDatapf2ettw<Hazardpf2ettw> {
    actions: HazardActionSheetData;
    editing: boolean;
    actorTraits: string[];
    rarity: Record<string, string>;
    rarityLabel: string;
    brokenThreshold: number;
    saves: HazardSaveSheetData[];
    stealthDC: number | null;

    hasHealth: boolean;
    hasHPDetails: boolean;
    hasSaves: boolean;
    hasIWR: boolean;
    hasStealth: boolean;
    hasStealthDescription: boolean;
    hasDescription: boolean;
    hasDisable: boolean;
    hasRoutineDetails: boolean;
    hasResetDetails: boolean;
}

interface HazardSaveSheetData {
    label: string;
    type: SaveType;
    mod?: number;
}

interface HazardActionSheetData {
    passive: ActionsDetails;
    free: ActionsDetails;
    reaction: ActionsDetails;
    action: ActionsDetails;
}

interface ActionsDetails {
    label: string;
    actions: ActionItempf2ettw[];
}

export { HazardActionSheetData, HazardSaveSheetData, HazardSheetData };
