import {
    ActorSystemData,
    ActorSystemSource,
    BaseActorDatapf2ettw,
    BaseActorSourcepf2ettw,
    BaseTraitsData,
    BaseTraitsSource,
    GangUpCircumstance,
} from "@actor/data/base";
import { Lootpf2ettw } from ".";

/** The stored source data of a loot actor */
type LootSource = BaseActorSourcepf2ettw<"loot", LootSystemSource>;

interface LootData
    extends Omit<LootSource, "data" | "effects" | "flags" | "items" | "token" | "type">,
        BaseActorDatapf2ettw<Lootpf2ettw, "loot", LootSystemData, LootSource> {}

/** The system-level data of loot actors. */
interface LootSystemSource extends ActorSystemSource {
    attributes: LootAttributesSource;
    details: LootDetailsSource;
    lootSheetType: "Merchant" | "Loot";
    hiddenWhenEmpty: boolean;
    traits: BaseTraitsSource;
}

interface LootSystemData extends LootSystemSource, Omit<ActorSystemData, "attributes"> {
    attributes: LootAttributesData;

    details: LootDetailsData;

    traits: BaseTraitsData;
}

interface LootAttributesSource {
    hp?: never;
    ac?: never;
}

interface LootAttributesData extends LootAttributesSource {
    flanking: {
        canFlank: false;
        canGangUp: GangUpCircumstance[];
        flankable: false;
        flatFootable: false;
    };
}

interface LootDetailsSource {
    description: {
        value: string;
    };
    level: {
        value: number;
    };
}

interface LootDetailsData extends LootDetailsSource {
    alliance: null;
}

export { LootData, LootSource, LootSystemData, LootSystemSource };
