import { ActorSheetpf2ettw } from "../sheet/base";
import { Lootpf2ettw } from "@actor/loot";
import { DistributeCoinsPopup } from "../sheet/popups/distribute-coins-popup";
import { LootNPCsPopup } from "../sheet/loot/loot-npcs-popup";
import { LootSheetDatapf2ettw } from "../sheet/data-types";
import { Itempf2ettw } from "@item";
import { DropCanvasItemDatapf2ettw } from "@module/canvas/drop-canvas-data";

export class LootSheetpf2ettw extends ActorSheetpf2ettw<Lootpf2ettw> {
    static override get defaultOptions(): ActorSheetOptions {
        const options = super.defaultOptions;

        return {
            ...options,
            editable: true,
            classes: [...options.classes, "loot"],
            width: 650,
            height: 680,
            tabs: [{ navSelector: ".sheet-navigation", contentSelector: ".sheet-content", initial: "inventory" }],
        };
    }

    override get template(): string {
        return "systems/pf2ettw/templates/actors/loot/sheet.html";
    }

    override get isLootSheet(): boolean {
        return !this.actor.isOwner && this.actor.isLootableBy(game.user);
    }

    override async getData(): Promise<LootSheetDatapf2ettw> {
        const sheetData = await super.getData();
        const isLoot = this.actor.data.data.lootSheetType === "Loot";
        return { ...sheetData, isLoot };
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        if (this.options.editable) {
            $html
                .find(".split-coins")
                .removeAttr("disabled")
                .on("click", (event) => this.distributeCoins(event));
            $html
                .find(".loot-npcs")
                .removeAttr("disabled")
                .on("click", (event) => this.lootNPCs(event));
            $html.find("i.fa-info-circle.help[title]").tooltipster({
                maxWidth: 275,
                position: "right",
                theme: "crb-hover",
                contentAsHTML: true,
            });
        }
    }

    protected prepareItems(): void {
        // no-op
    }

    // Events

    private async distributeCoins(event: JQuery.ClickEvent): Promise<void> {
        event.preventDefault();
        await new DistributeCoinsPopup(this.actor, {}).render(true);
    }

    private async lootNPCs(event: JQuery.ClickEvent): Promise<void> {
        event.preventDefault();
        if (canvas.tokens.controlled.some((token) => token.actor?.id !== this.actor.id)) {
            await new LootNPCsPopup(this.actor).render(true);
        } else {
            ui.notifications.warn("No tokens selected.");
        }
    }

    protected override async _onDropItem(
        event: ElementDragEvent,
        itemData: DropCanvasItemDatapf2ettw
    ): Promise<Itempf2ettw[]> {
        // Prevent a Foundry permissions error from being thrown when a player drops an item from an unowned
        // loot sheet to the same sheet
        if (this.actor.id === itemData.actorId && !this.actor.testUserPermission(game.user, "OWNER")) {
            return [];
        }
        return super._onDropItem(event, itemData);
    }
}
