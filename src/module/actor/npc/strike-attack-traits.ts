import { Modifierpf2ettw, MODIFIER_TYPE } from "@actor/modifiers";
import { Meleepf2ettw } from "@item/melee";
import { Predicatepf2ettw } from "@system/predication";

class StrikeAttackTraits {
    static createAttackModifiers(strike: Meleepf2ettw): Modifierpf2ettw[] {
        const traits = strike.data.data.traits.value;

        const getLabel = (traitOrTag: string): string => {
            const traits: Record<string, string | undefined> = CONFIG.pf2ettw.weaponTraits;
            const tags: Record<string, string | undefined> = CONFIG.pf2ettw.otherWeaponTags;
            return traits[traitOrTag] ?? tags[traitOrTag] ?? traitOrTag;
        };

        return traits.flatMap((trait) => {
            switch (trait.replace(/-d?\d{1,3}$/, "")) {
                case "sweep": {
                    return new Modifierpf2ettw({
                        label: getLabel(trait),
                        modifier: 1,
                        type: MODIFIER_TYPE.CIRCUMSTANCE,
                        predicate: new Predicatepf2ettw({ all: ["sweep-bonus"] }),
                    });
                }
                case "backswing": {
                    return new Modifierpf2ettw({
                        label: getLabel(trait),
                        modifier: 1,
                        type: MODIFIER_TYPE.CIRCUMSTANCE,
                        predicate: new Predicatepf2ettw({ all: ["backswing-bonus"] }),
                    });
                }
                default:
                    return [];
            }
        });
    }
}

export { StrikeAttackTraits };
