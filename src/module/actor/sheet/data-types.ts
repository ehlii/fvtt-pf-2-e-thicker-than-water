import { Actorpf2ettw } from "@actor/base";
import { InventoryBulk } from "@actor/inventory";
import { Lootpf2ettw } from "@actor/loot";
import { PhysicalItempf2ettw } from "@item";
import { Coins } from "@item/physical/data";
import { PhysicalItemType } from "@item/physical/types";
import { SheetOptions } from "@module/sheet/helpers";

export interface InventoryItem<D extends PhysicalItempf2ettw = PhysicalItempf2ettw> {
    item: D;
    editable: boolean;
    isContainer: boolean;
    canBeEquipped: boolean;
    isInvestable: boolean;
    isSellable: boolean;
    hasCharges: boolean;
    heldItems?: InventoryItem[];
}

interface CoinDisplayData {
    value: number;
    label: string;
}

export type CoinageSummary = { [K in keyof Coins]?: CoinDisplayData };

interface SheetItemList {
    label: string;
    type: PhysicalItemType;
    items: InventoryItem[];
    invested?: { value: number; max: number } | null;
    overInvested?: boolean;
}

export interface SheetInventory {
    sections: Record<Exclude<PhysicalItemType, "book">, SheetItemList>;
    bulk: InventoryBulk;
}

export interface ActorSheetDatapf2ettw<TActor extends Actorpf2ettw> extends ActorSheetData<TActor> {
    traits: SheetOptions;
    isTargetFlatFooted: boolean;
    user: { isGM: boolean };
    totalCoinage: CoinageSummary;
    totalCoinageGold: string;
    totalWealth: Coins;
    totalWealthGold: string;
    immunities: SheetOptions;
    hasImmunities: boolean;
    inventory: SheetInventory;
}

export interface LootSheetDatapf2ettw extends ActorSheetDatapf2ettw<Lootpf2ettw> {
    isLoot: boolean;
}
