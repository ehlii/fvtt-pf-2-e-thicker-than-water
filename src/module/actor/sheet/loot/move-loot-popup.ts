import { Actorpf2ettw } from "@actor/base";

interface MoveLootOptions extends FormApplicationOptions {
    maxQuantity: number;
    newStack: boolean;
    lockStack: boolean;
    split: boolean;
}
interface MoveLootFormData extends FormData {
    quantity: number;
    newStack: boolean;
}
type MoveLootCallback = (quantity: number, newStack: boolean) => void;

export class MoveLootPopup extends FormApplication<{}, MoveLootOptions> {
    onSubmitCallback: MoveLootCallback;

    constructor(object: Actorpf2ettw, options: Partial<MoveLootOptions>, callback: MoveLootCallback) {
        super(object, options);

        this.onSubmitCallback = callback;
    }

    override async getData() {
        return {
            ...(await super.getData()),
            maxQuantity: this.options.maxQuantity,
            newStack: this.options.newStack,
            lockStack: this.options.lockStack,
            split: this.options.split,
        };
    }

    static override get defaultOptions(): MoveLootOptions {
        return {
            ...super.defaultOptions,
            id: "MoveLootPopup",
            classes: [],
            title: game.i18n.localize("pf2ettw.loot.MoveLootPopupTitle"),
            template: "systems/pf2ettw/templates/popups/loot/move-loot-popup.html",
            width: "auto",
            maxQuantity: 1,
            newStack: false,
            lockStack: false,
            split: false,
        };
    }

    override async _updateObject(
        _event: ElementDragEvent,
        formData: Record<string, unknown> & MoveLootFormData
    ): Promise<void> {
        if (this.onSubmitCallback) {
            this.onSubmitCallback(formData.quantity, formData.newStack);
        }
    }
}
