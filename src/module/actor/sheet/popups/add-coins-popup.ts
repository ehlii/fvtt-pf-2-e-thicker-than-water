import { Actorpf2ettw } from "@actor/base";
import { Coins } from "@item/physical/data";

interface AddCoinsFormData extends Coins {
    combineStacks: boolean;
}

/**
 * @category Other
 */
export class AddCoinsPopup extends FormApplication<Actorpf2ettw> {
    static override get defaultOptions(): FormApplicationOptions {
        const options = super.defaultOptions;
        options.id = "add-coins";
        options.classes = [];
        options.title = "Add Coins";
        options.template = "systems/pf2ettw/templates/actors/add-coins.html";
        options.width = "auto";
        return options;
    }

    override async _updateObject(_event: Event, formData: Record<string, unknown> & AddCoinsFormData): Promise<void> {
        const combineStacks = formData.combineStacks;
        const coins = { pp: formData.pp, gp: formData.gp, sp: formData.sp, cp: formData.cp };
        this.object.inventory.addCoins(coins, { combineStacks });
    }
}
