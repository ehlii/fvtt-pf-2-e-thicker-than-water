import { SkillAbbreviation } from "@actor/creature/data";
import { identifyItem, IdentifyAlchemyDCs, IdentifyMagicDCs, GenericIdentifyDCs } from "@item/identification";
import { PhysicalItempf2ettw } from "@item/physical";
import { ChatMessagepf2ettw } from "@module/chat-message";
import { objectHasKey } from "@util";

export class IdentifyItemPopup extends FormApplication<PhysicalItempf2ettw> {
    static override get defaultOptions(): FormApplicationOptions {
        return {
            ...super.defaultOptions,
            id: "identify-item",
            title: game.i18n.localize("pf2ettw.identification.Identify"),
            template: "systems/pf2ettw/templates/actors/identify-item.html",
            width: "auto",
            classes: ["identify-popup"],
        };
    }

    get item(): PhysicalItempf2ettw {
        return this.object;
    }

    override async getData(): Promise<IdentifyPopupData> {
        const item = this.object;
        const notMatchingTraditionModifier = game.settings.get("pf2ettw", "identifyMagicNotMatchingTraditionModifier");
        const proficiencyWithoutLevel =
            game.settings.get("pf2ettw", "proficiencyVariant") === "ProficiencyWithoutLevel";
        const dcs = identifyItem(item, { proficiencyWithoutLevel, notMatchingTraditionModifier });

        return {
            ...(await super.getData()),
            isMagic: ["arc", "nat", "rel", "occ"].some((s) => s in dcs),
            isAlchemical: "cra" in dcs,
            dcs,
        };
    }

    override activateListeners($form: JQuery<HTMLFormElement>): void {
        $form.find<HTMLButtonElement>("button.update-identification").on("click", (event) => {
            const $button = $(event.delegateTarget);
            this.submit({ updateData: { status: $button.val() } });
        });
        // add listener on Post skill checks to chat button that posts item unidentified img and name and skill checks
        $form.find<HTMLButtonElement>("button.post-skill-checks").on("click", async () => {
            const item = this.item;
            const itemImg = item.data.data.identification.unidentified.img;
            const itemName = item.data.data.identification.unidentified.name;
            const identifiedName = item.data.data.identification.identified.name;
            const skills = $("div#identify-item")
                .find("tr")
                .toArray()
                .flatMap((row): { name: string; shortForm: SkillAbbreviation; dc: number } | never[] => {
                    const shortForm = row.dataset.skill;
                    const dc = Number(row.dataset.dc);
                    if (!(Number.isInteger(dc) && objectHasKey(CONFIG.pf2ettw.skills, shortForm))) {
                        return [];
                    }
                    const name = game.i18n.localize(CONFIG.pf2ettw.skills[shortForm]);

                    return { shortForm, name, dc };
                });

            const content = await renderTemplate(
                "systems/pf2ettw/templates/actors/identify-item-chat-skill-checks.html",
                {
                    itemImg,
                    itemName,
                    identifiedName,
                    skills,
                }
            );

            await ChatMessagepf2ettw.create({ user: game.user.id, content });
        });
    }

    protected override async _updateObject(_event: Event, formData: Record<string, unknown>): Promise<void> {
        const status = formData["status"];
        if (status === "identified") {
            await this.item.setIdentificationStatus(status);
        }
    }
}

interface IdentifyPopupData extends FormApplicationData {
    isMagic: boolean;
    isAlchemical: boolean;
    dcs: GenericIdentifyDCs | IdentifyMagicDCs | IdentifyAlchemyDCs;
}
