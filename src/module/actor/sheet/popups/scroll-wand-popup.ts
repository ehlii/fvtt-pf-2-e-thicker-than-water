import { Actorpf2ettw } from "@actor/index";
import { Spellpf2ettw } from "@item";
import { Errorpf2ettw } from "@util";

export class ScrollWandPopup extends FormApplication<Actorpf2ettw> {
    onSubmitCallback: ScrollWandCallback;
    spell?: Spellpf2ettw;

    constructor(
        object: Actorpf2ettw,
        options: Partial<FormApplicationOptions>,
        callback: ScrollWandCallback,
        spell: Spellpf2ettw
    ) {
        super(object, options);

        this.spell = spell;
        this.onSubmitCallback = callback;
    }

    static override get defaultOptions() {
        const options = super.defaultOptions;

        options.classes = [];
        options.title = game.i18n.localize("pf2ettw.ScrollWandPopup.title");
        options.template = "systems/pf2ettw/templates/popups/scroll-wand-popup.html";
        options.width = "auto";

        return options;
    }

    override async getData(): Promise<FormApplicationData<Actorpf2ettw>> {
        const sheetData: FormApplicationData<Actorpf2ettw> & { validLevels?: number[] } = await super.getData();

        if (!this.spell) {
            throw Errorpf2ettw("ScrollWandPopup | Could not read spelldata");
        }

        const minimumLevel = this.spell.baseLevel;
        const levels = Array.from(Array(11 - minimumLevel).keys()).map((index) => minimumLevel + index);
        sheetData.validLevels = levels;
        return sheetData;
    }

    override async _updateObject(_event: Event, formData: { itemType: string; level: number }) {
        if (formData.itemType === "wand" && formData.level === 10) {
            ui.notifications.warn(game.i18n.localize("pf2ettw.ScrollWandPopup.10thLevelWand"));
        } else if (this.onSubmitCallback && this.spell) {
            this.onSubmitCallback(formData.level, formData.itemType, this.spell);
        }
    }
}

type ScrollWandCallback = (level: number, itemType: string, spell: Spellpf2ettw) => Promise<void>;
