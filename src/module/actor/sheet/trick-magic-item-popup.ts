import { calculateTrickMagicItemCheckDC, TrickMagicItemDifficultyData } from "@item/consumable/spell-consumables";
import type { Consumablepf2ettw } from "@item";
import { Characterpf2ettw } from "@actor";
import { Localizepf2ettw } from "@module/system/localize";
import { Errorpf2ettw } from "@util";
import { SKILL_DICTIONARY } from "@actor/values";
import { TrickMagicItemEntry, TrickMagicItemSkill, TRICK_MAGIC_SKILLS } from "@item/spellcasting-entry/trick";

export class TrickMagicItemPopup {
    /** The wand or scroll being "tricked" */
    readonly item: Embedded<Consumablepf2ettw>;

    /** The actor doing the tricking */
    readonly actor!: Characterpf2ettw;

    /** The skill DC of the action's check */
    readonly checkDC: TrickMagicItemDifficultyData;

    private translations = Localizepf2ettw.translations.pf2ettw.TrickMagicItemPopup;

    constructor(item: Embedded<Consumablepf2ettw>) {
        this.item = item;
        if (item.data.type !== "consumable") {
            throw Errorpf2ettw("Unexpected item used for Trick Magic Item");
        }
        this.checkDC = calculateTrickMagicItemCheckDC(item.data);

        if (!(item.actor instanceof Characterpf2ettw)) {
            ui.notifications.warn(this.translations.InvalidActor);
            return;
        }
        this.actor = item.actor;

        this.initialize();
    }

    private async initialize() {
        const skills = TRICK_MAGIC_SKILLS.filter((skill) => skill in this.checkDC).map((value) => ({
            value,
            label: game.i18n.localize(`pf2ettw.Skill${value.capitalize()}`),
            modifier: this.actor.skills[value].check.mod,
        }));
        const buttons = skills.reduce((accumulated: Record<string, DialogButton>, skill) => {
            const button: DialogButton = {
                icon: '<i class="fas fa-dice-d20"></i>',
                label: `${skill.label} (${skill.modifier < 0 ? "" : "+"}${skill.modifier})`,
                callback: () => this.handleTrickItem(skill.value),
            };
            return { ...accumulated, [skill.value]: button };
        }, {});
        new Dialog(
            {
                title: this.translations.Title,
                content: `<p>${this.translations.Label}</p>`,
                buttons,
            },
            { classes: ["dialog", "trick-magic-item"], width: "auto" }
        ).render(true);
    }

    handleTrickItem(skill: TrickMagicItemSkill) {
        const options = ["all", "skill-check", "action:trick-magic-item"].concat(SKILL_DICTIONARY[skill]);
        const stat = this.actor.data.data.skills[skill];
        stat.roll({
            options: options,
            dc: { value: this.checkDC[skill] ?? 0 },
        });

        const trick = new TrickMagicItemEntry(this.actor, skill);
        this.item.castEmbeddedSpell(trick);
    }
}
