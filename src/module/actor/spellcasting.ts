import { Actorpf2ettw, Characterpf2ettw, NPCpf2ettw } from "@actor";
import { Consumablepf2ettw, SpellcastingEntrypf2ettw } from "@item";
import { Errorpf2ettw, tupleHasValue } from "@util";

export class ActorSpellcasting extends Collection<SpellcastingEntrypf2ettw> {
    constructor(public readonly actor: Actorpf2ettw, entries?: SpellcastingEntrypf2ettw[]) {
        super(entries?.map((entry) => [entry.id, entry]));
    }

    /** Returns a list of entries pre-filtered to SpellcastingEntrypf2ettw */
    get regular() {
        return this.filter((entry): entry is SpellcastingEntrypf2ettw => entry instanceof SpellcastingEntrypf2ettw);
    }

    /**
     * All spellcasting entries that count as prepared/spontaneous, which qualify as a
     * full fledged spellcasting feature for wands and scrolls.
     */
    get spellcastingFeatures() {
        return this.regular.filter((entry) => entry.isPrepared || entry.isSpontaneous);
    }

    canCastConsumable(item: Consumablepf2ettw): boolean {
        const spellData = item.data.data.spell?.data?.data;
        return (
            !!spellData &&
            this.spellcastingFeatures.some((entry) => tupleHasValue(spellData.traditions.value, entry.tradition))
        );
    }

    refocus(options: { all?: boolean } = {}) {
        if (!options.all) {
            throw Errorpf2ettw("Actors do not currently support regular refocusing");
        }

        if (this.actor instanceof NPCpf2ettw || this.actor instanceof Characterpf2ettw) {
            const focus = this.actor.data.data.resources.focus;

            const rechargeFocus = focus?.max && focus.value < focus.max;
            if (focus && rechargeFocus) {
                focus.value = focus.max;
                return { "data.resources.focus.value": focus.value };
            }
        }

        return null;
    }

    /**
     * Recharges all spellcasting entries based on the type of entry it is
     * @todo Support a timespan property of some sort and handle 1/hour innate spells
     */
    recharge() {
        type SpellcastingUpdate =
            | EmbeddedDocumentUpdateData<SpellcastingEntrypf2ettw>
            | EmbeddedDocumentUpdateData<SpellcastingEntrypf2ettw>[];

        const itemUpdates = this.contents.flatMap((entry): SpellcastingUpdate => {
            if (!(entry instanceof SpellcastingEntrypf2ettw)) return [];
            if (entry.isFocusPool) return [];

            // Innate spells should refresh uses instead
            if (entry.isInnate) {
                return entry.spells.map((spell) => {
                    const value = spell.data.data.location.uses?.max ?? 1;
                    return { _id: spell.id, "data.location.uses.value": value };
                });
            }

            // Spontaneous, and Prepared spells
            const slots = entry.data.data.slots;
            let updated = false;
            for (const slot of Object.values(slots)) {
                if (entry.isPrepared && !entry.isFlexible) {
                    for (const preparedSpell of Object.values(slot.prepared)) {
                        if (preparedSpell.expended) {
                            preparedSpell.expended = false;
                            updated = true;
                        }
                    }
                } else if (slot.value < slot.max) {
                    slot.value = slot.max;
                    updated = true;
                }
            }

            if (updated) {
                return { _id: entry.id, "data.slots": slots };
            }

            return [];
        });

        const actorUpdates = this.refocus({ all: true });
        return { itemUpdates, actorUpdates };
    }
}
