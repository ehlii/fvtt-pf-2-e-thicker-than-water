import { ActorSheetpf2ettw } from "../sheet/base";
import { Vehiclepf2ettw } from "@actor/vehicle";
import { ItemDatapf2ettw } from "@item/data";
import { isPhysicalData } from "@item/data/helpers";
import { PhysicalItempf2ettw } from "@item";
import { tupleHasValue } from "@util";

export class VehicleSheetpf2ettw extends ActorSheetpf2ettw<Vehiclepf2ettw> {
    static override get defaultOptions(): ActorSheetOptions {
        return {
            ...super.defaultOptions,
            classes: ["default", "sheet", "actor", "vehicle"],
            width: 670,
            height: 480,
            tabs: [{ navSelector: ".sheet-navigation", contentSelector: ".sheet-content", initial: "details" }],
        };
    }

    override get template(): string {
        return "systems/pf2ettw/templates/actors/vehicle/vehicle-sheet.html";
    }

    override async getData() {
        const sheetData: any = await super.getData();

        sheetData.actorSizes = CONFIG.pf2ettw.actorSizes;
        sheetData.actorSize = sheetData.actorSizes[sheetData.data.traits.size.value];

        sheetData.actorRarities = CONFIG.pf2ettw.rarityTraits;
        sheetData.actorRarity = sheetData.actorRarities[sheetData.data.traits.rarity];

        this.prepareItems(sheetData);

        return sheetData;
    }

    protected prepareItems(sheetData: any): void {
        const actorData = sheetData.actor;

        // Actions
        const actions: Record<"action" | "reaction" | "free", { label: string; actions: ItemDatapf2ettw[] }> = {
            action: { label: game.i18n.localize("pf2ettw.ActionsActionsHeader"), actions: [] },
            reaction: { label: game.i18n.localize("pf2ettw.ActionsReactionsHeader"), actions: [] },
            free: { label: game.i18n.localize("pf2ettw.ActionsFreeActionsHeader"), actions: [] },
        };

        for (const itemData of actorData.items) {
            const physicalData: ItemDatapf2ettw = itemData;
            const item = this.actor.items.get(itemData._id, { strict: true });
            if (item instanceof PhysicalItempf2ettw && isPhysicalData(physicalData)) {
                itemData.showEdit = sheetData.user.isGM || physicalData.data.identification.status === "identified";
                itemData.isInvestable = false;
                itemData.isIdentified = physicalData.data.identification.status === "identified";
                itemData.assetValue = item.assetValue;
            }

            // Actions
            if (itemData.type === "action") {
                const actionTypes = ["free", "reaction", "passive"] as const;
                const fromItem: string = itemData.data.actionType.value;
                const actionType = tupleHasValue(actionTypes, fromItem) ? fromItem : "action";
                itemData.img = Vehiclepf2ettw.getActionGraphics(
                    actionType,
                    parseInt((itemData.data.actions || {}).value, 10) || 1
                ).imageUrl;
                if (actionType === "passive") {
                    actions.free.actions.push(itemData);
                } else {
                    actions[actionType].actions.push(itemData);
                }
            }

            for (const itemData of sheetData.items) {
                const physicalData: ItemDatapf2ettw = itemData;
                if (isPhysicalData(physicalData)) {
                    itemData.showEdit = true;
                }
            }
        }

        actorData.actions = actions;
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);
        {
            // ensure correct tab name is displayed after actor update
            const title = $(".sheet-navigation .active").attr("title");
            if (title) {
                $html.find(".navigation-title").text(title);
            }
        }
        $html.find(".sheet-navigation").on("mouseover", ".item", (event) => {
            const title = event.currentTarget.title;
            if (title) {
                $(event.currentTarget).parents(".sheet-navigation").find(".navigation-title").text(title);
            }
        });
        $html.find(".sheet-navigation").on("mouseout", ".item", (event) => {
            const parent = $(event.currentTarget).parents(".sheet-navigation");
            const title = parent.find(".item.active").attr("title");
            if (title) {
                parent.find(".navigation-title").text(title);
            }
        });

        // get buttons
        $html.find(".crb-trait-selector").on("click", (event) => this.onTraitSelector(event));
    }
}
