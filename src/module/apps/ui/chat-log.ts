import { ChatMessagepf2ettw } from "@module/chat-message";
import { Checkpf2ettw } from "@system/rolls";
import { Errorpf2ettw } from "@util";

export class ChatLogpf2ettw extends ChatLog<ChatMessagepf2ettw> {
    protected override _getEntryContextOptions(): EntryContextOption[] {
        const canApplyDamage: ContextOptionCondition = ($html) => {
            const messageId = $html.attr("data-message-id") ?? "";
            const message = game.messages.get(messageId, { strict: true });

            return (
                canvas.tokens.controlled.length > 0 && message.isRoll && $html.find(".chat-damage-buttons").length === 1
            );
        };

        const canApplyTripleDamage: ContextOptionCondition = ($li) =>
            canApplyDamage($li) && $li.find("button.triple-damage").length === 1;

        const canApplyInitiative: ContextOptionCondition = ($li) => {
            const messageId = $li.attr("data-message-id") ?? "";
            const message = game.messages.get(messageId, { strict: true });

            // Rolling PC initiative from a regular skill is difficult because of bonuses that can apply to initiative specifically (e.g. Harmlessly Cute)
            // Avoid potential confusion and misunderstanding by just allowing NPCs to roll
            const validActor =
                message.token?.actor?.data.type === "npc" && (message.token.combatant?.initiative ?? null) === null;
            const validRollType = message.isRoll && message.isCheckRoll;
            return validActor && validRollType;
        };

        const canReroll: ContextOptionCondition = (li): boolean => {
            return game.messages.get(li.data("messageId"), { strict: true }).isRerollable;
        };

        const canHeroPointReroll: ContextOptionCondition = (li): boolean => {
            const message = game.messages.get(li.data("messageId"), { strict: true });
            const actor = message.actor;
            return message.isRerollable && !!actor?.isOfType("character") && actor.heroPoints.value > 0;
        };

        const canShowRollDetails: ContextOptionCondition = ($li): boolean => {
            const message = game.messages.get($li.data("messageId"), { strict: true });
            const rulesEnabled = game.settings.get("pf2ettw", "enabledRulesUI");
            return game.user.isGM && rulesEnabled && !!message.data.flags.pf2ettw.context;
        };

        const applyDamage = async ($li: JQuery, multiplier: number): Promise<void> => {
            const messageId = $li.attr("data-message-id") ?? "";
            const roll = game.messages.get(messageId, { strict: true }).roll;
            if (!roll) return;
            for await (const token of canvas.tokens.controlled) {
                if (!token.actor) continue;
                await token.actor.applyDamage(
                    roll.total * multiplier,
                    token,
                    CONFIG.pf2ettw.chatDamageButtonShieldToggle
                );
            }
        };

        const options = super._getEntryContextOptions();
        options.push(
            {
                name: "pf2ettw.ChatRollDetails.Select",
                icon: '<i class="fas fa-search"></i>',
                condition: canShowRollDetails,
                callback: ($li) => {
                    const message = game.messages.get($li.attr("data-message-id") ?? "", { strict: true });
                    message.showDetails();
                },
            },
            {
                name: "pf2ettw.DamageButton.FullContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyDamage,
                callback: (li: JQuery) => applyDamage(li, 1),
            },
            {
                name: "pf2ettw.DamageButton.HalfContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyDamage,
                callback: (li) => applyDamage(li, 0.5),
            },
            {
                name: "pf2ettw.DamageButton.DoubleContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyDamage,
                callback: (li) => applyDamage(li, 2),
            },
            {
                name: "pf2ettw.DamageButton.TripleContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyTripleDamage,
                callback: (li) => applyDamage(li, 3),
            },
            {
                name: "pf2ettw.DamageButton.HealingContext",
                icon: '<i class="fas fa-heart"></i>',
                condition: canApplyDamage,
                callback: (li: JQuery) => applyDamage(li, -1),
            },
            {
                name: "pf2ettw.ClickToSetInitiativeContext",
                icon: '<i class="fas fa-fist-raised"></i>',
                condition: canApplyInitiative,
                callback: ($li) => {
                    const message = game.messages.get($li.attr("data-message-id") ?? "", { strict: true });
                    const roll = message.isRoll ? message.roll : null;
                    if (!roll || Number.isNaN(roll.total || "NaN")) throw Errorpf2ettw("No roll found");

                    const token = message.token;
                    if (!token) {
                        ui.notifications.error(
                            game.i18n.format("pf2ettw.Encounter.NoTokenInScene", {
                                actor: message.actor?.name ?? message.user?.name ?? "",
                            })
                        );
                        return;
                    }

                    token.setInitiative({ initiative: roll.total });
                },
            },
            {
                name: "pf2ettw.RerollMenu.HeroPoint",
                icon: '<i class="fas fa-hospital-symbol"></i>',
                condition: canHeroPointReroll,
                callback: (li) =>
                    Checkpf2ettw.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true }), {
                        heroPoint: true,
                    }),
            },
            {
                name: "pf2ettw.RerollMenu.KeepNew",
                icon: '<i class="fas fa-dice"></i>',
                condition: canReroll,
                callback: (li) =>
                    Checkpf2ettw.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true })),
            },
            {
                name: "pf2ettw.RerollMenu.KeepWorst",
                icon: '<i class="fas fa-dice-one"></i>',
                condition: canReroll,
                callback: (li) =>
                    Checkpf2ettw.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true }), {
                        keep: "worst",
                    }),
            },
            {
                name: "pf2ettw.RerollMenu.KeepBest",
                icon: '<i class="fas fa-dice-six"></i>',
                condition: canReroll,
                callback: (li) =>
                    Checkpf2ettw.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true }), {
                        keep: "best",
                    }),
            }
        );

        return options;
    }
}
