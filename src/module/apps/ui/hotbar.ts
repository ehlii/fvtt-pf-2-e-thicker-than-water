import { SKILL_ABBREVIATIONS } from "@actor/values";
import { Effectpf2ettw, Itempf2ettw } from "@item";
import { Macropf2ettw } from "@module/macro";
import { createActionMacro, createItemMacro, createSkillMacro, createToggleEffectMacro } from "@scripts/macros/hotbar";
import { isObject, setHasElement } from "@util";

class Hotbarpf2ettw extends Hotbar<Macropf2ettw> {
    /** Handle macro creation from non-macros */
    override async _onDrop(event: ElementDragEvent): Promise<void> {
        const li = event.target.closest<HTMLElement>(".macro");
        const slot = Number(li?.dataset.slot) || null;
        if (!slot) return;

        const data: HotbarDropData = TextEditor.getDragEventData(event);
        if (data.type === "Macro") return super._onDrop(event);
        if (Hooks.call("hotbarDrop", this, data, slot) === false) return;

        if (data.type === "Item") {
            const itemId = data.id ?? (isObject<{ _id?: unknown }>(data.data) ? data.data._id : null);

            const prefix =
                typeof data.pack === "string"
                    ? `Compendium.${data.pack}`
                    : typeof data.actorId === "string"
                    ? `Actor.${data.actorId}.Item`
                    : "Item";
            const item = await fromUuid(`${prefix}.${itemId}`);

            if (item instanceof Effectpf2ettw) {
                return createToggleEffectMacro(item, slot);
            } else if (item instanceof Itempf2ettw) {
                return createItemMacro(item.toObject(), slot);
            }
        } else if (data.type === "RollOption" && this.hasRollOptionData(data)) {
            return this.createRollOptionToggleMacro(data, slot);
        } else if (data.type === "Skill" && data.actorId && setHasElement(SKILL_ABBREVIATIONS, data.skill)) {
            const skillName = data.skillName ?? game.i18n.localize(CONFIG.pf2ettw.skills[data.skill]);
            return createSkillMacro(data.skill, skillName, data.actorId, slot);
        } else if (isObject(data.pf2ettw) && data.actorId) {
            if (data.pf2ettw.type === "Action" && typeof data.pf2ettw.index === "number") {
                return createActionMacro(data.pf2ettw.index, data.actorId, slot);
            }
        }
    }

    private hasRollOptionData(data: Record<string, unknown>): data is RollOptionData {
        const { label, actorId, itemId, img, domain, option } = data;
        return (
            typeof label === "string" &&
            label.length > 0 &&
            typeof actorId === "string" &&
            actorId.length === 16 &&
            typeof img === "string" &&
            img.length > 0 &&
            typeof domain === "string" &&
            domain.length > 0 &&
            typeof option === "string" &&
            option.length > 0 &&
            (!("itemId" in data) || (typeof itemId === "string" && itemId.length === 16))
        );
    }

    private async createRollOptionToggleMacro(data: RollOptionData, slot: number): Promise<void> {
        const name = game.i18n.format("pf2ettw.ToggleWithName", { property: data.label });
        const img = data.img ?? "icons/svg/d20-grey.svg";

        const itemId = data.itemId ? `"${data.itemId}"` : null;
        const command = `const actor = game.actors.get("${data.actorId}");
await actor?.toggleRollOption("${data.domain}", "${data.option}", ${itemId});
if (!actor) {
    ui.notifications.error(game.i18n.localize("pf2ettw.MacroActionNoActorError"));
}`;

        const toggleMacro =
            game.macros.find((m) => m.name === name && m.data.command === command) ??
            (await Macropf2ettw.create({ type: "script", name, img, command }, { renderSheet: false })) ??
            null;

        await game.user.assignHotbarMacro(toggleMacro, slot);
    }
}

type HotbarDropData = Partial<DropCanvasData> & {
    pack?: string;
    actorId?: string;
    slot?: number;
    skill?: string;
    skillName?: string;
    pf2ettw?: {
        type: string;
        property: string;
        index?: number;
        label: string;
    };
};

type RollOptionData = {
    label: string;
    actorId: string;
    itemId: string;
    img?: ImagePath;
    domain: string;
    option: string;
};

export { Hotbarpf2ettw };
