import { AmbientLightDocumentpf2ettw } from "@module/scene";
import { LightingLayerpf2ettw } from ".";

export class AmbientLightpf2ettw extends AmbientLight<AmbientLightDocumentpf2ettw> {
    /** Is this light actually a source of darkness? */
    get isDarkness(): boolean {
        return this.source.isDarkness;
    }
}

export interface AmbientLightpf2ettw {
    get layer(): LightingLayerpf2ettw<this>;
}
