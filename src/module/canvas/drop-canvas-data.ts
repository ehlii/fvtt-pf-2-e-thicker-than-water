import type { Itempf2ettw } from "@item";

export type DropCanvasItemDatapf2ettw = DropCanvasData<"Item", Itempf2ettw> & {
    value?: number;
    level?: number;
};

export type DropCanvasDatapf2ettw<T extends string = string, D extends object = object> = T extends "Item"
    ? DropCanvasItemDatapf2ettw
    : DropCanvasData<T, D>;
