import { FogExplorationpf2ettw } from "@module/fog-exploration";
import { Tokenpf2ettw } from "../token";

export class SightLayerpf2ettw extends SightLayer<Tokenpf2ettw, FogExplorationpf2ettw> {
    /** Is the rules-based vision setting enabled? */
    get rulesBasedVision(): boolean {
        const settingEnabled = game.settings.get("pf2ettw", "automation.rulesBasedVision");
        return canvas.ready && !!canvas.scene && this.tokenVision && settingEnabled;
    }

    get hasLowLightVision(): boolean {
        return this.rulesBasedVision && this.sources.some((source) => source.object.hasLowLightVision);
    }

    get hasDarkvision(): boolean {
        return this.rulesBasedVision && this.sources.some((source) => source.object.hasDarkvision);
    }
}
