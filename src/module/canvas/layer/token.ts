import { Tokenpf2ettw } from "../token";

export class TokenLayerpf2ettw<TToken extends Tokenpf2ettw = Tokenpf2ettw> extends TokenLayer<TToken> {
    /** Cycle Z indices of a hovered token stack */
    cycleStack(): boolean {
        const hovered = this._hover;
        if (!hovered) return false;

        const stack = this.placeables.filter((t) => hovered.distanceTo(t) === 0);
        if (stack.length < 2) return false;

        const last = stack.pop();
        if (!last) return false;
        stack.unshift(last);

        for (let i = 0; i < stack.length; i++) {
            stack[i].zIndex = -1 * (stack.length - i);
        }

        return true;
    }
}
