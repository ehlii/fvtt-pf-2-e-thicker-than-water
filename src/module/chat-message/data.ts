import { ItemType } from "@item/data";
import { MagicTradition } from "@item/spell/types";
import { RawModifier } from "@actor/modifiers";
import { DegreeOfSuccessString } from "@system/degree-of-success";
import { CheckRollContextFlag } from "@system/rolls";
import { ChatMessagepf2ettw } from ".";

interface ChatMessageDatapf2ettw<TChatMessage extends ChatMessagepf2ettw = ChatMessagepf2ettw>
    extends foundry.data.ChatMessageData<TChatMessage> {
    readonly _source: ChatMessageSourcepf2ettw;
    flags: ChatMessageFlagspf2ettw;
}

interface ChatMessageSourcepf2ettw extends foundry.data.ChatMessageSource {
    flags: ChatMessageFlagspf2ettw;
}

type ChatMessageFlagspf2ettw = foundry.data.ChatMessageFlags & {
    pf2ettw: {
        damageRoll?: DamageRollFlag;
        context?: CheckRollContextFlag;
        origin?: { type: ItemType; uuid: string } | null;
        casting?: { id: string; tradition: MagicTradition } | null;
        modifierName?: string;
        modifiers?: RawModifier[];
        preformatted?: "flavor" | "content" | "both";
        isFromConsumable?: boolean;
        journalEntry?: DocumentUUID;
        [key: string]: unknown;
    };
    core: NonNullable<foundry.data.ChatMessageFlags["core"]>;
};

interface DamageRollFlag {
    outcome: DegreeOfSuccessString;
    rollMode: RollMode;
    total: number;
    traits: string[];
    types: Record<string, Record<string, number>>;
    diceResults: Record<string, Record<string, number[]>>;
    baseDamageDice: number;
}

export { ChatMessageDatapf2ettw, ChatMessageSourcepf2ettw, ChatMessageFlagspf2ettw, DamageRollFlag };
