import { Characterpf2ettw, NPCpf2ettw } from "@actor";
import { CharacterSheetpf2ettw } from "@actor/character/sheet";
import { RollInitiativeOptionspf2ettw } from "@actor/data";
import { SKILL_DICTIONARY } from "@actor/values";
import { Userpf2ettw } from "@module/user";
import { Scenepf2ettw } from "@scene";
import { Localizepf2ettw } from "@system/localize";
import { Combatantpf2ettw, RolledCombatant } from "./combatant";

export class Encounterpf2ettw extends Combat<Combatantpf2ettw> {
    get active(): boolean {
        return this.data.active;
    }

    /** Sort combatants by initiative rolls, falling back to tiebreak priority and then finally combatant ID (random) */
    protected override _sortCombatants(a: Embedded<Combatantpf2ettw>, b: Embedded<Combatantpf2ettw>): number {
        const resolveTie = (): number => {
            const [priorityA, priorityB] = [a, b].map(
                (combatant): number =>
                    combatant.overridePriority(combatant.initiative ?? 0) ??
                    (combatant.actor && "initiative" in combatant.actor.data.data.attributes
                        ? combatant.actor.data.data.attributes.initiative.tiebreakPriority
                        : 3)
            );
            return priorityA === priorityB ? a.id.localeCompare(b.id) : priorityA - priorityB;
        };
        return typeof a.initiative === "number" && typeof b.initiative === "number" && a.initiative === b.initiative
            ? resolveTie()
            : super._sortCombatants(a, b);
    }

    /** A public method to access _sortCombatants in order to get the combatant with the higher initiative */
    getCombatantWithHigherInit(a: RolledCombatant, b: RolledCombatant): RolledCombatant | null {
        const sortResult = this._sortCombatants(a, b);
        return sortResult > 0 ? b : sortResult < 0 ? a : null;
    }

    /** Exclude orphaned, loot-actor, and minion tokens from combat */
    override async createEmbeddedDocuments(
        embeddedName: "Combatant",
        data: PreCreate<foundry.data.CombatantSource>[],
        context: DocumentModificationContext = {}
    ): Promise<Embedded<Combatantpf2ettw>[]> {
        const createData = data.filter((datum) => {
            const token = canvas.tokens.placeables.find((canvasToken) => canvasToken.id === datum.tokenId);
            if (!token) return false;

            const { actor } = token;
            if (!actor) {
                ui.notifications.warn(`${token.name} has no associated actor.`);
                return false;
            }

            const actorTraits = actor.traits;
            if (actor.type === "loot" || ["minion", "eidolon"].some((t) => actorTraits.has(t))) {
                const translation = Localizepf2ettw.translations.pf2ettw.Encounter.ExcludingFromInitiative;
                const type = game.i18n.localize(
                    actorTraits.has("minion")
                        ? CONFIG.pf2ettw.creatureTraits.minion
                        : actorTraits.has("eidolon")
                        ? CONFIG.pf2ettw.creatureTraits.eidolon
                        : CONFIG.pf2ettw.actorTypes[actor.data.type]
                );
                ui.notifications.info(game.i18n.format(translation, { type, actor: actor.name }));
                return false;
            }
            return true;
        });
        return super.createEmbeddedDocuments(embeddedName, createData, context);
    }

    /** Call hooks for modules on turn change */
    override async nextTurn(): Promise<this> {
        Hooks.call("pf2ettw.endTurn", this.combatant ?? null, this, game.user.id);
        await super.nextTurn();
        Hooks.call("pf2ettw.startTurn", this.combatant ?? null, this, game.user.id);
        return this;
    }

    /** Roll initiative for PCs and NPCs using their prepared roll methods */
    override async rollInitiative(ids: string[], options: RollInitiativeOptionspf2ettw = {}): Promise<this> {
        const combatants = ids.flatMap((id) => this.combatants.get(id) ?? []);
        const fightyCombatants = combatants.filter(
            (combatant): combatant is Embedded<Combatantpf2ettw<Characterpf2ettw | NPCpf2ettw>> =>
                combatant.actor instanceof Characterpf2ettw || combatant.actor instanceof NPCpf2ettw
        );
        const rollResults = await Promise.all(
            fightyCombatants.map((combatant) => {
                const checkType = combatant.actor.data.data.attributes.initiative.ability;
                const skills: Record<string, string | undefined> = SKILL_DICTIONARY;
                const rollOptions = combatant.actor.getRollOptions([
                    "all",
                    "initiative",
                    skills[checkType] ?? checkType,
                ]);
                if (options.secret) rollOptions.push("secret");
                return combatant.actor.data.data.attributes.initiative.roll({
                    options: rollOptions,
                    updateTracker: false,
                    skipDialog: !!options.skipDialog,
                });
            })
        );

        const initiatives = rollResults.flatMap((result) =>
            result ? { id: result.combatant.id, value: result.roll.total } : []
        );

        this.setMultipleInitiatives(initiatives);

        // Roll the rest with the parent method
        const remainingIds = ids.filter((id) => !fightyCombatants.some((c) => c.id === id));
        return super.rollInitiative(remainingIds, options);
    }

    /** Set the initiative of multiple combatants */
    async setMultipleInitiatives(
        initiatives: { id: string; value: number; overridePriority?: number | null }[]
    ): Promise<void> {
        const currentId = this.combatant?.id;
        const updates = initiatives.map((i) => ({
            _id: i.id,
            initiative: i.value,
            flags: {
                pf2ettw: {
                    overridePriority: {
                        [i.value]: i.overridePriority,
                    },
                },
            },
        }));
        await this.updateEmbeddedDocuments("Combatant", updates);
        // Ensure the current turn is preserved
        await this.update({ turn: this.turns.findIndex((c) => c.id === currentId) });
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    /** Disable the initiative button on PC sheets if this was the only encounter */
    protected override _onDelete(options: DocumentModificationContext, userId: string): void {
        super._onDelete(options, userId);

        if (this.started) {
            Hooks.call("pf2ettw.endTurn", this.combatant ?? null, this, userId);
            game.pf2ettw.effectTracker.onEncounterEnd(this);
        }

        // Disable the initiative button if this was the only encounter
        if (!game.combat) {
            const pcSheets = Object.values(ui.windows).filter(
                (sheet): sheet is CharacterSheetpf2ettw => sheet instanceof CharacterSheetpf2ettw
            );
            for (const sheet of pcSheets) {
                sheet.disableInitiativeButton();
            }
        }

        // Clear targets to prevent unintentional targeting in future encounters
        game.user.clearTargets();
    }

    /** Enable the initiative button on PC sheets */
    protected override _onCreate(
        data: foundry.data.CombatSource,
        options: DocumentModificationContext,
        userId: string
    ): void {
        super._onCreate(data, options, userId);

        const pcSheets = Object.values(ui.windows).filter(
            (sheet): sheet is CharacterSheetpf2ettw => sheet instanceof CharacterSheetpf2ettw
        );
        for (const sheet of pcSheets) {
            sheet.enableInitiativeButton();
        }
    }

    /** Call onTurnStart for each rule element on the new turn's actor */
    protected override _onUpdate(
        changed: DeepPartial<foundry.data.CombatSource>,
        options: DocumentModificationContext,
        userId: string
    ): void {
        super._onUpdate(changed, options, userId);

        // No updates necessary if combat hasn't started or this combatant has already had a turn this round
        const combatant = this.combatant;
        const actor = combatant?.actor;
        const noActor = !combatant || !actor || !this.started;
        const alreadyWent = combatant?.roundOfLastTurn === this.round;

        const { previous } = this;
        const isNextRound =
            typeof changed.round === "number" && (previous.round === null || changed.round > previous.round);
        const isNextTurn = typeof changed.turn === "number" && (previous.turn === null || changed.turn > previous.turn);

        // Find the best user to make the update, since players can end turns and this runs for everyone
        const updater = ((): Userpf2ettw | null => {
            if (!actor) return null;
            const userUpdatingThis = game.users.get(userId, { strict: true });

            const activeUsers = game.users.filter((u) => u.active);
            const assignedUser = activeUsers.find((u) => u.character === actor);
            const firstGM = activeUsers.find((u) => u.isGM);
            const anyoneWithPermission = activeUsers.find((u) => actor.canUserModify(u, "update"));
            return userUpdatingThis.active && actor.canUserModify(userUpdatingThis, "update")
                ? userUpdatingThis
                : assignedUser ?? firstGM ?? anyoneWithPermission ?? null;
        })();

        // Update the combatant's data (if necessary), run any turn start events, then update the effect panel
        (async () => {
            if (!noActor && !alreadyWent && (isNextRound || isNextTurn) && game.user === updater) {
                const actorUpdates: Record<string, unknown> = {};

                // Run any turn start events before the effect tracker updates.
                // In pf2ettw rules, the order is interchangeable. We'll need to be more dynamic with this later.
                for (const rule of actor.rules) {
                    await rule.onTurnStart?.(actorUpdates);
                }

                // Now that a user has been found, make the updates if there are any
                await combatant.update({ "flags.pf2ettw.roundOfLastTurn": this.round });
                if (Object.keys(actorUpdates).length > 0) {
                    await actor.update(actorUpdates);
                }
            }

            await game.pf2ettw.effectTracker.refresh();
            game.pf2ettw.effectPanel.refresh();
        })();
    }
}

export interface Encounterpf2ettw {
    readonly data: foundry.data.CombatData<this, Combatantpf2ettw>;

    get scene(): Scenepf2ettw | undefined;

    rollNPC(options: RollInitiativeOptionspf2ettw): Promise<this>;
}
