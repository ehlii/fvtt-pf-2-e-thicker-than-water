import { Actorpf2ettw } from "@actor";
import { Itempf2ettw } from "@item";

type EnfolderableDocumentpf2ettw = Actorpf2ettw | Itempf2ettw | Exclude<EnfolderableDocument, Actor | Item>;

/** An empty subclass, used in the past to work around a Foundry bug and kept in place for later needs */
export class Folderpf2ettw<
    TDocument extends EnfolderableDocumentpf2ettw = EnfolderableDocumentpf2ettw
> extends Folder<TDocument> {
    get flattenedContents(): TDocument[] {
        return [this, ...this.getSubfolders()].flatMap((folder) => folder.contents);
    }
}
