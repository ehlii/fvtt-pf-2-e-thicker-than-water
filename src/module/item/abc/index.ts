import { Itempf2ettw } from "../index";
import type { AncestryData } from "@item/ancestry/data";
import type { BackgroundData } from "@item/background/data";
import type { ClassData } from "@item/class/data";
import { Featpf2ettw } from "@item/feat";

/** Abstract base class representing a Pathfinder (A)ncestry, (B)ackground, or (C)lass */
export abstract class ABCItempf2ettw extends Itempf2ettw {
    getLinkedFeatures(): Embedded<Featpf2ettw>[] {
        if (!this.actor) return [];
        const existingABCIds = this.actor.itemTypes[this.data.type].map((item: Embedded<ABCItempf2ettw>) => item.id);
        return this.actor.itemTypes.feat.filter((feat) => existingABCIds.includes(feat.data.data.location ?? ""));
    }

    protected logAutoChange(this: Embedded<ABCItempf2ettw>, path: string, value: string | number): void {
        if (value === 0) return;
        this.actor.data.data.autoChanges[path] = [
            {
                mode: "upgrade",
                level: 1,
                value: value,
                source: this.name,
            },
        ];
    }
}

export interface ABCItempf2ettw {
    readonly data: AncestryData | BackgroundData | ClassData;
}
