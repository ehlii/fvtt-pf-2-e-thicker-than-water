import { AbilityString } from "@actor/types";
import { Ancestrypf2ettw, Backgroundpf2ettw, Classpf2ettw, Featpf2ettw, Itempf2ettw } from "@item";
import { ABCFeatureEntryData } from "@item/abc/data";
import { FeatType } from "@item/feat/data";
import { Localizepf2ettw } from "@system/localize";
import { ItemSheetpf2ettw } from "../sheet/base";
import { ABCSheetData } from "../sheet/data-types";

type ABCItem = Ancestrypf2ettw | Backgroundpf2ettw | Classpf2ettw;

export abstract class ABCSheetpf2ettw<TItem extends ABCItem> extends ItemSheetpf2ettw<TItem> {
    static override get defaultOptions() {
        return {
            ...super.defaultOptions,
            scrollY: [".item-details"],
            dragDrop: [{ dropSelector: ".item-details" }],
        };
    }

    override async getData(options?: Partial<DocumentSheetOptions>): Promise<ABCSheetData<TItem>> {
        const itemType = this.item.type;

        const sheetData = this.getBaseData(options);
        sheetData.data.items = this.item.toObject().data.items; // Exclude any added during data preparation

        return {
            ...sheetData,
            hasSidebar: itemType === "ancestry",
            sidebarTemplate: () => `systems/pf2ettw/templates/items/${itemType}-sidebar.html`,
            hasDetails: true,
            detailsTemplate: () => `systems/pf2ettw/templates/items/${itemType}-details.html`,
        };
    }

    protected getLocalizedAbilities(traits: { value: AbilityString[] }): { [key: string]: string } {
        if (traits !== undefined && traits.value) {
            if (traits.value.length === 6) return { free: game.i18n.localize("pf2ettw.AbilityFree") };
            return Object.fromEntries(traits.value.map((x: AbilityString) => [x, CONFIG.pf2ettw.abilities[x]]));
        }

        return {};
    }

    /** Is the dropped feat or feature valid for the given section? */
    private isValidDrop(event: ElementDragEvent, feat: Featpf2ettw): boolean {
        const validFeatTypes: FeatType[] = $(event.target).closest(".abc-list").data("valid-drops")?.split(" ") ?? [];
        if (validFeatTypes.includes(feat.featType)) {
            return true;
        }

        const goodTypes = validFeatTypes.map((featType) => game.i18n.localize(CONFIG.pf2ettw.featTypes[featType]));
        if (goodTypes.length === 1) {
            const badType = game.i18n.localize(CONFIG.pf2ettw.featTypes[feat.featType]);
            const warning = game.i18n.format(Localizepf2ettw.translations.pf2ettw.Item.ABC.InvalidDrop, {
                badType,
                goodType: goodTypes[0],
            });
            ui.notifications.warn(warning);
            return false;
        }

        // No feat/feature type restriction value, so let it through
        return true;
    }

    protected override async _onDrop(event: ElementDragEvent): Promise<void> {
        event.preventDefault();
        const dataString = event.dataTransfer?.getData("text/plain");
        const dropData = JSON.parse(dataString ?? "");
        const item = await Itempf2ettw.fromDropData(dropData);

        if (!(item instanceof Featpf2ettw) || !this.isValidDrop(event, item)) {
            return;
        }

        const entry: ABCFeatureEntryData = {
            pack: dropData.pack || undefined,
            id: dropData.id,
            img: item.data.img,
            name: item.name,
            level: item.data.data.level.value,
        };

        const items = this.item.data.data.items;
        const pathPrefix = "data.items";

        let id: string;
        do {
            id = randomID(5);
        } while (items[id]);

        await this.item.update({
            [`${pathPrefix}.${id}`]: entry,
        });
    }

    private removeItem(event: JQuery.ClickEvent) {
        event.preventDefault();
        const target = $(event.target).parents("li");
        const containerId = target.parents("[data-container-id]").data("containerId");
        let path = `-=${target.data("index")}`;
        if (containerId) {
            path = `${containerId}.items.${path}`;
        }

        this.item.update({
            [`data.items.${path}`]: null,
        });
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);
        $html.on("click", "[data-action=remove]", (ev) => this.removeItem(ev));
    }
}
