import { ActionType, BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemSystemSource, ItemTraits } from "@item/data/base";
import { ActionItempf2ettw } from ".";
import { OneToThree } from "@module/data";

type ActionSource = BaseItemSourcepf2ettw<"action", ActionSystemSource>;

type ActionData = Omit<ActionSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<ActionItempf2ettw, "action", ActionSystemData, ActionSource>;

type ActionTrait = keyof Configpf2ettw["pf2ettw"]["actionTraits"];
type ActionTraits = ItemTraits<ActionTrait>;

interface ActionSystemSource extends ItemSystemSource {
    traits: ActionTraits;
    actionType: {
        value: ActionType;
    };
    actionCategory: {
        value: string;
    };
    actions: {
        value: OneToThree | null;
    };
    requirements: {
        value: string;
    };
    trigger: {
        value: string;
    };
    deathNote: boolean;
}

type ActionSystemData = ActionSystemSource;

export { ActionSource, ActionData, ActionTrait, ActionTraits };
