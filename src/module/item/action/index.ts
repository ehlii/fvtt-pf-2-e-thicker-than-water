import { Itempf2ettw } from "@item/base";
import { ActionData } from "./data";
import { OneToThree } from "@module/data";
import { Userpf2ettw } from "@module/user";
import { ActionCost } from "@item/data/base";

export class ActionItempf2ettw extends Itempf2ettw {
    get actionCost(): ActionCost | null {
        const actionType = this.data.data.actionType.value || "passive";
        if (actionType === "passive") return null;

        return {
            type: actionType,
            value: this.data.data.actions.value,
        };
    }

    override prepareData() {
        const data = super.prepareData();

        /**
         * @todo Fill this out like so or whatever we settle on
         * data.data.playMode.encounter ??= false; // etc.
         **/

        return data;
    }

    override getChatData(this: Embedded<ActionItempf2ettw>, htmlOptions: EnrichHTMLOptions = {}) {
        const data = this.data.data;

        // Feat properties
        const properties = [CONFIG.pf2ettw.actionTypes[data.actionType.value]].filter((property) => property);
        const traits = this.traitChatData(CONFIG.pf2ettw.featTraits);
        return this.processChatData(htmlOptions, { ...data, properties, traits });
    }

    protected override async _preUpdate(
        changed: DeepPartial<this["data"]["_source"]>,
        options: DocumentModificationContext<this>,
        user: Userpf2ettw
    ): Promise<void> {
        const actionCount = changed.data?.actions;
        if (actionCount) {
            actionCount.value = (Math.clamped(Number(actionCount.value), 0, 3) || null) as OneToThree | null;
        }
        await super._preUpdate(changed, options, user);
    }
}

export interface ActionItempf2ettw {
    readonly data: ActionData;
}
