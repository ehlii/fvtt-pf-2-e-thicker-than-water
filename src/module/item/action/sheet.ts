import { ActionItempf2ettw } from "@item/action";
import { ItemSheetDatapf2ettw } from "@item/sheet/data-types";
import { createSheetTags, SheetOptions } from "@module/sheet/helpers";
import { getActionIcon } from "@util";
import { ItemSheetpf2ettw } from "../sheet/base";

export class ActionSheetpf2ettw extends ItemSheetpf2ettw<ActionItempf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>): Promise<ActionSheetData> {
        const data: ItemSheetDatapf2ettw<ActionItempf2ettw> = await super.getData(options);

        // Update icon based on the action cost
        data.item.img = getActionIcon(this.item.actionCost);

        return {
            ...data,
            categories: CONFIG.pf2ettw.actionCategories,
            actionTypes: CONFIG.pf2ettw.actionTypes,
            actionsNumber: CONFIG.pf2ettw.actionsNumber,
            actionTraits: CONFIG.pf2ettw.actionTraits,
            skills: CONFIG.pf2ettw.skillList,
            proficiencies: CONFIG.pf2ettw.proficiencyLevels,
            traits: createSheetTags(CONFIG.pf2ettw.actionTraits, data.data.traits),
        };
    }
}

interface ActionSheetData extends ItemSheetDatapf2ettw<ActionItempf2ettw> {
    categories: Configpf2ettw["pf2ettw"]["actionCategories"];
    actionTypes: Configpf2ettw["pf2ettw"]["actionTypes"];
    actionsNumber: Configpf2ettw["pf2ettw"]["actionsNumber"];
    actionTraits: Configpf2ettw["pf2ettw"]["actionTraits"];
    skills: Configpf2ettw["pf2ettw"]["skillList"];
    proficiencies: Configpf2ettw["pf2ettw"]["proficiencyLevels"];
    traits: SheetOptions;
}
