import { CreatureTrait } from "@actor/creature/data";
import { Characterpf2ettw } from "@actor";
import { Size } from "@module/data";
import { ABCItempf2ettw, Featpf2ettw } from "@item";
import { AncestryData } from "./data";
import { sluggify } from "@util";
import { CreatureSensepf2ettw } from "@actor/creature/sense";
import { SIZE_TO_REACH } from "@actor/creature/values";

class Ancestrypf2ettw extends ABCItempf2ettw {
    get traits(): Set<CreatureTrait> {
        return new Set(this.data.data.traits.value);
    }

    get hitPoints(): number {
        return this.data.data.hp;
    }

    get speed(): number {
        return this.data.data.speed;
    }

    get size(): Size {
        return this.data.data.size;
    }

    /** Include all ancestry features in addition to any with the expected location ID */
    override getLinkedFeatures(): Embedded<Featpf2ettw>[] {
        if (!this.actor) return [];

        return Array.from(
            new Set([
                ...super.getLinkedFeatures(),
                ...this.actor.itemTypes.feat.filter((f) => f.featType === "ancestryfeature"),
            ])
        );
    }

    override prepareBaseData(): void {
        super.prepareBaseData();

        for (const boost of Object.values(this.data.data.boosts)) {
            if (boost.value.length === 1) {
                boost.selected = boost.value[0];
            }
        }

        for (const flaw of Object.values(this.data.data.flaws)) {
            if (flaw.value.length === 1) {
                flaw.selected = flaw.value[0];
            }
        }
    }

    /** Prepare a character's data derived from their ancestry */
    override prepareActorData(this: Embedded<Ancestrypf2ettw>): void {
        const { actor } = this;
        if (!(actor instanceof Characterpf2ettw)) {
            console.error("pf2ettw System | Only a character can have an ancestry");
            return;
        }

        actor.ancestry = this;
        const actorData = actor.data;
        const systemData = actorData.data;

        systemData.attributes.ancestryhp = this.hitPoints;
        this.logAutoChange("data.attributes.ancestryhp", this.hitPoints);

        systemData.traits.size.value = this.size;
        this.logAutoChange("data.traits.size.value", this.size);

        const reach = SIZE_TO_REACH[this.size];
        systemData.attributes.reach = { general: reach, manipulate: reach };

        systemData.attributes.speed.value = String(this.speed);

        // Add ability boosts and flaws
        const { build } = actor.data.data;

        for (const list of ["boosts", "flaws", "voluntaryBoosts", "voluntaryFlaws"] as const) {
            const target = ["boosts", "voluntaryBoosts"].includes(list) ? "boosts" : "flaws";
            for (const ability of Object.values(this.data.data[list])) {
                if (ability.selected) {
                    build.abilities[target].ancestry.push(ability.selected);
                }
            }
        }

        // Add languages
        const innateLanguages = this.data.data.languages.value;
        for (const language of innateLanguages) {
            if (!systemData.traits.languages.value.includes(language)) {
                systemData.traits.languages.value.push(language);
            }
        }

        // Add low-light vision or darkvision if the ancestry includes it
        const { senses } = systemData.traits;
        const { vision } = this.data.data;
        if (!(vision === "normal" || senses.some((sense) => sense.type === vision))) {
            senses.push(new CreatureSensepf2ettw({ type: vision, value: "", source: this.name }));
            const senseRollOptions = (actor.rollOptions["sense"] ??= {});
            senseRollOptions[`self:${sluggify(vision)}:from-ancestry`] = true;
        }

        // Add traits from this item
        systemData.traits.traits.value.push(...this.traits);

        const slug = this.slug ?? sluggify(this.name);
        systemData.details.ancestry = { name: this.name, trait: slug };

        // Set self: roll option for this ancestry and its associated traits
        actor.rollOptions.all[`self:ancestry:${slug}`] = true;
        for (const trait of this.traits) {
            actor.rollOptions.all[`self:trait:${trait}`] = true;
        }
    }
}

interface Ancestrypf2ettw {
    readonly data: AncestryData;
}

export { Ancestrypf2ettw };
