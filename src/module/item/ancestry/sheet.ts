import { Ancestrypf2ettw } from "@item/ancestry";
import { ABCSheetpf2ettw } from "@item/abc/sheet";
import { ABCSheetData, AncestrySheetData } from "../sheet/data-types";
import { createSheetOptions } from "@module/sheet/helpers";

export class AncestrySheetpf2ettw extends ABCSheetpf2ettw<Ancestrypf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>): Promise<AncestrySheetData> {
        const data: ABCSheetData<Ancestrypf2ettw> = await super.getData(options);
        const itemData = data.item;

        return {
            ...data,
            selectedBoosts: Object.fromEntries(
                Object.entries(itemData.data.boosts).map(([k, b]) => [k, this.getLocalizedAbilities(b)])
            ),
            selectedFlaws: Object.fromEntries(
                Object.entries(itemData.data.flaws).map(([k, b]) => [k, this.getLocalizedAbilities(b)])
            ),
            rarities: createSheetOptions(CONFIG.pf2ettw.rarityTraits, { value: [itemData.data.traits.rarity] }),
            sizes: createSheetOptions(CONFIG.pf2ettw.actorSizes, { value: [itemData.data.size] }),
            traits: createSheetOptions(CONFIG.pf2ettw.creatureTraits, itemData.data.traits),
            languages: createSheetOptions(CONFIG.pf2ettw.languages, itemData.data.languages),
            additionalLanguages: createSheetOptions(CONFIG.pf2ettw.languages, itemData.data.additionalLanguages),
        };
    }
}
