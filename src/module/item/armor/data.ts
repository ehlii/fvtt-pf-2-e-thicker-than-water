import {
    BasePhysicalItemData,
    BasePhysicalItemSource,
    Investable,
    PhysicalItemTraits,
    PhysicalSystemData,
    PhysicalSystemSource,
} from "@item/physical/data";
import { ZeroToFour, ZeroToThree } from "@module/data";
import type { Localizepf2ettw } from "@module/system/localize";
import type { Armorpf2ettw } from ".";

type ArmorSource = BasePhysicalItemSource<"armor", ArmorSystemSource>;

type ArmorData = Omit<ArmorSource, "data" | "effects" | "flags"> &
    BasePhysicalItemData<Armorpf2ettw, "armor", ArmorSystemData, ArmorSource>;

type ArmorTrait = keyof Configpf2ettw["pf2ettw"]["armorTraits"];
type ArmorTraits = PhysicalItemTraits<ArmorTrait>;

export type ArmorCategory = keyof Configpf2ettw["pf2ettw"]["armorTypes"];
export type ArmorGroup = keyof Configpf2ettw["pf2ettw"]["armorGroups"];
export type BaseArmorType = keyof typeof Localizepf2ettw.translations.pf2ettw.Item.Armor.Base;
export type ResilientRuneType = "" | "resilient" | "greaterResilient" | "majorResilient";

interface ArmorSystemSource extends Investable<PhysicalSystemSource> {
    traits: ArmorTraits;
    armor: {
        value: number;
    };
    category: ArmorCategory;
    group: ArmorGroup | null;
    baseItem: BaseArmorType | null;

    strength: {
        value: number;
    };
    dex: {
        value: number;
    };
    check: {
        value: number;
    };
    speed: {
        value: number;
    };
    potencyRune: {
        value: ZeroToFour;
    };
    resiliencyRune: {
        value: ResilientRuneType | "";
    };
    propertyRune1: {
        value: string;
    };
    propertyRune2: {
        value: string;
    };
    propertyRune3: {
        value: string;
    };
    propertyRune4: {
        value: string;
    };
}

interface ArmorSystemData
    extends Omit<ArmorSystemSource, "price" | "temporary" | "usage">,
        Investable<PhysicalSystemData> {
    baseItem: BaseArmorType;
    traits: ArmorTraits;
    runes: {
        potency: number;
        resilient: ZeroToThree;
        property: string[];
    };
}

const ARMOR_CATEGORIES = ["unarmored", "light", "medium", "heavy"] as const;

export { ArmorData, ArmorSource, ArmorSystemData, ArmorSystemSource, ArmorTrait, ARMOR_CATEGORIES };
