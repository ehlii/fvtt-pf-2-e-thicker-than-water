import { PhysicalItemSheetpf2ettw } from "@item/physical/sheet";
import { Localizepf2ettw } from "@system/localize";
import { getPropertySlots } from "../runes";
import { Armorpf2ettw } from ".";
import { createSheetTags } from "@module/sheet/helpers";

export class ArmorSheetpf2ettw extends PhysicalItemSheetpf2ettw<Armorpf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>) {
        const sheetData = await super.getData(options);

        // Armor property runes
        const totalSlots = getPropertySlots(sheetData.item);
        const propertyRuneSlots: Record<`propertyRuneSlots${number}`, boolean> = {};
        for (const slot of [1, 2, 3, 4]) {
            if (totalSlots >= slot) {
                propertyRuneSlots[`propertyRuneSlots${slot}`] = true;
            }
        }

        return {
            ...sheetData,
            armorPotencyRunes: CONFIG.pf2ettw.armorPotencyRunes,
            armorResiliencyRunes: CONFIG.pf2ettw.armorResiliencyRunes,
            armorPropertyRunes: CONFIG.pf2ettw.armorPropertyRunes,
            categories: CONFIG.pf2ettw.armorTypes,
            groups: CONFIG.pf2ettw.armorGroups,
            baseTypes: Localizepf2ettw.translations.pf2ettw.Item.Armor.Base,
            bulkTypes: CONFIG.pf2ettw.bulkTypes,
            preciousMaterials: CONFIG.pf2ettw.preciousMaterials,
            preciousMaterialGrades: CONFIG.pf2ettw.preciousMaterialGrades,
            sizes: CONFIG.pf2ettw.actorSizes,
            traits: createSheetTags(CONFIG.pf2ettw.armorTraits, sheetData.item.data.traits),
            ...propertyRuneSlots,
        };
    }
}
