import { SkillAbbreviation } from "@actor/creature/data";
import { AbilityString } from "@actor/types";
import { ABCSystemData } from "@item/abc/data";
import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemTraits } from "@item/data/base";
import { Backgroundpf2ettw } from ".";

type BackgroundSource = BaseItemSourcepf2ettw<"background", BackgroundSystemSource>;

type BackgroundData = Omit<BackgroundSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Backgroundpf2ettw, "background", BackgroundSystemData, BackgroundSource>;

interface BackgroundSystemSource extends ABCSystemData {
    traits: ItemTraits;
    boosts: Record<number, { value: AbilityString[]; selected: AbilityString | null }>;
    trainedLore: string;
    trainedSkills: {
        value: SkillAbbreviation[];
    };
}

type BackgroundSystemData = BackgroundSystemSource;

export { BackgroundData, BackgroundSource };
