import { ABCItempf2ettw, Featpf2ettw, Itempf2ettw } from "@item";
import { OneToFour } from "@module/data";
import { BackgroundData } from "./data";

class Backgroundpf2ettw extends ABCItempf2ettw {
    /** Set a skill feat granted by a GrantItem RE as one of this background's configured items */
    override prepareSiblingData(this: Embedded<Backgroundpf2ettw>): void {
        if (Object.keys(this.data.data.items).length > 0) return;
        const grantedSkillFeat = this.data.flags.pf2ettw.itemGrants
            .flatMap((g) => this.actor.items.get(g.id) ?? [])
            .find(
                (i: Embedded<Itempf2ettw> & { featType?: unknown }): i is Embedded<Featpf2ettw> =>
                    i.featType === "skill"
            );

        if (grantedSkillFeat) {
            this.data.data.items["GRANT"] = {
                id: grantedSkillFeat.id,
                img: grantedSkillFeat.img,
                name: grantedSkillFeat.name,
                level: 1,
            };
            grantedSkillFeat.data.data.location = this.id;
        }
    }

    override prepareActorData(this: Embedded<Backgroundpf2ettw>): void {
        if (!this.actor.isOfType("character")) {
            console.error("Only a character can have a background");
            return;
        }

        this.actor.background = this;
        const { build } = this.actor.data.data;

        // Add ability boosts
        const boosts = Object.values(this.data.data.boosts);
        for (const boost of boosts) {
            if (boost.selected) {
                build.abilities.boosts.background.push(boost.selected);
            }
        }

        const { trainedSkills } = this.data.data;
        if (trainedSkills.value.length === 1) {
            const key = trainedSkills.value[0];
            const skill = this.actor.data.data.skills[key];
            skill.rank = Math.max(skill.rank, 1) as OneToFour;
        }
    }
}

interface Backgroundpf2ettw {
    readonly data: BackgroundData;
}

export { Backgroundpf2ettw };
