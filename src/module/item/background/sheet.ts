import { ABCSheetpf2ettw } from "../abc/sheet";
import { Backgroundpf2ettw } from "@item/background";
import { BackgroundSheetData } from "../sheet/data-types";
import { createSheetOptions } from "@module/sheet/helpers";

export class BackgroundSheetpf2ettw extends ABCSheetpf2ettw<Backgroundpf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>): Promise<BackgroundSheetData> {
        const data = await super.getData(options);
        const itemData = data.item;

        return {
            ...data,
            rarities: createSheetOptions(CONFIG.pf2ettw.rarityTraits, { value: [itemData.data.traits.rarity] }),
            trainedSkills: createSheetOptions(CONFIG.pf2ettw.skills, itemData.data.trainedSkills),
            selectedBoosts: Object.fromEntries(
                Object.entries(itemData.data.boosts).map(([k, b]) => [k, this.getLocalizedAbilities(b)])
            ),
        };
    }
}
