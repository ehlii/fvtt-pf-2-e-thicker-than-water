import { PhysicalItempf2ettw } from "@item";
import { BookData } from "./data";

class Bookpf2ettw extends PhysicalItempf2ettw {}

interface Bookpf2ettw extends PhysicalItempf2ettw {
    readonly data: BookData;
}

export { Bookpf2ettw };
