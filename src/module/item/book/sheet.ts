import { PhysicalItemSheetpf2ettw } from "@item/physical/sheet";
import { PhysicalItemSheetData } from "@item/sheet/data-types";
import { Bookpf2ettw } from "./document";

export class BookSheetpf2ettw extends PhysicalItemSheetpf2ettw<Bookpf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>): Promise<PhysicalItemSheetData<Bookpf2ettw>> {
        const data = await super.getData(options);
        return data;
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);
    }
}
