import { AbilityString } from "@actor/types";
import { ABCSystemData } from "@item/abc/data";
import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemTraits } from "@item/data/base";
import { ZeroToFour } from "@module/data";
import type { Classpf2ettw } from ".";
import { CLASS_TRAITS } from "./values";

type ClassSource = BaseItemSourcepf2ettw<"class", ClassSystemSource>;

type ClassData = Omit<ClassSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Classpf2ettw, "class", ClassSystemData, ClassSource>;

interface ClassSystemSource extends ABCSystemData {
    traits: ItemTraits;
    keyAbility: { value: AbilityString[]; selected: AbilityString | null };
    hp: number;
    perception: ZeroToFour;
    savingThrows: {
        fortitude: ZeroToFour;
        reflex: ZeroToFour;
        will: ZeroToFour;
    };
    attacks: {
        simple: ZeroToFour;
        martial: ZeroToFour;
        advanced: ZeroToFour;
        unarmed: ZeroToFour;
        other: { name: string; rank: ZeroToFour };
    };
    defenses: {
        unarmored: ZeroToFour;
        light: ZeroToFour;
        medium: ZeroToFour;
        heavy: ZeroToFour;
    };
    trainedSkills: {
        value: string[];
        additional: number;
    };
    classDC: ZeroToFour;
    ancestryFeatLevels: { value: number[] };
    classFeatLevels: { value: number[] };
    generalFeatLevels: { value: number[] };
    skillFeatLevels: { value: number[] };
    skillIncreaseLevels: { value: number[] };
    vampireFeatLevels: { value: number[] };
}

type ClassSystemData = ClassSystemSource;

type ClassTrait = SetElement<typeof CLASS_TRAITS>;

export { ClassData, ClassSource, ClassSystemData, ClassTrait };
