import { Classpf2ettw } from "@item/class";
import { createSheetOptions, createSheetTags } from "@module/sheet/helpers";
import { ABCSheetpf2ettw } from "../abc/sheet";
import { ClassSheetData } from "./types";

export class ClassSheetpf2ettw extends ABCSheetpf2ettw<Classpf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>): Promise<ClassSheetData> {
        const data = await super.getData(options);
        const itemData = data.item;

        const items = Object.entries(data.data.items)
            .map(([key, item]) => ({ key, item }))
            .sort((first, second) => first.item.level - second.item.level);

        return {
            ...data,
            items,
            rarities: createSheetOptions(CONFIG.pf2ettw.rarityTraits, { value: [itemData.data.traits.rarity] }),
            skills: CONFIG.pf2ettw.skills,
            proficiencyChoices: CONFIG.pf2ettw.proficiencyLevels,
            selectedKeyAbility: this.getLocalizedAbilities(itemData.data.keyAbility),
            ancestryTraits: createSheetTags(CONFIG.pf2ettw.ancestryItemTraits, itemData.data.traits),
            trainedSkills: createSheetTags(CONFIG.pf2ettw.skills, itemData.data.trainedSkills),
            ancestryFeatLevels: createSheetTags(CONFIG.pf2ettw.levels, itemData.data.ancestryFeatLevels),
            classFeatLevels: createSheetTags(CONFIG.pf2ettw.levels, itemData.data.classFeatLevels),
            generalFeatLevels: createSheetTags(CONFIG.pf2ettw.levels, itemData.data.generalFeatLevels),
            skillFeatLevels: createSheetTags(CONFIG.pf2ettw.levels, itemData.data.skillFeatLevels),
            skillIncreaseLevels: createSheetTags(CONFIG.pf2ettw.levels, itemData.data.skillIncreaseLevels),
        };
    }
}
