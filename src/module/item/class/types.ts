import { ABCFeatureEntryData } from "@item/abc/data";
import { ABCSheetData } from "@item/sheet/data-types";
import { SheetOptions } from "@module/sheet/helpers";
import { Classpf2ettw } from ".";

interface ClassSheetData extends ABCSheetData<Classpf2ettw> {
    rarities: SheetOptions;
    items: { key: string; item: ABCFeatureEntryData }[];
    skills: typeof CONFIG.pf2ettw.skills;
    proficiencyChoices: typeof CONFIG.pf2ettw.proficiencyLevels;
    selectedKeyAbility: Record<string, string>;
    ancestryTraits: SheetOptions;
    trainedSkills: SheetOptions;
    ancestryFeatLevels: SheetOptions;
    classFeatLevels: SheetOptions;
    generalFeatLevels: SheetOptions;
    skillFeatLevels: SheetOptions;
    skillIncreaseLevels: SheetOptions;
}

export { ClassSheetData };
