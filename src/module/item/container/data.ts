import { EquipmentTrait } from "@item/equipment/data";
import {
    BasePhysicalItemData,
    BasePhysicalItemSource,
    Investable,
    PhysicalItemTraits,
    PhysicalSystemData,
    PhysicalSystemSource,
} from "@item/physical/data";
import { Containerpf2ettw } from ".";

type ContainerSource = BasePhysicalItemSource<"backpack", ContainerSystemSource>;

type ContainerData = Omit<ContainerSource, "data" | "effects" | "flags"> &
    BasePhysicalItemData<Containerpf2ettw, "backpack", ContainerSystemData, ContainerSource>;

type ContainerTraits = PhysicalItemTraits<EquipmentTrait>;

interface ContainerSystemSource extends Investable<PhysicalSystemSource> {
    traits: ContainerTraits;
    stowing: boolean;
    bulkCapacity: {
        value: string | null;
    };
    collapsed: boolean;
}

type ContainerSystemData = Omit<ContainerSystemSource, "price"> & PhysicalSystemData;

export { ContainerData, ContainerSource };
