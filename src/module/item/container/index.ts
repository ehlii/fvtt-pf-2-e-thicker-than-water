import { InventoryBulk } from "@actor/inventory";
import { EquipmentTrait } from "@item/equipment/data";
import { PhysicalItempf2ettw } from "@item/physical";
import { Bulk, weightToBulk } from "@item/physical/bulk";
import { ContainerData } from "./data";
import { hasExtraDimensionalParent } from "./helpers";

class Containerpf2ettw extends PhysicalItempf2ettw {
    /** This container's contents, reloaded every data preparation cycle */
    contents: Collection<Embedded<PhysicalItempf2ettw>> = new Collection();

    /** Is this an actual stowing container or merely one of the old pouches/quivers/etc.? */
    get stowsItems(): boolean {
        return this.data.data.stowing;
    }

    get isCollapsed(): boolean {
        return this.data.data.collapsed;
    }

    get capacity(): { value: Bulk; max: Bulk } {
        return {
            value: InventoryBulk.computeTotalBulk(this.contents.contents, this.actor?.size ?? "med"),
            max: weightToBulk(this.data.data.bulkCapacity.value) || new Bulk(),
        };
    }

    get capacityPercentage(): number {
        const { value, max } = this.capacity;
        return Math.min(100, Math.floor((value.toLightBulk() / max.toLightBulk()) * 100));
    }

    override get bulk(): Bulk {
        const canReduceBulk = !this.traits.has("extradimensional") || !hasExtraDimensionalParent(this);
        const reduction = canReduceBulk ? weightToBulk(this.data.data.negateBulk.value) : new Bulk();
        return super.bulk.plus(this.capacity.value.minus(reduction ?? new Bulk()));
    }

    /** Reload this container's contents following Actor embedded-document preparation */
    override prepareSiblingData(this: Embedded<Containerpf2ettw>): void {
        this.contents = new Collection(
            this.actor.inventory.filter((item) => item.container?.id === this.id).map((item) => [item.id, item])
        );
    }

    /** Move the contents of this container into the next-higher container or otherwise the main actor inventory */
    async ejectContents(): Promise<void> {
        if (!this.actor) return;

        const updates = this.contents.map((i) => ({ _id: i.id, "data.containerId": this.container?.id ?? null }));
        await this.actor.updateEmbeddedDocuments("Item", updates, { render: false });
    }

    override getChatData(
        this: Embedded<Containerpf2ettw>,
        htmlOptions: EnrichHTMLOptions = {}
    ): Record<string, unknown> {
        const data = this.data.data;
        const traits = this.traitChatData(CONFIG.pf2ettw.equipmentTraits);

        return this.processChatData(htmlOptions, { ...data, traits });
    }
}

interface Containerpf2ettw {
    readonly data: ContainerData;

    get traits(): Set<EquipmentTrait>;
}

export { Containerpf2ettw };
