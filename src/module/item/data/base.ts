import { CreatureTrait } from "@actor/creature/data";
import type { Itempf2ettw } from "@item/base";
import type { ActiveEffectpf2ettw } from "@module/active-effect";
import { RuleElementSource } from "@module/rules";
import { DocumentSchemaRecord, OneToThree, Rarity, ValuesList } from "@module/data";
import { ItemType } from ".";
import { PhysicalItemTrait } from "../physical/data";
import { NPCAttackTrait } from "@item/melee/data";
import { ActionTrait } from "@item/action/data";

interface BaseItemSourcepf2ettw<
    TType extends ItemType = ItemType,
    TSystemSource extends ItemSystemSource = ItemSystemSource
> extends foundry.data.ItemSource<TType, TSystemSource> {
    flags: ItemSourceFlagspf2ettw;
}

interface BaseItemDatapf2ettw<
    TItem extends Itempf2ettw = Itempf2ettw,
    TType extends ItemType = ItemType,
    TSystemData extends ItemSystemData = ItemSystemData,
    TSource extends BaseItemSourcepf2ettw<TType> = BaseItemSourcepf2ettw<TType>
> extends Omit<BaseItemSourcepf2ettw<TType, ItemSystemSource>, "effects">,
        foundry.data.ItemData<TItem, ActiveEffectpf2ettw> {
    readonly type: TType;
    readonly data: TSystemData;
    flags: ItemFlagspf2ettw;

    readonly _source: TSource;
}

type ItemTrait = ActionTrait | CreatureTrait | PhysicalItemTrait | NPCAttackTrait;

type ActionType = keyof Configpf2ettw["pf2ettw"]["actionTypes"];

interface ActionCost {
    type: ActionType;
    value: OneToThree | null;
}

interface ItemTraits<T extends ItemTrait = ItemTrait> extends ValuesList<T> {
    rarity: Rarity;
}

interface ItemFlagspf2ettw extends foundry.data.ItemFlags {
    pf2ettw: {
        rulesSelections: Record<string, string | number | object>;
        itemGrants: ItemGrantData[];
        grantedBy: ItemGrantData | null;
        [key: string]: unknown;
    };
}

interface ItemSourceFlagspf2ettw extends DeepPartial<foundry.data.ItemFlags> {
    pf2ettw?: {
        rulesSelections?: Record<string, string | number | object>;
        itemGrants?: ItemGrantSource[];
        grantedBy?: ItemGrantSource | null;
        [key: string]: unknown;
    };
}

type ItemGrantData = Required<ItemGrantSource>;

interface ItemGrantSource {
    id: string;
    onDelete?: ItemGrantDeleteAction;
}

type ItemGrantDeleteAction = "cascade" | "detach" | "restrict";

interface ItemLevelData {
    level: {
        value: number;
    };
}

interface ItemSystemSource {
    description: {
        value: string;
    };
    source: {
        value: string;
    };
    traits?: ItemTraits;
    options?: {
        value: string[];
    };
    rules: RuleElementSource[];
    slug: string | null;
    schema: DocumentSchemaRecord;
}

type ItemSystemData = ItemSystemSource;

export {
    ActionCost,
    ActionType,
    BaseItemDatapf2ettw,
    BaseItemSourcepf2ettw,
    ItemFlagspf2ettw,
    ItemGrantData,
    ItemGrantDeleteAction,
    ItemGrantSource,
    ItemLevelData,
    ItemSystemData,
    ItemSystemSource,
    ItemTrait,
    ItemTraits,
};
