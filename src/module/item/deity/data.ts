import { SkillAbbreviation } from "@actor/creature/data";
import { Alignment } from "@actor/creature/types";
import { AbilityString } from "@actor/types";
import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemSystemSource } from "@item/data/base";
import { BaseWeaponType } from "@item/weapon/types";
import type { Deitypf2ettw } from "./document";
import { DeityDomain } from "./types";

type DeitySource = BaseItemSourcepf2ettw<"deity", DeitySystemSource>;

type DeityData = Omit<DeitySource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Deitypf2ettw, "deity", DeitySystemData, DeitySource>;

interface DeitySystemSource extends ItemSystemSource {
    alignment: {
        own: Alignment | null;
        follower: Alignment[];
    };
    domains: {
        primary: DeityDomain[];
        alternate: DeityDomain[];
    };
    font: DivineFonts;
    ability: AbilityString[];
    skill: SkillAbbreviation | null;
    weapons: BaseWeaponType[];
    spells: Record<number, ItemUUID>;
    traits?: never;
}

type DivineFonts = ["harm"] | ["heal"] | ["harm", "heal"];

type DeitySystemData = DeitySystemSource;

export { DeityData, DeitySource, DeitySystemData, DeitySystemSource };
