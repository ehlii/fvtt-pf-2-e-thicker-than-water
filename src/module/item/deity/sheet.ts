import { SkillAbbreviation } from "@actor/creature/data";
import { Alignment } from "@actor/creature/types";
import { Deitypf2ettw, Itempf2ettw, Spellpf2ettw } from "@item";
import { ItemSheetpf2ettw } from "@item/sheet/base";
import { ItemSheetDatapf2ettw } from "@item/sheet/data-types";
import { createSheetOptions, SheetOptions } from "@module/sheet/helpers";
import { Errorpf2ettw, tagify } from "@util";
import { fromUUIDs } from "@util/from-uuids";

export class DeitySheetpf2ettw<TItem extends Deitypf2ettw = Deitypf2ettw> extends ItemSheetpf2ettw<TItem> {
    static override get defaultOptions(): DocumentSheetOptions {
        return {
            ...super.defaultOptions,
            scrollY: [".item-details"],
            dragDrop: [{ dropSelector: ".sheet-header, .sheet-content" }],
        };
    }

    override async getData(options?: Partial<DocumentSheetOptions>): Promise<DeitySheetData> {
        const sheetData = super.getBaseData(options);

        const spellEntries = Object.entries(sheetData.data.spells);
        const spells = (await fromUUIDs(Object.values(sheetData.data.spells)))
            .filter((i): i is Spellpf2ettw => i instanceof Spellpf2ettw)
            .map((spell) => {
                const level = Number(spellEntries.find(([, uuid]) => uuid === spell.uuid)?.at(0));
                return { uuid: spell.uuid, level, name: spell.name, img: spell.img };
            })
            .sort((spellA, spellB) => spellA.level - spellB.level);

        return {
            ...sheetData,
            hasDetails: true,
            detailsTemplate: () => "systems/pf2ettw/templates/items/deity-details.html",
            alignments: CONFIG.pf2ettw.alignments,
            skills: CONFIG.pf2ettw.skills,
            divineFonts: createSheetOptions(
                { harm: "pf2ettw.Item.Deity.DivineFont.Harm", heal: "pf2ettw.Item.Deity.DivineFont.Heal" },
                sheetData.data.font
            ),
            spells,
        };
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        // Create tagify selection inputs
        const html = $html.get(0)!;
        const getInput = (name: string): HTMLInputElement | null =>
            html.querySelector<HTMLInputElement>(`input[name="${name}"]`);

        tagify(getInput("data.ability"), { whitelist: CONFIG.pf2ettw.abilities, maxTags: 2 });
        tagify(getInput("data.alignment.follower"), { whitelist: CONFIG.pf2ettw.alignments, maxTags: 9 });
        tagify(getInput("data.weapons"), { whitelist: CONFIG.pf2ettw.baseWeaponTypes, maxTags: 2 });
        tagify(getInput("data.domains.primary"), { whitelist: CONFIG.pf2ettw.deityDomains, maxTags: 4 });
        tagify(getInput("data.domains.alternate"), { whitelist: CONFIG.pf2ettw.deityDomains, maxTags: 4 });

        const $clericSpells = $html.find(".cleric-spells");
        // View one of the spells
        $clericSpells.find("a[data-action=view-spell]").on("click", async (event): Promise<void> => {
            const $target = $(event.currentTarget);
            const uuid = $target.closest("li").attr("data-uuid") ?? "";
            const spell = await fromUuid(uuid);
            if (!(spell instanceof Spellpf2ettw)) {
                this.render(false);
                return ui.notifications.error(`A spell with the UUID "${uuid}" no longer exists`);
            }

            spell.sheet.render(true);
        });

        // Remove a stored spell reference
        $clericSpells.find("a[data-action=remove-spell]").on("click", async (event): Promise<void> => {
            const $target = $(event.currentTarget);
            const uuidToRemove = $target.closest("li").attr("data-uuid") ?? "";
            const [levelToRemove] =
                Object.entries(this.item.data.data.spells).find(([_level, uuid]) => uuid === uuidToRemove) ?? [];
            if (!levelToRemove) {
                this.render(false);
                return;
            }

            await this.item.update({ [`data.spells.-=${levelToRemove}`]: null });
        });

        // Update the level of a spell
        $clericSpells
            .find<HTMLInputElement>("input[data-action=update-spell-level]")
            .on("change", async (event): Promise<void> => {
                const oldLevel = Number(event.target.dataset.level);
                const uuid = this.item.data.data.spells[oldLevel];
                // Shouldn't happen unless the sheet falls out of sync
                if (!uuid) {
                    this.render(false);
                    return;
                }

                const newLevel = Math.clamped(Number(event.target.value) || 1, 1, 10);
                if (oldLevel !== newLevel) {
                    await this.item.update({ [`data.spells.-=${oldLevel}`]: null, [`data.spells.${newLevel}`]: uuid });
                }
            });
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    override async _onDrop(event: ElementDragEvent): Promise<void> {
        if (!this.isEditable) return;

        const item = await (async (): Promise<Itempf2ettw | null> => {
            try {
                const dataString = event.dataTransfer?.getData("text/plain");
                const dropData = JSON.parse(dataString ?? "");
                return (await Itempf2ettw.fromDropData(dropData)) ?? null;
            } catch {
                return null;
            }
        })();
        if (!(item instanceof Spellpf2ettw)) throw Errorpf2ettw("Invalid item drop on deity sheet");

        if (item.isCantrip || item.isFocusSpell || item.isRitual) {
            ui.notifications.error("pf2ettw.Item.Deity.ClericSpells.DropError", { localize: true });
            return;
        }

        await this.item.update({ [`data.spells.${item.level}`]: item.uuid });
    }

    /** Foundry inflexibly considers checkboxes to be booleans: set back to a string tuple for Divine Font */
    override async _updateObject(event: Event, formData: Record<string, unknown>): Promise<void> {
        // Null out empty strings for some properties
        for (const property of ["data.alignment.own", "data.skill"]) {
            if (typeof formData[property] === "string") formData[property] ||= null;
        }

        // Process Tagify outputs
        const inputNames = [
            "data.ability",
            "data.alignment.follower",
            "data.weapons",
            "data.domains.primary",
            "data.domains.alternate",
        ] as const;
        for (const path of inputNames) {
            const selections = formData[path];
            if (Array.isArray(selections)) {
                formData[path] = selections.map((w: { id: string }) => w.id);
            }
        }

        return super._updateObject(event, formData);
    }
}

interface DeitySheetData extends ItemSheetDatapf2ettw<Deitypf2ettw> {
    alignments: Record<Alignment, string>;
    skills: Record<SkillAbbreviation, string>;
    divineFonts: SheetOptions;
    spells: SpellBrief[];
}

interface SpellBrief {
    uuid: ItemUUID;
    level: number;
    name: string;
    img: ImagePath;
}
