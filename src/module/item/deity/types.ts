import enJSON from "../../../../static/lang/en.json";

type DeityDomain = Lowercase<keyof typeof enJSON["pf2ettw"]["Item"]["Deity"]["Domain"]>;

export { DeityDomain };
