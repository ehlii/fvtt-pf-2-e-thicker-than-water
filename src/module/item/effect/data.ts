import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemLevelData, ItemSystemData } from "@item/data/base";
import { OneToFour } from "@module/data";
import { Effectpf2ettw } from ".";

type EffectSource = BaseItemSourcepf2ettw<"effect", EffectSystemSource>;

type EffectData = Omit<EffectSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Effectpf2ettw, "effect", EffectSystemData, EffectSource>;

interface EffectSystemSource extends ItemSystemData, ItemLevelData {
    start: {
        value: number;
        initiative: number | null;
    };
    duration: {
        value: number;
        unit: "rounds" | "minutes" | "hours" | "days" | "encounter" | "unlimited";
        sustained: boolean;
        expiry: EffectExpiryType | null;
    };
    tokenIcon: {
        show: boolean;
    };
    target: string | null;
    expired?: boolean;
    badge: EffectBadge | null;
}

interface EffectSystemData extends ItemSystemData, EffectSystemSource {
    expired: boolean;
    remaining: string;
}

interface EffectBadge {
    value: number | DiceExpression;
    tickRule: EffectTickType;
}

type EffectExpiryType = "turn-start" | "turn-end";

type EffectTickType = "turn-start";

type DieFaceCount = 4 | 6 | 8 | 10 | 12 | 20;
type DiceExpression = `${OneToFour | ""}d${DieFaceCount}`;

export { EffectBadge, EffectData, EffectExpiryType, EffectSource, EffectSystemData, EffectTickType, DiceExpression };
