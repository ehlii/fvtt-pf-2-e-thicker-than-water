import {
    BasePhysicalItemData,
    BasePhysicalItemSource,
    Investable,
    PhysicalItemTraits,
    PhysicalSystemData,
    PhysicalSystemSource,
} from "@item/physical/data";
import type { Equipmentpf2ettw } from ".";

type EquipmentSource = BasePhysicalItemSource<"equipment", EquipmentSystemSource>;

type EquipmentData = Omit<EquipmentSource, "data" | "effects" | "flags"> &
    BasePhysicalItemData<Equipmentpf2ettw, "equipment", EquipmentSystemData, EquipmentSource>;

type EquipmentTrait = keyof Configpf2ettw["pf2ettw"]["equipmentTraits"];
type EquipmentTraits = PhysicalItemTraits<EquipmentTrait>;

interface EquipmentSystemSource extends Investable<PhysicalSystemSource> {
    traits: EquipmentTraits;
}

type EquipmentSystemData = Omit<EquipmentSystemSource, "price"> & PhysicalSystemData;

export { EquipmentData, EquipmentSource, EquipmentTrait, EquipmentTraits, EquipmentSystemData, EquipmentSystemSource };
