import { Localizepf2ettw } from "@module/system/localize";
import { objectHasKey } from "@util";
import { PhysicalItempf2ettw } from "../physical";
import { EquipmentData, EquipmentTrait } from "./data";

class Equipmentpf2ettw extends PhysicalItempf2ettw {
    override getChatData(
        this: Embedded<Equipmentpf2ettw>,
        htmlOptions: EnrichHTMLOptions = {}
    ): Record<string, unknown> {
        const data = this.data.data;
        const traits = this.traitChatData(CONFIG.pf2ettw.equipmentTraits);
        const properties = [this.isEquipped ? game.i18n.localize("pf2ettw.EquipmentEquippedLabel") : null].filter(
            (p) => p
        );
        return this.processChatData(htmlOptions, { ...data, properties, traits });
    }

    override generateUnidentifiedName({ typeOnly = false }: { typeOnly?: boolean } = { typeOnly: false }): string {
        const translations = Localizepf2ettw.translations.pf2ettw.identification;
        const slotType = /book\b/.test(this.slug ?? "")
            ? "Book"
            : /\bring\b/.test(this.slug ?? "")
            ? "Ring"
            : this.data.data.usage.value?.replace(/^worn/, "").capitalize() ?? "";

        const itemType = objectHasKey(translations.UnidentifiedType, slotType)
            ? translations.UnidentifiedType[slotType]
            : translations.UnidentifiedType.Object;

        if (typeOnly) return itemType;

        const formatString = translations.UnidentifiedItem;
        return game.i18n.format(formatString, { item: itemType });
    }
}

interface Equipmentpf2ettw {
    readonly data: EquipmentData;

    get traits(): Set<EquipmentTrait>;
}

export { Equipmentpf2ettw };
