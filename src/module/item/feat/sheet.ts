import { Featpf2ettw } from "@item/feat";
import { FeatSheetData, ItemSheetDatapf2ettw } from "../sheet/data-types";
import { ItemSheetpf2ettw } from "../sheet/base";
import { createSheetOptions, createSheetTags } from "@module/sheet/helpers";

export class FeatSheetpf2ettw extends ItemSheetpf2ettw<Featpf2ettw> {
    override async getData(options?: Partial<DocumentSheetOptions>): Promise<FeatSheetData> {
        const data: ItemSheetDatapf2ettw<Featpf2ettw> = await super.getData(options);

        const hasLineageTrait = this.item.traits.has("lineage");

        return {
            ...data,
            itemType: game.i18n.localize(this.item.isFeature ? "pf2ettw.LevelLabel" : "ITEM.TypeFeat"),
            featTypes: CONFIG.pf2ettw.featTypes,
            actionTypes: CONFIG.pf2ettw.actionTypes,
            actionsNumber: CONFIG.pf2ettw.actionsNumber,
            categories: CONFIG.pf2ettw.actionCategories,
            damageTypes: { ...CONFIG.pf2ettw.damageTypes, ...CONFIG.pf2ettw.healingTypes },
            prerequisites: JSON.stringify(this.item.data.data.prerequisites?.value ?? []),
            rarities: createSheetOptions(CONFIG.pf2ettw.rarityTraits, { value: [data.data.traits.rarity] }),
            traits: createSheetTags(CONFIG.pf2ettw.featTraits, data.data.traits),
            isFeat: this.item.isFeat,
            mandatoryTakeOnce: hasLineageTrait || data.data.onlyLevel1,
            hasLineageTrait,
        };
    }
}
