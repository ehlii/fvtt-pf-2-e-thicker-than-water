const FEAT_TYPES = new Set([
    "ancestry",
    "ancestryfeature",
    "class",
    "classfeature",
    "skill",
    "general",
    "archetype",
    "bonus",
    "pfsboon",
    "deityboon",
    "curse",
    "vampire",
] as const);

export { FEAT_TYPES };
