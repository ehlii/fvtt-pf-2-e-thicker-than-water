import { CreatureTraits } from "@item/ancestry/data";
import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemSystemData } from "@item/data/base";
import type { Heritagepf2ettw } from "./document";

type HeritageSource = BaseItemSourcepf2ettw<"heritage", HeritageSystemSource>;

type HeritageData = Omit<HeritageSource, "heritages" | "flags"> &
    BaseItemDatapf2ettw<Heritagepf2ettw, "heritage", HeritageSystemData, HeritageSource>;

interface HeritageSystemSource extends ItemSystemData {
    ancestry: {
        name: string;
        uuid: ItemUUID;
    } | null;
    traits: CreatureTraits;
}

export type HeritageSystemData = HeritageSystemSource;

export { HeritageData, HeritageSource, HeritageSystemSource };
