import { Characterpf2ettw } from "@actor";
import { CreatureTrait } from "@actor/creature/data";
import { Itempf2ettw } from "@item";
import { Rarity } from "@module/data";
import { sluggify } from "@util";
import { HeritageData } from "./data";

class Heritagepf2ettw extends Itempf2ettw {
    get traits(): Set<CreatureTrait> {
        return new Set(this.data.data.traits.value);
    }

    get rarity(): Rarity {
        return this.data.data.traits.rarity;
    }

    /** Prepare a character's data derived from their heritage */
    override prepareActorData(this: Embedded<Heritagepf2ettw>): void {
        this.actor.heritage = this;
        const systemData = this.actor.data.data;
        // Add and remove traits as specified
        systemData.traits.traits.value.push(...this.traits);

        const slug = this.slug ?? sluggify(this.name);
        systemData.details.heritage = {
            name: this.name,
            trait: slug in CONFIG.pf2ettw.ancestryTraits ? slug : null,
        };

        // Add a self: roll option for this heritage
        this.actor.rollOptions.all[`self:heritage:${slug}`] = true;
    }
}

interface Heritagepf2ettw extends Itempf2ettw {
    readonly parent: Characterpf2ettw | null;

    readonly data: HeritageData;
}

export { Heritagepf2ettw };
