import { Ancestrypf2ettw, Heritagepf2ettw, Itempf2ettw } from "@item";
import { ItemSheetpf2ettw } from "@item/sheet/base";
import { HeritageSheetData } from "@item/sheet/data-types";
import { createSheetOptions } from "@module/sheet/helpers";
import { Errorpf2ettw } from "@util";

export class HeritageSheetpf2ettw extends ItemSheetpf2ettw<Heritagepf2ettw> {
    static override get defaultOptions(): DocumentSheetOptions {
        return {
            ...super.defaultOptions,
            dragDrop: [{ dropSelector: ".sheet-sidebar" }],
        };
    }

    override async getData(options?: Partial<DocumentSheetOptions>): Promise<HeritageSheetData> {
        const sheetData = super.getBaseData(options);
        const ancestry = await (async (): Promise<Ancestrypf2ettw | null> => {
            const item = this.item.data.data.ancestry ? await fromUuid(this.item.data.data.ancestry.uuid) : null;
            return item instanceof Ancestrypf2ettw ? item : null;
        })();

        return {
            ...sheetData,
            ancestry,
            ancestryRefBroken: !!sheetData.data.ancestry && ancestry === null,
            hasSidebar: true,
            hasDetails: false,
            sidebarTemplate: () => "systems/pf2ettw/templates/items/heritage-sidebar.html",
            rarities: createSheetOptions(CONFIG.pf2ettw.rarityTraits, { value: [sheetData.data.traits.rarity] }),
            traits: createSheetOptions(CONFIG.pf2ettw.featTraits, sheetData.data.traits),
        };
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        // Remove ancestry reference
        $html.find('a[data-action="remove-ancestry"]').on("click", () => {
            this.item.update({ "data.ancestry": null });
        });
    }

    override async _onDrop(event: ElementDragEvent): Promise<void> {
        const item = await (async (): Promise<Itempf2ettw | null> => {
            try {
                const dataString = event.dataTransfer?.getData("text/plain");
                const dropData = JSON.parse(dataString ?? "");
                return (await Itempf2ettw.fromDropData(dropData)) ?? null;
            } catch {
                return null;
            }
        })();
        if (!(item instanceof Ancestrypf2ettw)) {
            throw Errorpf2ettw("Invalid item drop on heritage sheet");
        }
        const ancestryReference = { name: item.name, uuid: item.uuid };
        await this.item.update({ "data.ancestry": ancestryReference });
    }
}
