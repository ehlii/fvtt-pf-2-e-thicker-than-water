import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemSystemSource } from "@item/data/base";
import { PhysicalItemTraits, PartialPrice } from "@item/physical/data";
import type { Kitpf2ettw } from ".";

type KitSource = BaseItemSourcepf2ettw<"kit", KitSystemSource>;

type KitData = Omit<KitSource, "effects" | "flags"> & BaseItemDatapf2ettw<Kitpf2ettw, "kit", KitSystemData, KitSource>;

interface KitEntryData {
    pack?: string;
    id: string;
    img: ImagePath;
    quantity: number;
    name: string;
    isContainer: boolean;
    items?: Record<string, KitEntryData>;
}

interface KitSystemSource extends ItemSystemSource {
    traits: PhysicalItemTraits;
    items: Record<string, KitEntryData>;
    price: PartialPrice;
}

type KitSystemData = KitSystemSource;

export { KitData, KitEntryData, KitSource, KitSystemData, KitSystemSource };
