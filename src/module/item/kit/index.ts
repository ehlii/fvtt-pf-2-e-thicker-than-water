import { Actorpf2ettw } from "@actor/index";
import { Containerpf2ettw, Itempf2ettw, PhysicalItempf2ettw } from "@item/index";
import { Price } from "@item/physical/data";
import { Coinspf2ettw } from "@item/physical/helpers";
import { DENOMINATIONS } from "@item/physical/values";
import { Userpf2ettw } from "@module/user";
import { Errorpf2ettw, isObject } from "@util";
import { fromUUIDs } from "@util/from-uuids";
import { KitData, KitEntryData } from "./data";

class Kitpf2ettw extends Itempf2ettw {
    get entries(): KitEntryData[] {
        return Object.values(this.data.data.items);
    }

    get price(): Price {
        return {
            value: new Coinspf2ettw(this.data.data.price.value),
            per: this.data.data.price.per ?? 1,
        };
    }

    /** Expand a tree of kit entry data into a list of physical items */
    async inflate({
        entries = this.entries,
        containerId = null,
    }: { entries?: KitEntryData[]; containerId?: string | null } = {}): Promise<PhysicalItempf2ettw[]> {
        const itemUUIDs = entries.map((e): ItemUUID => (e.pack ? `Compendium.${e.pack}.${e.id}` : `Item.${e.id}`));
        const items: unknown[] = await fromUUIDs(itemUUIDs);
        if (entries.length !== items.length) throw Errorpf2ettw(`Some items from ${this.name} were not found`);
        if (!items.every((i): i is Itempf2ettw => i instanceof Itempf2ettw)) return [];

        return items.reduce(async (promise: PhysicalItempf2ettw[] | Promise<PhysicalItempf2ettw[]>, item, index) => {
            const prepared = await promise;
            const clone = item.clone({ _id: randomID() }, { keepId: true });
            const entry = entries[index];
            if (clone instanceof PhysicalItempf2ettw) {
                clone.data.update({
                    "data.quantity": entry.quantity,
                    "data.containerId": containerId,
                });
            }

            if (clone instanceof Containerpf2ettw && entry.items) {
                const contents = await this.inflate({
                    entries: Object.values(entry.items),
                    containerId: clone.id,
                });
                prepared.push(clone, ...contents);
            } else if (clone instanceof Kitpf2ettw) {
                const inflatedKit = await clone.inflate({ containerId });
                prepared.push(...inflatedKit);
            } else if (clone instanceof PhysicalItempf2ettw) {
                prepared.push(clone);
            }

            return prepared;
        }, []);
    }

    protected override async _preUpdate(
        changed: DeepPartial<this["data"]["_source"]>,
        options: DocumentModificationContext<this>,
        user: Userpf2ettw
    ): Promise<void> {
        if (!changed.data) return await super._preUpdate(changed, options, user);

        // Clear 0 price denominations
        if (isObject<Record<string, unknown>>(changed.data?.price)) {
            const price: Record<string, unknown> = changed.data.price;
            for (const denomination of DENOMINATIONS) {
                if (price[denomination] === 0) {
                    price[`-=denomination`] = null;
                }
            }
        }

        await super._preUpdate(changed, options, user);
    }

    /** Inflate this kit and add its items to the provided actor */
    async dumpContents({
        actor,
        containerId = null,
    }: {
        actor: Actorpf2ettw;
        containerId?: string | null;
    }): Promise<void> {
        const sources = (await this.inflate({ containerId })).map((i) => i.toObject());
        await actor.createEmbeddedDocuments("Item", sources, { keepId: true });
    }
}

interface Kitpf2ettw {
    readonly data: KitData;
}

export { Kitpf2ettw };
