import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemSystemSource } from "@item/data/base";
import { ZeroToFour } from "@module/data";
import type { Lorepf2ettw } from ".";

type LoreSource = BaseItemSourcepf2ettw<"lore", LoreSystemSource>;

type LoreData = Omit<LoreSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Lorepf2ettw, "lore", LoreSystemData, LoreSource>;

interface LoreSystemSource extends ItemSystemSource {
    mod: {
        value: number;
    };
    proficient: {
        value: ZeroToFour;
    };
    variants?: Record<string, { label: string; options: string }>;
}

type LoreSystemData = LoreSystemSource;

export { LoreData, LoreSource };
