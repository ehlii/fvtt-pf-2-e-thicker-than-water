import { Itempf2ettw } from "@item/base";
import { LoreData } from "./data";

export class Lorepf2ettw extends Itempf2ettw {}

export interface Lorepf2ettw {
    readonly data: LoreData;
}
