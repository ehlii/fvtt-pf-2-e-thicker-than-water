import {
    BaseItemDatapf2ettw,
    BaseItemSourcepf2ettw,
    ItemSystemData,
    ItemSystemSource,
    ItemTraits,
} from "@item/data/base";
import type { Meleepf2ettw } from ".";

export type MeleeSource = BaseItemSourcepf2ettw<"melee", MeleeSystemSource>;

type MeleeData = Omit<MeleeSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Meleepf2ettw, "melee", MeleeSystemData, MeleeSource>;

export interface MeleeDamageRoll {
    damage: string;
    damageType: string;
}

export type NPCAttackTrait = keyof Configpf2ettw["pf2ettw"]["npcAttackTraits"];
export type NPCAttackTraits = ItemTraits<NPCAttackTrait>;

export interface MeleeSystemSource extends ItemSystemSource {
    traits: NPCAttackTraits;
    attack: {
        value: string;
    };
    damageRolls: Record<string, MeleeDamageRoll>;
    bonus: {
        value: number;
    };
    attackEffects: {
        value: string[];
    };
    weaponType: {
        value: "melee" | "ranged";
    };
}

export type MeleeSystemData = MeleeSystemSource & Omit<ItemSystemData, "traits">;

export { MeleeData };
