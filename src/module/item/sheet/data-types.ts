import { Itempf2ettw, PhysicalItempf2ettw } from "@item";
import { ABCItempf2ettw } from "@item/abc";
import { Ancestrypf2ettw } from "@item/ancestry";
import { Backgroundpf2ettw } from "@item/background";
import { Featpf2ettw } from "@item/feat";
import { Heritagepf2ettw } from "@item/heritage";
import { ItemActivation } from "@item/physical/data";
import { Spellpf2ettw } from "@item/spell";
import { SpellSystemData } from "@item/spell/data";
import { SheetOptions } from "@module/sheet/helpers";

export interface ItemSheetDatapf2ettw<TItem extends Itempf2ettw> extends ItemSheetData<TItem> {
    itemType: string | null;
    hasSidebar: boolean;
    hasDetails: boolean;
    sidebarTemplate?: () => string;
    detailsTemplate?: () => string;
    item: TItem["data"];
    data: TItem["data"]["data"];
    isPhysical: boolean;
    user: { isGM: boolean };
    enabledRulesUI: boolean;
    ruleEditing: boolean;
    ruleSelection: {
        selected: string | null;
        types: Record<string, string>;
    };
}

export interface PhysicalItemSheetData<TItem extends PhysicalItempf2ettw> extends ItemSheetDatapf2ettw<TItem> {
    isPhysical: true;
    basePriceString: string;
    priceString: string;
    actionTypes: Configpf2ettw["pf2ettw"]["actionTypes"];
    actionsNumber: Configpf2ettw["pf2ettw"]["actionsNumber"];
    frequencies: Configpf2ettw["pf2ettw"]["frequencies"];
    activations: { action: ItemActivation; id: string; base: string }[];
}

export interface ABCSheetData<TItem extends ABCItempf2ettw> extends ItemSheetDatapf2ettw<TItem> {
    hasDetails: true;
}

export interface AncestrySheetData extends ABCSheetData<Ancestrypf2ettw> {
    selectedBoosts: Record<string, Record<string, string>>;
    selectedFlaws: Record<string, Record<string, string>>;
    rarities: SheetOptions;
    sizes: SheetOptions;
    traits: SheetOptions;
    languages: SheetOptions;
    additionalLanguages: SheetOptions;
}

export interface BackgroundSheetData extends ABCSheetData<Backgroundpf2ettw> {
    rarities: SheetOptions;
    trainedSkills: SheetOptions;
    selectedBoosts: Record<string, Record<string, string>>;
}

export interface FeatSheetData extends ItemSheetDatapf2ettw<Featpf2ettw> {
    featTypes: Configpf2ettw["pf2ettw"]["featTypes"];
    actionTypes: Configpf2ettw["pf2ettw"]["actionTypes"];
    actionsNumber: Configpf2ettw["pf2ettw"]["actionsNumber"];
    damageTypes: Configpf2ettw["pf2ettw"]["damageTypes"] & Configpf2ettw["pf2ettw"]["healingTypes"];
    categories: Configpf2ettw["pf2ettw"]["actionCategories"];
    prerequisites: string;
    rarities: SheetOptions;
    traits: SheetOptions;
    isFeat: boolean;
    mandatoryTakeOnce: boolean;
    hasLineageTrait: boolean;
}

export interface SpellSheetOverlayData {
    id: string | null;
    /** Base path to the property, dot delimited */
    base: string;
    /** Base path to the spell override data, dot delimited. Currently this is the same as base */
    dataPath: string;
    level: number;
    data: Partial<SpellSystemData>;
    type: "heighten";
    heightenLevels: number[];
    missing: { key: keyof SpellSystemData; label: string }[];
}

export interface SpellSheetData extends ItemSheetDatapf2ettw<Spellpf2ettw> {
    isCantrip: boolean;
    isFocusSpell: boolean;
    isRitual: boolean;
    magicSchools: Configpf2ettw["pf2ettw"]["magicSchools"];
    spellCategories: Configpf2ettw["pf2ettw"]["spellCategories"];
    spellLevels: Configpf2ettw["pf2ettw"]["spellLevels"];
    spellTypes: Configpf2ettw["pf2ettw"]["spellTypes"];
    magicTraditions: SheetOptions;
    damageCategories: Configpf2ettw["pf2ettw"]["damageCategories"];
    damageSubtypes: Configpf2ettw["pf2ettw"]["damageSubtypes"];
    spellComponents: string[];
    traits: SheetOptions;
    rarities: SheetOptions;
    areaSizes: Configpf2ettw["pf2ettw"]["areaSizes"];
    areaTypes: Configpf2ettw["pf2ettw"]["areaTypes"];
    heightenIntervals: number[];
    heightenOverlays: SpellSheetOverlayData[];
    canHeighten: boolean;
}

export interface HeritageSheetData extends ItemSheetDatapf2ettw<Heritagepf2ettw> {
    ancestry: Ancestrypf2ettw | null;
    ancestryRefBroken: boolean;
    traits: SheetOptions;
    rarities: SheetOptions;
}
