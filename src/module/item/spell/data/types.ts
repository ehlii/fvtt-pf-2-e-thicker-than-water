import { AbilityString, SaveType } from "@actor/types";
import type { Spellpf2ettw } from "@item";
import {
    BaseItemDatapf2ettw,
    BaseItemSourcepf2ettw,
    ItemLevelData,
    ItemSystemData,
    ItemSystemSource,
    ItemTraits,
} from "@item/data/base";
import { OneToTen, ValueAndMax, ValuesList } from "@module/data";
import { DamageType } from "@system/damage";
import { MagicSchool, MagicTradition, SpellComponent, SpellTrait } from "../types";

type SpellSource = BaseItemSourcepf2ettw<"spell", SpellSystemSource>;

type SpellData = Omit<SpellSource, "effects" | "flags"> &
    BaseItemDatapf2ettw<Spellpf2ettw, "spell", SpellSystemData, SpellSource>;

export type SpellTraits = ItemTraits<SpellTrait>;
type SpellDamageCategory = keyof Configpf2ettw["pf2ettw"]["damageCategories"];

export interface SpellDamageType {
    value: DamageType;
    subtype?: "persistent" | "splash";
    categories: SpellDamageCategory[];
}

export interface SpellDamage {
    value: string;
    applyMod?: boolean;
    type: SpellDamageType;
}

export interface SpellHeighteningInterval {
    type: "interval";
    interval: number;
    damage: Record<string, string>;
}

export interface SpellHeighteningFixed {
    type: "fixed";
    levels: Record<OneToTen, Partial<SpellSystemData>>;
}

export interface SpellHeightenLayer {
    level: number;
    data: Partial<SpellSystemData>;
}

interface SpellSystemSource extends ItemSystemSource, ItemLevelData {
    traits: SpellTraits;
    level: {
        value: OneToTen;
    };
    spellType: {
        value: keyof Configpf2ettw["pf2ettw"]["spellTypes"];
    };
    category: {
        value: keyof Configpf2ettw["pf2ettw"]["spellCategories"];
    };
    traditions: ValuesList<MagicTradition>;
    school: {
        value: MagicSchool;
    };
    components: Record<SpellComponent, boolean>;
    materials: {
        value: string;
    };
    target: {
        value: string;
    };
    range: {
        value: string;
    };
    area: {
        value: keyof Configpf2ettw["pf2ettw"]["areaSizes"];
        areaType: keyof Configpf2ettw["pf2ettw"]["areaTypes"];
    };
    time: {
        value: string;
    };
    duration: {
        value: string;
    };
    damage: {
        value: Record<string, SpellDamage>;
    };
    heightening?: SpellHeighteningFixed | SpellHeighteningInterval;
    save: {
        basic: string;
        value: SaveType | "";
        dc?: number;
        str?: string;
    };
    sustained: {
        value: false;
    };
    cost: {
        value: string;
    };
    ability: {
        value: AbilityString;
    };
    hasCounteractCheck: {
        value: boolean;
    };
    location: {
        value: string;
        signature?: boolean;
        heightenedLevel?: number;

        /** The level to heighten this spell to if it's a cantrip or focus spell */
        autoHeightenLevel?: OneToTen | null;

        /** Number of uses if this is an innate spell */
        uses?: ValueAndMax;
    };
}

interface SpellSystemData extends SpellSystemSource, ItemSystemData {
    traits: SpellTraits;
}

export { SpellData, SpellSource, SpellSystemData, SpellSystemSource };
