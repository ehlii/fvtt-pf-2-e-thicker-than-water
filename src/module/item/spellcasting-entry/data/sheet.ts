import { Spellpf2ettw } from "@item";
import { MagicTradition } from "@item/spell/types";
import { ZeroToTen } from "@module/data";
import { StatisticChatData } from "@system/statistic";
import { PreparationType } from ".";

/** Final render data used for showing a spell list  */
export interface SpellListData {
    id: string;
    name: string;
    levels: SpellcastingSlotLevel[];
}

/** Spell list render data for a SpellcastingEntrypf2ettw */
export interface SpellcastingEntryListData extends SpellListData {
    statistic: StatisticChatData;
    tradition: MagicTradition;
    castingType: PreparationType;
    isPrepared?: boolean;
    isSpontaneous?: boolean;
    isFlexible?: boolean;
    isInnate?: boolean;
    isFocusPool?: boolean;
    isRitual?: boolean;
    flexibleAvailable?: { value: number; max: number };
    spellPrepList: Record<number, SpellPrepEntry[]> | null;
}

export interface SpellcastingSlotLevel {
    label: string;
    level: ZeroToTen;
    isCantrip: boolean;

    /**
     * Number of uses and max slots or spells.
     * If this is null, allowed usages are infinite.
     * If value is undefined then it's not expendable, it's a count of total spells instead.
     */
    uses?: {
        value?: number;
        max: number;
    };

    active: (ActiveSpell | null)[];
}

export interface SpellPrepEntry {
    spell: Embedded<Spellpf2ettw>;
    signature?: boolean;
}

export interface ActiveSpell {
    spell: Embedded<Spellpf2ettw>;
    chatData: Record<string, unknown>;
    expended?: boolean;
    /** Is this spell marked as signature/collection */
    signature?: boolean;
    /** Is the spell not actually of this level? */
    virtual?: boolean;
}
