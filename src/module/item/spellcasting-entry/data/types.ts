import { Actorpf2ettw } from "@actor";
import { AbilityString } from "@actor/types";
import { Spellpf2ettw } from "@item";
import { BaseItemDatapf2ettw, BaseItemSourcepf2ettw, ItemSystemData } from "@item/data/base";
import { MagicTradition } from "@item/spell/types";
import { OneToFour, OneToTen, ZeroToEleven } from "@module/data";
import { RollNotepf2ettw } from "@module/notes";
import { Statistic, StatisticChatData } from "@system/statistic";
import { SpellcastingEntrypf2ettw } from "..";

interface BaseSpellcastingEntry {
    id: string;
    actor: Actorpf2ettw | null;
    ability: AbilityString;
    tradition: MagicTradition;
    statistic: Statistic;
    cast(spell: Spellpf2ettw, options: {}): Promise<void>;
}

interface SpellcastingEntry extends BaseSpellcastingEntry {
    isPrepared: boolean;
    isSpontaneous: boolean;
    isInnate: boolean;
    isFocusPool: boolean;
}

// temporary type until the spellcasting entry is migrated to no longer use slotX keys
type SlotKey = `slot${ZeroToEleven}`;

type SpellcastingEntrySource = BaseItemSourcepf2ettw<"spellcastingEntry", SpellcastingEntrySystemData>;

type SpellcastingEntryData = Omit<SpellcastingEntrySource, "effects" | "flags"> &
    BaseItemDatapf2ettw<
        SpellcastingEntrypf2ettw,
        "spellcastingEntry",
        SpellcastingEntrySystemData,
        SpellcastingEntrySource
    >;

interface SpellAttackRollModifier {
    breakdown: string;
    notes: RollNotepf2ettw[];
    roll: Function;
    value: number;
}

interface SpellDifficultyClass {
    breakdown: string;
    notes: RollNotepf2ettw[];
    value: number;
}

interface SpellPrepData {
    id: string | null;
    expended?: boolean;
    name?: string;
    prepared?: boolean;
}

interface SpellSlotData {
    prepared: Record<number, SpellPrepData>;
    value: number;
    max: number;
}

type PreparationType = keyof Configpf2ettw["pf2ettw"]["preparationType"];

interface SpellcastingEntrySystemData extends ItemSystemData {
    ability: {
        value: AbilityString | "";
    };
    spelldc: {
        value: number;
        dc: number;
        mod: number;
    };
    statisticData?: StatisticChatData;
    tradition: {
        value: MagicTradition | "";
    };
    prepared: {
        value: PreparationType;
        flexible?: boolean;
    };
    showSlotlessLevels: {
        value: boolean;
    };
    proficiency: {
        value: OneToFour;
    };
    slots: Record<SlotKey, SpellSlotData>;
    autoHeightenLevel: {
        value: OneToTen | null;
    };
}

export {
    BaseSpellcastingEntry,
    PreparationType,
    SlotKey,
    SpellAttackRollModifier,
    SpellDifficultyClass,
    SpellcastingEntry,
    SpellcastingEntryData,
    SpellcastingEntrySource,
    SpellcastingEntrySystemData,
};
