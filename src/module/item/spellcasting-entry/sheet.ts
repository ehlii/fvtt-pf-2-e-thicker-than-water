import { Actorpf2ettw } from "@actor";
import { ItemSummaryRendererpf2ettw } from "@actor/sheet/item-summary-renderer";
import { Itempf2ettw, Spellpf2ettw } from "@item";
import { ItemSourcepf2ettw, SpellSource } from "@item/data";
import { SpellcastingEntrypf2ettw } from ".";
import { SpellcastingEntryListData } from "./data";

class SpellPreparationSheet extends ActorSheet<Actorpf2ettw, Itempf2ettw> {
    /** Implementation used to handle the toggling and rendering of item summaries */
    itemRenderer: ItemSummaryRendererpf2ettw<Actorpf2ettw> = new ItemSummaryRendererpf2ettw(this);

    constructor(public item: Embedded<SpellcastingEntrypf2ettw>, options: Partial<ActorSheetOptions>) {
        super(item.actor, options);
    }

    static override get defaultOptions(): ActorSheetOptions {
        const options = super.defaultOptions;
        options.classes = ["default", "sheet", "spellcasting-entry"];
        options.width = 480;
        options.height = 600;
        options.template = "systems/pf2ettw/templates/actors/spellcasting-prep-sheet.html";
        options.scrollY = [".sheet-content"];
        return options;
    }

    /** Avoid conflicting with the real actor sheet */
    override get id(): string {
        return `${super.id}-spellprep-${this.item.id}`;
    }

    override get title() {
        return game.i18n.format("pf2ettw.Actor.Creature.SpellPreparation.Title", { actor: this.actor.name });
    }

    /**
     * This being an actor sheet saves us from most drag and drop re-implementation,
     * but we still have a gotcha in the form of the header buttons.
     * Reimplement to avoid sheet configuration and token options.
     */
    protected override _getHeaderButtons(): ApplicationHeaderButton[] {
        const buttons = [
            {
                label: "Close",
                class: "close",
                icon: "fas fa-times",
                onclick: () => this.close(),
            },
        ];
        return buttons;
    }

    override async getData(): Promise<SpellPreparationSheetData> {
        return {
            ...(await super.getData()),
            owner: this.actor.isOwner,
            entry: this.item.getSpellData(),
        };
    }

    override activateListeners($html: JQuery<HTMLElement>): void {
        super.activateListeners($html);
        this.itemRenderer.activateListeners($html);

        // Update an embedded item
        $html.find(".item-edit").on("click", (event) => {
            const item = this.getItemFromEvent(event);
            if (item) {
                item.sheet.render(true);
            }
        });

        // Delete Inventory Item
        $html.find(".item-delete").on("click", (event) => {
            this.getItemFromEvent(event)?.delete();
        });

        // Item Rolling
        $html.find(".item[data-item-id] .item-image").on("click", (event) => {
            this.getItemFromEvent(event)?.toChat(event);
        });

        // Flexible Casting toggle
        $html.find(".toggle-signature-spell").on("click", (event) => {
            const item = this.getItemFromEvent(event);
            if (item instanceof Spellpf2ettw) {
                item.update({ "data.location.signature": !item.data.data.location.signature });
            }
        });

        $html.find(".spell-create").on("click", (event) => {
            const data = duplicate(event.currentTarget.dataset);
            const level = Number(data.level ?? 1);
            const newLabel = game.i18n.localize("pf2ettw.NewLabel");
            const levelLabel = game.i18n.localize(`pf2ettw.SpellLevel${level}`);
            const spellLabel = level > 0 ? game.i18n.localize("pf2ettw.SpellLabel") : "";
            data.name = `${newLabel} ${levelLabel} ${spellLabel}`;
            mergeObject(data, {
                "data.level.value": level,
                "data.location.value": this.item.id,
            });

            this.actor.createEmbeddedDocuments("Item", [data]);
        });

        $html.find(".spell-browse").on("click", (event) => {
            const level = Number($(event.currentTarget).attr("data-level")) ?? null;
            game.pf2ettw.compendiumBrowser.openSpellTab(this.item, level);
        });
    }

    private getItemFromEvent(event: JQuery.TriggeredEvent): Embedded<Itempf2ettw> {
        const $li = $(event.currentTarget).closest("li[data-item-id]");
        const itemId = $li.attr("data-item-id") ?? "";
        return this.actor.items.get(itemId, { strict: true });
    }

    /** Allow adding new spells to the shortlist by dragging directly into the window */
    protected override async _onDropItemCreate(
        itemSource: ItemSourcepf2ettw | ItemSourcepf2ettw[]
    ): Promise<Itempf2ettw[]> {
        const sources = Array.isArray(itemSource) ? itemSource : [itemSource];
        const spellSources = sources.filter((source): source is SpellSource => source.type === "spell");
        for (const spellSource of spellSources) {
            spellSource.data.location.value = this.item.id;
        }

        return super._onDropItemCreate(spellSources);
    }

    /** Allow transferring spells between open windows */
    protected override async _onSortItem(event: ElementDragEvent, itemData: ItemSourcepf2ettw): Promise<Itempf2ettw[]> {
        if (itemData.type !== "spell") return [];

        const spell = this.actor.items.get(itemData._id);
        if (itemData.data.location.value !== this.item.id && spell instanceof Spellpf2ettw) {
            const addedSpell = await this.item.addSpell(spell);
            return [addedSpell ?? []].flat();
        }

        return super._onSortItem(event, itemData);
    }

    /** Override of inner render function to maintain item summary state */
    protected override async _renderInner(data: Record<string, unknown>, options: RenderOptions): Promise<JQuery> {
        return this.itemRenderer.saveAndRestoreState(() => {
            return super._renderInner(data, options);
        });
    }
}

interface SpellPreparationSheetData extends ActorSheetData<Actorpf2ettw> {
    actor: Actorpf2ettw;
    owner: boolean;
    entry: SpellcastingEntryListData;
}

export { SpellPreparationSheet };
