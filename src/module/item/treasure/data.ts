import {
    BasePhysicalItemData,
    BasePhysicalItemSource,
    PhysicalSystemData,
    PhysicalSystemSource,
} from "@item/physical/data";
import { Treasurepf2ettw } from ".";

type TreasureSource = BasePhysicalItemSource<"treasure", TreasureSystemSource>;

type TreasureData = Omit<TreasureSource, "data" | "effects" | "flags"> &
    BasePhysicalItemData<Treasurepf2ettw, "treasure", TreasureSystemData, TreasureSource>;

type TreasureSystemSource = PhysicalSystemSource;
type TreasureSystemData = PhysicalSystemData & {
    equipped: {
        invested?: never;
    };
};

export { TreasureData, TreasureSource, TreasureSystemData, TreasureSystemSource };
