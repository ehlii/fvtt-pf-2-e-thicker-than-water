import { PhysicalItempf2ettw } from "@item/physical";
import { DENOMINATIONS } from "@item/physical/values";
import { TreasureData } from "./data";

class Treasurepf2ettw extends PhysicalItempf2ettw {
    get isCoinage(): boolean {
        return this.data.data.stackGroup === "coins";
    }

    get denomination() {
        if (!this.isCoinage) return null;
        const options = DENOMINATIONS.filter((denomination) => !!this.price.value[denomination]);
        return options.length === 1 ? options[0] : null;
    }

    /** Set non-coinage treasure price from its numeric value and denomination */
    override prepareBaseData(): void {
        super.prepareBaseData();
        if (this.isCoinage) {
            this.data.data.size = "med";
        }
    }

    override getChatData(
        this: Embedded<Treasurepf2ettw>,
        htmlOptions: EnrichHTMLOptions = {}
    ): Record<string, unknown> {
        const data = this.data.data;
        const traits = this.traitChatData({});

        return this.processChatData(htmlOptions, { ...data, traits });
    }
}

interface Treasurepf2ettw {
    readonly data: TreasureData;
}

export { Treasurepf2ettw };
