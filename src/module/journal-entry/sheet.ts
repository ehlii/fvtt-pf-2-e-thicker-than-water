import { InlineRollLinks } from "@scripts/ui/inline-roll-links";
import { TextEditorpf2ettw } from "@system/text-editor";
import { Errorpf2ettw } from "@util";
import type * as TinyMCE from "tinymce";
import "../../styles/tinymce.scss";

class JournalSheetpf2ettw<TJournalEntry extends JournalEntry = JournalEntry> extends JournalSheet<TJournalEntry> {
    override get template(): string {
        if (this._sheetMode === "image") return ImagePopout.defaultOptions.template;
        return "systems/pf2ettw/templates/journal/sheet.html";
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);
        InlineRollLinks.listen($html);
    }

    override activateEditor(name: string, options: Partial<TinyMCE.EditorSettings> = {}, initialContent = ""): void {
        const editor = this.editors[name];
        if (!editor) throw Errorpf2ettw(`${name} is not a registered editor name!`);

        options = foundry.utils.mergeObject(editor.options, options);
        options.height = options.target?.offsetHeight;

        const defaults = (this.constructor as typeof JournalSheetpf2ettw).defaultOptions.classes;
        TextEditorpf2ettw.create(options, initialContent || editor.initial).then((mce) => {
            if (defaults.includes("pf2ettw")) {
                mce.getBody().classList.add("pf2ettw");
            }

            editor.mce = mce;
            editor.changed = false;
            editor.active = true;
            mce.focus();
            mce.on("change", () => (editor.changed = true));
        });
    }
}

class JournalSheetStyledpf2ettw extends JournalSheetpf2ettw {
    /** Use the system-themed styling only if the setting is enabled (on by default) */
    static override get defaultOptions(): DocumentSheetOptions {
        const options = super.defaultOptions;
        options.classes.push("pf2ettw");
        return options;
    }
}

export { JournalSheetpf2ettw, JournalSheetStyledpf2ettw };
