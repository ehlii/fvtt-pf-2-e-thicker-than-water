export class Macropf2ettw extends Macro {
    /** Raise permission requirement of world macro visibility to observer */
    override get visible(): boolean {
        return this.permission >= CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER;
    }
}
