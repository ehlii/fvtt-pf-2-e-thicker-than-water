import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";

export class Migration600Reach extends MigrationBase {
    static override version = 0.6;

    override async updateItem(item: ItemSourcepf2ettw) {
        if (item.type === "ancestry") {
            item.data.reach = 5;
        }
    }
}
