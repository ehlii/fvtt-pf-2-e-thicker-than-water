import { MigrationBase } from "../base";
import { ActorSourcepf2ettw } from "@actor/data";
import { Featpf2ettw } from "@item";

export class Migration602UpdateDiehardFeat extends MigrationBase {
    static override version = 0.602;
    override requiresFlush = true;

    private diehardPromise: Promise<ClientDocument | null>;

    constructor() {
        super();
        this.diehardPromise = fromUuid("Compendium.pf2ettw.feats-srd.I0BhPWqYf1bbzEYg");
    }

    override async updateActor(actorData: ActorSourcepf2ettw) {
        const diehard = actorData.items.find(
            (itemData) => itemData.data.slug === "diehard" && itemData.type === "feat"
        );

        if (actorData.type === "character" && diehard !== undefined) {
            actorData.data.attributes.dying.max = 4;
            const diehardIndex = actorData.items.indexOf(diehard);
            const newDiehard = await this.diehardPromise;
            if (!(newDiehard instanceof Featpf2ettw)) {
                throw Error("pf2ettw System | Expected item not found in Compendium");
            }
            actorData.items.splice(diehardIndex, 1, newDiehard.toObject());
        }
    }
}
