import { isObject } from "@util";
import { MigrationBase } from "../base";

export class Migration603ResetQuickRollDefault extends MigrationBase {
    static override version = 0.603;

    override async updateUser(userData: foundry.data.UserSource): Promise<void> {
        const flags = userData.flags;
        if (
            isObject<Record<string, unknown>>(flags.pf2ettw) &&
            isObject<Record<string, unknown>>(flags.pf2ettw.settings) &&
            typeof flags.pf2ettw.settings.quickD20roll === "boolean"
        ) {
            flags.pf2ettw.settings.quickD20roll = false;
        }
    }
}
