import { ItemSourcepf2ettw } from "@item/data";
import { SpellcastingEntrySystemData } from "@item/spellcasting-entry/data";
import { MigrationBase } from "../base";

export class Migration606SignatureSpells extends MigrationBase {
    static override version = 0.606;

    override async updateItem(item: ItemSourcepf2ettw) {
        if (item.type === "spellcastingEntry") {
            const data: SpellcastingEntrySystemDataOld = item.data;
            if (!data.signatureSpells) {
                data.signatureSpells = {
                    value: [],
                };
            }
        }
    }
}

interface SpellcastingEntrySystemDataOld extends SpellcastingEntrySystemData {
    signatureSpells?: {
        value: string[];
    };
}
