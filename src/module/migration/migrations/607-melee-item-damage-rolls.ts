import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";
import { MeleeDamageRoll } from "@item/melee/data";

/** Convert damageRolls arrays to objects. */
export class Migration607MeleeItemDamageRolls extends MigrationBase {
    static override version = 0.607;

    override async updateItem(itemData: ItemSourcepf2ettw) {
        if (itemData.type === "melee") {
            if (Array.isArray(itemData.data.damageRolls)) {
                const damageRolls: Record<string, MeleeDamageRoll> = {};
                itemData.data.damageRolls.forEach((roll) => {
                    const key = randomID(20);
                    damageRolls[key] = roll;
                });
                itemData.data.damageRolls = damageRolls;
            }
        }
    }
}
