import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";
import { ActorSourcepf2ettw } from "@actor/data";

/** Unbreak actor sheets that have kit items in their inventories */
export class Migration608DeletePersistedKits extends MigrationBase {
    static override version = 0.608;

    override async updateItem(itemData: ItemSourcepf2ettw, actorData?: ActorSourcepf2ettw) {
        if (actorData && itemData.type === "kit") {
            const index = actorData.items.indexOf(itemData);
            actorData.items.splice(index, 1);
        }
    }
}
