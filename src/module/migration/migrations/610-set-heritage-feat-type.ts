import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";

/** Convert heritage "feats" be of type "heritage" */
export class Migration610SetHeritageFeatType extends MigrationBase {
    static override version = 0.61;

    override async updateItem(itemSource: ItemSourcepf2ettw) {
        const itemTraits: string[] | undefined = itemSource.data.traits?.value;
        if (itemSource.type === "feat" && itemTraits?.includes("heritage")) {
            const featType: { value: string } = itemSource.data.featType;
            featType.value = "heritage";
            const index = itemTraits.indexOf("heritage");
            itemTraits.splice(index, 1);
        }
    }
}
