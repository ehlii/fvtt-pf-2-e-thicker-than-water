import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";

/** Remove "instinct" trait from feats */
export class Migration615RemoveInstinctTrait extends MigrationBase {
    static override version = 0.615;

    override async updateItem(itemData: ItemSourcepf2ettw) {
        const traits: { value: string[] } | undefined = itemData.data.traits;
        if (!traits) return;
        if (typeof traits.value === "string") {
            // Catch trait.value properties that missed migration 597
            traits.value = [];
        } else {
            traits.value = traits.value.filter((trait) => trait !== "instinct");
        }
    }
}
