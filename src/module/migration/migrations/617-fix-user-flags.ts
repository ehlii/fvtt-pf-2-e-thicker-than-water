import { isObject } from "@util";
import { MigrationBase } from "../base";

export class Migration617FixUserFlags extends MigrationBase {
    static override version = 0.617;

    override async updateUser(userData: foundry.data.UserSource): Promise<void> {
        const flags: Record<string, Record<string, unknown>> & { "-=pf2ettw"?: null } = userData.flags;
        const settings = flags.pf2ettw?.settings;
        if (isObject<Record<string, unknown>>(settings) && typeof settings.color === "string") {
            const uiTheme = settings.color ?? "blue";
            const showRollDialogs = !settings.quickD20roll;
            flags.pf2ettw ??= {};
            flags.pf2ettw.settings = {
                uiTheme,
                showEffectPanel: flags.pf2ettw?.showEffectPanel ?? true,
                showRollDialogs,
            };
            delete flags.pf2ettw;
            flags["-=pf2ettw"] = null;
        }
    }
}
