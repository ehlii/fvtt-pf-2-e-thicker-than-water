import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";

export class Migration623NumifyPotencyRunes extends MigrationBase {
    static override version = 0.623;

    override async updateItem(itemData: ItemSourcepf2ettw): Promise<void> {
        if (!(itemData.type === "weapon" || itemData.type === "armor")) return;

        const potencyRune: { value: number | null } | undefined = itemData.data.potencyRune;
        if (potencyRune) {
            potencyRune.value = Number(itemData.data.potencyRune.value) || null;
        } else {
            itemData.data.potencyRune = { value: null };
        }
    }
}
