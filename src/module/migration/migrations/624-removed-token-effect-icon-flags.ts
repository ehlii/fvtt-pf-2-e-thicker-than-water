import { ActorSourcepf2ettw } from "@actor/data";
import { MigrationBase } from "../base";

export class Migration624RemoveTokenEffectIconFlags extends MigrationBase {
    static override version = 0.624;

    override async updateActor(actorData: ActorSourcepf2ettw): Promise<void> {
        // remove deprecated rule element token effect flags
        const flags = actorData.flags as TokenEffectsFlag;
        if (flags.pf2ettw?.token?.effects) {
            delete flags.pf2ettw.token.effects;
            if ("game" in globalThis) {
                flags.pf2ettw.token["-=effects"] = null;
            }
        }
    }

    override async updateToken(tokenData: foundry.data.TokenSource): Promise<void> {
        // remove deprecated rule element token effects
        const flags = (tokenData.actorData.flags ?? {}) as TokenEffectsFlag;
        if (flags.pf2ettw?.token?.effects) {
            delete flags.pf2ettw.token.effects;
            if ("game" in globalThis) {
                flags.pf2ettw.token["-=effects"] = null;
            }
        }
    }
}

type TokenEffectsFlag = {
    pf2ettw?: {
        token?: {
            effects?: Record<string, unknown>;
            "-=effects"?: null;
        };
    };
};
