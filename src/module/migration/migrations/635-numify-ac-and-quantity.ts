import { ActorSourcepf2ettw } from "@actor/data";
import { ItemSourcepf2ettw } from "@item/data";
import { isPhysicalData } from "@item/data/helpers";
import { MigrationBase } from "../base";

/** Ensure AC and quantity values are numeric */
export class Migration635NumifyACAndQuantity extends MigrationBase {
    static override version = 0.635;

    override async updateActor(actorData: ActorSourcepf2ettw): Promise<void> {
        if (actorData.type === "hazard" || actorData.type === "npc" || actorData.type === "vehicle") {
            actorData.data.attributes.ac.value = Number(actorData.data.attributes.ac.value);
        }
    }

    override async updateItem(itemData: ItemSourcepf2ettw): Promise<void> {
        if (isPhysicalData(itemData)) {
            const quantity = itemData.data.quantity || { value: 0 };
            if (quantity instanceof Object) {
                quantity.value = Number(quantity.value);
            }
        }
    }
}
