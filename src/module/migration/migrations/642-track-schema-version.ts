import { ActorSourcepf2ettw } from "@actor/data";
import { ItemSourcepf2ettw } from "@item/data";
import { MigrationBase } from "../base";

/** Start recording the schema version and other details of a migration */
export class Migration642TrackSchemaVersion extends MigrationBase {
    static override version = 0.642;

    override async updateActor(actorSource: ActorSourcepf2ettw): Promise<void> {
        actorSource.data.schema ??= {
            version: null,
            lastMigration: null,
        };
    }

    override async updateItem(itemSource: ItemSourcepf2ettw): Promise<void> {
        itemSource.data.schema ??= {
            version: null,
            lastMigration: null,
        };
    }
}
