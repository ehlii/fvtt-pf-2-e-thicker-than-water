import { Actorpf2ettw } from "@actor";
import { ActorSourcepf2ettw } from "@actor/data";
import { MigrationBase } from "../base";

/** Set default linkToActorSize flag */
export class Migration662LinkToActorSizeDefaults extends MigrationBase {
    static override version = 0.662;

    override async updateActor(actorSource: ActorSourcepf2ettw): Promise<void> {
        const linkToActorSize = !["hazard", "loot"].includes(actorSource.type);
        actorSource.token.flags ??= { pf2ettw: { linkToActorSize } };
        actorSource.token.flags.pf2ettw ??= { linkToActorSize };
        actorSource.token.flags.pf2ettw.linkToActorSize ??= linkToActorSize;
    }

    override async updateToken(tokenSource: foundry.data.TokenSource, actor: Actorpf2ettw): Promise<void> {
        const linkToActorSize = !["hazard", "loot"].includes(actor.type);
        tokenSource.flags.pf2ettw ??= { linkToActorSize };
        tokenSource.flags.pf2ettw.linkToActorSize ??= linkToActorSize;
    }
}
