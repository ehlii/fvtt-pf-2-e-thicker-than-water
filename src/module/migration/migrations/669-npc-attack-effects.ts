import { ActorSourcepf2ettw } from "@actor/data";
import { ItemSourcepf2ettw } from "@item/data";
import { MigrationBase } from "../base";
import { sluggify } from "@util";

export class Migration669NPCAttackEffects extends MigrationBase {
    static override version = 0.669;

    override async updateItem(item: ItemSourcepf2ettw, actor?: ActorSourcepf2ettw): Promise<void> {
        if (!actor || item.type !== "melee") return;
        item.data.attackEffects ??= { value: [] };
        if (Array.isArray(item.data.attackEffects.value)) {
            item.data.attackEffects.value.forEach((entry, index, arr) => {
                arr[index] = sluggify(entry);
            });
        }
    }
}
