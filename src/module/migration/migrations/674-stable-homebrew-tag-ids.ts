import { ActorSourcepf2ettw } from "@actor/data";
import { ItemSourcepf2ettw } from "@item/data";
import { HomebrewElements, HomebrewTag } from "@system/settings/homebrew";
import { Localizepf2ettw } from "@system/localize";
import { sluggify } from "@util";
import { MigrationBase } from "../base";

export class Migration674StableHomebrewTagIDs extends MigrationBase {
    static override version = 0.674;

    private homebrewKeys = deepClone(HomebrewElements.SETTINGS);

    private homebrewTags = this.homebrewKeys.reduce(
        (settings, key) => mergeObject(settings, { [key]: game.settings.get("pf2ettw", `homebrew.${key}`) }),
        {} as Record<typeof this.homebrewKeys[number], HomebrewTag[]>
    );

    private updateDocumentTags(documentTags: string[] = []): void {
        for (const key of this.homebrewKeys) {
            const homebrewTags = this.homebrewTags[key];
            for (const tag of homebrewTags) {
                const index = documentTags.indexOf(tag.id);
                if (index !== -1) documentTags.splice(index, 1, `hb_${sluggify(tag.value)}`);
            }
        }
    }

    override async updateActor(actorSource: ActorSourcepf2ettw): Promise<void> {
        if (actorSource.type === "familiar") return;

        this.updateDocumentTags(actorSource.data.traits.traits.value);
        if (actorSource.type === "character" || actorSource.type === "npc") {
            this.updateDocumentTags(actorSource.data.traits?.languages.value);
        }
    }

    override async updateItem(itemSource: ItemSourcepf2ettw): Promise<void> {
        this.updateDocumentTags(itemSource.data.traits?.value);
    }

    override async migrate(): Promise<void> {
        for await (const key of this.homebrewKeys) {
            const tags: { id: string; value: string }[] = this.homebrewTags[key];

            for (const tag of tags) {
                tag.id = `hb_${sluggify(tag.value)}`;
                const tagMap: Record<string, string> =
                    key === "baseWeapons" ? Localizepf2ettw.translations.pf2ettw.Weapon.Base : CONFIG.pf2ettw[key];
                tagMap[tag.id] = tag.value;
                delete tagMap[key];
            }
            if (tags.length > 0) await game.settings.set("pf2ettw", `homebrew.${key}`, tags);
        }
    }
}
