import { MigrationBase } from "../base";
import { ItemSourcepf2ettw } from "@item/data";
import { Spellpf2ettw } from "@item";
import { fromUUIDs } from "@util/from-uuids";
import { setHasElement } from "@util";

/** Update the descriptions of several spells with new effect items */
export class Migration720UpdateSpellDescriptions extends MigrationBase {
    static override version = 0.72;

    private spellUUIDs: Set<CompendiumUUID> = new Set([
        "Compendium.pf2ettw.spells-srd.GoKkejPj5yWJPIPK", // Adaptive Ablation
        "Compendium.pf2ettw.spells-srd.1b55SgYTV65JvmQd", // Blessing of Defiance
        "Compendium.pf2ettw.spells-srd.b515AZlB0sridKSq", // Calm Emotions
        "Compendium.pf2ettw.spells-srd.NBSBFHxBm88qxQUy", // Chromatic Armor
        "Compendium.pf2ettw.spells-srd.9TauMFkIsmvKJNzZ", // Elemental Absorption
        "Compendium.pf2ettw.spells-srd.LoBjvguamA12iyW0", // Energy Absorption
        "Compendium.pf2ettw.spells-srd.IWUe32Y5k2QFd7YQ", // Gravity Weapon
        "Compendium.pf2ettw.spells-srd.WBmvzNDfpwka3qT4", // Light
    ]);

    private spells = fromUUIDs([...this.spellUUIDs]);

    override async updateItem(source: ItemSourcepf2ettw): Promise<void> {
        if (!(source.type === "spell" && setHasElement(this.spellUUIDs, source.flags.core?.sourceId))) {
            return;
        }

        const spells: unknown[] = await this.spells;
        const spell = spells.find((s): s is Spellpf2ettw => s instanceof Spellpf2ettw && s.slug === source.data.slug);

        if (spell) source.data.description.value = spell.description;
    }
}
