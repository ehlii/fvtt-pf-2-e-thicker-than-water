import { Spellpf2ettw } from "@item";
import { ItemSourcepf2ettw, SpellData, SpellSource } from "@item/data";
import { sluggify } from "@util";
import { fromUUIDs } from "@util/from-uuids";
import { MigrationBase } from "../base";

/** Handle spells gaining fixed level heightening */
export class Migration747FixedHeightening extends MigrationBase {
    static override version = 0.747;

    override async updateItem(item: ItemSourcepf2ettw): Promise<void> {
        if (item.type !== "spell") return;

        const isAcidSplash = (item.data.slug ?? sluggify(item.name)) === "acid-splash";
        if (item.data.heightening?.type === "fixed" && !isAcidSplash) return;

        const sourceId = item.flags.core?.sourceId;
        if (sourceId && this.fixedHeightenSpells.has(sourceId)) {
            const spells = await this.loadSpells();
            const spell = spells[sourceId];
            if (spell && spell.data.data.heightening?.type === "fixed") {
                item.data.heightening = spell.data.data.heightening;
                this.overwriteDamage(item, spell.data);
            }
        }
    }

    protected overwriteDamage(item: SpellSource, newItem: SpellData) {
        const newDamage = newItem.data.damage;
        const newKeys = new Set(Object.keys(newDamage.value));
        const diff = Object.keys(item.data.damage.value).filter((key) => !newKeys.has(key));
        const damage: { value: Record<string, unknown> } = item.data.damage;
        damage.value = newDamage.value;
        for (const deleteKey of diff) {
            damage.value[`-=${deleteKey}`] = null;
        }
    }

    #loadedSpells?: Record<string, Spellpf2ettw | undefined>;

    // Ensure compendium is only hit if the migration runs, and only once
    protected async loadSpells() {
        if (this.#loadedSpells) {
            return this.#loadedSpells;
        }

        const spells = await fromUUIDs([...this.fixedHeightenSpells]);
        this.#loadedSpells = spells.reduce((record, spell) => ({ ...record, [spell.uuid]: spell }), {});
        return this.#loadedSpells;
    }

    fixedHeightenSpells = new Set<DocumentUUID>([
        "Compendium.pf2ettw.spells-srd.0fKHBh5goe2eiFYL",
        "Compendium.pf2ettw.spells-srd.10VcmSYNBrvBphu1",
        "Compendium.pf2ettw.spells-srd.2gQYrCPwBmwau26O",
        "Compendium.pf2ettw.spells-srd.2iQKhCQBijhj5Rf3",
        "Compendium.pf2ettw.spells-srd.4koZzrnMXhhosn0D",
        "Compendium.pf2ettw.spells-srd.5WM3WjshXgrkVCg6",
        "Compendium.pf2ettw.spells-srd.7CUgqHunmHfW2lC5",
        "Compendium.pf2ettw.spells-srd.7OFKYR1VY6EXDuiR",
        "Compendium.pf2ettw.spells-srd.9s5tqqXNzcoKamWx",
        "Compendium.pf2ettw.spells-srd.BCuHKrDeJ4eq53M6",
        "Compendium.pf2ettw.spells-srd.CxpFy4HJHf4ACbxF",
        "Compendium.pf2ettw.spells-srd.D2nPKbIS67m9199U",
        "Compendium.pf2ettw.spells-srd.DCQHaLrYXMI37dvW",
        "Compendium.pf2ettw.spells-srd.DgcSiOCR1uDXGaEA",
        "Compendium.pf2ettw.spells-srd.EfFMLVbmkBWmzoLF",
        "Compendium.pf2ettw.spells-srd.Et8RSCLx8w7uOLvo",
        "Compendium.pf2ettw.spells-srd.F23T5tHPo3WsFiHW",
        "Compendium.pf2ettw.spells-srd.FhOaQDTSnsY7tiam",
        "Compendium.pf2ettw.spells-srd.Fr58LDSrbndgld9n",
        "Compendium.pf2ettw.spells-srd.GaRQlC9Yw1BGKHfN",
        "Compendium.pf2ettw.spells-srd.HGmBY8KjgLV97nUp",
        "Compendium.pf2ettw.spells-srd.HHGUBGle4OjoxvNR",
        "Compendium.pf2ettw.spells-srd.HTou8cG05yuSkesj",
        "Compendium.pf2ettw.spells-srd.HWrNMQENi9WSGbnF",
        "Compendium.pf2ettw.spells-srd.HcIAQZjNXHemoXSU",
        "Compendium.pf2ettw.spells-srd.Ifc2b6bNVdjKV7Si",
        "Compendium.pf2ettw.spells-srd.JHntYF0SbaWKq7wR",
        "Compendium.pf2ettw.spells-srd.LQzlKbYjZSMFQawP",
        "Compendium.pf2ettw.spells-srd.LiGbewa9pO0yjbsY",
        "Compendium.pf2ettw.spells-srd.Llx0xKvtu8S4z6TI",
        "Compendium.pf2ettw.spells-srd.Mkbq9xlAUxHUHyR2",
        "Compendium.pf2ettw.spells-srd.OAt2ZEns1gIOCgrn",
        "Compendium.pf2ettw.spells-srd.OhD2Z6rIGGD5ocZA",
        "Compendium.pf2ettw.spells-srd.PRrZ7anETWPm90YY",
        "Compendium.pf2ettw.spells-srd.PjhUmyKnq6K5uDby",
        "Compendium.pf2ettw.spells-srd.Popa5umI3H33levx",
        "Compendium.pf2ettw.spells-srd.Pwq6T7xpfAJXV5aj",
        "Compendium.pf2ettw.spells-srd.Q7QQ91vQtyi1Ux36",
        "Compendium.pf2ettw.spells-srd.Seaah9amXg70RKw2",
        "Compendium.pf2ettw.spells-srd.U58aQWJ47VrI36yP",
        "Compendium.pf2ettw.spells-srd.UmXhuKrYZR3W16mQ",
        "Compendium.pf2ettw.spells-srd.VTb0yI6P1bLkzuRr",
        "Compendium.pf2ettw.spells-srd.VlNcjmYyu95vOUe8",
        "Compendium.pf2ettw.spells-srd.W02bHXylIpoXbO4e",
        "Compendium.pf2ettw.spells-srd.WsUwpfmhKrKwoIe3",
        "Compendium.pf2ettw.spells-srd.Wt94cw03L77sbud7",
        "Compendium.pf2ettw.spells-srd.XhgMx9WC6NfXd9RP",
        "Compendium.pf2ettw.spells-srd.ZAX0OOcKtYMQlquR",
        "Compendium.pf2ettw.spells-srd.ZqmP9gijBmK7y8Xy",
        "Compendium.pf2ettw.spells-srd.aIHY2DArKFweIrpf",
        "Compendium.pf2ettw.spells-srd.atlgGNI1E1Ox3O3a",
        "Compendium.pf2ettw.spells-srd.bay4AfSu2iIozNNW",
        "Compendium.pf2ettw.spells-srd.czO0wbT1i320gcu9",
        "Compendium.pf2ettw.spells-srd.dINQzhqGmIsqGMUY",
        "Compendium.pf2ettw.spells-srd.drmvQJETA3WZzXyw",
        "Compendium.pf2ettw.spells-srd.e36Z2t6tLdW3RUzZ",
        "Compendium.pf2ettw.spells-srd.fprqWKUc0jnMIyGU",
        "Compendium.pf2ettw.spells-srd.gISYsBFby1TiXfBt",
        "Compendium.pf2ettw.spells-srd.ivKnEtI1z4UqEKIA",
        "Compendium.pf2ettw.spells-srd.kuoYff1csM5eAcAP",
        "Compendium.pf2ettw.spells-srd.lbrWMnS2pecKaSVB",
        "Compendium.pf2ettw.spells-srd.lsR3RLEdBG4rcSzd",
        "Compendium.pf2ettw.spells-srd.nXmC2Xx9WmS5NsAo",
        "Compendium.pf2ettw.spells-srd.o6YCGx4lycsYpww4",
        "Compendium.pf2ettw.spells-srd.pZTqGY1MLRjgKasV",
        "Compendium.pf2ettw.spells-srd.pt3gEnzA159uHcJC",
        "Compendium.pf2ettw.spells-srd.pwzdSlJgYqN7bs2w",
        "Compendium.pf2ettw.spells-srd.q5qmNn144ZJGxnvJ",
        "Compendium.pf2ettw.spells-srd.qTr2oCgIXl703Whb",
        "Compendium.pf2ettw.spells-srd.qwlh6aDgi86U3Q7H",
        "Compendium.pf2ettw.spells-srd.r4HLQcYwB62bTayl",
        "Compendium.pf2ettw.spells-srd.sFwoKj0TsacsmoWj",
        "Compendium.pf2ettw.spells-srd.vLA0q0WOK2YPuJs6",
        "Compendium.pf2ettw.spells-srd.vLzFcIaSXs7YTIqJ",
        "Compendium.pf2ettw.spells-srd.vTQvfYu2llKQedmY",
        "Compendium.pf2ettw.spells-srd.vctIUOOgSmxAF0KG",
        "Compendium.pf2ettw.spells-srd.wzctak6BxOW8xvFV",
        "Compendium.pf2ettw.spells-srd.x5rGOmhDRDVQPrnW",
        "Compendium.pf2ettw.spells-srd.x7SPrsRxGb2Vy2nu",
        "Compendium.pf2ettw.spells-srd.x9RIFhquazom4p02",
        "Compendium.pf2ettw.spells-srd.xRgU9rrhmGAgG4Rc",
        "Compendium.pf2ettw.spells-srd.yH13KXUK2x093NUv",
        "Compendium.pf2ettw.spells-srd.yM3KTTSAIHhyuP14",
        "Compendium.pf2ettw.spells-srd.zlnXpME1T2uvn8Lr",
        "Compendium.pf2ettw.spells-srd.zul5cBTfr7NXHBZf",
    ]);
}
