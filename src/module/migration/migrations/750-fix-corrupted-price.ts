import { ItemSourcepf2ettw } from "@item/data";
import { isPhysicalData } from "@item/data/helpers";
import { Coinspf2ettw } from "@item/physical/helpers";
import { MigrationBase } from "../base";

export class Migration750FixCorruptedPrice extends MigrationBase {
    static override version = 0.75;

    override async updateItem(item: ItemSourcepf2ettw) {
        if (!isPhysicalData(item) && item.type !== "kit") return;

        if (typeof item.data.price === "string") {
            item.data.price = { value: Coinspf2ettw.fromString(item.data.price).strip() };
        }
    }
}
