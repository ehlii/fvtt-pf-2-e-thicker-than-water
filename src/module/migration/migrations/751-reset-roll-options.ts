import { ActorSourcepf2ettw } from "@actor/data";
import { isObject } from "@util";
import { MigrationBase } from "../base";

/** Reset all roll options now that most are no longer stored on actors */
export class Migration751ResetRollOptions extends MigrationBase {
    static override version = 0.751;

    override async updateActor(source: ActorSourcepf2ettw): Promise<void> {
        if (isObject(source.flags.pf2ettw) && "rollOptions" in source.flags.pf2ettw) {
            source.flags.pf2ettw["-=rollOptions"] = null;
        }
    }
}
