import { RuleElementpf2ettw, RuleElementData, RuleElementSource, RuleElementOptions } from "..";
import { ActorType } from "@actor/data";
import { Characterpf2ettw } from "@actor";
import { Itempf2ettw } from "@item";

/**
 * @category RuleElement
 */
class CraftingFormulaRuleElement extends RuleElementpf2ettw {
    protected static override validActorTypes: ActorType[] = ["character"];

    constructor(data: CraftingFormulaSource, item: Embedded<Itempf2ettw>, options?: RuleElementOptions) {
        super(data, item, options);

        if (!(typeof data.uuid === "string" && /^(?:Compendium|Item)\..*[a-z0-9]{16}$/i.test(data.uuid))) {
            this.failValidation(`Crafting formula rule element on ${item.name} (${item.uuid}) has a malformed UUID`);
        }
    }

    override beforePrepareData(): void {
        if (this.ignored) return;

        this.actor.data.data.crafting.formulas.push({ uuid: this.data.uuid });
    }
}

interface CraftingFormulaRuleElement extends RuleElementpf2ettw {
    data: CraftingFormulaData;

    get actor(): Characterpf2ettw;
}

interface CraftingFormulaData extends RuleElementData {
    uuid: ItemUUID;
}

interface CraftingFormulaSource extends RuleElementSource {
    uuid?: unknown;
}

export { CraftingFormulaRuleElement };
