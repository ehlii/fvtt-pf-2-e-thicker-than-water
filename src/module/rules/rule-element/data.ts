import { DamageDicepf2ettw, DeferredValue, ModifierAdjustment, Modifierpf2ettw } from "@actor/modifiers";
import { Weaponpf2ettw } from "@item";
import { Predicatepf2ettw, RawPredicate } from "@system/predication";
import { CreatureSensepf2ettw } from "@actor/creature/sense";
import { RollNotepf2ettw } from "@module/notes";
import { MAPSynthetic } from "./multiple-attack-penalty";
import { Strikingpf2ettw } from "./striking";
import { WeaponPotencypf2ettw } from "./weapon-potency";
import { StrikeAdjustment } from "./adjust-strike";

export type RuleElementSource = {
    key: string;
    data?: unknown;
    selector?: string;
    value?: RuleValue | BracketedValue;
    label?: string;
    slug?: unknown;
    predicate?: RawPredicate;
    /** The place in order of application (ascending), among an actor's list of rule elements */
    priority?: number;
    ignored?: unknown;
    requiresInvestment?: unknown;
    requiresEquipped?: unknown;
    removeUponCreate?: unknown;
};

export interface RuleElementData extends RuleElementSource {
    key: string;
    data?: object;
    selector?: string;
    value?: RuleValue | BracketedValue;
    label: string;
    slug?: string | null;
    predicate?: Predicatepf2ettw;
    priority: number;
    ignored: boolean;
    removeUponCreate?: boolean;
}

export type RuleValue = string | number | boolean | object | null;

export interface Bracket<T extends object | number | string> {
    start?: number;
    end?: number;
    value: T;
}

export interface BracketedValue<T extends object | number | string = object | number | string> {
    field?: string;
    brackets: Bracket<T>[];
}

type DeferredModifier = DeferredValue<Modifierpf2ettw | null>;

interface RuleElementSynthetics {
    criticalSpecalizations: {
        standard: CritSpecSynthetic[];
        alternate: CritSpecSynthetic[];
    };
    damageDice: Record<string, DamageDicepf2ettw[]>;
    modifierAdjustments: Record<string, ModifierAdjustment[]>;
    multipleAttackPenalties: Record<string, MAPSynthetic[]>;
    rollNotes: Record<string, RollNotepf2ettw[]>;
    rollSubstitutions: Record<string, RollSubstitution[]>;
    rollTwice: Record<string, RollTwiceSynthetic[]>;
    senses: SenseSynthetic[];
    statisticsModifiers: Record<string, DeferredModifier[]>;
    strikeAdjustments: StrikeAdjustment[];
    strikes: Map<string, Embedded<Weaponpf2ettw>>;
    striking: Record<string, Strikingpf2ettw[]>;
    weaponPotency: Record<string, WeaponPotencypf2ettw[]>;
    preparationWarnings: {
        /** Adds a new preparation warning to be printed when flushed */
        add: (warning: string) => void;
        /** Prints all preparation warnings, but this printout is debounced to handle prep and off-prep cycles */
        flush: () => void;
    };
}

type CritSpecSynthetic = (weapon: Embedded<Weaponpf2ettw>, options: Set<string>) => RollNotepf2ettw | null;

interface RollSubstitution {
    slug: string;
    label: string;
    predicate: Predicatepf2ettw | null;
    value: number;
    ignored: boolean;
    effectType: "fortune" | "misfortune";
}

interface RollTwiceSynthetic {
    keep: "higher" | "lower";
    predicate?: Predicatepf2ettw;
}

interface SenseSynthetic {
    sense: CreatureSensepf2ettw;
    predicate: Predicatepf2ettw | null;
    force: boolean;
}

export { DeferredModifier, RollSubstitution, RollTwiceSynthetic, RuleElementSynthetics, StrikeAdjustment };
