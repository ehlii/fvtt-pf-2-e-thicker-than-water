import type { Characterpf2ettw, NPCpf2ettw } from "@actor";
import { ActorType } from "@actor/data";
import { RuleElementpf2ettw } from "./";

/**
 * @category RuleElement
 */
class DexterityModifierCapRuleElement extends RuleElementpf2ettw {
    protected static override validActorTypes: ActorType[] = ["character", "npc"];

    override beforePrepareData(): void {
        if (!this.test()) return;

        const value = this.resolveValue(this.data.value);
        if (typeof value === "number") {
            this.actor.data.data.attributes.dexCap.push({
                value,
                source: this.label,
            });
        } else {
            console.warn("pf2ettw | Dexterity modifier cap requires at least a label field or item name, and a value");
        }
    }
}

interface DexterityModifierCapRuleElement extends RuleElementpf2ettw {
    get actor(): Characterpf2ettw | NPCpf2ettw;
}

export { DexterityModifierCapRuleElement };
