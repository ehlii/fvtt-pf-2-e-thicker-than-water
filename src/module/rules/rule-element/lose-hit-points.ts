import { Creaturepf2ettw } from "@actor";
import { Itempf2ettw } from "@item";
import { RuleElementpf2ettw, RuleElementSource } from "./";
import { RuleElementOptions } from "./base";

/** Reduce current hit points without applying damage */
export class LoseHitPointsRuleElement extends RuleElementpf2ettw {
    constructor(data: RuleElementSource, item: Embedded<Itempf2ettw>, options?: RuleElementOptions) {
        super(data, item, options);
        const actorIsCreature = this.actor instanceof Creaturepf2ettw;
        const valueIsValid = typeof data.value === "number" || typeof data.value === "string";
        if (!(actorIsCreature && valueIsValid)) this.ignored = true;
    }

    override onCreate(actorUpdates: Record<string, unknown>): void {
        if (this.ignored) return;
        const value = Math.abs(Number(this.resolveValue()) || 0);
        if (typeof value === "number") {
            const currentHP = this.actor.data._source.data.attributes.hp.value;
            actorUpdates["data.attributes.hp.value"] = Math.max(currentHP - value, 0);
        }
    }
}

export interface LoseHitPointsRuleElement extends RuleElementpf2ettw {
    get actor(): Creaturepf2ettw;
}
