import { RuleElementpf2ettw, RuleElementData, RuleElementSource, RuleElementOptions } from ".";
import { Characterpf2ettw } from "@actor";
import { MartialProficiency } from "@actor/character/data";
import { ActorType } from "@actor/data";
import { Itempf2ettw } from "@item";
import { ProficiencyRank } from "@item/data";
import { WeaponCategory } from "@item/weapon/types";
import { PROFICIENCY_RANKS, ZeroToFour } from "@module/data";
import { Predicatepf2ettw, RawPredicate } from "@system/predication";

class MartialProficiencyRuleElement extends RuleElementpf2ettw {
    protected static override validActorTypes: ActorType[] = ["character"];

    constructor(data: MartialProficiencySource, item: Embedded<Itempf2ettw>, options?: RuleElementOptions) {
        data.priority = 9;
        data.immutable = Boolean(data.immutable ?? true);
        data.value ??= 1;

        super(data, item, options);
    }

    private validateData(): void {
        const data = this.data;

        if (typeof data.slug !== "string") {
            this.failValidation("A martial proficiency must have a slug");
        }

        if (!(this.data.definition instanceof Object)) {
            this.failValidation("A martial proficiency must have a definition");
        }

        if (!Predicatepf2ettw.validate(data.definition)) {
            this.failValidation('The "definition" property is invalid');
        }

        if ("sameAs" in data) {
            if (typeof data.sameAs !== "string" || !(data.sameAs in CONFIG.pf2ettw.weaponCategories)) {
                this.failValidation('The "sameAs" property is invalid');
            }
        }

        if ("maxRank" in data) {
            const validRanks: string[] = PROFICIENCY_RANKS.filter((rank) => rank !== "untrained");
            if (!(typeof data.maxRank === "string") || !validRanks.includes(data.maxRank)) {
                this.failValidation('The "maxRank" property is invalid');
            }
        }
    }

    override onApplyActiveEffects(): void {
        this.validateData();
        if (!this.test()) return;

        this.actor.data.data.martial[this.data.slug] = this.createValue();
    }

    /** Set this martial proficiency as an AELike value  */
    private createValue(): MartialProficiency {
        const rank = Math.clamped(Number(this.resolveValue()), 1, 4) as ZeroToFour;
        const proficiency: MartialProficiency = {
            definition: new Predicatepf2ettw(this.resolveInjectedProperties(this.data.definition)),
            immutable: this.data.immutable ?? true,
            label: this.label,
            rank,
            value: 0,
            breakdown: "",
        };
        if (this.data.sameAs) proficiency.sameAs = this.data.sameAs;
        if (this.data.maxRank) proficiency.maxRank = this.data.maxRank;

        return proficiency;
    }
}

interface MartialProficiencyRuleElement extends RuleElementpf2ettw {
    data: MartialProficiencyData;

    get actor(): Characterpf2ettw;
}

interface MartialProficiencyData extends RuleElementData {
    key: "MartialProficiency";
    /** The key to be used for this proficiency in `Characterpf2ettw#data#data#martial` */
    slug: string;
    /** The criteria for matching qualifying weapons and other attacks */
    definition: RawPredicate;
    /** Whether this proficiency's rank can be manually changed */
    immutable: boolean;
    /** The attack category to which this proficiency's rank is linked */
    sameAs: WeaponCategory;
    /** The maximum rank this proficiency can reach, if any */
    maxRank?: Exclude<ProficiencyRank, "untrained">;
    /** Initially a number indicating rank, changed into a `MartialProficiency` object for overriding as an AE-like */
    value: number | MartialProficiency;
}

export interface MartialProficiencySource extends RuleElementSource {
    definition?: unknown;
    sameAs?: unknown;
    immutable?: unknown;
    maxRank?: unknown;
}

export { MartialProficiencyRuleElement };
