import { Predicatepf2ettw } from "@system/predication";
import { RuleElementpf2ettw } from "./";

/**
 * @category RuleElement
 */
export class MultipleAttackPenaltyRuleElement extends RuleElementpf2ettw {
    override beforePrepareData(): void {
        const selector = this.resolveInjectedProperties(this.data.selector);
        const label = this.resolveInjectedProperties(this.label);
        const value = Number(this.resolveValue(this.data.value)) || 0;
        if (selector && label && value) {
            const map: MAPSynthetic = { label, penalty: value };
            if (this.data.predicate) map.predicate = this.data.predicate;
            const penalties = (this.actor.synthetics.multipleAttackPenalties[selector] ??= []);
            penalties.push(map);
        } else {
            console.warn(
                "pf2ettw | Multiple attack penalty requires at least a selector field and a non-empty value field"
            );
        }
    }
}

export interface MAPSynthetic {
    label: string;
    penalty: number;
    predicate?: Predicatepf2ettw;
}
