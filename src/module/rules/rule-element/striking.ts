import { RuleElementpf2ettw } from "./";
import { getStrikingDice } from "@item/runes";
import { Weaponpf2ettw } from "@item";
import { ActorType } from "@actor/data";
import { Predicatepf2ettw } from "@system/predication";

/**
 * @category RuleElement
 */
export class StrikingRuleElement extends RuleElementpf2ettw {
    protected static override validActorTypes: ActorType[] = ["character", "npc"];

    override beforePrepareData(): void {
        const selector = this.resolveInjectedProperties(this.data.selector);
        const strikingValue =
            "value" in this.data
                ? this.data.value
                : this.item instanceof Weaponpf2ettw
                ? getStrikingDice(this.item.data.data)
                : 0;
        const value = this.resolveValue(strikingValue);
        if (selector && typeof value === "number") {
            const label = this.data.label.includes(":")
                ? this.label.replace(/^[^:]+:\s*|\s*\([^)]+\)$/g, "")
                : this.data.label;
            const striking: Strikingpf2ettw = { label, bonus: value };
            if (this.data.predicate) {
                striking.predicate = this.data.predicate;
            }
            const strikings = (this.actor.synthetics.striking[selector] ??= []);
            strikings.push(striking);
        } else {
            console.warn("pf2ettw | Striking requires at least a selector field and a non-empty value field");
        }
    }
}

export interface Strikingpf2ettw {
    label: string;
    bonus: number;
    predicate?: Predicatepf2ettw;
}
