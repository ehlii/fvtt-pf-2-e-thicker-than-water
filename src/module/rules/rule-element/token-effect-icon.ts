import { TokenEffect } from "@actor/token-effect";
import { RuleElementpf2ettw } from "./";

/**
 * Add an effect icon to an actor's token
 * @category RuleElement
 */
export class TokenEffectIconRuleElement extends RuleElementpf2ettw {
    override afterPrepareData(): void {
        if (!this.test()) return;

        const path =
            typeof this.data.value === "string" ? this.resolveInjectedProperties(this.data.value) : this.item.img;
        this.actor.data.data.tokenEffects.push(new TokenEffect(path.trim()));
    }
}
