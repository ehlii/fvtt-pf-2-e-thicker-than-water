import { RuleElementpf2ettw } from "./";
import { Weaponpf2ettw } from "@item";
import { ActorType } from "@actor/data";
import { Predicatepf2ettw } from "@system/predication";
import { MODIFIER_TYPE } from "@actor/modifiers";
import { WeaponPropertyRuneType } from "@item/weapon/types";

/**
 * Copies potency runes from the weapon its attached to, to another weapon based on a predicate.
 * @category RuleElement
 */
class WeaponPotencyRuleElement extends RuleElementpf2ettw {
    protected static override validActorTypes: ActorType[] = ["character", "npc"];

    override beforePrepareData(): void {
        if (this.ignored) return;

        const { weaponPotency } = this.actor.synthetics;
        const selector = this.resolveInjectedProperties(this.data.selector);
        const { item } = this;
        const potencyValue = this.data.value ?? (item instanceof Weaponpf2ettw ? item.data.data.potencyRune.value : 0);
        const value = this.resolveValue(potencyValue);
        if (selector && typeof value === "number") {
            const bonusType =
                game.settings.get("pf2ettw", "automaticBonusVariant") === "noABP"
                    ? MODIFIER_TYPE.ITEM
                    : MODIFIER_TYPE.ITEM;

            const label = this.data.label.includes(":")
                ? this.label.replace(/^[^:]+:\s*|\s*\([^)]+\)$/g, "")
                : this.data.label;
            const potency: WeaponPotencypf2ettw = { label, bonus: value, type: bonusType };
            if (this.data.predicate) {
                potency.predicate = this.data.predicate;
            }
            weaponPotency[selector] = (weaponPotency[selector] || []).concat(potency);
        } else {
            this.failValidation("Weapon potency requires at least a selector field and a non-empty value field");
        }
    }
}

interface WeaponPotencypf2ettw {
    label: string;
    bonus: number;
    type: "item" | "potency";
    predicate?: Predicatepf2ettw;
    property?: WeaponPropertyRuneType[];
}

export { WeaponPotencyRuleElement, WeaponPotencypf2ettw };
