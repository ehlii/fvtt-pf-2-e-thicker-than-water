import { AmbientLightpf2ettw } from "@module/canvas";
import { Scenepf2ettw } from ".";

class AmbientLightDocumentpf2ettw extends AmbientLightDocument {
    /** Is this light actually a source of darkness? */
    get isDarkness(): boolean {
        return this.object.source.isDarkness;
    }
}

interface AmbientLightDocumentpf2ettw extends AmbientLightDocument {
    readonly data: foundry.data.AmbientLightData<AmbientLightDocumentpf2ettw>;

    readonly parent: Scenepf2ettw | null;

    get object(): AmbientLightpf2ettw;
}

export { AmbientLightDocumentpf2ettw };
