import { ZeroToTwo } from "@module/data";
import type {
    AmbientLightDocumentpf2ettw,
    MeasuredTemplateDocumentpf2ettw,
    Scenepf2ettw,
    TileDocumentpf2ettw,
    TokenDocumentpf2ettw,
} from ".";

export interface SceneDatapf2ettw<T extends Scenepf2ettw>
    extends foundry.data.SceneData<
        T,
        TokenDocumentpf2ettw,
        AmbientLightDocumentpf2ettw,
        AmbientSoundDocument,
        DrawingDocument,
        MeasuredTemplateDocumentpf2ettw,
        NoteDocument,
        TileDocumentpf2ettw,
        WallDocument
    > {
    flags: {
        pf2ettw: {
            [key: string]: unknown;
            syncDarkness: "enabled" | "disabled" | "default";
        };
        [key: string]: Record<string, unknown>;
    };
}

export enum LightLevels {
    DARKNESS = 1 / 4,
    BRIGHT_LIGHT = 3 / 4,
}

export type LightLevel = ZeroToTwo;
