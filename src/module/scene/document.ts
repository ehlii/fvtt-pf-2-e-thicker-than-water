import { LightLevels, SceneDatapf2ettw } from "./data";
import { SceneConfigpf2ettw } from "./sheet";
import {
    AmbientLightDocumentpf2ettw,
    MeasuredTemplateDocumentpf2ettw,
    TileDocumentpf2ettw,
    TokenDocumentpf2ettw,
} from ".";

class Scenepf2ettw extends Scene<
    AmbientLightDocumentpf2ettw,
    MeasuredTemplateDocumentpf2ettw,
    TileDocumentpf2ettw,
    TokenDocumentpf2ettw
> {
    /** Is the rules-based vision setting enabled? */
    get rulesBasedVision(): boolean {
        const settingEnabled = game.settings.get("pf2ettw", "automation.rulesBasedVision");
        return this.data.tokenVision && settingEnabled;
    }

    get lightLevel(): number {
        return 1 - this.data.darkness;
    }

    get isBright(): boolean {
        return this.lightLevel >= LightLevels.BRIGHT_LIGHT;
    }

    get isDimlyLit(): boolean {
        return !this.isBright && !this.isDark;
    }

    get isDark(): boolean {
        return this.lightLevel <= LightLevels.DARKNESS;
    }

    /** Toggle Unrestricted Global Vision according to scene darkness level */
    override prepareBaseData(): void {
        super.prepareBaseData();
        if (this.rulesBasedVision) {
            this.data.globalLight = true;
            this.data.hasGlobalThreshold = true;
            this.data.globalLightThreshold = 1 - (LightLevels.DARKNESS + 0.001);
        }

        this.data.flags.pf2ettw ??= { syncDarkness: "default" };
        this.data.flags.pf2ettw.syncDarkness ??= "default";
    }
}

interface Scenepf2ettw {
    _sheet: SceneConfigpf2ettw<this> | null;

    readonly data: SceneDatapf2ettw<this>;

    get sheet(): SceneConfigpf2ettw<this>;
}

export { Scenepf2ettw };
