import { MeasuredTemplatepf2ettw } from "@module/canvas/measured-template";
import { Scenepf2ettw } from "./document";

export class MeasuredTemplateDocumentpf2ettw extends MeasuredTemplateDocument {}

export interface MeasuredTemplateDocumentpf2ettw extends MeasuredTemplateDocument {
    readonly parent: Scenepf2ettw | null;

    readonly _object: MeasuredTemplatepf2ettw | null;
}
