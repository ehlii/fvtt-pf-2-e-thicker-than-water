import { TokenDocumentpf2ettw } from "@scene";

export interface TokenDatapf2ettw<T extends TokenDocumentpf2ettw = TokenDocumentpf2ettw>
    extends foundry.data.TokenData<T> {
    actorData: DeepPartial<NonNullable<T["actor"]>["data"]["_source"]>;
    flags: {
        pf2ettw: {
            [key: string]: unknown;
            linkToActorSize: boolean;
            autoscale: boolean;
        };
        [key: string]: Record<string, unknown>;
    };
}
