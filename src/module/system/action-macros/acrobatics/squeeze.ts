import { ActionMacroHelpers, SkillActionOptions } from "..";

export function squeeze(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "acrobatics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph,
        title: "pf2ettw.Actions.Squeeze.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:squeeze"],
        extraOptions: ["action:squeeze"],
        traits: ["exploration", "move"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Squeeze", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Squeeze", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Squeeze", "criticalFailure"),
        ],
    });
}
