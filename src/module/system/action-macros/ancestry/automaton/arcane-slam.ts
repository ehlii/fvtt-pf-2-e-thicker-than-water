import { ActionMacroHelpers, SkillActionOptions } from "../..";
import { RollNotepf2ettw } from "@module/notes";
import { Predicatepf2ettw } from "@system/predication";
import { Actorpf2ettw, Creaturepf2ettw } from "@actor";
import { MODIFIER_TYPE, Modifierpf2ettw } from "@actor/modifiers";

export function arcaneSlam(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");

    const { actor, token } = ActionMacroHelpers.target();

    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "D",
        title: "pf2ettw.Actions.ArcaneSlam.Title",
        subtitle,
        modifiers: (roller: Actorpf2ettw) => {
            const modifiers = options.modifiers?.length ? [...options.modifiers] : [];
            if (roller instanceof Creaturepf2ettw && actor instanceof Creaturepf2ettw) {
                const attackerSize = roller.data.data.traits.size;
                const targetSize = actor.data.data.traits.size;
                const sizeDifference = attackerSize.difference(targetSize);
                const sizeModifier = new Modifierpf2ettw(
                    "pf2ettw.Actions.ArcaneSlam.Modifier.SizeDifference",
                    Math.clamped(2 * sizeDifference, -4, 4),
                    MODIFIER_TYPE.CIRCUMSTANCE
                );
                if (sizeModifier.modifier) {
                    modifiers.push(sizeModifier);
                }
            }
            return modifiers;
        },
        rollOptions: ["all", checkType, stat, "action:arcane-slam"],
        extraOptions: ["action:arcane-slam"],
        traits: ["automaton"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.fortitude,
        extraNotes: (selector: string) => {
            const notes = [
                ActionMacroHelpers.note(selector, "pf2ettw.Actions.ArcaneSlam", "criticalSuccess"),
                ActionMacroHelpers.note(selector, "pf2ettw.Actions.ArcaneSlam", "success"),
                ActionMacroHelpers.note(selector, "pf2ettw.Actions.ArcaneSlam", "failure"),
                ActionMacroHelpers.note(selector, "pf2ettw.Actions.ArcaneSlam", "criticalFailure"),
            ];
            if (!actor) {
                const translated = game.i18n.localize("pf2ettw.Actions.ArcaneSlam.Notes.NoTarget");
                notes.unshift(
                    new RollNotepf2ettw({
                        selector,
                        text: `<p class="compact-text">${translated}</p>`,
                        predicate: new Predicatepf2ettw(),
                        outcome: [],
                    })
                );
            }
            return notes;
        },
        target: () => (actor && token ? { actor, token } : null),
    });
}
