import { ActionMacroHelpers, SkillActionOptions } from "..";

export function disarm(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.Disarm.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:disarm"],
        extraOptions: ["action:disarm"],
        traits: ["attack"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.reflex,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Disarm", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Disarm", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Disarm", "criticalFailure"),
        ],
        weapon: options?.weapon,
        weaponTrait: "disarm",
    });
}
