import { ActionMacroHelpers, SkillActionOptions } from "..";

export function highJump(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "D",
        title: "pf2ettw.Actions.HighJump.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:stride", "action:leap", "action:high-jump"],
        extraOptions: ["action:stride", "action:leap", "action:high-jump"],
        traits: ["move"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.HighJump", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.HighJump", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.HighJump", "failure"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.HighJump", "criticalFailure"),
        ],
    });
}
