import { ActionMacroHelpers, SkillActionOptions } from "..";

export function hurl(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.Hurl.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:hurl"],
        extraOptions: ["action:hurl"],
        traits: ["move"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Hurl", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Hurl", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Hurl", "failure"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Hurl", "criticalFailure"),
        ],
    });
}
