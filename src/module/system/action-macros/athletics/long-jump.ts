import { ActionMacroHelpers, SkillActionOptions } from "..";

export function longJump(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "D",
        title: "pf2ettw.Actions.LongJump.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:stride", "action:leap", "action:long-jump"],
        extraOptions: ["action:stride", "action:leap", "action:long-jump"],
        traits: ["move"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.LongJump", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.LongJump", "failure"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.LongJump", "criticalFailure"),
        ],
    });
}
