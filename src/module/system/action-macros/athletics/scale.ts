import { ActionMacroHelpers, SkillActionOptions } from "..";

export function scale(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.Scale.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:scale"],
        extraOptions: ["action:scale"],
        traits: ["attack"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Scale", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Scale", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Scale", "criticalFailure"),
        ],
        weapon: options?.weapon,
        weaponTrait: "scale",
    });
}
