import { ActionMacroHelpers, SkillActionOptions } from "..";

export function trip(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.Trip.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:trip"],
        extraOptions: ["action:trip"],
        traits: ["attack"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.reflex,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Trip", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Trip", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Trip", "criticalFailure"),
        ],
        weapon: options?.weapon,
        weaponTrait: "trip",
        weaponTraitWithPenalty: "ranged-trip",
    });
}
