import { ActionMacroHelpers, SkillActionOptions } from "..";
import { MODIFIER_TYPE, Modifierpf2ettw } from "@actor/modifiers";
import { Creaturepf2ettw } from "@actor";
import type { Actorpf2ettw } from "@actor/base";
import { ActorSizepf2ettw } from "@actor/data/size";

function determineSizeBonus(actorSize: ActorSizepf2ettw, targetSize: ActorSizepf2ettw) {
    const sizeDifference = actorSize.difference(targetSize);

    return Math.clamped(2 * sizeDifference, -4, 4);
}

export function whirlingThrow(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "athletics");

    const actors = canvas.tokens.controlled.map((token) => token.actor) as Actorpf2ettw[];
    const actor = actors[0];

    const targets = Array.from(game.user.targets).filter((token) => token.actor instanceof Creaturepf2ettw);
    const target = targets[0].actor;

    options.modifiers ||= [];

    if (target instanceof Creaturepf2ettw && actor instanceof Creaturepf2ettw) {
        const actorSize = actor.data.data.traits.size;
        const targetSize = target.data.data.traits.size;
        const sizeModifier = new Modifierpf2ettw(
            "Size Modifier",
            determineSizeBonus(actorSize, targetSize),
            MODIFIER_TYPE.CIRCUMSTANCE
        );
        if (sizeModifier.modifier) {
            options.modifiers.push(sizeModifier);
        }
    }

    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.WhirlingThrow.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:whirling-throw"],
        extraOptions: ["action:whirling-throw"],
        traits: ["monk"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.fortitude,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.WhirlingThrow", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.WhirlingThrow", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.WhirlingThrow", "failure"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.WhirlingThrow", "criticalFailure"),
        ],
    });
}
