import { ActionMacroHelpers, SkillActionOptions } from "..";

export function impersonate(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "deception");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph,
        title: "pf2ettw.Actions.Impersonate.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:impersonate"],
        extraOptions: ["action:impersonate"],
        traits: ["concentrate", "exploration", "manipulate", "secret"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.perception,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Impersonate", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Impersonate", "failure"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Impersonate", "criticalFailure"),
        ],
    });
}
