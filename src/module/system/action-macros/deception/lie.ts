import { ActionMacroHelpers, SkillActionOptions } from "..";

export function lie(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "deception");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph,
        title: "pf2ettw.Actions.Lie.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:lie"],
        extraOptions: ["action:lie"],
        traits: ["auditory", "concentrate", "linguistic", "mental", "secret"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.perception,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Lie", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Lie", "failure"),
        ],
    });
}
