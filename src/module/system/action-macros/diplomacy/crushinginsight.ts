import { ActionMacroHelpers, SkillActionOptions } from "..";

export function crushingInsight(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "diplomacy");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph,
        title: "pf2ettw.Actions.CrushingInsight.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:crushing-insight"],
        extraOptions: ["action:crushing-insight"],
        traits: ["auditory", "concentrate", "linguistic", "mental"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.will,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.CrushingInsight", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.CrushingInsight", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.CrushingInsight", "criticalFailure"),
        ],
    });
}
