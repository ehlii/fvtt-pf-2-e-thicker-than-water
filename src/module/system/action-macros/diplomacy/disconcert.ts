import { ActionMacroHelpers, SkillActionOptions } from "..";

export function disconcertdip(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "diplomacy");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.Disconcert.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:disconcertdip"],
        extraOptions: ["action:disconcertdip"],
        traits: ["visual", "emotion", "mental"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.will,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Disconcert", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.Disconcert", "criticalFailure"),
        ],
    });
}
