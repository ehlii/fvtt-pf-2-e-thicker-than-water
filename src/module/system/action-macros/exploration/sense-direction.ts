import { ActionMacroHelpers, SkillActionOptions } from "..";
import { Modifierpf2ettw } from "@actor/modifiers";

export function senseDirection(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "survival");

    const modifiers = (options.modifiers ?? []).concat(
        new Modifierpf2ettw({
            label: "pf2ettw.Actions.SenseDirection.Modifier.NoCompass",
            modifier: -2,
            predicate: {
                not: ["compass-in-possession"],
            },
        })
    );

    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph,
        title: "pf2ettw.Actions.SenseDirection.Title",
        subtitle,
        modifiers,
        rollOptions: ["all", checkType, stat, "action:sense-direction"],
        extraOptions: ["action:sense-direction"],
        traits: ["exploration", "secret"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.SenseDirection", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.SenseDirection", "success"),
        ],
    });
}
