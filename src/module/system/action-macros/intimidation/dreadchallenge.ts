import { ActionMacroHelpers, SkillActionOptions } from "..";

export function dreadChallenge(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "intimidation");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.DreadChallenge.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:dreadchallenge"],
        extraOptions: ["action:dreadchallenge"],
        traits: ["mental", "auditory"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.will,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.DreadChallenge", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.DreadChallenge", "success"),
        ],
    });
}
