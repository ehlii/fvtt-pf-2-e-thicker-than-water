import { ActionMacroHelpers, SkillActionOptions } from "..";

export function innocentGuise(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionMacroHelpers.resolveStat(options?.skill ?? "intimidation");
    ActionMacroHelpers.simpleRollActionCheck({
        actors: options.actors,
        statName: property,
        actionGlyph: options.glyph ?? "A",
        title: "pf2ettw.Actions.InnocentGuise.Title",
        subtitle,
        modifiers: options.modifiers,
        rollOptions: ["all", checkType, stat, "action:innocentguise"],
        extraOptions: ["action:innocentguise"],
        traits: ["charm", "emotion", "mental"],
        checkType,
        event: options.event,
        callback: options.callback,
        difficultyClass: options.difficultyClass,
        difficultyClassStatistic: (target) => target.saves.will,
        extraNotes: (selector: string) => [
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.InnocentGuise", "criticalSuccess"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.InnocentGuise", "success"),
            ActionMacroHelpers.note(selector, "pf2ettw.Actions.InnocentGuise", "failure"),
        ],
    });
}
