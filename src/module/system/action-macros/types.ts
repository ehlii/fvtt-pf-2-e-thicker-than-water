import { Actorpf2ettw, Creaturepf2ettw } from "@actor";
import { Modifierpf2ettw } from "@actor/modifiers";
import { WeaponTrait } from "@item/weapon/types";
import { RollNotepf2ettw } from "@module/notes";
import { TokenDocumentpf2ettw } from "@scene";
import { CheckRoll } from "@system/check/roll";
import { CheckDC, DegreeOfSuccessString } from "@system/degree-of-success";
import { CheckType } from "@system/rolls";
import { Statistic, StatisticDataWithDC } from "@system/statistic";

type ActionGlyph = "A" | "D" | "T" | "R" | "F" | "a" | "d" | "t" | "r" | "f" | 1 | 2 | 3 | "1" | "2" | "3";

interface CheckResultCallback {
    actor: Actorpf2ettw;
    message?: ChatMessage;
    outcome: DegreeOfSuccessString | null | undefined;
    roll: Rolled<CheckRoll>;
}

interface SimpleRollActionCheckOptions {
    actors: Actorpf2ettw | Actorpf2ettw[] | undefined;
    statName: string;
    actionGlyph: ActionGlyph | undefined;
    title: string;
    subtitle: string;
    content?: (title: string) => Promise<string | null | undefined | void> | string | null | undefined | void;
    modifiers: ((roller: Actorpf2ettw) => Modifierpf2ettw[] | undefined) | Modifierpf2ettw[] | undefined;
    rollOptions: string[];
    extraOptions: string[];
    traits: string[];
    checkType: CheckType;
    event: JQuery.TriggeredEvent;
    difficultyClass?: CheckDC;
    difficultyClassStatistic?: (creature: Creaturepf2ettw) => Statistic<StatisticDataWithDC>;
    extraNotes?: (selector: string) => RollNotepf2ettw[];
    callback?: (result: CheckResultCallback) => void;
    createMessage?: boolean;
    weapon?: string;
    weaponTrait?: WeaponTrait;
    weaponTraitWithPenalty?: WeaponTrait;
    target?: () => { token: TokenDocumentpf2ettw; actor: Actorpf2ettw } | null;
}

interface ActionDefaultOptions {
    event: JQuery.TriggeredEvent;
    actors?: Actorpf2ettw | Actorpf2ettw[];
    glyph?: ActionGlyph;
    modifiers?: Modifierpf2ettw[];
    callback?: (result: CheckResultCallback) => void;
    weapon?: string;
}

interface SkillActionOptions extends ActionDefaultOptions {
    skill?: string;
    difficultyClass?: CheckDC;
}

export { ActionGlyph, CheckResultCallback, SimpleRollActionCheckOptions, ActionDefaultOptions, SkillActionOptions };
