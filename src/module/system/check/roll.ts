import { Userpf2ettw } from "@module/user";
import { RollDatapf2ettw } from "@system/rolls";

class CheckRoll extends Roll<RollDatapf2ettw> {
    roller: Userpf2ettw | null;

    isReroll: boolean;

    isRerollable: boolean;

    constructor(formula: string, data: Partial<RollDatapf2ettw> = {}, options?: Partial<RollDatapf2ettw>) {
        super(formula, data, options);

        this.isReroll = data.isReroll ?? false;
        this.isRerollable =
            !this.isReroll && !this.dice.some((d) => d.modifiers.includes("kh") || d.modifiers.includes("kl"));
        this.roller = game.users.get(this.data.rollerId ?? "") ?? null;
    }

    override toJSON(): CheckRollJSON {
        return mergeObject(super.toJSON(), { data: { isReroll: this.isReroll, rollerId: this.roller?.id } });
    }
}

interface CheckRollJSON extends RollJSON {
    data?: Partial<RollDatapf2ettw>;
}

export { CheckRoll, CheckRollJSON };
