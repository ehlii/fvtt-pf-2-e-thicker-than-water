import { Actorpf2ettw, Characterpf2ettw /* , Creaturepf2ettw*/ } from "@actor";
import { ItemEffectData } from "@actor/character/data";
import { Tokenpf2ettw } from "@module/canvas";
import { Errorpf2ettw } from "@util";
import { Effectpf2ettw } from "@item/effect";
import { ITEM_EFFECT_COMPENDIUMS } from "./values";

/** A helper class to manage pf2ettw item effects. */
export class EffectManager {
    static #initialized = false;
    static #visible = false;

    static effects: Map<string, Effectpf2ettw> = new Map();

    static toggleVisible(): void {
        this.#visible = !this.#visible;
    }

    static hide(): void {
        this.#visible = false;
    }

    static async initialize(force = false): Promise<void> {
        if (this.#initialized && !force) return;
        if (!canvas.ready) return;

        console.log("pf2ettw System | Initializing EffectManager Module");

        type EffectCollection = CompendiumCollection<Effectpf2ettw>;
        const itemeffects: Effectpf2ettw[] = [];

        for (const compendium of ITEM_EFFECT_COMPENDIUMS) {
            const content = await game.packs.get<EffectCollection>(`pf2ettw.${compendium}`)?.getDocuments();
            const valid = content?.filter((e): e is Effectpf2ettw => e instanceof Effectpf2ettw) ?? [];
            itemeffects.push(...valid);
        }
        const entries = itemeffects?.map((e): [string, Effectpf2ettw] => [e.id, e]);
        this.effects = new Map(entries);

        if (!force) this.hookIntoFoundry();
        this.#initialized = true;
    }

    /**
     * Get an effect using the effect id.
     * @param id A document item id
     */
    static getEffect(id: string): Effectpf2ettw {
        const effect = EffectManager.effects.get(id);
        if (!effect) throw Errorpf2ettw(`Unexpected failure looking up effect ${id}`);

        return effect;
    }

    /** Hook item effects into FoundryVTT */
    static hookIntoFoundry() {
        Hooks.on("renderTokenHUD", (_app, html, data) => {
            EffectManager.hookOnRenderTokenHUD(html, data);
        });
        Hooks.on("clearTokenHUD", (tokenHUD, _token) => {
            if (!(tokenHUD instanceof TokenHUD)) return;

            if (tokenHUD._state === TokenHUD.RENDER_STATES.NONE) {
                EffectManager.hide();
            }
        });
    }

    private static hookOnRenderTokenHUD(html: JQuery, tokenData: TokenHUDData) {
        const token = canvas.tokens.get(tokenData._id);

        if (!token) {
            throw Errorpf2ettw(`EffectManager | Could not find token with id: ${tokenData._id}`);
        }

        // Render additional item effects button and menu
        this.createItemEffectMenu(html, token);

        const appliedEffects = token.actor?.itemTypes.effect ?? [];
        const $itemEffectIcons = html.find("img.item-control");

        for (const icon of $itemEffectIcons) {
            const $icon = $(icon);

            const id = $icon.attr("data-id") ?? "";
            const applied = appliedEffects.find((effect) => effect.sourceId?.includes(id));

            if ($icon.hasClass("active") && !applied) {
                $icon.removeClass("active");
            } else if (!$icon.hasClass("active") && applied) {
                $icon.addClass("active");
            }
        }
    }

    /** Creates the button and menu for item effects */
    private static createItemEffectMenu(html: JQuery, token: Tokenpf2ettw): void {
        const actor: Actorpf2ettw | null = token.actor;

        if (!(actor instanceof Characterpf2ettw)) return;

        const actorEffects = actor.itemEffects;
        const allyEffects = [];

        for (const ally of game.actors.values()) {
            if (!(ally instanceof Characterpf2ettw) || !ally.isAllyOf(actor) || ally.id === actor.id) continue;

            const applicable = ally.itemEffects.filter((effect) =>
                ["consumable-effects", "spell-effects"].includes(effect.compendium)
            );
            if (applicable.length > 0) allyEffects.push({ name: ally.name, effects: applicable });
        }

        if (actorEffects.length > 0 || allyEffects.length > 0) {
            const button = document.createElement("div");
            button.classList.add("control-icon");
            if (this.#visible) button.classList.add("active");
            button.dataset.action = "item-effects";

            const icon = document.createElement("i");
            icon.classList.add("fas", "fa-music", "fa-fw");

            let menu = document.createElement("div");
            menu.classList.add("item-effects");
            if (this.#visible) menu.classList.add("active");

            const summary = document.createElement("div");
            summary.classList.add("item-effect-summary");

            if (actorEffects.length > 0) {
                menu = this.createEffectEntries(menu, actor.name, actorEffects);
            }

            for (const ally of allyEffects) {
                menu = this.createEffectEntries(menu, ally.name, ally.effects);
            }

            button.appendChild(icon);
            menu.appendChild(summary);
            button.appendChild(menu);
            html.find("div.right").append(button);

            this.setItemEffectControls(html, actor);
        }
    }

    private static createEffectEntries(menu: HTMLDivElement, name: string, effects: ItemEffectData[]): HTMLDivElement {
        const header = document.createElement("div");
        header.classList.add("item-effect-header");
        header.innerHTML = name;
        menu.append(header);

        effects
            .map((effect) => ({ effect: effect, item: game.pf2ettw.EffectManager.getEffect(effect.id) }))
            .sort((a, b) => a.item.name.localeCompare(b.item.name))
            .map((e) => {
                const img = document.createElement("img");
                img.classList.add("item-control");
                img.dataset.compendium = e.effect.compendium;
                img.dataset.id = e.effect.id;
                img.src = e.item.img;
                img.dataset.effect = e.item.name;

                menu.appendChild(img);
            });

        return menu;
    }

    private static setItemEffectControls(html: JQuery, actor: Actorpf2ettw): void {
        const itemEffects = html.find(".item-effects");
        itemEffects
            .on("click contextmenu", ".item-control", this.toggleItemEffect.bind(actor))
            .on("mouseover mouseout", ".item-control", this.showEffectLabel);

        const button = html.find(".control-icon[data-action=item-effects]");
        button.on("click", (event: JQuery.TriggeredEvent<HTMLElement>) => {
            const target = $(event.currentTarget);
            target.toggleClass("active");
            target.find(".item-effects").toggleClass("active");
            game.pf2ettw.EffectManager.toggleVisible();
        });
    }

    /** Show the Item Effect name and summary on mouseover of the token HUD */
    private static showEffectLabel(event: JQuery.TriggeredEvent) {
        const $toggle = $(event.currentTarget);
        const statusDescr = $("div.item-effect-summary");
        const label = $toggle.attr("data-effect");
        if (label) {
            statusDescr.text(label).toggleClass("active");
        }
    }

    private static async toggleItemEffect(this: Actorpf2ettw, event: JQuery.TriggeredEvent): Promise<void> {
        event.preventDefault();
        event.stopImmediatePropagation();

        const $target = $(event.currentTarget);
        await game.pf2ettw.EffectManager.toggleEffectOnActor($target[0].dataset.id, this);
    }

    /**
     * Adds an item to a token.
     * @param id ID of item to be added to the actor.
     * @param token The token to add the effect to.
     */
    static addEffectToToken(id: string, token: Tokenpf2ettw): Promise<Effectpf2ettw | null>;
    static addEffectToToken(id: string, actor: Actorpf2ettw): Promise<Effectpf2ettw | null>;
    static addEffectToToken(id: string, actorOrToken: Actorpf2ettw | Tokenpf2ettw): Promise<Effectpf2ettw | null>;
    static async addEffectToToken(
        id: string,
        actorOrToken: Actorpf2ettw | Tokenpf2ettw
    ): Promise<Effectpf2ettw | null> {
        const actor = actorOrToken instanceof Actorpf2ettw ? actorOrToken : actorOrToken.actor;
        const effectSource = this.getEffect(id).toObject();

        if (actor) {
            const condition = await actor.createEmbeddedDocuments("Item", [effectSource]);
            return actor.itemTypes.effect.find((effect) => effect.id === condition[0].id) ?? null;
        }

        return null;
    }

    /**
     * A convience alias for adding an item effect to an actor
     * @param id ID of item to be added to the actor.
     * @param actor The actor to add the effect to.
     */
    static async addEffectToActor(id: string, actor: Actorpf2ettw): Promise<Effectpf2ettw | null> {
        return this.addEffectToToken(id, actor);
    }

    static removeEffectFromToken(itemId: string | string[], token: Tokenpf2ettw): Promise<void>;
    static removeEffectFromToken(itemId: string | string[], actor: Actorpf2ettw): Promise<void>;
    static removeEffectFromToken(itemId: string | string[], actorOrToken: Actorpf2ettw | Tokenpf2ettw): Promise<void>;
    static async removeEffectFromToken(
        itemId: string | string[],
        actorOrToken: Actorpf2ettw | Tokenpf2ettw
    ): Promise<void> {
        const actor = actorOrToken instanceof Actorpf2ettw ? actorOrToken : actorOrToken.actor;
        itemId = itemId instanceof Array ? itemId : [itemId];

        if (actor) {
            // not checking that the item is actually an effect
            await Effectpf2ettw.deleteDocuments(itemId, { parent: actor });
        }
    }

    /** A convenience alias for removing an effect from an actor */
    static async removeEffectFromActor(itemId: string | string[], actor: Actorpf2ettw): Promise<void> {
        return this.removeEffectFromToken(itemId, actor);
    }

    /** Convenience function for toggling an item effect existing on an actor on or off */
    static toggleEffectOnToken(id: string, token: Tokenpf2ettw): Promise<Effectpf2ettw | null>;
    static toggleEffectOnToken(id: string, actor: Actorpf2ettw): Promise<Effectpf2ettw | null>;
    static toggleEffectOnToken(id: string, actorOrToken: Actorpf2ettw | Tokenpf2ettw): Promise<Effectpf2ettw | null>;
    static async toggleEffectOnToken(
        id: string,
        actorOrToken: Actorpf2ettw | Tokenpf2ettw
    ): Promise<Effectpf2ettw | null> {
        const actor = actorOrToken instanceof Actorpf2ettw ? actorOrToken : actorOrToken.actor;

        if (actor) {
            const applied = actor.itemTypes.effect.filter((effect) => effect.sourceId?.includes(id)) ?? [];

            if (!applied.length) {
                const effect = await this.addEffectToActor(id, actor);
                if (effect) return effect;
            } else {
                await this.removeEffectFromActor(
                    applied.map((e) => e.id),
                    actor
                );
            }
        }

        return null;
    }

    static async toggleEffectOnActor(id: string, actor: Actorpf2ettw): Promise<Effectpf2ettw | null> {
        return this.toggleEffectOnToken(id, actor);
    }
}
