// import { ConditionSlug } from "@item/condition/data";
import { ITEM_EFFECT_COMPENDIUMS } from "./values";

export type EffectCompendium = typeof ITEM_EFFECT_COMPENDIUMS[number];
