export const ITEM_EFFECT_COMPENDIUMS = [
    "bestiary-effects",
    "campaign-effects",
    "consumable-effects",
    "equipment-effects",
    "feat-effects",
    "feature-effects",
    "movement-effects",
    "spell-effects",
] as const;
