import { Characterpf2ettw } from "@actor";
import { Itempf2ettw } from "@item";
import { AncestrySource, BackgroundSource, ClassSource } from "@item/data";
import { ABCManagerOptions, AncestryBackgroundClassManager } from "@item/abc/manager";
import { Errorpf2ettw } from "@util";

export class ActorImporter {
    /**
     * Adds an ancestry to a given character. Linked features are created automatically.
     * @param {Characterpf2ettw} character The character actor to create the item in
     * @param {string | AncestrySource} ancestry The english ancestry name as a string or the source of the ancestry item to create
     * @param {ABCManagerOptions} options Additional options that are passed to the ABC-Manager
     * @returns {Promise<Itempf2ettw[]>} A Promise which resolve to an array of the items that were created
     */
    static async addAncestry(
        character: Characterpf2ettw,
        ancestry: string | AncestrySource,
        options: ABCManagerOptions
    ): Promise<Itempf2ettw[]> {
        if (!(character instanceof Characterpf2ettw)) {
            throw Errorpf2ettw(`Invalid Actor type. Ancestry items can only be added to characters.`);
        }
        if (typeof ancestry === "string") {
            ancestry = await AncestryBackgroundClassManager.getItemSource("pf2ettw.ancestries", ancestry);
        }
        return AncestryBackgroundClassManager.addABCItem(ancestry, character, options);
    }

    /**
     * Adds a background to a given character. Linked features are created automatically.
     * @param {Characterpf2ettw} character The character actor to create the item in
     * @param {string | BackgroundSource} background The english background name as a string or the source of the background item to create
     * @param {ABCManagerOptions} options Additional options that are passed to the ABC-Manager
     * @returns {Promise<Itempf2ettw[]>} A Promise which resolve to an array of the items that were created
     */
    static async addBackground(
        character: Characterpf2ettw,
        background: string | BackgroundSource,
        options: ABCManagerOptions
    ): Promise<Itempf2ettw[]> {
        if (!(character instanceof Characterpf2ettw)) {
            throw Errorpf2ettw(`Invalid Actor type. Background items can only be added to characters.`);
        }
        if (typeof background === "string") {
            background = await AncestryBackgroundClassManager.getItemSource("pf2ettw.backgrounds", background);
        }
        return AncestryBackgroundClassManager.addABCItem(background, character, options);
    }

    /**
     * Adds a class to a given character. Linked features are created automatically.
     * @param {Characterpf2ettw} character The character actor to create the item in
     * @param {string | ClassSource} cls The english class name as a string or the source of the class item to create
     * @param {ABCManagerOptions} options Additional options that are passed to the ABC-Manager
     * @returns {Promise<Itempf2ettw[]>} A Promise which resolve to an array of the items that were created
     */
    static async addClass(
        character: Characterpf2ettw,
        cls: string | ClassSource,
        options: ABCManagerOptions
    ): Promise<Itempf2ettw[]> {
        if (!(character instanceof Characterpf2ettw)) {
            throw Errorpf2ettw(`Invalid Actor type. Class items can only be added to characters.`);
        }
        if (typeof cls === "string") {
            cls = await AncestryBackgroundClassManager.getItemSource("pf2ettw.classes", cls);
        }
        return AncestryBackgroundClassManager.addABCItem(cls, character, options);
    }
}
