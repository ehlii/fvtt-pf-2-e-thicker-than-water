import { Errorpf2ettw } from "@util";
import MainTranslations from "static/lang/en.json";
import RETranslations from "static/lang/re-en.json";

type Translationspf2ettw = Record<string, TranslationDictionaryValue> & typeof MainTranslations & typeof RETranslations;

export class Localizepf2ettw {
    static ready = false;

    private static _translations: Translationspf2ettw;

    static get translations(): Translationspf2ettw {
        if (!this.ready) {
            throw Errorpf2ettw("Localizepf2ettw instantiated too early");
        }
        if (this._translations === undefined) {
            this._translations = mergeObject(game.i18n._fallback, game.i18n.translations, {
                enforceTypes: true,
            }) as Translationspf2ettw;
        }
        return this._translations;
    }
}
