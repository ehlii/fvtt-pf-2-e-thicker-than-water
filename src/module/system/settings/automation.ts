import { PartialSettingsData, SettingsMenupf2ettw } from "./menu";

type Configpf2ettwListName = typeof AutomationSettings.SETTINGS[number];

export class AutomationSettings extends SettingsMenupf2ettw {
    static override readonly namespace = "automation";

    static override readonly SETTINGS = [
        "rulesBasedVision",
        "effectExpiration",
        "removeExpiredEffects",
        "flankingDetection",
        "lootableNPCs",
    ] as const;

    protected static override get settings(): Record<Configpf2ettwListName, PartialSettingsData> {
        return {
            rulesBasedVision: {
                name: CONFIG.pf2ettw.SETTINGS.automation.rulesBasedVision.name,
                hint: CONFIG.pf2ettw.SETTINGS.automation.rulesBasedVision.hint,
                default: true,
                type: Boolean,
                onChange: () => {
                    window.location.reload();
                },
            },
            effectExpiration: {
                name: CONFIG.pf2ettw.SETTINGS.automation.effectExpiration.name,
                hint: CONFIG.pf2ettw.SETTINGS.automation.effectExpiration.hint,
                default: true,
                type: Boolean,
                onChange: () => {
                    game.actors.forEach((actor) => {
                        actor.prepareData();
                        actor.sheet.render(false);
                        actor.getActiveTokens().forEach((token) => token.drawEffects());
                    });
                },
            },
            removeExpiredEffects: {
                name: CONFIG.pf2ettw.SETTINGS.automation.removeExpiredEffects.name,
                hint: CONFIG.pf2ettw.SETTINGS.automation.removeExpiredEffects.hint,
                default: false,
                type: Boolean,
            },
            flankingDetection: {
                name: CONFIG.pf2ettw.SETTINGS.automation.flankingDetection.name,
                hint: CONFIG.pf2ettw.SETTINGS.automation.flankingDetection.hint,
                default: true,
                type: Boolean,
            },
            lootableNPCs: {
                name: CONFIG.pf2ettw.SETTINGS.automation.lootableNPCs.name,
                hint: CONFIG.pf2ettw.SETTINGS.automation.lootableNPCs.hint,
                default: false,
                type: Boolean,
            },
        };
    }
}
