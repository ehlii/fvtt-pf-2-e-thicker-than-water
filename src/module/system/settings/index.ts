import { VariantRulesSettings } from "./variant-rules";
import { WorldClockSettings } from "./world-clock";
import { HomebrewElements } from "./homebrew";
import { StatusEffects } from "@scripts/actor/status-effects";
import { objectHasKey } from "@util";
import { MigrationRunner } from "@module/migration/runner";
import { AutomationSettings } from "./automation";
import { MetagameSettings } from "./metagame";

export function registerSettings() {
    if (BUILD_MODE === "development") {
        registerWorldSchemaVersion();
    }

    game.settings.register("pf2ettw", "tokens.autoscale", {
        name: "pf2ettw.SETTINGS.Tokens.Autoscale.Name",
        hint: "pf2ettw.SETTINGS.Tokens.Autoscale.Hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
    });

    game.settings.register("pf2ettw", "identifyMagicNotMatchingTraditionModifier", {
        name: "pf2ettw.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Name",
        hint: "pf2ettw.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Hint",
        choices: {
            0: "pf2ettw.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.0",
            2: "pf2ettw.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.2",
            5: "pf2ettw.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.5",
            10: "pf2ettw.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.10",
        },
        type: Number,
        default: 5,
        scope: "world",
        config: true,
    });

    game.settings.register("pf2ettw", "critRule", {
        name: "pf2ettw.SETTINGS.CritRule.Name",
        hint: "pf2ettw.SETTINGS.CritRule.Hint",
        scope: "world",
        config: true,
        default: "doubledamage",
        type: String,
        choices: {
            doubledamage: "pf2ettw.SETTINGS.CritRule.Choices.Doubledamage",
            doubledice: "pf2ettw.SETTINGS.CritRule.Choices.Doubledice",
        },
    });

    game.settings.register("pf2ettw", "compendiumBrowserPacks", {
        name: "pf2ettw.SETTINGS.CompendiumBrowserPacks.Name",
        hint: "pf2ettw.SETTINGS.CompendiumBrowserPacks.Hint",
        default: "{}",
        type: String,
        scope: "world",
        onChange: () => {
            game.pf2ettw.compendiumBrowser.loadSettings();
        },
    });

    game.settings.register("pf2ettw", "enabledRulesUI", {
        name: "pf2ettw.SETTINGS.EnabledRulesUI.Name",
        hint: "pf2ettw.SETTINGS.EnabledRulesUI.Hint",
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
    });

    game.settings.register("pf2ettw", "critFumbleButtons", {
        name: game.i18n.localize("pf2ettw.SETTINGS.critFumbleCardButtons.name"),
        hint: game.i18n.localize("pf2ettw.SETTINGS.critFumbleCardButtons.hint"),
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => {
            window.location.reload();
        },
    });

    game.settings.register("pf2ettw", "drawCritFumble", {
        name: game.i18n.localize("pf2ettw.SETTINGS.critFumbleCards.name"),
        hint: game.i18n.localize("pf2ettw.SETTINGS.critFumbleCards.hint"),
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => {
            window.location.reload();
        },
    });

    const iconChoices = {
        blackWhite: "pf2ettw.SETTINGS.statusEffectType.blackWhite",
        default: "pf2ettw.SETTINGS.statusEffectType.default",
        legacy: "pf2ettw.SETTINGS.statusEffectType.legacy",
    };
    game.settings.register("pf2ettw", "statusEffectType", {
        name: "pf2ettw.SETTINGS.statusEffectType.name",
        hint: "pf2ettw.SETTINGS.statusEffectType.hint",
        scope: "world",
        config: true,
        default: "blackWhite",
        type: String,
        choices: iconChoices,
        onChange: (iconType = "") => {
            if (objectHasKey(iconChoices, iconType)) {
                StatusEffects.migrateStatusEffectUrls(iconType);
            }
        },
    });

    game.settings.register("pf2ettw", "deathIcon", {
        name: "pf2ettw.Settings.DeathIcon.Name",
        hint: "pf2ettw.Settings.DeathIcon.Hint",
        scope: "world",
        config: false,
        default: "icons/svg/skull.svg",
        type: String,
        onChange: (choice?: string) => {
            if (choice) CONFIG.controlIcons.defeated = choice;
        },
    });

    // Don't tell Nath
    game.settings.register("pf2ettw", "nathMode", {
        name: "pf2ettw.SETTINGS.NathMode.Name",
        hint: "pf2ettw.SETTINGS.NathMode.Hint",
        scope: "world",
        config: BUILD_MODE === "development",
        default: false,
        type: Boolean,
    });

    game.settings.register("pf2ettw", "statusEffectShowCombatMessage", {
        name: "pf2ettw.SETTINGS.statusEffectShowCombatMessage.name",
        hint: "pf2ettw.SETTINGS.statusEffectShowCombatMessage.hint",
        scope: "client",
        config: true,
        default: true,
        type: Boolean,
    });

    game.settings.register("pf2ettw", "worldSystemVersion", {
        name: "World System Version",
        scope: "world",
        config: false,
        default: game.system.data.version,
        type: String,
    });

    game.settings.registerMenu("pf2ettw", "automation", {
        name: "pf2ettw.SETTINGS.Automation.Name",
        label: "pf2ettw.SETTINGS.Automation.Label",
        hint: "pf2ettw.SETTINGS.Automation.Hint",
        icon: "fas fa-robot",
        type: AutomationSettings,
        restricted: true,
    });
    game.settings.register("pf2ettw", "automation.actorsDeadAtZero", {
        name: CONFIG.pf2ettw.SETTINGS.automation.actorsDeadAtZero.name,
        scope: "world",
        config: false,
        default: "npcsOnly",
        type: String,
    });
    AutomationSettings.registerSettings();

    game.settings.registerMenu("pf2ettw", "metagame", {
        name: "pf2ettw.SETTINGS.Metagame.Name",
        label: "pf2ettw.SETTINGS.Metagame.Label",
        hint: "pf2ettw.SETTINGS.Metagame.Hint",
        icon: "fas fa-brain",
        type: MetagameSettings,
        restricted: true,
    });
    MetagameSettings.registerSettings();

    game.settings.registerMenu("pf2ettw", "variantRules", {
        name: "pf2ettw.SETTINGS.Variant.Name",
        label: "pf2ettw.SETTINGS.Variant.Label",
        hint: "pf2ettw.SETTINGS.Variant.Hint",
        icon: "fas fa-book",
        type: VariantRulesSettings,
        restricted: true,
    });
    VariantRulesSettings.registerSettings();

    game.settings.registerMenu("pf2ettw", "homebrew", {
        name: "pf2ettw.SETTINGS.Homebrew.Name",
        label: "pf2ettw.SETTINGS.Homebrew.Label",
        hint: "pf2ettw.SETTINGS.Homebrew.Hint",
        icon: "fas fa-beer",
        type: HomebrewElements,
        restricted: true,
    });
    HomebrewElements.registerSettings();

    game.settings.registerMenu("pf2ettw", "worldClock", {
        name: game.i18n.localize(CONFIG.pf2ettw.SETTINGS.worldClock.name),
        label: game.i18n.localize(CONFIG.pf2ettw.SETTINGS.worldClock.label),
        hint: game.i18n.localize(CONFIG.pf2ettw.SETTINGS.worldClock.hint),
        icon: "far fa-clock",
        type: WorldClockSettings,
        restricted: true,
    });
    WorldClockSettings.registerSettings();

    game.settings.register("pf2ettw", "campaignFeats", {
        name: CONFIG.pf2ettw.SETTINGS.CampaignFeats.name,
        hint: CONFIG.pf2ettw.SETTINGS.CampaignFeats.hint,
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
    });

    // this section starts questionable rule settings, all of them should have a 'RAI.' at the start of their name
    // will be removed soon, it has been confirmed that the answer is "yes".
    game.settings.register("pf2ettw", "RAI.TreatWoundsAltSkills", {
        name: "pf2ettw.SETTINGS.RAI.TreatWoundsAltSkills.Name",
        hint: "pf2ettw.SETTINGS.RAI.TreatWoundsAltSkills.Hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
    });

    game.settings.register("pf2ettw", "npcAttacksFromWeapons", {
        name: "NPC Attacks From Weapons",
        scope: "world",
        config: false,
        default: false,
        type: Boolean,
    });

    if (BUILD_MODE === "production") {
        registerWorldSchemaVersion();
    }
}

function registerWorldSchemaVersion(): void {
    game.settings.register("pf2ettw", "worldSchemaVersion", {
        name: "pf2ettw.SETTINGS.WorldSchemaVersion.Name",
        hint: "pf2ettw.SETTINGS.WorldSchemaVersion.Hint",
        scope: "world",
        config: true,
        default: MigrationRunner.LATEST_SCHEMA_VERSION,
        type: Number,
    });
}
