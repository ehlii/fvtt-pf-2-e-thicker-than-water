import { PartialSettingsData, SettingsMenupf2ettw } from "./menu";

type Configpf2ettwListName = typeof MetagameSettings.SETTINGS[number];

export class MetagameSettings extends SettingsMenupf2ettw {
    static override readonly namespace = "metagame";

    static override readonly SETTINGS = [
        "showDC",
        "showResults",
        "tokenSetsNameVisibility",
        "secretDamage",
        "secretCondition",
        "partyVision",
    ] as const;

    protected static override get settings(): Record<Configpf2ettwListName, PartialSettingsData> {
        return {
            showDC: {
                name: "pf2ettw.SETTINGS.Metagame.ShowDC.Name",
                hint: "pf2ettw.SETTINGS.Metagame.ShowDC.Hint",
                default: "gm",
                type: String,
                choices: {
                    none: "pf2ettw.SETTINGS.Metagame.ShowDC.None",
                    gm: "pf2ettw.SETTINGS.Metagame.ShowDC.Gm",
                    owner: "pf2ettw.SETTINGS.Metagame.ShowDC.Owner",
                    all: "pf2ettw.SETTINGS.Metagame.ShowDC.All",
                },
            },
            showResults: {
                name: "pf2ettw.SETTINGS.Metagame.ShowResults.Name",
                hint: "pf2ettw.SETTINGS.Metagame.ShowResults.Hint",
                default: "all",
                type: String,
                choices: {
                    none: "pf2ettw.SETTINGS.Metagame.ShowResults.None",
                    gm: "pf2ettw.SETTINGS.Metagame.ShowResults.Gm",
                    owner: "pf2ettw.SETTINGS.Metagame.ShowResults.Owner",
                    all: "pf2ettw.SETTINGS.Metagame.ShowResults.All",
                },
            },
            tokenSetsNameVisibility: {
                name: "pf2ettw.SETTINGS.Metagame.TokenSetsNameVisibility.Name",
                hint: "pf2ettw.SETTINGS.Metagame.TokenSetsNameVisibility.Hint",
                default: false,
                type: Boolean,
                onChange: async () => {
                    await ui.combat.render();
                    const renderedMessages = document.querySelectorAll<HTMLLIElement>("#chat-log > li");
                    for await (const rendered of Array.from(renderedMessages)) {
                        const message = game.messages.get(rendered?.dataset.messageId ?? "");
                        if (!message) continue;
                        await ui.chat.updateMessage(message);
                    }
                },
            },
            secretDamage: {
                name: "pf2ettw.SETTINGS.Metagame.SecretDamage.Name",
                hint: "pf2ettw.SETTINGS.Metagame.SecretDamage.Hint",
                default: false,
                type: Boolean,
            },
            secretCondition: {
                name: "pf2ettw.SETTINGS.Metagame.SecretCondition.Name",
                hint: "pf2ettw.SETTINGS.Metagame.SecretCondition.Hint",
                default: false,
                type: Boolean,
            },
            partyVision: {
                name: "pf2ettw.SETTINGS.Metagame.PartyVision.Name",
                hint: "pf2ettw.SETTINGS.Metagame.PartyVision.Hint",
                default: false,
                type: Boolean,
                onChange: () => {
                    window.location.reload();
                },
            },
        };
    }
}
