const SETTINGS: Record<string, SettingRegistration> = {
    staminaVariant: {
        name: "pf2ettw.SETTINGS.Variant.Stamina.Name",
        hint: "pf2ettw.SETTINGS.Variant.Stamina.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Number,
        choices: {
            0: "pf2ettw.SETTINGS.Variant.Stamina.Choices.0",
            1: "pf2ettw.SETTINGS.Variant.Stamina.Choices.1", // I plan to expand this, hence the dropdown.
        },
    },
    ancestryParagonVariant: {
        name: "pf2ettw.SETTINGS.Variant.AncestryParagon.Name",
        hint: "pf2ettw.SETTINGS.Variant.AncestryParagon.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Boolean,
    },
    freeArchetypeVariant: {
        name: "pf2ettw.SETTINGS.Variant.FreeArchetype.Name",
        hint: "pf2ettw.SETTINGS.Variant.FreeArchetype.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Boolean,
    },
    dualClassVariant: {
        name: "pf2ettw.SETTINGS.Variant.DualClass.Name",
        hint: "pf2ettw.SETTINGS.Variant.DualClass.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Boolean,
    },
    automaticBonusVariant: {
        name: "pf2ettw.SETTINGS.Variant.AutomaticBonus.Name",
        hint: "pf2ettw.SETTINGS.Variant.AutomaticBonus.Hint",
        scope: "world",
        config: false,
        default: "noABP",
        type: String,
        choices: {
            noABP: "pf2ettw.SETTINGS.Variant.AutomaticBonus.Choices.noABP",
            ABPFundamentalPotency: "pf2ettw.SETTINGS.Variant.AutomaticBonus.Choices.ABPFundamentalPotency",
            ABPRulesAsWritten: "pf2ettw.SETTINGS.Variant.AutomaticBonus.Choices.ABPRulesAsWritten",
        },
    },
    proficiencyVariant: {
        name: "pf2ettw.SETTINGS.Variant.Proficiency.Name",
        hint: "pf2ettw.SETTINGS.Variant.Proficiency.Hint",
        scope: "world",
        config: false,
        default: "ProficiencyWithLevel",
        type: String,
        choices: {
            ProficiencyWithLevel: "pf2ettw.SETTINGS.Variant.Proficiency.Choices.ProficiencyWithLevel",
            ProficiencyWithoutLevel: "pf2ettw.SETTINGS.Variant.Proficiency.Choices.ProficiencyWithoutLevel",
        },
    },
    proficiencyUntrainedModifier: {
        name: "pf2ettw.SETTINGS.Variant.UntrainedModifier.Name",
        hint: "pf2ettw.SETTINGS.Variant.UntrainedModifier.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Number,
    },
    proficiencyTrainedModifier: {
        name: "pf2ettw.SETTINGS.Variant.TrainedModifier.Name",
        hint: "pf2ettw.SETTINGS.Variant.TrainedModifier.Hint",
        scope: "world",
        config: false,
        default: 2,
        type: Number,
    },
    proficiencyExpertModifier: {
        name: "pf2ettw.SETTINGS.Variant.ExpertModifier.Name",
        hint: "pf2ettw.SETTINGS.Variant.ExpertModifier.Hint",
        scope: "world",
        config: false,
        default: 4,
        type: Number,
    },
    proficiencyMasterModifier: {
        name: "pf2ettw.SETTINGS.Variant.MasterModifier.Name",
        hint: "pf2ettw.SETTINGS.Variant.MasterModifier.Hint",
        scope: "world",
        config: false,
        default: 6,
        type: Number,
    },
    proficiencyLegendaryModifier: {
        name: "pf2ettw.SETTINGS.Variant.LegendaryModifier.Name",
        hint: "pf2ettw.SETTINGS.Variant.LegendaryModifier.Hint",
        scope: "world",
        config: false,
        default: 8,
        type: Number,
    },
};

export class VariantRulesSettings extends FormApplication {
    static override get defaultOptions(): FormApplicationOptions {
        return {
            ...super.defaultOptions,
            title: "pf2ettw.SETTINGS.Variant.Title",
            id: "variant-rules-settings",
            template: "systems/pf2ettw/templates/system/settings/variant-rules-settings.html",
            width: 550,
            height: "auto",
            closeOnSubmit: true,
        };
    }

    override async getData(): Promise<Record<string, { value: unknown; setting: SettingRegistration }>> {
        return Object.entries(SETTINGS).reduce(
            (data: Record<string, { value: unknown; setting: SettingRegistration }>, [key, setting]) => ({
                ...data,
                [key]: { value: game.settings.get("pf2ettw", key), setting },
            }),
            {}
        );
    }

    static registerSettings(): void {
        for (const [k, v] of Object.entries(SETTINGS)) {
            game.settings.register("pf2ettw", k, v);
        }
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);
        $html.find('button[name="reset"]').on("click", (event) => this.onResetDefaults(event));
    }

    /**
     * Handle button click to reset default settings
     * @param event The initial button click event
     */
    private async onResetDefaults(event: JQuery.ClickEvent): Promise<this> {
        event.preventDefault();
        for await (const [k, v] of Object.entries(SETTINGS)) {
            await game.settings.set("pf2ettw", k, v?.default);
        }
        return this.render();
    }

    protected override async _onSubmit(
        event: Event,
        options: OnSubmitFormOptions = {}
    ): Promise<Record<string, unknown>> {
        event.preventDefault();
        return super._onSubmit(event, options);
    }

    protected override async _updateObject(_event: Event, data: Record<string, unknown>): Promise<void> {
        for await (const key of Object.keys(SETTINGS)) {
            game.settings.set("pf2ettw", key, data[key]);
        }
    }
}
