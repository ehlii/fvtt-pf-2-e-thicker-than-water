import { Userpf2ettw } from "./document";
import { UserSettingspf2ettw } from "./player-config";

export interface UserDatapf2ettw<T extends Userpf2ettw> extends foundry.data.UserData<T> {
    _source: UserSourcepf2ettw;

    flags: UserFlagspf2ettw;
}

interface UserSourcepf2ettw extends foundry.data.UserSource {
    flags: UserFlagspf2ettw;
}

type UserFlagspf2ettw = {
    [key: string]: Record<string, unknown>;
    pf2ettw: {
        settings: UserSettingspf2ettw;
    };
};
