import { Actorpf2ettw } from "@actor/base";
import { UserDatapf2ettw } from "./data";
import { PlayerConfigpf2ettw, UserSettingspf2ettw } from "./player-config";

export class Userpf2ettw extends User<Actorpf2ettw> {
    override prepareData(): void {
        super.prepareData();
        if (canvas.ready && canvas.tokens.controlled.length > 0) {
            game.pf2ettw.effectPanel.refresh();
        }
    }

    /** Set user settings defaults */
    override prepareBaseData(): void {
        super.prepareBaseData();
        this.data.flags = mergeObject(
            {
                pf2ettw: {
                    settings: deepClone(PlayerConfigpf2ettw.defaultSettings),
                },
            },
            this.data.flags
        );
    }

    get settings(): Readonly<UserSettingspf2ettw> {
        return this.data.flags.pf2ettw.settings;
    }

    /** Alternative to calling `#updateTokenTargets()` with no argument or an empty array */
    clearTargets(): void {
        this.updateTokenTargets();
    }
}

export interface Userpf2ettw extends User<Actorpf2ettw> {
    readonly data: UserDatapf2ettw<this>;
}
