const USER_SETTINGS_KEYS = ["uiTheme", "showEffectPanel", "showRollDialogs", "darkvisionFilter"] as const;

/** Player-specific settings, stored as flags on each world User */
export class PlayerConfigpf2ettw extends FormApplication {
    settings: UserSettingspf2ettw = game.user.settings;

    static readonly defaultSettings: UserSettingspf2ettw = {
        uiTheme: "blue",
        showEffectPanel: true,
        showRollDialogs: true,
        darkvisionFilter: false,
        searchPackContents: false,
    };

    static override get defaultOptions(): Required<FormApplicationOptions> {
        return mergeObject(super.defaultOptions, {
            id: "pf2ettw-player-config-panel",
            title: "pf2ettw Player Settings",
            template: "systems/pf2ettw/templates/user/player-config.html",
            classes: ["sheet"],
            width: 500,
            height: "auto",
            resizable: false,
        });
    }

    override getData(): PlayerConfigData {
        return { ...super.getData(), ...this.settings, developMode: BUILD_MODE === "development" };
    }

    static activateColorScheme(): void {
        console.debug("pf2ettw System | Activating Player Configured color scheme");
        const color = game.user.settings.uiTheme;

        const cssLink = `<link id="pf2ettw-color-scheme" href="systems/pf2ettw/styles/user/color-scheme-${color}.css" rel="stylesheet" type="text/css">`;
        $("head").append(cssLink);
    }

    /**
     * Creates a div for the module and button for the Player Configuration
     * @param html the html element where the button will be created
     */
    static hookOnRenderSettings(): void {
        Hooks.on("renderSettings", (_app, html) => {
            const configButton = $(
                `<button id="pf2ettw-player-config" data-action="pf2ettw-player-config">
                    <i class="fas fa-cogs"></i> ${PlayerConfigpf2ettw.defaultOptions.title}
                 </button>`
            );

            const setupButton = html.find("#settings-game");
            setupButton.prepend(configButton);

            configButton.on("click", () => {
                new PlayerConfigpf2ettw().render(true);
            });
        });
    }

    async _updateObject(_event: Event, formData: Record<string, unknown> & UserSettingspf2ettw): Promise<void> {
        const settings = USER_SETTINGS_KEYS.reduce((currentSettings: Record<UserSettingsKey, unknown>, key) => {
            currentSettings[key] = formData[key] ?? this.settings[key];
            return currentSettings;
        }, this.settings);

        await game.user.update({ "flags.pf2ettw.settings": settings });
        $("link#pf2ettw-color-scheme").attr({
            href: `systems/pf2ettw/styles/user/color-scheme-${formData["uiTheme"]}.css`,
        });
    }
}

interface PlayerConfigData extends FormApplicationData, UserSettingspf2ettw {
    developMode: boolean;
}

type UserSettingsKey = typeof USER_SETTINGS_KEYS[number];
export interface UserSettingspf2ettw {
    uiTheme: "blue" | "red" | "original" | "ui";
    showEffectPanel: boolean;
    showRollDialogs: boolean;
    darkvisionFilter: boolean;
    searchPackContents: boolean;
}
