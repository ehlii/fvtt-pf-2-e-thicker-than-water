import { MystifiedTraits } from "@item/data/values";
import { ChatLogpf2ettw, CompendiumDirectorypf2ettw, EncounterTrackerpf2ettw } from "@module/apps/ui";
import { Hotbarpf2ettw } from "@module/apps/ui/hotbar";
import {
    AmbientLightpf2ettw,
    LightingLayerpf2ettw,
    MeasuredTemplatepf2ettw,
    SightLayerpf2ettw,
    TemplateLayerpf2ettw,
    TokenLayerpf2ettw,
    Tokenpf2ettw,
} from "@module/canvas";
import { PlayerConfigpf2ettw } from "@module/user/player-config";
import { registerHandlebarsHelpers } from "@scripts/handlebars";
import { registerKeybindings } from "@scripts/register-keybindings";
import { registerTemplates } from "@scripts/register-templates";
import { SetGamepf2ettw } from "@scripts/set-game-pf2ettw";
import { Check } from "@system/check";
import { registerSettings } from "@system/settings";
import { pf2ettwCONFIG } from "../config";

export const Init = {
    listen: (): void => {
        Hooks.once("init", () => {
            console.log("pf2ettw System | Initializing Pathfinder 2nd Edition System");

            // Support v10 `system` property in v9
            if (game.release.generation === 9) {
                for (const Document of [Actor, Item]) {
                    Object.defineProperty(Document.prototype, "system", {
                        get() {
                            return this.data.data;
                        },
                        configurable: true,
                        enumerable: true,
                    });
                }
            }

            CONFIG.pf2ettw = pf2ettwCONFIG;
            CONFIG.debug.ruleElement ??= false;

            CONFIG.Dice.rolls.push(Check.Roll, Check.StrikeAttackRoll);

            // Assign canvas layer and placeable classes
            CONFIG.AmbientLight.layerClass = LightingLayerpf2ettw;
            CONFIG.AmbientLight.objectClass = AmbientLightpf2ettw;

            CONFIG.MeasuredTemplate.objectClass = MeasuredTemplatepf2ettw;
            CONFIG.MeasuredTemplate.layerClass = TemplateLayerpf2ettw;
            CONFIG.MeasuredTemplate.defaults.angle = 90;
            CONFIG.MeasuredTemplate.defaults.width = 1;

            CONFIG.Token.objectClass = Tokenpf2ettw;
            CONFIG.Token.layerClass = TokenLayerpf2ettw;

            CONFIG.Canvas.layers.lighting.layerClass = LightingLayerpf2ettw;
            CONFIG.Canvas.layers.sight.layerClass = SightLayerpf2ettw;
            CONFIG.Canvas.layers.templates.layerClass = TemplateLayerpf2ettw;
            CONFIG.Canvas.layers.tokens.layerClass = TokenLayerpf2ettw;

            // Make darkness visibility a little more appropriate for basic map use
            CONFIG.Canvas.lightLevels.dim = 0.25;
            CONFIG.Canvas.darknessColor = PIXI.utils.rgb2hex([0.25, 0.25, 0.4]);
            CONFIG.Canvas.exploredColor = PIXI.utils.rgb2hex([0.6, 0.6, 0.6]);

            // Automatically advance world time by 6 seconds each round
            CONFIG.time.roundTime = 6;
            // Decimals are 😠
            CONFIG.Combat.initiative.decimals = 0;

            // Assign the pf2ettw Sidebar subclasses
            CONFIG.ui.combat = EncounterTrackerpf2ettw;
            CONFIG.ui.chat = ChatLogpf2ettw;
            CONFIG.ui.compendium = CompendiumDirectorypf2ettw;
            CONFIG.ui.hotbar = Hotbarpf2ettw;

            // Remove fonts available only on Windows 10/11
            CONFIG.fontFamilies = CONFIG.fontFamilies.filter((f) => !["Courier", "Helvetica", "Times"].includes(f));

            // Insert templates into DOM tree so Applications can render into
            if (document.querySelector("#ui-top") !== null) {
                // Template element for effects-panel
                const uiTop = document.querySelector("#ui-top");
                const template = document.createElement("template");
                template.setAttribute("id", "pf2ettw-effects-panel");
                uiTop?.insertAdjacentElement("afterend", template);
            }

            // configure the bundled TinyMCE editor with PF2-specific options
            CONFIG.TinyMCE.extended_valid_elements = "pf2-action[action|glyph]";
            CONFIG.TinyMCE.content_css = CONFIG.TinyMCE.content_css.concat("systems/pf2ettw/styles/main.css");
            CONFIG.TinyMCE.style_formats = (CONFIG.TinyMCE.style_formats ?? []).concat({
                title: "pf2ettw",
                items: [
                    {
                        title: "Icons A D T F R",
                        inline: "span",
                        classes: ["pf2-icon"],
                        wrapper: true,
                    },
                    {
                        title: "Inline Header",
                        block: "h4",
                        classes: "inline-header",
                    },
                    {
                        title: "Info Block",
                        block: "section",
                        classes: "info",
                        wrapper: true,
                        exact: true,
                        merge_siblings: false,
                    },
                    {
                        title: "Stat Block",
                        block: "section",
                        classes: "statblock",
                        wrapper: true,
                        exact: true,
                        merge_siblings: false,
                    },
                    {
                        title: "Trait",
                        block: "section",
                        classes: "traits",
                        wrapper: true,
                    },
                    {
                        title: "Written Note",
                        block: "p",
                        classes: "message",
                    },
                    {
                        title: "GM Text Block",
                        block: "div",
                        wrapper: true,
                        attributes: {
                            "data-visibility": "gm",
                        },
                    },
                    {
                        title: "GM Text Inline",
                        inline: "span",
                        attributes: {
                            "data-visibility": "gm",
                        },
                    },
                ],
            });

            // Soft-set system-preferred core settings until they've been explicitly set by the GM
            const schema = foundry.data.PrototypeTokenData.schema;
            schema.displayName.default = schema.displayBars.default = CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER;

            // Register stuff with the Foundry client
            registerSettings();
            registerKeybindings();
            registerTemplates();
            registerHandlebarsHelpers();

            PlayerConfigpf2ettw.hookOnRenderSettings();
            MystifiedTraits.compile();

            // Create and populate initial game.pf2ettw interface
            SetGamepf2ettw.onInit();
        });
    },
};
