import { Actorpf2ettw } from "@actor";
import { AutomaticBonusProgression } from "@actor/character/automatic-bonus-progression";
import { Itempf2ettw } from "@item";
import { ActiveEffectpf2ettw } from "@module/active-effect";
import { ChatMessagepf2ettw } from "@module/chat-message";
import { Actorspf2ettw } from "@module/collection/actors";
import { Combatantpf2ettw, Encounterpf2ettw } from "@module/encounter";
import { FogExplorationpf2ettw } from "@module/fog-exploration";
import { Folderpf2ettw } from "@module/folder";
import { Macropf2ettw } from "@module/macro";
import { Userpf2ettw } from "@module/user";
import {
    AmbientLightDocumentpf2ettw,
    MeasuredTemplateDocumentpf2ettw,
    Scenepf2ettw,
    TileDocumentpf2ettw,
    TokenConfigpf2ettw,
    TokenDocumentpf2ettw,
} from "@scene";

/** Not an actual hook listener but rather things to run on initial load */
export const Load = {
    listen(): void {
        // Assign document classes
        CONFIG.ActiveEffect.documentClass = ActiveEffectpf2ettw;
        CONFIG.Actor.collection = Actorspf2ettw;
        CONFIG.Actor.documentClass = Actorpf2ettw;
        CONFIG.AmbientLight.documentClass = AmbientLightDocumentpf2ettw;
        CONFIG.ChatMessage.documentClass = ChatMessagepf2ettw;
        CONFIG.Combat.documentClass = Encounterpf2ettw;
        CONFIG.Combatant.documentClass = Combatantpf2ettw;
        CONFIG.FogExploration.documentClass = FogExplorationpf2ettw;
        CONFIG.Folder.documentClass = Folderpf2ettw;
        CONFIG.Item.documentClass = Itempf2ettw;
        CONFIG.Macro.documentClass = Macropf2ettw;
        CONFIG.MeasuredTemplate.documentClass = MeasuredTemplateDocumentpf2ettw;
        CONFIG.Scene.documentClass = Scenepf2ettw;
        CONFIG.Tile.documentClass = TileDocumentpf2ettw;
        CONFIG.Token.documentClass = TokenDocumentpf2ettw;
        CONFIG.Token.prototypeSheetClass = TokenConfigpf2ettw;
        CONFIG.User.documentClass = Userpf2ettw;

        // Mystery Man but with a drop shadow
        foundry.data.ActorData.DEFAULT_ICON = "systems/pf2ettw/icons/default-icons/mystery-man.svg";

        Roll.MATH_PROXY = mergeObject(Roll.MATH_PROXY, {
            eq: (a: number, b: number) => a === b,
            gt: (a: number, b: number) => a > b,
            gte: (a: number, b: number) => a >= b,
            lt: (a: number, b: number) => a < b,
            lte: (a: number, b: number) => a <= b,
            ne: (a: number, b: number) => a !== b,
            ternary: (condition: boolean | number, ifTrue: number, ifFalse: number) => (condition ? ifTrue : ifFalse),
        });

        // Make available immediately on load for module subclassing
        window.AutomaticBonusProgression = AutomaticBonusProgression;

        // Prevent buttons from retaining focus when clicked so that canvas hotkeys still work
        document.addEventListener("mouseup", (): void => {
            if (document.activeElement instanceof HTMLButtonElement) {
                document.activeElement.blur();
            }
        });
    },
};
