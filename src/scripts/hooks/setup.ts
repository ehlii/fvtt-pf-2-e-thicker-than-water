import { Localizepf2ettw } from "@system/localize";
import { registerSheets } from "../register-sheets";
import { HomebrewElements } from "@system/settings/homebrew";
import { SetGamepf2ettw } from "@scripts/set-game-pf2ettw";

/** This runs after game data has been requested and loaded from the servers, so entities exist */
export const Setup = {
    listen: (): void => {
        Hooks.once("setup", () => {
            Localizepf2ettw.ready = true;

            // Register actor and item sheets
            registerSheets();

            CONFIG.controlIcons.defeated = game.settings.get("pf2ettw", "deathIcon");
            game.pf2ettw.StatusEffects.setIconTheme();

            // Assign the homebrew elements to their respective `CONFIG.pf2ettw` objects
            const homebrew = new HomebrewElements();

            homebrew.refreshTags();
            homebrew.registerModuleTags();

            // Some of game.pf2ettw must wait until the setup phase
            SetGamepf2ettw.onSetup();

            // Forced panning is intrinsicly annoying, so set default to false
            game.settings.settings.get("core.chatBubblesPan").default = false;

            // Set Hover by Owner as defaults for Default Token Configuration
            const defaultTokenSettingsDefaults = game.settings.settings.get("core.defaultToken").default;
            defaultTokenSettingsDefaults.displayName = CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER;
            defaultTokenSettingsDefaults.displayBars = CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER;
        });
    },
};
