import { Characterpf2ettw, NPCpf2ettw } from "@actor";
import { Effectpf2ettw } from "@item";
import { ChatMessagepf2ettw } from "@module/chat-message";
import { ActionDefaultOptions } from "@system/action-macros";
import { Localizepf2ettw } from "@system/localize";
import { Errorpf2ettw } from "@util";

/** Effect: Raise a Shield */
const ITEM_UUID = "Compendium.pf2ettw.equipment-effects.2YgXoHvJfrDHucMr";

const TEMPLATES = {
    flavor: "./systems/pf2ettw/templates/chat/action/flavor.html",
    content: "./systems/pf2ettw/templates/chat/action/content.html",
};

/** A macro for the Raise a Shield action */
export async function raiseAShield(options: ActionDefaultOptions): Promise<void> {
    const translations = Localizepf2ettw.translations.pf2ettw.Actions.RaiseAShield;

    const actors = Array.isArray(options.actors) ? options.actors : [options.actors];
    const actor = actors[0];
    if (actors.length > 1 || !(actor instanceof Characterpf2ettw || actor instanceof NPCpf2ettw)) {
        ui.notifications.error(translations.BadArgs);
        return;
    }

    const shield = actor.heldShield;
    const speaker = ChatMessagepf2ettw.getSpeaker({ actor: actor });

    const isSuccess = await (async (): Promise<boolean> => {
        const existingEffect = actor.itemTypes.effect.find((e) => e.data.flags.core?.sourceId === ITEM_UUID);
        if (existingEffect) {
            await existingEffect.delete();
            return false;
        }

        if (shield?.isBroken === false) {
            const effect = await fromUuid(ITEM_UUID);
            if (!(effect instanceof Effectpf2ettw)) {
                throw Errorpf2ettw("Raise a Shield effect not found");
            }
            await actor.createEmbeddedDocuments("Item", [effect.toObject()]);
            return true;
        } else if (shield?.isBroken) {
            ui.notifications.warn(
                game.i18n.format(translations.ShieldIsBroken, { actor: speaker.alias, shield: shield.name })
            );
            return false;
        } else {
            ui.notifications.warn(game.i18n.format(translations.NoShieldEquipped, { actor: speaker.alias }));
            return false;
        }
    })();

    if (isSuccess) {
        const combatActor = (game.combat?.started && game.combat.combatant?.actor) || null;
        const [actionType, glyph] =
            combatActor && combatActor !== actor ? (["Reaction", "R"] as const) : (["SingleAction", "1"] as const);

        const title = translations[`${actionType}Title` as const];

        const content = await renderTemplate(TEMPLATES.content, {
            imgPath: shield!.img,
            message: game.i18n.format(translations.Content, { actor: speaker.alias }),
        });
        const flavor = await renderTemplate(TEMPLATES.flavor, {
            action: { title, typeNumber: glyph },
        });

        await ChatMessagepf2ettw.create({
            type: CONST.CHAT_MESSAGE_TYPES.EMOTE,
            speaker,
            flavor,
            content,
        });
    }
}
