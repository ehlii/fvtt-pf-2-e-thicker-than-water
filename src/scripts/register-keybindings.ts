export function registerKeybindings(): void {
    game.keybindings.register("pf2ettw", "cycle-token-stack", {
        name: "pf2ettw.Keybinding.CycleTokenStack.Label",
        hint: "pf2ettw.Keybinding.CycleTokenStack.Hint",
        editable: [{ key: "KeyZ", modifiers: [] }],
        onUp: (): boolean => canvas.tokens.cycleStack(),
    });
}
