import { CharacterSheetpf2ettw } from "@actor/character/sheet";
import { ActionSheetpf2ettw } from "@item/action/sheet";
import { HazardSheetpf2ettw } from "@actor/hazard/sheet";
import { LootSheetpf2ettw } from "@actor/loot/sheet";
import { FamiliarSheetpf2ettw } from "@actor/familiar/sheet";
import { VehicleSheetpf2ettw } from "@actor/vehicle/sheet";
import { NPCSheetpf2ettw } from "@actor/npc/sheet";
import { ItemSheetpf2ettw } from "@item/sheet/base";
import { KitSheetpf2ettw } from "@item/kit/sheet";
import { AncestrySheetpf2ettw } from "@item/ancestry/sheet";
import { BackgroundSheetpf2ettw } from "@item/background/sheet";
import { ClassSheetpf2ettw } from "@item/class/sheet";
import { SpellSheetpf2ettw } from "@item/spell/sheet";
import { Localizepf2ettw } from "@system/localize";
import { PhysicalItemSheetpf2ettw } from "@item/physical/sheet";
import { FeatSheetpf2ettw } from "@item/feat/sheet";
import { PHYSICAL_ITEM_TYPES } from "@item/physical/values";
import { WeaponSheetpf2ettw } from "@item/weapon/sheet";
import { EffectSheetpf2ettw } from "@item/effect/sheet";
import { BookSheetpf2ettw } from "@item/book/sheet";
import { DeitySheetpf2ettw } from "@item/deity/sheet";
import { ArmorSheetpf2ettw } from "@item/armor/sheet";
import { HeritageSheetpf2ettw } from "@item/heritage";
import { JournalSheetpf2ettw, JournalSheetStyledpf2ettw } from "@module/journal-entry/sheet";
import { SceneConfigpf2ettw } from "@scene/sheet";
import { TokenConfigpf2ettw, TokenDocumentpf2ettw } from "@scene";
import { HazardSheetGreenpf2ettw } from "@actor/hazard/sheet-new";

export function registerSheets() {
    const translations = Localizepf2ettw.translations.pf2ettw;
    const sheetLabel = translations.SheetLabel;

    DocumentSheetConfig.unregisterSheet(JournalEntry, "core", JournalSheet);
    DocumentSheetConfig.registerSheet(JournalEntry, "pf2ettw", JournalSheetpf2ettw, {
        label: game.i18n.localize("pf2ettw.JournalEntry.FoundryTheme"),
    });

    DocumentSheetConfig.registerSheet(JournalEntry, "pf2ettw", JournalSheetStyledpf2ettw, {
        label: game.i18n.localize("pf2ettw.JournalEntry.pf2ettwTheme"),
        makeDefault: true,
    });

    DocumentSheetConfig.registerSheet(Scene, "pf2ettw", SceneConfigpf2ettw, { makeDefault: true });
    DocumentSheetConfig.registerSheet(TokenDocumentpf2ettw, "pf2ettw", TokenConfigpf2ettw, { makeDefault: true });

    // ACTORS
    Actors.unregisterSheet("core", ActorSheet);

    const localizeType = (type: string) => {
        const entityType = type in CONFIG.pf2ettw.Actor.documentClasses ? "ACTOR" : "ITEM";
        const camelized = type[0].toUpperCase() + type.slice(1).toLowerCase();
        return game.i18n.localize(`${entityType}.Type${camelized}`);
    };

    Actors.registerSheet("pf2ettw", CharacterSheetpf2ettw, {
        types: ["character"],
        label: game.i18n.format(sheetLabel, { type: localizeType("character") }),
        makeDefault: true,
    });

    // Regiser NPC Sheet
    Actors.registerSheet("pf2ettw", NPCSheetpf2ettw, {
        types: ["npc"],
        label: game.i18n.format(sheetLabel, { type: localizeType("npc") }),
        makeDefault: true,
    });

    // Register Hazard Sheet
    Actors.registerSheet("pf2ettw", HazardSheetpf2ettw, {
        types: ["hazard"],
        label: game.i18n.format(sheetLabel, { type: localizeType("hazard") }),
        makeDefault: true,
    });

    // Register Beta Hazard Sheet
    Actors.registerSheet("pf2ettw", HazardSheetGreenpf2ettw, {
        types: ["hazard"],
        label: "Hazard Sheet (Beta)",
    });

    // Register Loot Sheet
    Actors.registerSheet("pf2ettw", LootSheetpf2ettw, {
        types: ["loot"],
        label: game.i18n.format(sheetLabel, { type: localizeType("loot") }),
        makeDefault: true,
    });

    // Register Familiar Sheet
    Actors.registerSheet("pf2ettw", FamiliarSheetpf2ettw, {
        types: ["familiar"],
        label: game.i18n.format(sheetLabel, { type: localizeType("familiar") }),
        makeDefault: true,
    });

    // Register Vehicle Sheet
    Actors.registerSheet("pf2ettw", VehicleSheetpf2ettw, {
        types: ["vehicle"],
        label: game.i18n.format(sheetLabel, { type: localizeType("vehicle") }),
        makeDefault: true,
    });

    // ITEMS
    Items.unregisterSheet("core", ItemSheet);

    const itemTypes = ["condition", "lore", "melee", "spellcastingEntry"];
    for (const itemType of itemTypes) {
        Items.registerSheet("pf2ettw", ItemSheetpf2ettw, {
            types: [itemType],
            label: game.i18n.format(sheetLabel, { type: localizeType(itemType) }),
            makeDefault: true,
        });
    }

    const sheetEntries = [
        ["action", ActionSheetpf2ettw],
        ["ancestry", AncestrySheetpf2ettw],
        ["armor", ArmorSheetpf2ettw],
        ["background", BackgroundSheetpf2ettw],
        ["book", BookSheetpf2ettw],
        ["class", ClassSheetpf2ettw],
        ["deity", DeitySheetpf2ettw],
        ["effect", EffectSheetpf2ettw],
        ["feat", FeatSheetpf2ettw],
        ["heritage", HeritageSheetpf2ettw],
        ["kit", KitSheetpf2ettw],
        ["spell", SpellSheetpf2ettw],
        ["weapon", WeaponSheetpf2ettw],
    ] as const;
    for (const [type, Sheet] of sheetEntries) {
        Items.registerSheet("pf2ettw", Sheet, {
            types: [type],
            label: game.i18n.format(sheetLabel, { type: localizeType(type) }),
            makeDefault: true,
        });
    }

    // Add any missing physical item sheets
    for (const itemType of PHYSICAL_ITEM_TYPES) {
        if (sheetEntries.some(([type, _sheet]) => itemType === type)) continue;
        Items.registerSheet("pf2ettw", PhysicalItemSheetpf2ettw, {
            types: [itemType],
            label: game.i18n.format(sheetLabel, { type: localizeType(itemType) }),
            makeDefault: true,
        });
    }
}
