/** Handlebars template subcomponents */
export function registerTemplates() {
    const templatePaths = [
        // effect panel
        "systems/pf2ettw/templates/system/effects-panel.html",

        // world clock
        "systems/pf2ettw/templates/system/world-clock.html",

        // Actor Sheets Partials (Character Tooltip)
        "systems/pf2ettw/templates/actors/character/partials/modifiers-tooltip.html",
        "systems/pf2ettw/templates/actors/character/partials/traits.html",
        "systems/pf2ettw/templates/actors/character/partials/background.html",
        "systems/pf2ettw/templates/actors/character/partials/abilities.html",
        "systems/pf2ettw/templates/actors/character/partials/header.html",
        "systems/pf2ettw/templates/actors/character/partials/granted-feat.html",

        // Actor Sheets Partials (CRB-Syle Sidebar)
        "systems/pf2ettw/templates/actors/character/sidebar/armor-class.html",
        "systems/pf2ettw/templates/actors/character/sidebar/class-dc.html",
        "systems/pf2ettw/templates/actors/character/sidebar/health.html",
        "systems/pf2ettw/templates/actors/character/sidebar/stamina.html",
        "systems/pf2ettw/templates/actors/character/sidebar/resistances.html",
        "systems/pf2ettw/templates/actors/character/sidebar/perception.html",
        "systems/pf2ettw/templates/actors/character/sidebar/initiative.html",
        "systems/pf2ettw/templates/actors/character/sidebar/saves.html",
        "systems/pf2ettw/templates/actors/character/sidebar/vampire-dc.html",

        // Actor Sheets Partials (Character Main Section)
        "systems/pf2ettw/templates/actors/character/tabs/general.html",
        "systems/pf2ettw/templates/actors/character/tabs/actions.html",
        "systems/pf2ettw/templates/actors/character/tabs/biography.html",
        "systems/pf2ettw/templates/actors/character/tabs/effects.html",
        "systems/pf2ettw/templates/actors/character/tabs/feats.html",
        "systems/pf2ettw/templates/actors/character/tabs/inventory.html",
        "systems/pf2ettw/templates/actors/character/tabs/pfs.html",
        "systems/pf2ettw/templates/actors/character/tabs/proficiencies.html",
        "systems/pf2ettw/templates/actors/character/tabs/spellcasting.html",
        "systems/pf2ettw/templates/actors/character/tabs/crafting.html",

        // Actor Sheets Partials (Hazards)
        "systems/pf2ettw/templates/actors/hazard/partials/header.html",
        "systems/pf2ettw/templates/actors/hazard/partials/sidebar.html",

        // Actor Sheet Partials (General)
        "systems/pf2ettw/templates/actors/partials/coinage.html",
        "systems/pf2ettw/templates/actors/partials/item-line.html",
        "systems/pf2ettw/templates/actors/partials/carry-type.html",
        "systems/pf2ettw/templates/actors/partials/conditions.html",
        "systems/pf2ettw/templates/actors/partials/dying-pips.html",
        "systems/pf2ettw/templates/actors/crafting-entry-alchemical.html",
        "systems/pf2ettw/templates/actors/crafting-entry-list.html",
        "systems/pf2ettw/templates/actors/spellcasting-spell-list.html",
        "systems/pf2ettw/templates/actors/character/partials/proficiencylevels-dropdown.html",
        "systems/pf2ettw/templates/actors/ability-builder.html",

        // SVG icons
        "systems/pf2ettw/templates/actors/character/icons/d20.html",
        "systems/pf2ettw/templates/actors/character/icons/pfs.html",
        "systems/pf2ettw/templates/actors/character/icons/plus.html",

        // Actor Sheet Partials (SVG images)
        "systems/pf2ettw/templates/actors/partials/images/header_stroke.html",
        "systems/pf2ettw/templates/actors/partials/images/header_stroke_large.html",

        // NPC partials
        "systems/pf2ettw/templates/actors/npc/tabs/main.html",
        "systems/pf2ettw/templates/actors/npc/tabs/inventory.html",
        "systems/pf2ettw/templates/actors/npc/tabs/effects.html",
        "systems/pf2ettw/templates/actors/npc/tabs/spells.html",
        "systems/pf2ettw/templates/actors/npc/tabs/notes.html",
        "systems/pf2ettw/templates/actors/npc/partials/header.html",
        "systems/pf2ettw/templates/actors/npc/partials/sidebar.html",
        "systems/pf2ettw/templates/actors/npc/partials/action.html",
        "systems/pf2ettw/templates/actors/npc/partials/attack.html",

        // Item Sheet Partials
        "systems/pf2ettw/templates/items/rules-panel.html",
        "systems/pf2ettw/templates/items/action-details.html",
        "systems/pf2ettw/templates/items/action-sidebar.html",
        "systems/pf2ettw/templates/items/ancestry-details.html",
        "systems/pf2ettw/templates/items/ancestry-sidebar.html",
        "systems/pf2ettw/templates/items/armor-details.html",
        "systems/pf2ettw/templates/items/armor-sidebar.html",
        "systems/pf2ettw/templates/items/background-details.html",
        "systems/pf2ettw/templates/items/backpack-details.html",
        "systems/pf2ettw/templates/items/backpack-sidebar.html",
        "systems/pf2ettw/templates/items/book-details.html",
        "systems/pf2ettw/templates/items/book-sidebar.html",
        "systems/pf2ettw/templates/items/treasure-sidebar.html",
        "systems/pf2ettw/templates/items/class-details.html",
        "systems/pf2ettw/templates/items/consumable-details.html",
        "systems/pf2ettw/templates/items/consumable-sidebar.html",
        "systems/pf2ettw/templates/items/condition-details.html",
        "systems/pf2ettw/templates/items/condition-sidebar.html",
        "systems/pf2ettw/templates/items/deity-details.html",
        "systems/pf2ettw/templates/items/effect-sidebar.html",
        "systems/pf2ettw/templates/items/equipment-details.html",
        "systems/pf2ettw/templates/items/equipment-sidebar.html",
        "systems/pf2ettw/templates/items/feat-details.html",
        "systems/pf2ettw/templates/items/feat-sidebar.html",
        "systems/pf2ettw/templates/items/heritage-sidebar.html",
        "systems/pf2ettw/templates/items/kit-details.html",
        "systems/pf2ettw/templates/items/kit-sidebar.html",
        "systems/pf2ettw/templates/items/lore-details.html",
        "systems/pf2ettw/templates/items/lore-sidebar.html",
        "systems/pf2ettw/templates/items/mystify-panel.html",
        "systems/pf2ettw/templates/items/spell-details.html",
        "systems/pf2ettw/templates/items/spell-overlay.html",
        "systems/pf2ettw/templates/items/spell-sidebar.html",
        "systems/pf2ettw/templates/items/melee-details.html",
        "systems/pf2ettw/templates/items/weapon-details.html",
        "systems/pf2ettw/templates/items/weapon-sidebar.html",
        "systems/pf2ettw/templates/items/activation-panel.html",

        // Loot partials
        "systems/pf2ettw/templates/actors/loot/inventory.html",
        "systems/pf2ettw/templates/actors/loot/sidebar.html",

        // Vehicle partials
        "systems/pf2ettw/templates/actors/vehicle/vehicle-sheet.html",
        "systems/pf2ettw/templates/actors/vehicle/vehicle-header.html",
        "systems/pf2ettw/templates/actors/vehicle/sidebar/vehicle-health.html",
        "systems/pf2ettw/templates/actors/vehicle/sidebar/vehicle-armorclass.html",
        "systems/pf2ettw/templates/actors/vehicle/sidebar/vehicle-saves.html",
        "systems/pf2ettw/templates/actors/vehicle/sidebar/vehicle-resistances.html",
        "systems/pf2ettw/templates/actors/vehicle/tabs/vehicle-details.html",
        "systems/pf2ettw/templates/actors/vehicle/tabs/vehicle-actions.html",
        "systems/pf2ettw/templates/actors/vehicle/tabs/vehicle-inventory.html",
        "systems/pf2ettw/templates/actors/vehicle/tabs/vehicle-description.html",

        // Compendium Browser Partials
        "systems/pf2ettw/templates/compendium-browser/action-browser.html",
        "systems/pf2ettw/templates/compendium-browser/bestiary-browser.html",
        "systems/pf2ettw/templates/compendium-browser/inventory-browser.html",
        "systems/pf2ettw/templates/compendium-browser/feat-browser.html",
        "systems/pf2ettw/templates/compendium-browser/hazard-browser.html",
        "systems/pf2ettw/templates/compendium-browser/spell-browser.html",
        "systems/pf2ettw/templates/compendium-browser/browser-settings.html",
        "systems/pf2ettw/templates/compendium-browser/filters.html",

        // Action Partial
        "systems/pf2ettw/templates/system/actions/repair/chat-button-partial.html",
        "systems/pf2ettw/templates/system/actions/repair/repair-result-partial.html",
        "systems/pf2ettw/templates/system/actions/repair/item-heading-partial.html",
    ];
    return loadTemplates(templatePaths);
}
