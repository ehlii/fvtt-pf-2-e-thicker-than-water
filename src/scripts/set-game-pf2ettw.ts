import { AutomaticBonusProgression } from "@actor/character/automatic-bonus-progression";
import { CompendiumBrowser } from "@module/apps/compendium-browser";
import { EffectsPanel } from "@module/apps/effects-panel";
import { LicenseViewer } from "@module/apps/license-viewer";
import { WorldClock } from "@module/apps/world-clock";
import {
    AbilityModifier,
    CheckModifier,
    Modifierpf2ettw,
    MODIFIER_TYPE,
    ProficiencyModifier,
    StatisticModifier,
} from "@actor/modifiers";
import { RuleElementpf2ettw, RuleElements } from "@module/rules";
import { StatusEffects } from "@scripts/actor/status-effects";
import { Dicepf2ettw } from "@scripts/dice";
import { earnIncome } from "@scripts/macros/earn-income";
import { encouragingWords } from "@scripts/macros/encouraging-words";
import { rollActionMacro, rollItemMacro } from "@scripts/macros/hotbar";
import { raiseAShield } from "@scripts/macros/raise-a-shield";
import { restForTheNight } from "@scripts/macros/rest-for-the-night";
import { steelYourResolve } from "@scripts/macros/steel-your-resolve";
import { launchTravelSheet } from "@scripts/macros/travel/travel-speed-sheet";
import { calculateXP } from "@scripts/macros/xp";
import { remigrate } from "@scripts/system/remigrate";
import { ActionMacros } from "@system/action-macros";
import { ConditionManager } from "@system/conditions";
import { EffectManager } from "@system/effect";
import { EffectTracker } from "@system/effect-tracker";
import { ActorImporter } from "@system/importer/actor-importer";
import { Checkpf2ettw } from "@system/rolls";
import { TextEditorpf2ettw } from "@system/text-editor";
import { sluggify } from "@util";
import { Coinspf2ettw } from "@item/physical/helpers";

/** Expose public game.pf2ettw interface */
export const SetGamepf2ettw = {
    onInit: (): void => {
        Object.defineProperty(globalThis.game, "pf2ettw", { value: {} });

        const actions: Record<string, Function> = {
            earnIncome,
            encouragingWords,
            raiseAShield,
            restForTheNight,
            steelYourResolve,
            ...ActionMacros,
        };

        const initSafe: Partial<typeof game["pf2ettw"]> = {
            actions,
            AbilityModifier: AbilityModifier,
            Check: Checkpf2ettw,
            CheckModifier: CheckModifier,
            ConditionManager: ConditionManager,
            Coins: Coinspf2ettw,
            Dice: Dicepf2ettw,
            EffectManager: EffectManager,
            effectPanel: new EffectsPanel(),
            effectTracker: new EffectTracker(),
            gm: { calculateXP, launchTravelSheet },
            importer: { actor: ActorImporter },
            licenseViewer: new LicenseViewer(),
            Modifier: Modifierpf2ettw,
            ModifierType: MODIFIER_TYPE,
            ProficiencyModifier: ProficiencyModifier,
            rollActionMacro,
            rollItemMacro,
            RuleElement: RuleElementpf2ettw,
            RuleElements: RuleElements,
            StatisticModifier: StatisticModifier,
            StatusEffects: StatusEffects,
            system: { moduleArt: new Map(), remigrate, sluggify },
            TextEditor: TextEditorpf2ettw,
            variantRules: { AutomaticBonusProgression },
        };

        mergeObject(game.pf2ettw, initSafe);
    },

    onSetup: (): void => {},

    onReady: (): void => {
        game.pf2ettw.compendiumBrowser = new CompendiumBrowser();
        game.pf2ettw.worldClock = new WorldClock();
    },
};
