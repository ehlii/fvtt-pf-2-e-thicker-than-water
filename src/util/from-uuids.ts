import { Actorpf2ettw } from "@actor";
import { Itempf2ettw } from "@item";
import { TokenDocumentpf2ettw } from "@scene";
import { Errorpf2ettw } from "./misc";

type IDLookups = Record<"actor" | "item" | "scene", DocumentUUID[]> & {
    pack: Record<string, string[]>;
};

/** Retrieve multiple documents by UUID */
export async function fromUUIDs(
    uuids: Exclude<ActorUUID | TokenDocumentUUID, CompendiumUUID>[]
): Promise<Actorpf2ettw[]>;
export async function fromUUIDs(uuids: Exclude<ItemUUID, CompendiumUUID>[]): Promise<Itempf2ettw[]>;
export async function fromUUIDs(uuids: DocumentUUID[]): Promise<ClientDocument[]>;
export async function fromUUIDs(uuids: DocumentUUID[]): Promise<ClientDocument[]> {
    const lookups = uuids.reduce(
        (partialLookups: IDLookups, uuid) => {
            const parts = uuid.split(".");
            switch (parts[0]) {
                case "Compendium": {
                    const [scope, packId, id]: (string | undefined)[] = parts.slice(1);
                    if (!(scope && packId && id)) throw Errorpf2ettw(`Unable to parse UUID: ${uuid}`);
                    (partialLookups.pack[`${scope}.${packId}`] ??= []).push(id);
                    break;
                }
                case "Actor": {
                    if (/\bItem\b/.test(uuid)) {
                        partialLookups.item.push(uuid);
                    } else {
                        partialLookups.actor.push(uuid);
                    }
                    break;
                }
                case "Item": {
                    partialLookups.item.push(uuid);
                    break;
                }
                case "Scene": {
                    partialLookups.scene.push(uuid);
                    break;
                }
            }
            return partialLookups;
        },
        { actor: [], item: [], pack: {}, scene: [] }
    );

    const actors: Actorpf2ettw[] = [];
    const items: Itempf2ettw[] = [];

    if (lookups.actor.length > 0) {
        actors.push(
            ...(await Promise.all(lookups.actor.map((uuid) => fromUuid(uuid)))).filter(
                (document): document is Actorpf2ettw => document instanceof Actorpf2ettw
            )
        );
    } else if (lookups.item.length > 0) {
        items.push(
            ...(await Promise.all(lookups.item.map((uuid) => fromUuid(uuid)))).filter(
                (document): document is Itempf2ettw => document instanceof Itempf2ettw
            )
        );
    }
    if (lookups.scene.length > 0) {
        actors.push(
            ...(await Promise.all(lookups.scene.map((uuid) => fromUuid(uuid))))
                .filter((document): document is TokenDocumentpf2ettw => document instanceof TokenDocumentpf2ettw)
                .flatMap((tokenDoc) => tokenDoc.actor ?? [])
        );
    }
    if (Object.keys(lookups.pack).length > 0) {
        for (const packId of Object.keys(lookups.pack)) {
            const pack = game.packs.get(packId);
            if (!pack) {
                console.warn(`pf2ettw System | Pack with id not found: ${packId}`);
                continue;
            }

            const ids = lookups.pack[packId];
            const cacheHits: string[] = [];
            for (const cached of ids.flatMap((id) => pack.get(id) ?? [])) {
                if (cached instanceof Actorpf2ettw) {
                    actors.push(cached);
                    cacheHits.push(cached.id);
                } else if (cached instanceof Itempf2ettw) {
                    items.push(cached);
                    cacheHits.push(cached.id);
                }
            }

            const cacheMisses = ids.filter((id) => !cacheHits.includes(id));
            if (cacheMisses.length === 0) continue;
            const fromServer = await pack.getDocuments({ _id: { $in: cacheMisses } });
            for (const uncached of fromServer) {
                if (uncached instanceof Actorpf2ettw) {
                    actors.push(uncached);
                } else if (uncached instanceof Itempf2ettw) {
                    items.push(uncached);
                }
            }
        }
    }
    return actors.length > 0 ? actors : items;
}

export function isItemUUID(uuid: unknown): uuid is ItemUUID {
    if (typeof uuid !== "string") return false;
    if (uuid.startsWith("Item.")) return true;

    const [type, scope, packId, id]: (string | undefined)[] = uuid.split(".");
    if (type !== "Compendium") return false;
    if (!(scope && packId && id)) throw Errorpf2ettw(`Unable to parse UUID: ${uuid}`);

    const pack = game.packs.get(`${scope}.${packId}`);
    return pack?.documentName === "Item";
}
