import type { Actorpf2ettw } from "@actor";
import { ActorSourcepf2ettw } from "@actor/data";
import type { Itempf2ettw } from "@item";
import { ItemSourcepf2ettw } from "@item/data";
import { ActiveEffectpf2ettw } from "@module/active-effect";
import { FakeCollection } from "./fake-collection";
import { FakeItem } from "./fake-item";

export class FakeActor {
    _data: ActorSourcepf2ettw;

    items: FakeCollection<Itempf2ettw> = new FakeCollection();

    effects: FakeCollection<ActiveEffectpf2ettw> = new FakeCollection();

    _itemGuid = 1;

    constructor(data: ActorSourcepf2ettw, public options: DocumentConstructionContext<Actorpf2ettw> = {}) {
        this._data = duplicate(data);
        this._data.items ??= [];
        this.prepareData();
    }

    get id(): string {
        return this.data._id;
    }

    get data() {
        return this._data;
    }

    get name() {
        return this._data.name;
    }

    prepareData(): void {
        const sourceIds = this._data.items.map((source) => source._id);
        for (const item of this.items) {
            if (!sourceIds.includes(item.id)) {
                this.items.delete(item.id);
            }
        }

        for (const source of this._data.items) {
            const item = this.items.get(source._id);
            if (item) {
                (item as any)._data = duplicate(source);
            } else {
                this.items.set(
                    source._id,
                    new FakeItem(source, { parent: this as unknown as Actorpf2ettw }) as unknown as Itempf2ettw
                );
            }
        }
    }

    static fromToken(token: Token): Actorpf2ettw | null {
        let actor = game.actors.get(token.data.actorId ?? "");
        if (!actor) return null;
        if (!token.data._id) return actor;
        if (!token.data.actorLink) actor = FakeActor.createTokenActor(actor, token);
        return actor;
    }

    static createTokenActor(baseActor: Actorpf2ettw, token: Token): Actorpf2ettw {
        const actor = game.actors.tokens[token.id];
        if (actor) return actor;
        const actorData = mergeObject(baseActor.data, token.data.actorData, { inplace: false }) as ActorSourcepf2ettw;
        return new this(actorData, { token }) as unknown as Actorpf2ettw;
    }

    update(changes: Record<string, any>) {
        delete changes.items;
        for (const [k, v] of Object.entries(changes)) {
            global.setProperty(this._data, k, v);
        }
        this.prepareData();
    }

    static async updateDocuments(
        updates: DocumentUpdateData<Actorpf2ettw>[] = [],
        _context: DocumentModificationContext = {}
    ): Promise<Actorpf2ettw[]> {
        return updates.flatMap((update) => {
            const actor = game.actors.find((actor) => actor.id === update._id);
            if (!actor) throw Error("PANIC!");

            const itemUpdates = (update.items ?? []) as DeepPartial<ItemSourcepf2ettw>[];
            delete update.items;
            mergeObject(actor.data, update);
            for (const partial of itemUpdates) {
                const source = (actor as any)._data.items.find(
                    (maybeSource: ItemSourcepf2ettw) => maybeSource._id === partial._id
                );
                if (source) mergeObject(source, partial);
            }
            actor.prepareData();
            return actor;
        });
    }

    async updateEmbeddedDocuments(type: string, data: any[]): Promise<void> {
        for (const changes of data) {
            if (type === "Item") {
                const source = this.data.items.find((itemData: ItemSourcepf2ettw) => itemData._id === changes._id);
                if (source) mergeObject(source, changes);
            }
        }
        this.prepareData();
    }

    async createEmbeddedDocuments(type: string, data: any[], _context: DocumentModificationContext): Promise<void> {
        if (type === "Item") {
            for (const source of data) {
                source._id = `item${this._itemGuid}`;
                this._itemGuid += 1;
                this._data.items.push(source);
            }
        }
        this.prepareData();
    }

    async deleteEmbeddedDocuments(type: string, data: string[]): Promise<void> {
        if (type === "Item") {
            this._data.items = this._data.items.filter((source: { _id: string }) => !data.includes(source._id));
        }
        this.prepareData();
    }

    toObject(source = true) {
        return source ? duplicate(this._data) : duplicate(this.data);
    }
}
