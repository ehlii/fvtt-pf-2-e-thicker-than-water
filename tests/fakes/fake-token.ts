import { Actorpf2ettw } from "@actor/base";
import { Scenepf2ettw } from "@module/scene";
import { TokenDocumentpf2ettw } from "@module/scene/token-document";

export class FakeToken {
    _actor: Actorpf2ettw | null;
    parent: Scenepf2ettw | null;
    data: foundry.data.TokenData<TokenDocumentpf2ettw>;

    constructor(data: foundry.data.TokenSource, context: TokenDocumentConstructionContext<TokenDocumentpf2ettw> = {}) {
        this.data = duplicate(data) as foundry.data.TokenData<TokenDocumentpf2ettw>;
        this.parent = context.parent ?? null;
        this._actor = context.actor ?? null;
    }

    get actor() {
        return this._actor;
    }

    get scene() {
        return this.parent;
    }

    get id() {
        return this.data._id;
    }

    get name() {
        return this.data.name;
    }

    update(changes: EmbeddedDocumentUpdateData<TokenDocument>, context: DocumentModificationContext = {}) {
        changes["_id"] = this.id;
        this.scene?.updateEmbeddedDocuments("Token", [changes], context);
    }
}
